# Games Platform

ASE project for group 4 QSE

# Easy Deployment

If you are using Ubuntu, the following script should install all necessary dependencies
and setup kubernetes with all components:

```./setup.sh```

# Developer Setup

1. Install minikube
2. Use `eval $(minikube docker-env) `
   1. This makes it possible to use docker images in the kubernetes cluster.
3. The k8 folder contains the local development setup
   1. Run `kubectl apply -f k8/postgres-local`
   2. Run `kubectl apply -f k8/rabbitmq`
   2. Run `kubectl apply -f k8/mail`
   3. You cannot run the application unless you forward the port
      1. Run `sh portforwarding.sh`
      2. Now you can access the db on localhost with port `5499`
      3. Now you can access the rabbitmq on localhost with port `5672`
4. For certain functionalities, it is required to build the execution engine locally
   3. Go to `execution-engine`
   4. Run `build_image.sh` 
      1. This will build the executable in release mode and push it to local image registry

## Building the execution engine for kubernetes

1. Install rustup
2. Install `apt-get install ncurses-dev`
3. Go to the folder `execution-engine` and run `sh build_image`
   1. This will automatically push the image to local registry
   2. I recommend that you delete the old image when something changes in the `execution-engine` folder
   3. You can do that with `minikube image rm docker.io/library/execution-engine`