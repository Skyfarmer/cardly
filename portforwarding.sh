kubectl port-forward service/postgres-service 5499:5499 &
kubectl port-forward service/rabbitmq-service 5672:5672 &
kubectl port-forward service/rabbitmq-service 15672:15672 &
kubectl port-forward service/mail-service 1025:1025 &
kubectl port-forward service/mail-service 8025:8025 &
