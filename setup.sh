#!/usr/bin/bash

if ! command -v minikube &> /dev/null 
then
	echo Setting up minikube
	curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube_latest_amd64.deb
	sudo dpkg -i minikube_latest_amd64.deb
	sudo snap install kubectl --classic
fi

if ! command -v npm &> /dev/null 
then
	echo Setting up PPA-repository for latest nodejs
	sudo apt install -y curl

	curl -fsSL https://deb.nodesource.com/setup_18.x | sudo -E bash -
	sudo apt install -y nodejs build-essential openjdk-17-jdk-headless
fi

if ! command -v docker &> /dev/null 
then
	echo Setting up docker
	sudo mkdir -p /etc/apt/keyrings
	curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg

	echo \
	  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
	  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
	sudo apt update
	sudo apt install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin libncurses-dev
	sudo usermod -aG docker $(whoami)
	echo Please reboot and restart this script
	exit 0
fi

if ! command -v rustc &> /dev/null 
then
	echo Setting up Rust
	curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y
	source $HOME/.cargo/env
fi

echo Starting Minikube
minikube start

echo Building Execution Engine
cd ./execution-engine && cargo build --release && minikube image build -t execution-engine . && cd ..

echo Building Frontend
cd frontend
sed -i -- 's/aseloves.us/localhost:8080/g' ./src/environments/environment.prod.ts
npm install && npm run build
minikube image build -t frontend .
cd ..

echo Building Backend
cd backend
./../mvnw clean compile
./../mvnw -B com.google.cloud.tools:jib-maven-plugin:dockerBuild -Djib.to.image=backend
minikube image load backend
cd ..

echo Deploying
kubectl apply -f k8/postgres
kubectl apply -f k8/rabbitmq
kubectl apply -f k8/mail
kubectl apply -f k8/backend
kubectl apply -f k8/frontend

echo Waiting 30 seconds for deployment
sleep 30

trap "trap - SIGTERM && kill -- -$$" SIGINT SIGTERM EXIT
kubectl port-forward service/frontend-service 8079:80 &
kubectl port-forward service/backend-service 8080:8080 &

echo Deployment completed, open a browser and navigate to http://localhost:8079
echo Quit with CTRL + C
while :
do
  sleep 60
done
