#!/usr/bin/env bash

ID=100
NAME=my_game
uno=`cat ../execution-engine/games/thegame.lua | base64`

JSON_STRING=$( jq -R -n \
                  --arg id "$ID" \
                  --arg name "$NAME" \
                  --arg source_code "$uno" \
                  --arg visibility "PUBLIC" \
                  '{id: $id | tonumber, visibility: $visibility, game: { id: $id | tonumber, name: $name, source_code: $source_code} }' )

echo $JSON_STRING | tr -d '\n' > input_session.json
