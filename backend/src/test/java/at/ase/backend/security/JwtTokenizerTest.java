package at.ase.backend.security;


import at.ase.backend.TestData;
import at.ase.backend.security.properties.JwtProperties;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
public class JwtTokenizerTest extends TestData {
    @Mock
    SecurityConfiguration configuration;

    JwtTokenizer tokenizer;

    @BeforeEach
    void beforeEach() {
        this.tokenizer = new JwtTokenizer(configuration);
    }

    @Test
    void getAuthToken_should_get_jwt() {
        JwtProperties properties = new JwtProperties();
        properties.setType("typeValue");
        properties.setAudience("audienceValue");
        properties.setIssuer("issuerValue");
        properties.setExpirationTime(1000L);
        properties.setSecret("secretValueSecretValueSecretValueSecretValueSecretValueSecretValueSecretValueSecretValue");
        when(configuration.getJwt()).thenReturn(properties);

        String token = tokenizer.getAuthToken("userValue", List.of("roleValue"), 1L);
        assertThat(token).isNotBlank();
    }

}
