package at.ase.backend.security;

import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.impl.DefaultClaims;
import io.jsonwebtoken.impl.DefaultJws;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.Collections;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class JwtAuthorizationFilterTest {
    @Mock
    AuthenticationManager authenticationManager;

    @Mock
    JwtParser jwtParser;

    JwtAuthorizationFilter filter;

    @BeforeEach
    void beforeEach() {
        filter = new JwtAuthorizationFilter(authenticationManager, jwtParser);
    }

    @Test
    void doFilterInternal_should_set_security_context_when_token_empty() throws ServletException, IOException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        SecurityContextHolder.setContext(securityContext);

        when(request.getHeader("Authentication")).thenReturn("");

        filter.doFilterInternal(request, response, mock(FilterChain.class));

        verify(securityContext, times(0)).setAuthentication(any(Authentication.class));
        verifyNoMoreInteractions(securityContext);
    }

    @Test
    void doFilterInternal_should_set_security_context_when_token_null() throws ServletException, IOException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        SecurityContextHolder.setContext(securityContext);

        when(request.getHeader("Authentication")).thenReturn(null);

        filter.doFilterInternal(request, response, mock(FilterChain.class));

        verify(securityContext, times(0)).setAuthentication(any(Authentication.class));
        verifyNoMoreInteractions(securityContext);
    }

    @Test
    void doFilterInternal_should_set_security_context_when_token_does_not_start_with_Bearer() throws ServletException, IOException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        SecurityContextHolder.setContext(securityContext);

        when(request.getHeader("Authentication")).thenReturn("notBearer");

        filter.doFilterInternal(request, response, mock(FilterChain.class));

        verify(securityContext, times(0)).setAuthentication(any(Authentication.class));
        verifyNoMoreInteractions(securityContext);
    }

    @Test
    void doFilterInternal_should_not_set_security_context_when_subject_null() throws ServletException, IOException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        SecurityContextHolder.setContext(securityContext);
        DefaultClaims claims = mock(DefaultClaims.class);
        when(claims.getSubject()).thenReturn(null);
        DefaultJws jws = mock(DefaultJws.class);
        when(jws.getBody()).thenReturn(claims);
        when(jwtParser.parseClaimsJws("test")).thenReturn(jws);

        when(request.getHeader("Authentication")).thenReturn("Bearer test");

        filter.doFilterInternal(request, response, mock(FilterChain.class));

        verify(securityContext, times(0)).setAuthentication(any(Authentication.class));
    }

    @Test
    void doFilterInternal_should_not_set_security_context_when_subject_empty() throws ServletException, IOException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        SecurityContextHolder.setContext(securityContext);
        DefaultClaims claims = mock(DefaultClaims.class);
        when(claims.getSubject()).thenReturn("");
        DefaultJws jws = mock(DefaultJws.class);
        when(jws.getBody()).thenReturn(claims);
        when(jwtParser.parseClaimsJws("test")).thenReturn(jws);

        when(request.getHeader("Authentication")).thenReturn("Bearer test");

        filter.doFilterInternal(request, response, mock(FilterChain.class));

        verify(securityContext, times(0)).setAuthentication(any(Authentication.class));
    }

    @Test
    void doFilterInternal_should_set_security_context() throws ServletException, IOException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        SecurityContext securityContext = Mockito.mock(SecurityContext.class);
        SecurityContextHolder.setContext(securityContext);
        DefaultClaims claims = mock(DefaultClaims.class);
        when(claims.getSubject()).thenReturn("subjectValue");
        when(claims.get("rol")).thenReturn(Collections.emptyList());
        DefaultJws jws = mock(DefaultJws.class);
        when(jws.getBody()).thenReturn(claims);
        when(jwtParser.parseClaimsJws("test")).thenReturn(jws);

        when(request.getHeader("Authentication")).thenReturn("Bearer test");

        filter.doFilterInternal(request, response, mock(FilterChain.class));

        verify(securityContext, times(1)).setAuthentication(any(Authentication.class));
    }
}
