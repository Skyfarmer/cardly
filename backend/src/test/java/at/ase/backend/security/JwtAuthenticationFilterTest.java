package at.ase.backend.security;


import at.ase.backend.TestData;
import at.ase.backend.exception.NotFoundException;
import at.ase.backend.exception.PersistenceException;
import at.ase.backend.security.properties.JwtProperties;
import at.ase.backend.service.PlayerService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.DelegatingServletInputStream;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.catchThrowable;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
public class JwtAuthenticationFilterTest extends TestData {
    @Mock
    AuthenticationManager manager;

    @Mock
    JwtTokenizer tokenizer;

    @Mock
    PlayerService playerService;

    JwtAuthenticationFilter filter;

    @BeforeEach
    void beforeEach() {
        filter = new JwtAuthenticationFilter(manager, tokenizer, playerService);
    }

    @Test
    void attemptAuthentication_should_return_object() throws IOException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);

        when(request.getInputStream()).thenReturn(new DelegatingServletInputStream(new ByteArrayInputStream("{ \"email\": \"test@email.com\", \"password\": \"test\"}".getBytes(StandardCharsets.UTF_8))));
        when(manager.authenticate(any(Authentication.class))).thenReturn(new UsernamePasswordAuthenticationToken("principalValue", "credentialValue"));

        Authentication authentication = filter.attemptAuthentication(request, response);
        assertThat(authentication).isNotNull();
        assertThat(authentication.isAuthenticated()).isFalse();
        assertThat(authentication.getPrincipal()).isEqualTo("principalValue");
        assertThat(authentication.getCredentials()).isEqualTo("credentialValue");
    }

    @Test
    void attemptAuthentication_should_throw_exception_when_invalid_email() throws IOException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);

        when(request.getInputStream()).thenReturn(new DelegatingServletInputStream(new ByteArrayInputStream("{ \"email\": \"invalidValue\", \"password\": \"test\"}".getBytes(StandardCharsets.UTF_8))));

        final Throwable throwable = catchThrowable(() -> filter.attemptAuthentication(request, response));

        assertThat(throwable).isNotNull();
        assertThat(throwable).isExactlyInstanceOf(InsufficientAuthenticationException.class);
    }

    @Test
    void attemptAuthentication_should_throw_exception_when_invalid_req() throws IOException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);

        when(request.getInputStream()).thenReturn(new DelegatingServletInputStream(new ByteArrayInputStream(" }".getBytes(StandardCharsets.UTF_8))));

        final Throwable throwable = catchThrowable(() -> filter.attemptAuthentication(request, response));

        assertThat(throwable).isNotNull();
        assertThat(throwable).isExactlyInstanceOf(BadCredentialsException.class);
    }

    @Test
    void unsuccessfulAuthentication_should_return_response() throws IOException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        AuthenticationException exception = mock(AuthenticationException.class);
        PrintWriter writer = mock(PrintWriter.class);

        when(exception.getMessage()).thenReturn("messageValue");
        doNothing().when(response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        when(response.getWriter()).thenReturn(writer);
        doNothing().when(writer).write("messageValue");

        filter.unsuccessfulAuthentication(request, response, exception);

        verify(response, times(1)).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        verify(writer, times(1)).write("messageValue");

        verifyNoMoreInteractions(response, writer);
        verifyNoInteractions(request);
    }

    @Test
    void successfulAuthentication_should_return_response() throws IOException, PersistenceException, NotFoundException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        Authentication authentication = mock(Authentication.class);
        PrintWriter writer = mock(PrintWriter.class);

        when(authentication.getPrincipal()).thenReturn(new User("emailValue", "passwordValue", Collections.emptyList()));
        when(playerService.findPlayerByEmail("emailValue")).thenReturn(createPlayerDto());
        doNothing().when(playerService).resetLoginAttempts("emailValue");

        when(tokenizer.getAuthToken("emailValue", Collections.emptyList(), 1L)).thenReturn("tokenValue");
        when(response.getWriter()).thenReturn(writer);
        doNothing().when(writer).write("tokenValue");

        filter.successfulAuthentication(request, response, mock(FilterChain.class), authentication);

        verify(playerService, times(1)).findPlayerByEmail("emailValue");
        verify(playerService, times(1)).resetLoginAttempts("emailValue");
        verify(writer, times(1)).write("tokenValue");
    }

}
