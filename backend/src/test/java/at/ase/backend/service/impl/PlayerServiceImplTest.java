package at.ase.backend.service.impl;

import at.ase.backend.AbstractSpringBootTest;
import at.ase.backend.dto.*;
import at.ase.backend.exception.InvalidRequestException;
import at.ase.backend.exception.NotFoundException;
import at.ase.backend.exception.PersistenceException;
import at.ase.backend.mapper.PlayerMapper;
import at.ase.backend.model.PlayerEntity;
import at.ase.backend.repository.PlayerRepository;
import at.ase.backend.service.MailService;
import at.ase.backend.service.PlayerService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.catchThrowable;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class PlayerServiceImplTest extends AbstractSpringBootTest {
    @MockBean
    PlayerRepository playerRepository;

    @Autowired
    PlayerMapper playerMapper;

    @MockBean
    PasswordEncoder passwordEncoder;

    @MockBean
    MailService mailService;

    @Autowired
    PlayerService playerService;

    @Test
    void findPlayerByEmail_should_get_player_by_email() throws PersistenceException, NotFoundException {
        when(playerRepository.findByEmail("emailValue")).thenReturn(Optional.of(createPlayerEntity()));

        PlayerDto dto = playerService.findPlayerByEmail("emailValue");

        assertThat(dto).isNotNull();
        assertThat(dto.getId()).isEqualTo(1L);
        assertThat(dto.getEmail()).isEqualTo("emailValue");
    }

    @Test
    void findPlayerByEmail_should_throw_exception_when_does_not_exist() {
        when(playerRepository.findByEmail("emailValue")).thenReturn(Optional.empty());

        final Throwable throwable = catchThrowable(() -> playerService.findPlayerByEmail("emailValue"));

        assertThat(throwable).isNotNull();
        assertThat(throwable).isExactlyInstanceOf(NotFoundException.class);
    }

    @Test
    void findPlayerByEmail_should_throw_exception_when_persistence_fails() {
        when(playerRepository.findByEmail("emailValue")).thenThrow(DataIntegrityViolationException.class);

        final Throwable throwable = catchThrowable(() -> playerService.findPlayerByEmail("emailValue"));

        assertThat(throwable).isNotNull();
        assertThat(throwable).isExactlyInstanceOf(PersistenceException.class);
    }

    @Test
    void findPlayerById_should_get_player_by_id() throws PersistenceException, NotFoundException {
        when(playerRepository.findById(1L)).thenReturn(Optional.of(createPlayerEntity()));

        PlayerDto playerDto = playerService.findPlayerById(1L);
        assertThat(playerDto).isNotNull();
        assertThat(playerDto.getId()).isEqualTo(1L);
    }

    @Test
    void findPlayerById_should_throw_exception_when_persistence_fails() {
        when(playerRepository.findById(1L)).thenThrow(DataIntegrityViolationException.class);

        final Throwable throwable = catchThrowable(() -> playerService.findPlayerById(1L));

        assertThat(throwable).isNotNull();
        assertThat(throwable).isExactlyInstanceOf(PersistenceException.class);
    }
    @Test
    void findPlayerById_should_throw_exception_when_does_not_exist() {
        when(playerRepository.findById(1L)).thenReturn(Optional.empty());

        final Throwable throwable = catchThrowable(() -> playerService.findPlayerById(1L));

        assertThat(throwable).isNotNull();
        assertThat(throwable).isExactlyInstanceOf(NotFoundException.class);
    }

    @Test
    void resetLoginAttempts_should_reset() throws PersistenceException, NotFoundException {
        when(playerRepository.findByEmail("emailValue")).thenReturn(Optional.of(createPlayerEntity()));
        when(playerRepository.save(any())).thenAnswer(returnsFirstArg());

        playerService.resetLoginAttempts("emailValue");

        verify(playerRepository, times(1)).save(any(PlayerEntity.class));
    }

    @Test
    void resetLoginAttempt_should_throw_exception_when_does_not_exist() {
        when(playerRepository.findByEmail("emailValue")).thenReturn(Optional.empty());

        final Throwable throwable = catchThrowable(() -> playerService.resetLoginAttempts("emailValue"));

        assertThat(throwable).isNotNull();
        assertThat(throwable).isExactlyInstanceOf(NotFoundException.class);
    }

    @Test
    void resetLoginAttempt_should_throw_exception_when_persistence_fails() {
        when(playerRepository.findByEmail("emailValue")).thenThrow(DataIntegrityViolationException.class);

        final Throwable throwable = catchThrowable(() -> playerService.resetLoginAttempts("emailValue"));

        assertThat(throwable).isNotNull();
        assertThat(throwable).isExactlyInstanceOf(PersistenceException.class);
    }

    @Test
    void registerPlayer_should_register() throws PersistenceException, NotFoundException, InvalidRequestException {
        when(playerRepository.save(any())).thenAnswer(returnsFirstArg());
        when(passwordEncoder.encode("passwordValue")).thenReturn("encodedPasswordValue");
        when(playerRepository.findByAlias("aliasValue")).thenReturn(Optional.empty());

        PlayerDto dto = playerService.registerPlayer(CreatePlayerDto.builder()
                .email("emailValue")
                .password("encodedPasswordValue")
                .build());

        assertThat(dto.getEmail()).isEqualTo("emailValue");

        verify(playerRepository, times(1)).save(any(PlayerEntity.class));
    }

    @Test
    void registerPlayer_should_throw_exception_when_persistence_fails() {
        when(playerRepository.save(any())).thenThrow(DataIntegrityViolationException.class);
        when(playerRepository.findByAlias("aliasValue")).thenReturn(Optional.empty());

        final Throwable throwable = catchThrowable(() -> playerService.registerPlayer(CreatePlayerDto.builder().build()));

        assertThat(throwable).isNotNull();
        assertThat(throwable).isExactlyInstanceOf(PersistenceException.class);
    }

    @Test
    void registerPlayer_should_throw_exception_when_player_already_registered() {
        when(playerRepository.findByEmail("emailValue")).thenReturn(Optional.of(createPlayerEntity()));
        when(playerRepository.findByAlias("aliasValue")).thenReturn(Optional.empty());

        final Throwable throwable = catchThrowable(() -> playerService.registerPlayer(
                CreatePlayerDto
                        .builder()
                        .email("emailValue")
                        .alias("aliasValue")
                        .build()));

        assertThat(throwable).isNotNull();
        assertThat(throwable).isExactlyInstanceOf(InvalidRequestException.class);
        assertThat(throwable).hasMessage("User already exists");
    }

    @Test
    void registerPlayer_should_throw_exception_when_alias_already_exists() {
        when(playerRepository.findByEmail("emailValue")).thenReturn(Optional.of(createPlayerEntity()));
        when(playerRepository.findByAlias("aliasValue")).thenReturn(Optional.of(createPlayerEntity()));

        final Throwable throwable = catchThrowable(() -> playerService.registerPlayer(
                CreatePlayerDto
                        .builder()
                        .email("emailValue")
                        .alias("aliasValue")
                        .build()));

        assertThat(throwable).isNotNull();
        assertThat(throwable).isExactlyInstanceOf(InvalidRequestException.class);
        assertThat(throwable).hasMessage("User already exists");
    }

    @Test
    void loadUserByUsername_should_return_user() {
        when(playerRepository.findByEmail("emailValue")).thenReturn(Optional.of(createPlayerEntity()));

        UserDetails details = playerService.loadUserByUsername("emailValue");

        assertThat(details.getUsername()).isEqualTo("emailValue");
        assertThat(details.getPassword()).isEqualTo("passwordValue");
        assertThat(details.getAuthorities().size()).isEqualTo(1);
    }

    @Test
    void loadUserByUsername_should_throw_exception_when_user_does_not_exist() {
        when(playerRepository.findByEmail("emailValue")).thenReturn(Optional.empty());

        final Throwable throwable = catchThrowable(() -> playerService.loadUserByUsername("emailValue"));

        assertThat(throwable).isNotNull();
        assertThat(throwable).isExactlyInstanceOf(UsernameNotFoundException.class);
    }

    @Test
    void loadUserByUsername_should_throw_exception_when_user_is_locked() {
        PlayerEntity entity = createPlayerEntity();
        entity.setBanned(true);
        when(playerRepository.findByEmail("emailValue")).thenReturn(Optional.of(entity));

        final Throwable throwable = catchThrowable(() -> playerService.loadUserByUsername("emailValue"));

        assertThat(throwable).isNotNull();
        assertThat(throwable).isExactlyInstanceOf(LockedException.class);
    }

    @Test
    void requestPasswordForgottenEmail_should_do_nothing_when_does_not_exist() throws PersistenceException {
        when(playerRepository.findByEmail("emailValue")).thenReturn(Optional.empty());

        playerService.requestPasswordForgottenEmail(PasswordForgottenDto.builder().email("emailValue").build());

        verify(playerRepository, times(1)).findByEmail("emailValue");
        verifyNoMoreInteractions(playerRepository, mailService);
    }

    @Test
    void requestPasswordForgottenEmail_should_throw_exception_when_persistence_fails() throws PersistenceException {
        when(playerRepository.findByEmail("emailValue")).thenThrow(DataIntegrityViolationException.class);

        final Throwable thrown = catchThrowable(() -> playerService.requestPasswordForgottenEmail(PasswordForgottenDto.builder().email("emailValue").build()));

        assertThat(thrown).isNotNull();
        assertThat(thrown).isExactlyInstanceOf(PersistenceException.class);
    }

    @Test
    void requestPasswordForgottenEmail_should_send_email() throws PersistenceException {
        when(playerRepository.findByEmail("emailValue")).thenReturn(Optional.of(createPlayerEntity()));

        playerService.requestPasswordForgottenEmail(PasswordForgottenDto.builder().email("emailValue").build());

        verify(mailService, times(1)).sendPasswordForgottenEmail(eq("emailValue"), anyString());

        verifyNoMoreInteractions(mailService);
    }

    @Test
    void resetPassword_should_throw_exception_when_persistence_fails() {
        when(playerRepository.findByPasswordResetToken("tokenValue")).thenThrow(DataIntegrityViolationException.class);

        final Throwable throwable = catchThrowable(() -> playerService.resetPassword(PasswordResetDto.builder()
                .token("tokenValue")
                .newPassword("newPasswordValue")
                .build()));

        assertThat(throwable).isNotNull();
        assertThat(throwable).isExactlyInstanceOf(PersistenceException.class);
    }

    @Test
    void resetPassword_should_throw_exception_when_not_found() {
        when(playerRepository.findByPasswordResetToken("tokenValue")).thenReturn(Optional.empty());

        final Throwable throwable = catchThrowable(() -> playerService.resetPassword(PasswordResetDto.builder()
                .token("tokenValue")
                .newPassword("newPasswordValue")
                .build()));

        assertThat(throwable).isNotNull();
        assertThat(throwable).isExactlyInstanceOf(NotFoundException.class);
    }

    @Test
    void resetPassword_should_change_password() throws PersistenceException, NotFoundException, InvalidRequestException {
        when(playerRepository.findByPasswordResetToken("tokenValue")).thenReturn(Optional.of(createPlayerEntity()));
        when(passwordEncoder.matches("currentPasswordValue", "passwordValue")).thenReturn(true);
        when(playerRepository.save(any())).thenAnswer(returnsFirstArg());

        playerService.resetPassword(PasswordResetDto.builder()
                .token("tokenValue")
                .newPassword("newPasswordValue")
                .build());

        verify(playerRepository, times(1)).findByPasswordResetToken("tokenValue");
        verify(playerRepository, times(1)).save(any());

        verifyNoMoreInteractions(playerRepository);
    }

    @Test
    void updatePlayer_should_throw_exception_when_persistence_fails() {
        when(playerRepository.findByEmail("userValue")).thenThrow(DataIntegrityViolationException.class);

        final Throwable throwable = catchThrowable(() -> playerService.updatePlayer("userValue", createPlayerDto()));

        assertThat(throwable).isNotNull();
        assertThat(throwable).isExactlyInstanceOf(PersistenceException.class);
    }

    @Test
    void updatePlayer_should_throw_exception_when_not_found() {
        when(playerRepository.findByEmail("userValue")).thenReturn(Optional.empty());
        when(playerRepository.findByAlias("aliasValue")).thenReturn(Optional.empty());

        final Throwable throwable = catchThrowable(() -> playerService.updatePlayer("userValue", createPlayerDto()));

        assertThat(throwable).isNotNull();
        assertThat(throwable).isExactlyInstanceOf(NotFoundException.class);
    }

    @Test
    void updatePlayer_should_change_alias() throws PersistenceException, NotFoundException, InvalidRequestException {
        when(playerRepository.findByEmail("emailValue")).thenReturn(Optional.of(createPlayerEntity()));
        when(playerRepository.findByAlias("aliasValue")).thenReturn(Optional.empty());
        when(playerRepository.save(any())).thenAnswer(returnsFirstArg());

        PlayerDto dto = createPlayerDto();
        dto.setAlias("updatedAlias");
        PlayerDto result = playerService.updatePlayer("emailValue", dto);

        assertThat(result).isNotNull();
        assertThat(result.getId()).isEqualTo(1L);
        assertThat(result.getEmail()).isEqualTo("emailValue@email.com");
        assertThat(result.getAlias()).isEqualTo("updatedAlias");

        verify(playerRepository, times(1)).findByEmail("emailValue");
        verify(playerRepository, times(1)).findByAlias("updatedAlias");
        verify(playerRepository, times(1)).save(any());

        verifyNoMoreInteractions(playerRepository);
    }

    @Test
    void updatePlayer_should_throw_exception_when_alias_already_exists() {
        when(playerRepository.findByEmail("userValue")).thenReturn(Optional.of(createPlayerEntity()));
        when(playerRepository.findByAlias("updatedAlias")).thenReturn(Optional.of(createPlayerEntity()));

        PlayerDto dto = createPlayerDto();
        dto.setAlias("updatedAlias");
        Throwable throwable = catchThrowable(() -> playerService.updatePlayer("userValue", dto));

        assertThat(throwable).isNotNull();
        assertThat(throwable).isExactlyInstanceOf(InvalidRequestException.class);
    }

    @Test
    void updatePassword_should_throw_exception_when_persistence_fails() {
        when(playerRepository.findByEmail("userValue")).thenThrow(DataIntegrityViolationException.class);

        final Throwable throwable = catchThrowable(() -> playerService.updatePassword("userValue", createPasswordChangeDto()));

        assertThat(throwable).isNotNull();
        assertThat(throwable).isExactlyInstanceOf(PersistenceException.class);
    }

    @Test
    void updatePassword_should_throw_exception_when_not_found() {
        when(playerRepository.findByEmail("userValue")).thenReturn(Optional.empty());

        final Throwable throwable = catchThrowable(() -> playerService.updatePassword("userValue", createPasswordChangeDto()));

        assertThat(throwable).isNotNull();
        assertThat(throwable).isExactlyInstanceOf(NotFoundException.class);
    }

    @Test
    void updatePassword_should_update() throws PersistenceException, NotFoundException, InvalidRequestException {
        when(playerRepository.findByEmail("emailValue")).thenReturn(Optional.of(createPlayerEntity()));
        when(playerRepository.save(any())).thenAnswer(returnsFirstArg());
        when(passwordEncoder.matches("passwordValue", "passwordValue")).thenReturn(true);

        PasswordChangeDto dto = createPasswordChangeDto();
        dto.setNewPassword("newPassword");
        dto.setCurrentPassword("passwordValue");
        PlayerDto result = playerService.updatePassword("emailValue", dto);

        assertThat(result).isNotNull();
        assertThat(result.getId()).isEqualTo(1L);
        assertThat(result.getEmail()).isEqualTo("emailValue");
        assertThat(result.getAlias()).isEqualTo("aliasValue");

        verify(playerRepository, times(1)).findByEmail("emailValue");
        verify(playerRepository, times(1)).save(any());

        verifyNoMoreInteractions(playerRepository);
    }

    @Test
    void updatePassword_should_throw_exception_password_does_not_match() {
        when(playerRepository.findByEmail("emailValue")).thenReturn(Optional.of(createPlayerEntity()));
        when(playerRepository.save(any())).thenAnswer(returnsFirstArg());
        when(passwordEncoder.matches("passwordValue", "passwordValue")).thenReturn(false);

        PasswordChangeDto dto = createPasswordChangeDto();
        dto.setNewPassword("newPassword");
        dto.setCurrentPassword("passwordValue");
        Throwable throwable = catchThrowable(() -> playerService.updatePassword("emailValue", dto));

        assertThat(throwable).isNotNull();
        assertThat(throwable).isExactlyInstanceOf(InvalidRequestException.class);
        assertThat(throwable).hasMessage("Old password is not correct");
    }
}