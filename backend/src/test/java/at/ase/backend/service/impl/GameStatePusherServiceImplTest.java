package at.ase.backend.service.impl;

import at.ase.backend.AbstractSpringBootTest;
import at.ase.backend.dto.SessionDto;
import at.ase.backend.dto.SlotDto;
import at.ase.backend.exception.*;
import at.ase.backend.mapper.MessageMapper;
import at.ase.backend.mapper.SessionMapper;
import at.ase.backend.mapper.SlotMapper;
import at.ase.backend.messaging.Message;
import at.ase.backend.model.GameStatus;
import at.ase.backend.model.SessionEntity;
import at.ase.backend.repository.SessionRepository;
import at.ase.backend.repository.SlotRepository;
import at.ase.backend.service.GameService;
import at.ase.backend.service.KubernetesService;
import io.kubernetes.client.openapi.ApiException;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;

import java.io.IOException;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.catchThrowable;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

public class GameStatePusherServiceImplTest extends AbstractSpringBootTest {
    @Mock
    Exchange exchange_requests;

    @Mock
    Exchange exchange_responses;

    @Mock
    AmqpTemplate template;

    @Mock
    MessageMapper messageMapper;

    @Mock
    RabbitAdmin rabbitAdmin;

    @Mock
    SlotRepository slotRepository;

    @Mock
    SessionRepository sessionRepository;

    @Autowired
    SessionMapper sessionMapper;

    @Mock
    KubernetesService kubernetesService;

    @Autowired
    SlotMapper slotMapper;

    GameStatePusherServiceImpl gameStatePusherService;

    @BeforeEach
    void beforeEach() {
        this.gameStatePusherService = new GameStatePusherServiceImpl(exchange_requests,
                exchange_responses,
                messageMapper,
                template,
                rabbitAdmin,
                slotRepository,
                slotMapper,
                sessionRepository,
                sessionMapper,
                kubernetesService);
    }

    @Test
    void startGame_should_setup_queue_and_send_message() throws MessagingException, MessagingSetupException {
        when(rabbitAdmin.declareQueue(any())).thenReturn("");
        doNothing().when(rabbitAdmin).declareBinding(any());
        when(messageMapper.toStartGameMessage(any())).thenReturn(createStartMessage());
        when(exchange_responses.getName()).thenReturn("exchangeNameValue");
        doNothing().when(template).convertAndSend(eq("exchangeNameValue"), eq("session.1.#"), eq("{\"headers\":{},\"key\":\"keyId\",\"value\":{}}"));

        gameStatePusherService.startGame(createSessionDto());

        verify(template, times(1)).convertAndSend(eq("exchangeNameValue"), eq("session.1.#"), eq("{\"headers\":{},\"key\":\"keyId\",\"value\":{}}"));
        verifyNoMoreInteractions(template);
    }

    @Test
    void startGame_should_throw_exception_when_setup_fails() {
        when(rabbitAdmin.declareQueue(any())).thenThrow(AmqpException.class);

        final Throwable thrown = catchThrowable(() -> gameStatePusherService.startGame(createSessionDto()));

        assertThat(thrown).isNotNull();
        assertThat(thrown).isExactlyInstanceOf(MessagingSetupException.class);
        verifyNoInteractions(template);
    }

    @Test
    void startGame_should_remove_queue_when_sending_fails() {
        when(rabbitAdmin.declareQueue(any())).thenReturn("");
        doNothing().when(rabbitAdmin).declareBinding(any());
        when(rabbitAdmin.purgeQueue("session_with_id_1_responses")).thenReturn(1);
        when(messageMapper.toStartGameMessage(any())).thenReturn(createStartMessage());
        when(exchange_responses.getName()).thenReturn("exchangeNameValue");
        doThrow(AmqpException.class).when(template).convertAndSend(eq("exchangeNameValue"), eq("session.1.#"), eq("{\"headers\":{},\"key\":\"keyId\",\"value\":{}}"));

        final Throwable thrown = catchThrowable(() -> gameStatePusherService.startGame(createSessionDto()));

        assertThat(thrown).isNotNull();
        assertThat(thrown).isExactlyInstanceOf(MessagingException.class);

        verify(template, times(1)).convertAndSend(eq("exchangeNameValue"), eq("session.1.#"), eq("{\"headers\":{},\"key\":\"keyId\",\"value\":{}}"));
        verify(rabbitAdmin, times(1)).deleteQueue("session_with_id_1_responses");
        verify(rabbitAdmin, times(1)).declareQueue(any(Queue.class));
        verify(rabbitAdmin, times(1)).declareBinding(any(Binding.class));
        verifyNoMoreInteractions(template, rabbitAdmin);
    }

    @Test
    void publishMove_should_send_message() throws MessagingException, NotFoundException, PersistenceException, IOException, MessagingSetupException, KubernetesException {
        when(exchange_responses.getName()).thenReturn("exchangeNameValue");
        when(messageMapper.toUserMoveGameMessage(eq("1"), any(SlotDto.class), any())).thenReturn(createPublishMoveMessage());
        doNothing().when(template).convertAndSend(eq("exchangeNameValue"), eq("session.1.#"), any(Message.class));
        when(slotRepository.findBySessionIdAndPlayerId(1L, 1L)).thenReturn(Optional.of(createSlotEntity(0L)));

        gameStatePusherService.publishMove("1", "1", mapToJsonNode(createPublishMoveMessage()));

        verify(template, times(1)).convertAndSend(eq("exchangeNameValue"), eq("session.1.1"), eq("{\"headers\":{},\"key\":\"keyId\",\"value\":{\"type\":\"typeValue\"}}"));
        verifyNoMoreInteractions(template);
    }

    @Test
    void publishMove_should_shutdown_game_when_win() throws MessagingException, NotFoundException, PersistenceException, IOException, MessagingSetupException, KubernetesException {
        when(exchange_responses.getName()).thenReturn("exchangeNameValue");
        when(messageMapper.toUserMoveGameMessage(eq("1"), any(SlotDto.class), any())).thenReturn(createPublishMoveMessage());
        doNothing().when(template).convertAndSend(eq("exchangeNameValue"), eq("session.1.#"), any(Message.class));
        when(slotRepository.findBySessionIdAndPlayerId(1L, 1L)).thenReturn(Optional.of(createSlotEntity(0L)));
        when(sessionRepository.findById(1L)).thenReturn(Optional.ofNullable(createSessionEntity()));
        when(sessionRepository.save(any(SessionEntity.class))).thenReturn(createSessionEntity());

        gameStatePusherService.publishMove("1", "1", mapToJsonNode(createWinMessage()));

        verify(template, times(1)).convertAndSend(eq("exchangeNameValue"), eq("session.1.1"), eq("{\"headers\":{},\"key\":\"keyId\",\"value\":{\"type\":\"typeValue\"}}"));
        verifyNoMoreInteractions(template);
    }

    @Test
    void publishMove_throw_exception_when_sending_fails() {
        when(exchange_responses.getName()).thenReturn("exchangeNameValue");
        when(messageMapper.toUserMoveGameMessage(eq("1"), any(SlotDto.class), any())).thenReturn(createPublishMoveMessage());
        doThrow(AmqpException.class).when(template).convertAndSend(eq("exchangeNameValue"), eq("session.1.1"), anyString());
        when(slotRepository.findBySessionIdAndPlayerId(1L, 1L)).thenReturn(Optional.of(createSlotEntity(0L)));

        final Throwable thrown = catchThrowable(() -> gameStatePusherService.publishMove("1", "1", mapToJsonNode(createPublishMoveMessage())));

        assertThat(thrown).isNotNull();
        assertThat(thrown).isExactlyInstanceOf(MessagingException.class);
    }

    @Test
    void stopGame_should_remove_queues() throws MessagingSetupException {
        when(rabbitAdmin.deleteQueue("session_with_id_1_responses")).thenReturn(true);

        gameStatePusherService.stopGame(createSessionDto());

        verify(rabbitAdmin, times(1)).deleteQueue("session_with_id_1_responses");
        verifyNoMoreInteractions(rabbitAdmin);
    }

    @Test
    void stopGame_should_throw_exception_when_deletion_fails() {
        when(rabbitAdmin.deleteQueue("session_with_id_1_responses")).thenThrow(AmqpException.class);

        final Throwable thrown = catchThrowable(() -> gameStatePusherService.stopGame(createSessionDto()));

        assertThat(thrown).isNotNull();
        assertThat(thrown).isExactlyInstanceOf(MessagingSetupException.class);
    }

    @Test
    void shutdownSession_should_throw_exception_when_does_not_exist() {
        final Throwable throwable = Assertions.catchThrowable(() -> gameStatePusherService.shutdownSession(1L));

        assertThat(throwable).isNotNull();
        assertThat(throwable).isExactlyInstanceOf(NotFoundException.class);
    }

    @Test
    void shutdownSession_should_throw_exception_when_persistence_fails() {
        when(sessionRepository.findById(1L)).thenThrow(DataIntegrityViolationException.class);

        final Throwable throwable = Assertions.catchThrowable(() -> gameStatePusherService.shutdownSession(1L));

        assertThat(throwable).isNotNull();
        assertThat(throwable).isExactlyInstanceOf(PersistenceException.class);
    }

    @Test
    void shutdownSession_should_stop_game() throws IOException, ApiException, PersistenceException, NotFoundException, MessagingSetupException, KubernetesException {
        when(sessionRepository.findById(1L)).thenReturn(Optional.of(createSessionEntity()));
        SessionEntity entity = createSessionEntity();
        entity.setStatus(GameStatus.FINISHED);
        when(sessionRepository.save(any())).thenReturn(entity);
        doNothing().when(kubernetesService).stopExecutionEngine(anyString(), anyString());

        SessionDto dto = gameStatePusherService.shutdownSession(1L);

        assertThat(dto).isNotNull();
        assertThat(dto.getStatus()).isEqualTo(GameStatus.FINISHED);
        verify(kubernetesService, times(1)).stopExecutionEngine(anyString(), anyString());
        verifyNoMoreInteractions(kubernetesService);
    }
}
