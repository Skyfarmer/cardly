package at.ase.backend.service.impl;

import at.ase.backend.AbstractSpringBootTest;
import at.ase.backend.dto.GameDto;
import at.ase.backend.dto.PlayerDto;
import at.ase.backend.dto.SessionDto;
import at.ase.backend.exception.*;
import at.ase.backend.mapper.GameMapper;
import at.ase.backend.mapper.MessageMapper;
import at.ase.backend.mapper.SessionMapper;
import at.ase.backend.model.*;
import at.ase.backend.repository.GameRepository;
import at.ase.backend.repository.PlayerRepository;
import at.ase.backend.repository.SessionRepository;
import at.ase.backend.repository.SlotRepository;
import at.ase.backend.service.GameService;
import at.ase.backend.service.GameStatePusherService;
import at.ase.backend.service.KubernetesService;
import at.ase.backend.socket.FrontendCommunicationHandler;
import io.kubernetes.client.openapi.ApiException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.io.IOException;
import java.time.Instant;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class GameServiceImplTest extends AbstractSpringBootTest {
    @Autowired
    GameMapper gameMapper;

    @Mock
    GameRepository gameRepository;

    @Mock
    KubernetesService kubernetesService;

    @Mock
    GameStatePusherService gameStatePusherService;

    @Autowired
    SessionMapper sessionMapper;

    @Mock
    SessionRepository sessionRepository;

    @Mock
    PlayerRepository playerRepository;

    @Mock
    SlotRepository slotRepository;

    @Mock
    FrontendCommunicationHandler frontendCommunicationHandler;

    @Autowired
    MessageMapper messageMapper;

    GameService gameService;

    @BeforeEach
    void before() {
        gameService = new GameServiceImpl(gameRepository,
                gameMapper,
                kubernetesService,
                gameStatePusherService,
                sessionMapper,
                sessionRepository,
                playerRepository,
                slotRepository,
                frontendCommunicationHandler,
                messageMapper);
    }

    @Test
    void createGame_should_call_repository() throws PersistenceException, NotFoundException {
        when(gameRepository.save(any())).thenReturn(createGameEntity(1L, "nameValue"));
        when(playerRepository.findByEmail("emailValue")).thenReturn(Optional.ofNullable(createPlayerEntity()));

        GameDto result = gameService.createGame(createGameDto(), "emailValue");

        assertThat(result).isNotNull();
        assertThat(result.getName()).isEqualTo("nameValue");
    }

    @Test
    void createGame_should_throw_exception_when_persistence_fails() {
        when(gameRepository.save(any())).thenThrow(DataIntegrityViolationException.class);
        when(playerRepository.findByEmail("emailValue")).thenReturn(Optional.ofNullable(createPlayerEntity()));

        final Throwable throwable = catchThrowable(() -> gameService.createGame(createGameDto(), "emailValue"));

        assertThat(throwable).isNotNull();
        assertThat(throwable).isExactlyInstanceOf(PersistenceException.class);
    }

    @Test
    void listGames_should_throwException_when_page_negative() {
        final Throwable throwable = catchThrowable(() -> gameService.listGames(-1, 10));

        assertThat(throwable).isNotNull();
        assertThat(throwable.getMessage()).isEqualTo("Parameter 'page' is invalid.");
    }

    @Test
    void listGames_should_throwException_when_size_negative() {
        final Throwable throwable = catchThrowable(() -> gameService.listGames(1, -1));

        assertThat(throwable).isNotNull();
        assertThat(throwable.getMessage()).isEqualTo("Parameter 'size' is invalid.");
    }

    @Test
    void listGames_should_throwException_when_size_zero() {
        final Throwable throwable = catchThrowable(() -> gameService.listGames(1, 0));

        assertThat(throwable).isNotNull();
        assertThat(throwable.getMessage()).isEqualTo("Parameter 'size' is invalid.");
    }

    @Test
    void listGames_should_return_empty_list() throws InvalidListingConfigurationException, PersistenceException {
        when(gameRepository.findAll(any(PageRequest.class))).thenReturn(Page.empty());

        List<GameDto> dtos = gameService.listGames(10, 10);

        assertThat(dtos).isNotNull();
        assertThat(dtos).hasSize(0);
    }

    @Test
    void listGames_should_throw_exception_when_persistence_fails() {
        when(gameRepository.findAll(any(PageRequest.class))).thenThrow(DataIntegrityViolationException.class);

        final Throwable throwable = catchThrowable(() -> gameService.listGames(1, 1));

        assertThat(throwable).isNotNull();
        assertThat(throwable).isExactlyInstanceOf(PersistenceException.class);
    }

    @Test
    void getGameById_should_find_game() throws PersistenceException, NotFoundException {
        when(gameRepository.findById(any())).thenReturn(Optional.of(createGameEntity(1L, "nameValue")));

        GameDto dto = gameService.getGameById(1L);

        assertThat(dto).isNotNull();
        assertThat(dto.getName()).isEqualTo("nameValue");
    }

    @Test
    void getGameById_should_throw_exception_when_does_not_exist() {
        when(gameRepository.findById(1L)).thenReturn(Optional.empty());

        final Throwable throwable = catchThrowable(() -> gameService.getGameById(1L));

        assertThat(throwable).isNotNull();
        assertThat(throwable).isExactlyInstanceOf(NotFoundException.class);
    }

    @Test
    void getGameById_should_throw_exception_when_persistence_fails() {
        when(gameRepository.findById(1L)).thenThrow(DataIntegrityViolationException.class);

        final Throwable throwable = catchThrowable(() -> gameService.getGameById(1L));

        assertThat(throwable).isNotNull();
        assertThat(throwable).isExactlyInstanceOf(PersistenceException.class);
    }

    @Test
    void getGamesByName_should_find_games() throws PersistenceException {
        when(gameRepository.findByNameContainingIgnoreCase(any())).thenReturn(List.of(createGameEntity(1L, "nameValue")));
        List<GameDto> games = gameService.getGamesByName("nameValue");

        assertThat(games).isNotNull();
        assertThat(games.get(0).getName()).isEqualTo("nameValue");
    }

    @Test
    void getGamesByName_should_find_games_with_partial_name() throws PersistenceException {
        when(gameRepository.findByNameContainingIgnoreCase(any())).thenReturn(List.of(createGameEntity(1L, "nameValue")));
        List<GameDto> games = gameService.getGamesByName("N");

        assertThat(games).isNotNull();
        assertThat(games.get(0).getName()).isEqualTo("nameValue");
    }

    @Test
    void getGamesByName_should_throw_exception_when_persistence_fails() {
        when(gameRepository.findByNameContainingIgnoreCase("not existing")).thenThrow(DataIntegrityViolationException.class);

        final Throwable throwable = catchThrowable(() -> gameService.getGamesByName("not existing"));

        assertThat(throwable).isNotNull();
        assertThat(throwable).isExactlyInstanceOf(PersistenceException.class);
    }

    @Test
    void getGamesByCreator_should_find_games() throws PersistenceException {
        when(gameRepository.findByCreator_Id(any())).thenReturn(List.of(createGameEntity(1L, "nameValue")));
        List<GameDto> games = gameService.getGamesByCreator(1L);

        assertThat(games).isNotNull();
        assertThat(games.get(0).getCreator().getId()).isEqualTo(1L);
    }

    @Test
    void getGamesByCreator_should_throw_exception_when_persistence_fails() {
        when(gameRepository.findByCreator_Id(1L)).thenThrow(DataIntegrityViolationException.class);

        final Throwable throwable = catchThrowable(() -> gameService.getGamesByCreator(1L));

        assertThat(throwable).isNotNull();
        assertThat(throwable).isExactlyInstanceOf(PersistenceException.class);
    }

    @Test
    void getSessionById_should_find_game() throws PersistenceException, NotFoundException {
        when(sessionRepository.findById(any())).thenReturn(Optional.of(createSessionEntity()));

        SessionDto dto = gameService.getSessionById(1L);

        assertThat(dto).isNotNull();
    }

    @Test
    void getSessionById_should_throw_exception_when_does_not_exist() {
        when(sessionRepository.findById(1L)).thenReturn(Optional.empty());

        final Throwable throwable = catchThrowable(() -> gameService.getSessionById(1L));

        assertThat(throwable).isNotNull();
        assertThat(throwable).isExactlyInstanceOf(NotFoundException.class);
    }

    @Test
    void getSessionById_should_throw_exception_when_persistence_fails() {
        when(sessionRepository.findById(1L)).thenThrow(DataIntegrityViolationException.class);

        final Throwable throwable = catchThrowable(() -> gameService.getSessionById(1L));

        assertThat(throwable).isNotNull();
        assertThat(throwable).isExactlyInstanceOf(PersistenceException.class);
    }

    @Test
    void startSession_should_throw_exception_when_game_not_found() {
        when(sessionRepository.findById(1L)).thenReturn(Optional.empty());
        final Throwable throwable = catchThrowable(() -> gameService.startSession(1L));

        assertThat(throwable).isNotNull();
        assertThat(throwable).isExactlyInstanceOf(NotFoundException.class);

        verify(sessionRepository, times(1)).findById(1L);
        verifyNoMoreInteractions(sessionRepository);
    }

    @Test
    void startSession_should_throw_exception_when_session_has_not_enough_players() {
        SessionEntity entity = createSessionEntity();
        entity.setSlots(Collections.emptySet());
        when(sessionRepository.findById(1L)).thenReturn(Optional.of(entity));
        final Throwable throwable = catchThrowable(() -> gameService.startSession(1L));

        assertThat(throwable).isNotNull();
        assertThat(throwable).isExactlyInstanceOf(InvalidSessionStateException.class);

        verify(sessionRepository, times(1)).findById(1L);
        verifyNoMoreInteractions(sessionRepository);
    }

    @Test
    void startSession_should_throw_exception_when_game_state_is_not_new() {
        SessionEntity entity = createSessionEntity();
        entity.setStatus(GameStatus.STARTED);
        when(sessionRepository.findById(1L)).thenReturn(Optional.of(entity));

        final Throwable thrown = catchThrowable(() -> gameService.startSession(1L));

        assertThat(thrown).isExactlyInstanceOf(InvalidSessionStateException.class);
        verify(sessionRepository, times(1)).findById(1L);
        verifyNoMoreInteractions(sessionRepository);
    }

    @Test
    void startSession_should_throw_exception_when_persistence_fails() {
        when(sessionRepository.findById(1L)).thenReturn(Optional.empty());

        final Throwable throwable = catchThrowable(() -> gameService.startSession(1L));

        assertThat(throwable).isNotNull();
        assertThat(throwable).isExactlyInstanceOf(NotFoundException.class);

        verify(sessionRepository, times(1)).findById(1L);
        verifyNoMoreInteractions(sessionRepository);
    }

    @Test
    void startSession_should_throw_exception_when_persistence_fails_during_save() throws IOException, KubernetesException, MessagingException, MessagingSetupException, ApiException {
        when(sessionRepository.findById(1L)).thenReturn(Optional.of(createSessionEntity()));
        doNothing().when(kubernetesService).startExecutionEngine("nameValue", "1");
        doThrow(MessagingSetupException.class).when(gameStatePusherService).startGame(any(SessionDto.class));
        when(sessionRepository.save(any())).thenThrow(DataIntegrityViolationException.class);

        when(playerRepository.findById(0L)).thenReturn(Optional.of(createPlayerEntity()));
        when(playerRepository.findById(1L)).thenReturn(Optional.of(createPlayerEntity()));

        SlotEntity slot = createSlotEntity(0L);
        slot.getPlayer().setId(0L);
        SlotEntity slot2 = createSlotEntity(1L);
        HashSet<SlotEntity> slots = new HashSet<>();
        slots.add(slot);
        slots.add(slot2);

        SessionEntity session = createSessionEntity();
        session.setSlots(slots);
        when(sessionRepository.findById(1L)).thenReturn(Optional.of(session));

        final Throwable throwable = catchThrowable(() -> gameService.startSession(1L));

        assertThat(throwable).isNotNull();
        assertThat(throwable).isExactlyInstanceOf(PersistenceException.class);
    }

    @Test
    void startSession_should_return_updated_entity() throws PersistenceException, NotFoundException, IOException, MessagingException, MessagingSetupException, KubernetesException, ApiException {
        when(sessionRepository.findById(1L)).thenReturn(Optional.of(createSessionEntity()));
        when(sessionRepository.save(any())).thenAnswer(returnsFirstArg());
        doNothing().when(kubernetesService).startExecutionEngine("nameValue", "1");
        doNothing().when(gameStatePusherService).startGame(any(SessionDto.class));

        when(playerRepository.findById(0L)).thenReturn(Optional.of(createPlayerEntity()));
        when(playerRepository.findById(1L)).thenReturn(Optional.of(createPlayerEntity()));

        SlotEntity slot = createSlotEntity(0L);
        slot.getPlayer().setId(0L);
        SlotEntity slot2 = createSlotEntity(1L);
        HashSet<SlotEntity> slots = new HashSet<>();
        slots.add(slot);
        slots.add(slot2);

        SessionEntity session = createSessionEntity();
        session.setSlots(slots);
        when(sessionRepository.findById(1L)).thenReturn(Optional.of(session));
        SessionDto sessionDto = gameService.startSession(1L);

        assertThat(sessionDto).isNotNull();
        assertThat(sessionDto.getStatus()).isEqualTo(GameStatus.STARTED);
    }

    @Test
    void startSession_should_stop_execution_engine_when_pushing_message_fails() throws IOException, ApiException, MessagingException, MessagingSetupException, PersistenceException, NotFoundException, KubernetesException {
        when(sessionRepository.findById(1L)).thenReturn(Optional.of(createSessionEntity()));
        when(sessionRepository.save(any())).thenReturn(createSessionEntity());
        doNothing().when(kubernetesService).startExecutionEngine("nameValue", "1");
        doNothing().when(kubernetesService).stopExecutionEngine("nameValue", "1");
        doThrow(MessagingException.class).when(gameStatePusherService).startGame(any(SessionDto.class));

        when(playerRepository.findById(0L)).thenReturn(Optional.of(createPlayerEntity()));
        when(playerRepository.findById(1L)).thenReturn(Optional.of(createPlayerEntity()));

        SlotEntity slot = createSlotEntity(0L);
        slot.getPlayer().setId(0L);
        SlotEntity slot2 = createSlotEntity(1L);
        HashSet<SlotEntity> slots = new HashSet<>();
        slots.add(slot);
        slots.add(slot2);

        SessionEntity session = createSessionEntity();
        session.setSlots(slots);
        when(sessionRepository.findById(1L)).thenReturn(Optional.of(session));
        SessionDto sessionDto = gameService.startSession(1L);

        assertThat(sessionDto).isNotNull();
        assertThat(sessionDto.getStatus()).isEqualTo(GameStatus.NEW); // Rollback to initial

        verify(kubernetesService, times(1)).startExecutionEngine("nameValue", "1");
        verify(kubernetesService, times(1)).stopExecutionEngine("nameValue", "1");
        verifyNoMoreInteractions(kubernetesService);
    }


    @Test
    void createSession_should_create_session() throws NotFoundException, PersistenceException {
        when(sessionRepository.findById(1L)).thenReturn(Optional.of(createSessionEntity()));
        when(gameRepository.findById(1L)).thenReturn(Optional.of(createGameEntity(1L, "nameValue")));
        when(sessionRepository.save(any())).thenAnswer(returnsFirstArg());

        SessionDto dto = gameService.createSession(1L, createSessionDto());

        assertThat(dto).isNotNull();
        assertThat(dto.getGame()).isNotNull();
        assertThat(dto.getGame().getId()).isEqualTo(1);
        assertThat(dto.getVisibility()).isEqualTo(SessionVisibility.PRIVATE);

        verifyNoInteractions(kubernetesService, gameStatePusherService);
    }

    @Test
    void createSession_should_throw_exception_when_persistence_fails() {
        when(gameRepository.findById(1L)).thenReturn(Optional.of(createGameEntity(1L, "nameValue")));
        when(sessionRepository.save(any())).thenThrow(DataIntegrityViolationException.class);

        final Throwable throwable = catchThrowable(() -> gameService.createSession(1L, createSessionDto()));

        assertThat(throwable).isNotNull();
        assertThat(throwable).isExactlyInstanceOf(PersistenceException.class);
    }

    @Test
    void updateGame_should_update_session() throws NotFoundException, PersistenceException, InvalidCreatorException {
        when(gameRepository.findById(1L)).thenReturn(Optional.of(createGameEntity(1L, "nameValue")));
        when(playerRepository.findByEmail("emailValue")).thenReturn(Optional.of(createPlayerEntity()));

        GameDto given = createGameDto();
        given.setId(1L);
        given.setName("updatedNameValue");
        GameDto updated = gameService.updateGame(given, "emailValue");

        assertThat(updated.getId()).isEqualTo(1);
        assertThat(updated.getName()).isEqualTo("updatedNameValue");
    }

    @Test
    void updateGame_should_throw_exception_when_no_game_not_found() throws NotFoundException {
        when(gameRepository.findById(1L)).thenReturn(Optional.empty());
        final Throwable throwable = catchThrowable(() -> gameService.updateGame(createGameDto(), "test@test.com"));

        assertThat(throwable).isExactlyInstanceOf(NotFoundException.class);
    }

    @Test
    void updateGame_should_throw_exception_when_persistence_fails() {
        when(gameRepository.findById(1L)).thenThrow(DataIntegrityViolationException.class);

        final Throwable throwable = catchThrowable(() -> gameService.updateGame(createGameDto(), "test@test.com"));

        assertThat(throwable).isNotNull();
        assertThat(throwable).isExactlyInstanceOf(PersistenceException.class);

        verify(gameRepository, times(1)).findById(1L);
        verifyNoMoreInteractions(gameRepository);
    }

    @Test
    void updateGame_should_throw_exception_when_creator_is_invalid() {
        when(gameRepository.findById(1L)).thenReturn(Optional.of(createGameEntity(1L, "nameValue")));
        PlayerEntity creator = createPlayerEntity();
        creator.setEmail("test@test.com");
        creator.setId(2L);
        when(playerRepository.findByEmail("test@test.com")).thenReturn(Optional.of(creator));

        final Throwable throwable = catchThrowable(() -> gameService.updateGame(createGameDto(), "test@test.com"));

        assertThat(throwable).isNotNull();
        assertThat(throwable).isExactlyInstanceOf(InvalidCreatorException.class);

        verify(gameRepository, times(1)).findById(1L);
        verifyNoMoreInteractions(gameRepository);
    }

    @Test
    void addPlayer_should_update_session() throws NotFoundException, PersistenceException, InvalidRequestException {
        when(sessionRepository.findById(1L)).thenReturn(Optional.of(createSessionEntity()));
        when(playerRepository.findById(1L)).thenReturn(Optional.of(createPlayerEntity()));

        SessionDto updated = gameService.addPlayer(1L, 1L);

        assertThat(updated.getId()).isEqualTo(1);
        assertThat(updated.getSlots().size()).isEqualTo(1);
        assertThat(updated.getSlots().get(0).getPlayer()).isNotNull();
    }

    @Test
    void addPlayer_should_throw_exception_when_no_game_not_found() {
        when(sessionRepository.findById(1L)).thenReturn(Optional.empty());
        final Throwable throwable = catchThrowable(() -> gameService.addPlayer(1L, 1L));

        assertThat(throwable).isExactlyInstanceOf(NotFoundException.class);
    }

    @Test
    void addPlayer_should_throw_exception_when_capacity_max() {
        SessionEntity sessionEntity = createSessionEntity();
        SlotEntity slotEntity1 = createSlotEntity(0L);
        SlotEntity slotEntity2 = createSlotEntity(1L);
        PlayerEntity entity = createPlayerEntity();
        entity.setId(2L);
        slotEntity2.setPlayer(entity);

        PlayerEntity newPlayer = createPlayerEntity();
        newPlayer.setId(3L);

        sessionEntity.setSlots(new HashSet<>(List.of(slotEntity1, slotEntity2)));
        when(sessionRepository.findById(1L)).thenReturn(Optional.of(sessionEntity));
        when(playerRepository.findById(1L)).thenReturn(Optional.of(createPlayerEntity()));
        when(playerRepository.findById(2L)).thenReturn(Optional.of(entity));
        when(playerRepository.findById(3L)).thenReturn(Optional.of(newPlayer));
        final Throwable throwable = catchThrowable(() -> gameService.addPlayer(1L, 3L));

        assertThat(throwable).isExactlyInstanceOf(InvalidRequestException.class);
        assertThat(throwable).hasMessage("Game already full");
    }

    @Test
    void addPlayer_should_throw_exception_when_persistence_fails() {
        when(sessionRepository.findById(1L)).thenThrow(DataIntegrityViolationException.class);

        final Throwable throwable = catchThrowable(() -> gameService.addPlayer(1L, 1L));

        assertThat(throwable).isNotNull();
        assertThat(throwable).isExactlyInstanceOf(PersistenceException.class);
    }

    @Test
    void addPlayer_should_throw_exception_when_player_added() {
        SessionEntity entity = createSessionEntity();
        SlotEntity slot1 = createSlotEntity(0L);
        entity.setSlots(Set.of(slot1));

        when(sessionRepository.findById(1L)).thenReturn(Optional.of(entity));
        when(playerRepository.findById(1L)).thenReturn(Optional.of(createPlayerEntity()));

        final Throwable throwable = catchThrowable(() -> gameService.addPlayer(1L, 1L));

        assertThat(throwable).isNotNull();
        assertThat(throwable).isExactlyInstanceOf(InvalidRequestException.class);
    }

    @Test
    void addPlayer_should_fillup_reexisting_slot() throws PersistenceException, NotFoundException, InvalidRequestException {
        SessionEntity entity = createSessionEntity();
        SlotEntity slot1 = createSlotEntity(0L);
        SlotEntity slot2 = createSlotEntity(1L);
        slot2.setPlayer(null);
        SlotEntity slot3 = createSlotEntity(1L);
        slot3.getPlayer().setId(99L);
        entity.setSlots(Set.of(slot1, slot2, slot3));

        PlayerEntity playerEntity = createPlayerEntity();
        playerEntity.setId(2L);

        when(sessionRepository.findById(1L)).thenReturn(Optional.of(entity));
        when(playerRepository.findById(2L)).thenReturn(Optional.of(playerEntity));
        when(sessionRepository.save(any())).thenAnswer(returnsFirstArg());

        SessionDto updated = gameService.addPlayer(1L, 2L);

        assertThat(updated).isNotNull();
        assertThat(updated.getSlots()).hasSize(3);
        assertThat(updated.getSlots().stream().allMatch(x -> x.getPlayer() != null)).isTrue();
    }

    @Test
    void removePlayer_should_update_session() throws NotFoundException, PersistenceException {
        SlotEntity slot1 = createSlotEntity(0L);
        SessionEntity entity = createSessionEntity();
        entity.setSlots(Set.of(slot1));
        when(sessionRepository.findById(1L)).thenReturn(Optional.of(entity));
        when(playerRepository.findById(1L)).thenReturn(Optional.of(createPlayerEntity()));

        SessionDto updated = gameService.removePlayer(1L, 1L);

        assertThat(updated.getId()).isEqualTo(1);
        assertThat(updated.getSlots()).hasSize(1);
        assertThat(updated.getSlots().get(0).getPlayer()).isNull();
    }

    @Test
    void removePlayer_should_update_session_when_no_players() throws NotFoundException, PersistenceException {
        SessionEntity entity = createSessionEntity();
        entity.setSlots(Set.of());
        when(sessionRepository.findById(1L)).thenReturn(Optional.of(entity));
        when(playerRepository.findById(1L)).thenReturn(Optional.of(createPlayerEntity()));

        SessionDto updated = gameService.removePlayer(1L, 1L);

        assertThat(updated.getId()).isEqualTo(1);
        assertThat(updated.getSlots()).hasSize(0);
    }

    @Test
    void removePlayer_should_update_session_when_first_slot_has_no_player() throws NotFoundException, PersistenceException {
        SlotEntity slot1 = createSlotEntity(0L);
        slot1.setPlayer(null);
        SlotEntity slot2 = createSlotEntity(0L);
        SessionEntity entity = createSessionEntity();
        entity.setSlots(Set.of(slot1, slot2));
        when(sessionRepository.findById(1L)).thenReturn(Optional.of(entity));
        when(playerRepository.findById(1L)).thenReturn(Optional.of(createPlayerEntity()));

        SessionDto updated = gameService.removePlayer(1L, 1L);

        assertThat(updated.getId()).isEqualTo(1);
        assertThat(updated.getSlots()).hasSize(2);
        assertThat(updated.getSlots().get(0).getPlayer()).isNull();
        assertThat(updated.getSlots().get(1).getPlayer()).isNull();
    }

    @Test
    void removePlayer_should_throw_exception_when_no_game_not_found() {
        when(sessionRepository.findById(1L)).thenReturn(Optional.empty());
        final Throwable throwable = catchThrowable(() -> gameService.removePlayer(1L, 1L));

        assertThat(throwable).isExactlyInstanceOf(NotFoundException.class);
    }

    @Test
    void removePlayer_should_throw_exception_when_persistence_fails() {
        when(sessionRepository.findById(1L)).thenThrow(DataIntegrityViolationException.class);

        final Throwable throwable = catchThrowable(() -> gameService.removePlayer(1L, 1L));

        assertThat(throwable).isNotNull();
        assertThat(throwable).isExactlyInstanceOf(PersistenceException.class);
    }

    @Test
    void matchMaking_should_create_new_session() throws PersistenceException, NotFoundException, InvalidRequestException {
        when(gameRepository.findById(1L)).thenReturn(Optional.of(createGameEntity(1L, "nameValue")));
        when(playerRepository.findById(1L)).thenReturn(Optional.of(createPlayerEntity()));
        when(sessionRepository.findSessionEntitiesByStatus(GameStatus.NEW)).thenReturn(List.of());

        when(sessionRepository.findById(1L)).thenReturn(Optional.of(createSessionEntity()));
        when(sessionRepository.save(any())).thenAnswer(returnsFirstArg());

        SessionDto dto = gameService.matchMaking(1L, 1L);

        assertThat(dto).isNotNull();
        assertThat(dto.getGame()).isNotNull();
        assertThat(dto.getGame().getId()).isEqualTo(1);
    }

    @Test
    void matchMaking_should_return_existing_session() throws PersistenceException, NotFoundException, InvalidRequestException {
        when(gameRepository.findById(1L)).thenReturn(Optional.of(createGameEntity(1L, "nameValue")));
        when(playerRepository.findById(1L)).thenReturn(Optional.of(createPlayerEntity()));
        when(sessionRepository.findSessionEntitiesByStatus(GameStatus.NEW)).thenReturn(List.of(createSessionEntity(SessionVisibility.PUBLIC)));
        when(sessionRepository.findById(1L)).thenReturn(Optional.of(createSessionEntity()));

        SessionDto dto = gameService.matchMaking(1L, 1L);

        assertThat(dto).isNotNull();
        assertThat(dto.getGame()).isNotNull();
        assertThat(dto.getGame().getId()).isEqualTo(1);

        verify(sessionRepository, times(0)).save(any());
    }

    @Test
    void matchMaking_should_return_earlier_session() throws PersistenceException, NotFoundException, InvalidRequestException {
        when(gameRepository.findById(1L)).thenReturn(Optional.of(createGameEntity(1L, "nameValue")));
        when(playerRepository.findById(1L)).thenReturn(Optional.of(createPlayerEntity()));

        SessionEntity older = createSessionEntity(SessionVisibility.PUBLIC);
        older.setCreatedAt(Instant.ofEpochMilli(0));
        SessionEntity newer = createSessionEntity(SessionVisibility.PUBLIC);
        newer.setId(2L);

        when(sessionRepository.findSessionEntitiesByStatus(GameStatus.NEW)).thenReturn(List.of(newer, older));
        when(sessionRepository.findById(1L)).thenReturn(Optional.of(createSessionEntity()));

        SessionDto dto = gameService.matchMaking(1L, 1L);

        assertThat(dto).isNotNull();
        assertThat(dto.getId()).isEqualTo(1L);
        assertThat(dto.getGame()).isNotNull();
        assertThat(dto.getGame().getId()).isEqualTo(1);

        verify(sessionRepository, times(0)).save(any());
    }

    @Test
    void getSessionByToken_should_get_session() throws PersistenceException, NotFoundException {
        when(sessionRepository.findByJoinToken("joinTokenValue")).thenReturn(Optional.of(createSessionEntity()));

        SessionDto dto = gameService.getSessionByToken("joinTokenValue");

        assertThat(dto).isNotNull();
        assertThat(dto.getJoinToken()).isEqualTo("joinTokenValue");

        verify(sessionRepository, times(1)).findByJoinToken("joinTokenValue");
        verifyNoMoreInteractions(sessionRepository);
    }

    @Test
    void getSessionByToken_should_throw_exception_when_persistence_fails() {
        when(sessionRepository.findByJoinToken("joinTokenValue")).thenThrow(DataIntegrityViolationException.class);

        final Throwable throwable = catchThrowable(() -> gameService.getSessionByToken("joinTokenValue"));

        assertThat(throwable).isNotNull();
        assertThat(throwable).isExactlyInstanceOf(PersistenceException.class);
    }

    @Test
    void getSessionByToken_should_throw_exception_when_not_found() {
        when(sessionRepository.findByJoinToken("joinTokenValue")).thenReturn(Optional.empty());

        final Throwable throwable = catchThrowable(() -> gameService.getSessionByToken("joinTokenValue"));

        assertThat(throwable).isNotNull();
        assertThat(throwable).isExactlyInstanceOf(NotFoundException.class);
    }
}
