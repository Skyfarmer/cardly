package at.ase.backend.service.impl;

import at.ase.backend.AbstractSpringBootTest;
import at.ase.backend.dto.GameDto;
import at.ase.backend.exception.ImageStorageException;
import at.ase.backend.exception.NotFoundException;
import at.ase.backend.exception.PersistenceException;
import at.ase.backend.mapper.GameMapper;
import at.ase.backend.model.GameEntity;
import at.ase.backend.model.ImageScope;
import at.ase.backend.property.ImageStorageProperties;
import at.ase.backend.repository.ImageRepository;
import at.ase.backend.service.ImageStorageService;
import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.Resource;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class ImageStorageServiceImplTest extends AbstractSpringBootTest {

    private static final Long ID = 0L;
    private static final Logger LOG = LoggerFactory.getLogger(ImageStorageServiceImplTest.class);
    private static final MultipartFile image;

    static {
        try {
            Path path = Paths.get("src/test/resources/pizza.png");
            String name = "pizza.png";
            String originalFileName = "pizza.png";
            String contentType = "image/png";
            byte[] content = Files.readAllBytes(path);
            image = new MockMultipartFile(name,
                    originalFileName, contentType, content);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    ImageStorageService imageStorageService;

    @Mock
    ImageStorageProperties properties;

    @Mock
    ImageRepository imageRepository;

    @Autowired
    GameMapper gameMapper;

    @Mock
    GameDto game;

    @Mock
    GameServiceImpl gameService;

    @BeforeEach
    public void before() throws IOException {
        game.setId(ID);
        String tmpdir = Files.createTempDirectory("temp").toFile().getAbsolutePath();
        when(properties.getUploadDir()).thenReturn(tmpdir);
        this.imageStorageService = new ImageStorageServiceImpl(properties, imageRepository, gameService, gameMapper);
    }

    @Test
    void storeImage_does_not_throw_exception() throws PersistenceException, NotFoundException, IOException {
        when(gameService.getGameById(any())).thenReturn(game);

        String fileName = imageStorageService.storeImage(ID, image, ImageScope.PRESENTATION);
        assertThat(fileName).isNotNull();
    }

    @Test
    void storeImage_creates_required_directory() throws PersistenceException, NotFoundException, IOException {
        // the directory should always be empty since we use a different directory as for the real implementation
        // and all created files are deleted in the after method
        Path dir = Path.of(properties.getUploadDir() + "/" + ID);
        if (Files.exists(dir)) {
                Files.delete(dir);
        }
        imageStorageService.storeImage(ID, image, ImageScope.PRESENTATION);
        assertThat(dir).exists();
    }

    @Test
    void storeImage_for_non_image_type_should_throw_exception() {
        MultipartFile image = new MockMultipartFile("Test", "Test", "application/octet-stream", new byte[1]);
        final Throwable throwable = catchThrowable(() -> imageStorageService.storeImage(ID, image, ImageScope.PRESENTATION));

        assertThat(throwable).isNotNull();
        assertThat(throwable).isExactlyInstanceOf(ImageStorageException.class);
    }

    @Test
    void storeImage_should_throw_exception_when_unix_invalid_char() {
        MultipartFile file = mock(MultipartFile.class);
        when(file.getContentType()).thenReturn("image");
        when(file.getOriginalFilename()).thenReturn("\000");

        final Throwable throwable = catchThrowable(() -> imageStorageService.storeImage(1L, file, ImageScope.PRESENTATION));

        assertThat(throwable).isNotNull();
        assertThat(throwable).isExactlyInstanceOf(ImageStorageException.class);
        assertThat(throwable).hasMessage("Image contains invalid character");
    }

    @Test
    void storeImage_should_throw_exception_when_windows_invalid_char() {
        MultipartFile file = mock(MultipartFile.class);
        when(file.getContentType()).thenReturn("image");
        when(file.getOriginalFilename()).thenReturn("*");

        final Throwable throwable = catchThrowable(() -> imageStorageService.storeImage(1L, file, ImageScope.PRESENTATION));

        assertThat(throwable).isNotNull();
        assertThat(throwable).isExactlyInstanceOf(ImageStorageException.class);
        assertThat(throwable).hasMessage("Image contains invalid character");
    }

    @Test
    void loadImage_is_identical_to_stored_file() throws IOException, NotFoundException, PersistenceException {
        String fileName = imageStorageService.storeImage(ID, image, ImageScope.PRESENTATION);
        Resource loaded = imageStorageService.loadImage(ID, fileName);
        assertThat(loaded).isNotNull();
        assertThat(Files.readAllBytes(loaded.getFile().toPath())).isEqualTo(image.getBytes());
    }

    @Test
    void loadImage_for_not_existing_image_throws_exception() {
        final Throwable throwable = catchThrowable(() -> imageStorageService.loadImage(ID, "test"));

        assertThat(throwable).isNotNull();
        assertThat(throwable).isExactlyInstanceOf(NotFoundException.class);
    }


    @Test
    void deleteImage_can_delete_image() throws PersistenceException, NotFoundException, IOException {
        String fileName = imageStorageService.storeImage(ID, image, ImageScope.PRESENTATION);
        imageStorageService.deleteImage(ID, fileName);
        Path location = Paths.get(properties.getUploadDir()).resolve(String.valueOf(ID)).resolve(fileName).toAbsolutePath();
        assertThat(Files.exists(location)).isFalse();
    }

    @Test
    void deleteImage_does_not_fail_if_image_already_deleted() throws PersistenceException, NotFoundException, IOException {
        String fileName = imageStorageService.storeImage(ID, image, ImageScope.PRESENTATION);
        imageStorageService.deleteImage(ID, fileName);
        imageStorageService.deleteImage(ID, fileName);
    }


}
