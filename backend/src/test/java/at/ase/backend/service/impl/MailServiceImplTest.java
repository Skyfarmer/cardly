package at.ase.backend.service.impl;

import at.ase.backend.AbstractSpringBootTest;
import at.ase.backend.service.MailService;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.*;

public class MailServiceImplTest extends AbstractSpringBootTest {
    @MockBean
    JavaMailSender sender;

    @Autowired
    MailService mailService;

    @Test
    void sendPasswordForgottenEmail_should_send_mail() {
        mailService.sendPasswordForgottenEmail("emailValue", "?token=refreshUrlValue");

        ArgumentCaptor<SimpleMailMessage> argumentCaptor = ArgumentCaptor.forClass(SimpleMailMessage.class);
        verify(sender, times(1)).send(argumentCaptor.capture());

        assertThat(argumentCaptor.getValue()).isNotNull();
        assertThat(argumentCaptor.getValue().getFrom()).isEqualTo("noreply@gameplatform.at");
        assertThat(argumentCaptor.getValue().getTo()[0]).isEqualTo("emailValue");
        assertThat(argumentCaptor.getValue().getSubject()).isEqualTo("Reset your Password");
        assertThat(argumentCaptor.getValue().getText()).isEqualTo("Reset your password at https://aseloves.us/#/reset-password?token=refreshUrlValue");
    }

}
