package at.ase.backend.controller;

import at.ase.backend.AbstractSpringBootTest;
import at.ase.backend.dto.GameDto;
import at.ase.backend.dto.PlayerDto;
import at.ase.backend.exception.InvalidListingConfigurationException;
import at.ase.backend.exception.KubernetesException;
import at.ase.backend.exception.NotFoundException;
import at.ase.backend.service.GameService;
import at.ase.backend.service.PlayerService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.Collections;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.log;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class UserControllerMvcTest extends AbstractSpringBootTest {
    @MockBean
    PlayerService playerService;

    @Test
    @WithMockUser("emailValue@email.com")
    void update_should_update_user() throws Exception {
        PlayerDto playerDto = createPlayerDto();
        playerDto.setEmail("emailValue@email.com");
        when(playerService.updatePlayer(eq("emailValue@email.com"), any())).thenReturn(playerDto);

        this.mockMvc.perform(put("/api/v1/user").contentType(MediaType.APPLICATION_JSON).content("{" +
                        "\"id\"" + ":" + "1," +
                        "\"alias\"" + ":" + "\"aliasValue\"," +
                        "\"email\"" + ":" + "\"emailValue@email.com\"" +
                        "}"))
                .andDo(log())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json("{" +
                        "\"id\":" + "1," +
                        "\"alias\":" + "\"aliasValue\"," +
                        "\"email\"" + ":" + "\"emailValue@email.com\"" +
                        "}"));
    }


    @Test
    @WithMockUser("emailValue@email.com")
    void updatePassword_should_update_user() throws Exception {
        PlayerDto playerDto = createPlayerDto();
        when(playerService.updatePassword(eq("emailValue@email.com"), any())).thenReturn(playerDto);

        this.mockMvc.perform(put("/api/v1/user/password").contentType(MediaType.APPLICATION_JSON).content("{" +
                        "\"currentPassword\"" + ":" + "\"oldPasswordValue\"," +
                        "\"newPassword\"" + ":" + "\"newPasswordValue\"" +
                        "}"))
                .andDo(log())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json("{" +
                        "\"id\":" + "1," +
                        "\"alias\":" + "\"aliasValue\"," +
                        "\"email\"" + ":" + "\"emailValue@email.com\"" +
                        "}"));
    }
}