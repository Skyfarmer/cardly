package at.ase.backend.controller;

import at.ase.backend.AbstractSpringBootTest;
import at.ase.backend.dto.GameDto;
import at.ase.backend.exception.InvalidListingConfigurationException;
import at.ase.backend.exception.KubernetesException;
import at.ase.backend.exception.NotFoundException;
import at.ase.backend.service.GameService;
import at.ase.backend.service.GameStatePusherService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.Collections;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.log;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class GameControllerMvcTest extends AbstractSpringBootTest {
    @MockBean
    GameService gameService;

    @MockBean
    GameStatePusherService gameStatePusherService;

    @Test
    @WithMockUser("test@test.com")
    void createGame_should_save_game() throws Exception {
        GameDto dto = createGameDto();
        when(gameService.createGame(any(), eq("test@test.com"))).thenReturn(dto);

        this.mockMvc.perform(post("/api/v1/game").contentType(MediaType.APPLICATION_JSON).content("{" +
                        "\"name\"" + ":" + "\"nameValue\"," +
                        "\"source_code\"" + ":" + "\"sourceCodeValue\"," +
                        "\"description\"" + ":" + "\"descriptionValue\"," +
                        "\"min_capacity\"" + ":" + 2 + "," +
                        "\"max_capacity\"" + ":" + 2 +
                        "}"))
                .andDo(log())
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json("{" +
                        "\"name\":" + "\"nameValue\"," +
                        "\"source_code\"" + ":" + "\"sourceCodeValue\"," +
                        "\"description\"" + ":" + "\"descriptionValue\"," +
                        "\"min_capacity\"" + ":" + 2 + "," +
                        "\"max_capacity\"" + ":" + 2 +
                        "}"));
    }

    @Test
    @WithMockUser("test@test.com")
    void listGames_should_use_default_page_value() throws Exception {
        when(gameService.listGames(0, 10)).thenReturn(Collections.emptyList());

        this.mockMvc.perform(get("/api/v1/games?size=10"))
                .andDo(log())
                .andExpect(status().isOk());

        verify(gameService, times(1)).listGames(0, 10);
    }

    @Test
    @WithMockUser("test@test.com")
    void listGames_should_use_default_size_value() throws Exception {
        when(gameService.listGames(0, 10)).thenReturn(Collections.emptyList());

        this.mockMvc.perform(get("/api/v1/games?page=0"))
                .andDo(log())
                .andExpect(status().isOk());

        verify(gameService, times(1)).listGames(0, 10);
    }

    @Test
    @WithMockUser("test@test.com")
    void listGames_should_be_bad_request_when_service_throws_exception() throws Exception {
        when(gameService.listGames(-1, -1)).thenThrow(new InvalidListingConfigurationException("messageValue"));

        this.mockMvc.perform(get("/api/v1/games?page=-1&size=-1"))
                .andDo(log())
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser("test@test.com")
    void getGameById_should_return() throws Exception {
        when(gameService.getGameById(1L)).thenReturn(createGameDto());

        this.mockMvc.perform(get("/api/v1/game/1"))
                .andDo(log())
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser("test@test.com")
    void getGameById_should_be_not_found_when_does_not_exist() throws Exception {
        when(gameService.getGameById(1L)).thenThrow(NotFoundException.class);

        this.mockMvc.perform(get("/api/v1/game/1"))
                .andDo(log())
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser("test@test.com")
    void startGame_should_return_ok() throws Exception, KubernetesException {
        when(gameService.startSession(1L)).thenReturn(createSessionDto());

        this.mockMvc.perform(post("/api/v1/session/1/start"))
                .andDo(log())
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser("test@test.com")
    void startGame_should_be_not_found_when_does_not_exist() throws Exception, KubernetesException {
        when(gameService.startSession(1L)).thenThrow(NotFoundException.class);

        this.mockMvc.perform(post("/api/v1/session/1/start"))
                .andDo(log())
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser("test@test.com")
    void shutdownGame_should_be_not_found_when_does_not_exist() throws Exception, KubernetesException {
        when(gameStatePusherService.shutdownSession(1L)).thenThrow(NotFoundException.class);

        this.mockMvc.perform(post("/api/v1/session/1/shutdown"))
                .andDo(log())
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser("test@test.com")
    void shutdownGame_should_return_ok() throws Exception, KubernetesException {
        when(gameStatePusherService.shutdownSession(1L)).thenReturn(createSessionDto());

        this.mockMvc.perform(post("/api/v1/session/1/shutdown"))
                .andDo(log())
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser("test@test.com")
    void createSession_should_return_ok() throws Exception {
        when(gameService.createSession(eq(1L), any())).thenReturn(createSessionDto());

        this.mockMvc.perform(post("/api/v1/game/1/session").contentType(MediaType.APPLICATION_JSON).content("{ \"visibility\": \"PUBLIC\" }"))
                .andDo(log())
                .andExpect(status().isCreated());
    }

    @Test
    @WithMockUser("test@test.com")
    void createSession_should_return_not_found() throws Exception {
        when(gameService.createSession(eq(1L), any())).thenThrow(NotFoundException.class);

        this.mockMvc.perform(post("/api/v1/game/1/session").contentType(MediaType.APPLICATION_JSON).content("{ \"visibility\": \"PUBLIC\" }"))
                .andDo(log())
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser("test@test.com")
    void updateGame_should_return_ok() throws Exception {
        when(gameService.updateGame(any(GameDto.class),eq("test@test.com"))).thenReturn(createGameDto());

        this.mockMvc.perform(put("/api/v1/game/1").contentType(MediaType.APPLICATION_JSON).content("{ \"name\": \"test\", \"source_code\": \"sourceCodeValue\", \"min_capacity\": 2, \"max_capacity\": 2, \"description\": \"descriptionValue\" }"))
                .andDo(log())
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser("test@test.com")
    void updateGame_should_return_not_found() throws Exception {
        when(gameService.updateGame(any(GameDto.class),eq("test@test.com"))).thenThrow(NotFoundException.class);

        this.mockMvc.perform(put("/api/v1/game/1").contentType(MediaType.APPLICATION_JSON).content("{ \"name\": \"test\", \"source_code\": \"sourceCodeValue\", \"min_capacity\": 2, \"max_capacity\": 2, \"description\": \"descriptionValue\" }"))
                .andDo(log())
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser("test@test.com")
    void addPlayer_should_return_ok() throws Exception {
        when(gameService.addPlayer(any(Long.class), any(Long.class))).thenReturn(createSessionDto());

        this.mockMvc.perform(post("/api/v1/session/1/player/1").contentType(MediaType.APPLICATION_JSON).content("{ }"))
                .andDo(log())
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser("test@test.com")
    void addPlayer_should_return_not_found() throws Exception {
        when(gameService.addPlayer(any(Long.class), any(Long.class))).thenThrow(NotFoundException.class);

        this.mockMvc.perform(post("/api/v1/session/1/player/1").contentType(MediaType.APPLICATION_JSON).content("{ }"))
                .andDo(log())
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser("test@test.com")
    void removePlayer_should_return_ok() throws Exception {
        when(gameService.removePlayer(any(Long.class), any(Long.class))).thenReturn(createSessionDto());

        this.mockMvc.perform(delete("/api/v1/session/1/player/1").contentType(MediaType.APPLICATION_JSON).content("{ }"))
                .andDo(log())
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser("test@test.com")
    void removePlayer_should_return_ok_when_not_found() throws Exception {
        when(gameService.removePlayer(any(Long.class), any(Long.class))).thenThrow(NotFoundException.class);

        this.mockMvc.perform(delete("/api/v1/session/1/player/1").contentType(MediaType.APPLICATION_JSON).content("{ }"))
                .andDo(log())
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser("test@test.com")
    void getSessionByJoinToken_should_return_ok() throws Exception {
        when(gameService.getSessionByToken("joinTokenValue")).thenReturn(createSessionDto());

        this.mockMvc.perform(get("/api/v1/join?token=joinTokenValue").contentType(MediaType.APPLICATION_JSON))
                .andDo(log())
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser("test@test.com")
    void getSessionByJoinToken_should_return_not_found_when_not_found() throws Exception {
        when(gameService.getSessionByToken("joinTokenValue")).thenThrow(NotFoundException.class);

        this.mockMvc.perform(get("/api/v1/?token=joinTokenValue").contentType(MediaType.APPLICATION_JSON))
                .andDo(log())
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser("test@test.com")
    void getSessionById_should_return_ok() throws Exception {
        when(gameService.getSessionById(1L)).thenReturn(createSessionDto());

        this.mockMvc.perform(get("/api/v1/session/1").contentType(MediaType.APPLICATION_JSON))
                .andDo(log())
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser("test@test.com")
    void getSessionById_should_return_not_found_when_not_found() throws Exception {
        when(gameService.getSessionById(1L)).thenThrow(NotFoundException.class);

        this.mockMvc.perform(get("/api/v1/session/1").contentType(MediaType.APPLICATION_JSON))
                .andDo(log())
                .andExpect(status().isNotFound());
    }
}