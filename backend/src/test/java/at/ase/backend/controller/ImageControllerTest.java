package at.ase.backend.controller;

import at.ase.backend.AbstractSpringBootTest;
import at.ase.backend.exception.ImageStorageException;
import at.ase.backend.exception.NotFoundException;
import at.ase.backend.mapper.GameMapper;
import at.ase.backend.model.ImageScope;
import at.ase.backend.property.ImageStorageProperties;
import at.ase.backend.repository.ImageRepository;
import at.ase.backend.service.GameService;
import at.ase.backend.service.ImageStorageService;
import at.ase.backend.service.impl.ImageStorageServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.catchThrowable;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.log;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ImageControllerTest extends AbstractSpringBootTest {
    private static final Long ID = 0L;
    private static final Logger LOG = LoggerFactory.getLogger(ImageControllerTest.class);

    @MockBean
    ImageStorageService imageStorageService;

    @MockBean
    ImageRepository imageRepository;

    @MockBean
    GameService gameService;

    @Autowired
    GameMapper gameMapper;

    @Mock
    Resource resource;

    @Mock
    File file;

    @Mock
    ImageStorageProperties imageStorageProperties;

    @Test
    @WithMockUser("test@test.com")
    void uploadImage_should_return_data() throws Exception {
        when(imageStorageService.storeImage(any(), any(), any())).thenReturn("test.png");

        MockMultipartFile image = new MockMultipartFile("file", "src/test/resources/pizza.png",
                MediaType.MULTIPART_FORM_DATA_VALUE,
                Files.readAllBytes(Path.of("src/test/resources/pizza.png")));

        this.mockMvc.perform(MockMvcRequestBuilders.multipart("/api/v1/game/" + ID + "/image/CARD")
                        .file(image))
                .andDo(log())
                .andExpect(status().isOk())
                .andExpect(content().json("{" +
                        "\"gameId\":0," +
                        "\"imageName\":\"test.png\"," +
                        "\"imageType\":\"multipart/form-data\"," +
                        "\"size\":9690}"));
    }

    @Test
    @WithMockUser("test@test.com")
    void uploadImage_should_use_default_scope_if_invalid() throws Exception {
        when(imageStorageService.storeImage(any(), any(), eq(ImageScope.PRESENTATION)))
                .thenThrow(new ImageStorageException("works"));

        MockMultipartFile image = new MockMultipartFile("file", "src/test/resources/pizza.png",
                MediaType.MULTIPART_FORM_DATA_VALUE,
                Files.readAllBytes(Path.of("src/test/resources/pizza.png")));


        this.mockMvc.perform(MockMvcRequestBuilders.multipart(
                        "/api/v1/game/" + ID + "/image/INVALID")
                .file(image))
                .andDo(log())
                .andExpect(status().isInternalServerError())
                .andExpect(content().json("{ \"error\":  \"works\" }"));
    }

    @Test
    @WithMockUser("test@test.com")
    void multiupload_should_return_DTO_for_all_images() throws Exception {
        when(imageStorageService.storeImage(any(), any(), any())).thenReturn("test.png");


        MockMultipartFile firstImage = new MockMultipartFile("files", "pizza1.png", MediaType.MULTIPART_FORM_DATA_VALUE,
                Files.readAllBytes(
                        Path.of("src/test/resources/pizza.png")));

        MockMultipartFile secondImage = new MockMultipartFile("files", "pizza1.png",
                MediaType.MULTIPART_FORM_DATA_VALUE,
                Files.readAllBytes(
                        Path.of("src/test/resources/pizza.png")));

        this.mockMvc.perform(MockMvcRequestBuilders.multipart("/api/v1/game/" + ID + "/images/PRESENTATION")
                        .file(firstImage).file(secondImage))
                .andDo(log())
                .andExpect(status().isOk())
                .andExpect(content().json("[" +
                        "{" +
                        "\"gameId\":0," +
                        "\"imageName\":\"test.png\"," +
                        "\"imageType\":\"multipart/form-data\"," +
                        "\"size\":9690" +
                        "},{" +
                        "\"gameId\":0," +
                        "\"imageName\":\"test.png\"," +
                        "\"imageType\":\"multipart/form-data\"," +
                        "\"size\":9690" +
                        "}]"));
    }

    @Test
    @WithMockUser("test@test.com")
    void loadImage_should_return_existing_image() throws Exception {
        when(imageStorageService.loadImage(any(), any()))
                .thenReturn(new UrlResource(Path.of("src/test/resources/pizza.png").normalize().toUri()));

        this.mockMvc.perform(get("/api/v1/game/" + ID + "/image/pizza.png"))
                .andDo(log())
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser("test@test.com")
    void loadImage_should_return_404_for_non_existing_image() throws Exception {
        when(imageStorageService.loadImage(any(), any())).thenThrow(new NotFoundException());

        this.mockMvc.perform(get("/api/v1/game/" + ID + "/image/pizza.png"))
                .andDo(log())
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser("test@test.com")
    void loadImage_should_use_png_if_contenttype_cannot_be_determined() throws Exception {
        when(imageStorageService.loadImage(any(), any())).thenReturn(resource);
        when(resource.getFile()).thenReturn(file);
        when(resource.getFile().getAbsolutePath()).thenReturn("src/test/resources/pizza");
        when(resource.getInputStream()).thenReturn(new FileInputStream("src/test/resources/pizza.png"));

        this.mockMvc.perform(get("/api/v1/game/" + ID + "/image/pizza.png"))
                .andDo(log())
                .andExpect(content().contentType("image/png"));
    }

    @Test
    @WithMockUser("test@test.com")
    void deleteImage_should_return_200() throws Exception {
        doNothing().when(imageStorageService).deleteImage(any(), any());

        this.mockMvc.perform(delete("/api/v1/game/" + ID + "/image/pizza.png"))
                .andDo(log())
                .andExpect(status().isOk());
    }

}
