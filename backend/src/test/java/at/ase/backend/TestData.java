package at.ase.backend;

import at.ase.backend.dto.*;
import at.ase.backend.messaging.Message;
import at.ase.backend.model.*;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;

import java.time.Instant;
import java.util.*;

public abstract class TestData {
    protected GameDto createGameDto() {
        return GameDto
                .builder()
                .id(1L)
                .name("nameValue")
                .sourceCode("sourceCodeValue")
                .maxCapacity(2)
                .minCapacity(2)
                .description("descriptionValue")
                .build();
    }

    protected GameEntity createGameEntity(Long id, String name) {
        return GameEntity
                .builder()
                .id(id)
                .name(name)
                .versions(new HashSet<>())
                .maxCapacity(2)
                .minCapacity(2)
                .createdAt(Instant.now())
                .updatedAt(Instant.now())
                .creator(createPlayerEntity())
                .description("descriptionValue")
                .build();
    }

    protected Message createStartMessage() {
        return Message
                .builder()
                .key("keyId")
                .headers(Collections.emptyMap())
                .value(mapToJsonNode(Collections.emptyMap()))
                .build();
    }

    protected Message createPublishMoveMessage() {
        return Message
                .builder()
                .key("keyId")
                .headers(Collections.emptyMap())
                .value(mapToJsonNode(Map.of("type", "typeValue")))
                .build();
    }

    protected Message createWinMessage() {
        return Message
                .builder()
                .key("keyId")
                .headers(Collections.emptyMap())
                .value(mapToJsonNode(Map.of("type", "GameOver")))
                .build();
    }

    @SneakyThrows
    protected JsonNode mapToJsonNode(Object o) {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readTree(mapper.writeValueAsString(o));
    }

    protected SessionEntity createSessionEntity() {
        return SessionEntity
                .builder()
                .id(1L)
                .game(createGameEntity(1L, "nameValue"))
                .status(GameStatus.NEW)
                .slots(new HashSet<>())
                .createdAt(Instant.now())
                .updatedAt(Instant.now())
                .visibility(SessionVisibility.PRIVATE)
                .joinToken("joinTokenValue")
                .build();
    }

    protected SessionEntity createSessionEntity(SessionVisibility sessionVisibility) {
        return SessionEntity
                .builder()
                .id(1L)
                .game(createGameEntity(1L, "nameValue"))
                .status(GameStatus.NEW)
                .slots(new HashSet<>())
                .createdAt(Instant.now())
                .updatedAt(Instant.now())
                .visibility(sessionVisibility)
                .joinToken("joinTokenValue")
                .build();
    }

    protected SessionDto createSessionDto() {
        SlotDto slot0 = createSlotDto(0L);
        SlotDto slot1 = createSlotDto(1L);
        slot0.getPlayer().setId(0L);
        slot1.getPlayer().setId(1L);

        return SessionDto
                .builder()
                .id(1L)
                .game(createGameDto())
                .slots(List.of(slot0, slot1))
                .visibility(SessionVisibility.PRIVATE)
                .status(GameStatus.NEW)
                .build();
    }

    protected SlotDto createSlotDto(Long i) {
        return SlotDto
                .builder()
                .id(1L)
                .player(createPlayerDto())
                .slotNumber(i)
                .build();
    }

    protected SlotDto createSlotDto(Long i, Long k) {
        return SlotDto
                .builder()
                .id(1L)
                .player(createPlayerDto(k))
                .slotNumber(i)
                .build();
    }

    protected SlotEntity createSlotEntity(Long i) {
        return SlotEntity
                .builder()
                .id(1L)
                .player(createPlayerEntity())
                .slotNumber(i)
                .build();
    }

    protected PlayerDto createPlayerDto() {
        return PlayerDto
                .builder()
                .id(1L)
                .email("emailValue@email.com")
                .alias("aliasValue")
                .build();
    }

    protected PasswordChangeDto createPasswordChangeDto() {
        return PasswordChangeDto
                .builder()
                .newPassword("newPassword")
                .currentPassword("oldPassword")
                .build();
    }

    protected PlayerDto createPlayerDto(Long i) {
        return PlayerDto
                .builder()
                .id(i)
                .email("emailValue")
                .alias("aliasValue")
                .build();
    }

    protected PlayerEntity createPlayerEntity() {
        return PlayerEntity
                .builder()
                .id(1L)
                .email("emailValue")
                .loginAttempts(0)
                .password("passwordValue")
                .banned(false)
                .alias("aliasValue")
                .build();
    }

    @SneakyThrows
    protected Message createMessage() {
        com.fasterxml.jackson.databind.ObjectMapper mapper = new com.fasterxml.jackson.databind.ObjectMapper();
        return Message
                .builder()
                .key("sessionIdValue")
                .headers(Map.of(
                        "session_id", "sessionIdValue",
                        "destination_player_id", "playerIdValue"
                ))
                .value(mapper.readTree("{ \"test\": \"test\" }"))
                .build();

    }
}
