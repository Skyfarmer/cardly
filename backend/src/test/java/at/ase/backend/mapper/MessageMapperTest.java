package at.ase.backend.mapper;

import at.ase.backend.AbstractSpringBootTest;
import at.ase.backend.messaging.Message;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class MessageMapperTest extends AbstractSpringBootTest {
    @Autowired
    MessageMapper mapper;

    @Test
    void toStartGameMessage_should_create_message() {
        Message message = mapper.toStartGameMessage(createSessionDto());

        assertThat(message).isNotNull();
        assertThat(message.getKey()).isEqualTo("1");
        assertThat(message.getHeaders().get("session_id")).isEqualTo("1");
        assertThat(message.getValue().at("/id").toString()).isNotNull();
    }

    @Test
    void toUserMoveMessage_should_create_message() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        Message message = mapper.toUserMoveGameMessage("1", createSlotDto(1L), objectMapper.readTree(objectMapper.writeValueAsString(createGameDto())));

        assertThat(message).isNotNull();
        assertThat(message.getKey()).isEqualTo("1");
        assertThat(message.getHeaders().get("session_id")).isEqualTo("1");
        assertThat(message.getHeaders().get("source_player_id")).isEqualTo("1");
        assertThat(message.getValue().at("/id")).isNotNull();
    }

    @Test
    void toPlayerJoinsMessage_should_create_message() throws IOException {
        Message message = mapper.toPlayerJoinsGameMessage("1", createSlotDto(1L));

        assertThat(message).isNotNull();
        assertThat(message.getKey()).isEqualTo("1");
        assertThat(message.getHeaders().get("session_id")).isEqualTo("1");
        assertThat(message.getValue().at("/id")).isNotNull();
    }

    @Test
    void toPlayerLeavesMessage_should_create_message() throws IOException {
        Message message = mapper.toPlayerLeavesGameMessage("1", createSlotDto(1L));

        assertThat(message).isNotNull();
        assertThat(message.getKey()).isEqualTo("1");
        assertThat(message.getHeaders().get("session_id")).isEqualTo("1");
        assertThat(message.getValue().at("/id")).isNotNull();
        assertThat(message.getValue().at("/type").toString()).isEqualTo("\"PlayerLeave\"");
        assertThat(message.getValue().at("/id").toString()).isNotNull();
        assertThat(message.getValue().at("/email").toString()).isNotNull();
    }

    @Test
    void toPlayerLeavesGameMessage_should_create_message() {
        Message message = mapper.toPlayerLeavesGameMessage("sessionIdValue", createSlotDto(1L));

        assertThat(message).isNotNull();
        assertThat(message.getKey()).isEqualTo("sessionIdValue");
        assertThat(message.getHeaders().get("session_id")).isEqualTo("sessionIdValue");
        assertThat(message.getValue().at("/type").toString()).isEqualTo("\"PlayerLeave\"");
        assertThat(message.getValue().at("/id").toString()).isNotNull();
        assertThat(message.getValue().at("/email").toString()).isNotNull();
    }

    @Test
    void toGameStoppedMessage_should_create_message() {
        Message message = mapper.toGameStoppedMessage("sessionIdValue");

        assertThat(message).isNotNull();
        assertThat(message.getKey()).isEqualTo("sessionIdValue");
        assertThat(message.getHeaders().get("session_id")).isEqualTo("sessionIdValue");
        assertThat(message.getValue().at("/").toString()).isNotNull();
    }
}
