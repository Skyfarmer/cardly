package at.ase.backend.messaging;

import at.ase.backend.AbstractSpringBootTest;
import at.ase.backend.socket.FrontendCommunicationHandler;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.core.Queue;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.io.IOException;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.catchThrowable;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class ResponseConsumerTest extends AbstractSpringBootTest {
    @Mock
    FrontendCommunicationHandler frontendCommunicationHandler;

    ResponseConsumer responseConsumer;

    @BeforeEach
    void beforeEach() {
        responseConsumer = new ResponseConsumer(frontendCommunicationHandler);
    }

    @Test
    void received_message_should_relay_message() throws IOException {
        // Given + Mocks
        Message message = createMessage();
        doNothing().when(frontendCommunicationHandler).sendRequest(any());

        // When
        responseConsumer.receivedMessage(message);

        // Then
        ArgumentCaptor<Message> argumentCaptor = ArgumentCaptor.forClass(Message.class);
        verify(frontendCommunicationHandler, times(1)).sendRequest(argumentCaptor.capture());
        assertThat(argumentCaptor.getValue()).isNotNull();
        assertThat(argumentCaptor.getValue().getKey()).isEqualTo("sessionIdValue");
        assertThat(argumentCaptor.getValue().getHeaders().get("session_id")).isEqualTo("sessionIdValue");
        assertThat(argumentCaptor.getValue().getHeaders().get("destination_player_id")).isEqualTo("playerIdValue");
        assertThat(argumentCaptor.getValue().getValue()).isNotNull();
    }

    @Test
    void received_message_should_throw_exception() throws IOException {
        // Given + Mocks
        Message message = createMessage();
        doThrow(RuntimeException.class).when(frontendCommunicationHandler).sendRequest(any());

        // When
        final Throwable throwable = catchThrowable(() -> {
            responseConsumer.receivedMessage(message);
        });

        // Then
        assertThat(throwable).isExactlyInstanceOf(AmqpRejectAndDontRequeueException.class);
    }
}
