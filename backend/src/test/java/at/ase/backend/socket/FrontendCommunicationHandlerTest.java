package at.ase.backend.socket;

import at.ase.backend.AbstractSpringBootTest;
import at.ase.backend.messaging.Message;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.*;

public class FrontendCommunicationHandlerTest extends AbstractSpringBootTest {
    @Autowired
    ObjectMapper objectMapper;

    FrontendCommunicationHandler frontendCommunicationHandler;

    private static final String SESSION_ID = "session_id";
    private static final String PLAYER_ID = "player_id";

    @BeforeEach
    void beforeEach() {
        this.frontendCommunicationHandler = new FrontendCommunicationHandler(objectMapper);
    }

    @Test
    void add_connection_should_add_connection() {
        // Given + Mock
        Map<String, Object> attributes = Map.of(
                SESSION_ID, "sessionIdValue",
                PLAYER_ID, "playerIdValue"
        );
        WebSocketSession session = mock(WebSocketSession.class);
        when(session.getAttributes()).thenReturn(attributes);

        // When
        frontendCommunicationHandler.addConnection(session, "0");

        // Then
        ConcurrentHashMap<String, ConcurrentHashMap<String, WebSocketSession>> sessions = frontendCommunicationHandler.getSessions();
        assertThat(sessions).isNotNull();
        assertThat(sessions.size()).isEqualTo(1);
        assertThat(sessions.get("sessionIdValue")).isNotNull();
        assertThat(sessions.get("sessionIdValue").get("0")).isNotNull();
    }

    @Test
    void add_connection_should_add_connection_when_session_already_exists() {
        // Given + Mock
        Map<String, Object> attributes = Map.of(
                SESSION_ID, "sessionIdValue",
                PLAYER_ID, "playerIdValue"
        );
        WebSocketSession session = mock(WebSocketSession.class);
        when(session.getAttributes()).thenReturn(attributes);

        // When
        frontendCommunicationHandler.getSessions().put("sessionIdValue", new ConcurrentHashMap<>());
        frontendCommunicationHandler.addConnection(session, "0");

        // Then
        ConcurrentHashMap<String, ConcurrentHashMap<String, WebSocketSession>> sessions = frontendCommunicationHandler.getSessions();
        assertThat(sessions).isNotNull();
        assertThat(sessions.size()).isEqualTo(1);
        assertThat(sessions.get("sessionIdValue")).isNotNull();
        assertThat(sessions.get("sessionIdValue").get("0")).isNotNull();
    }

    @Test
    void remove_connection_should_remove_connection() {
        // Given + Mock
        Map<String, Object> attributes = Map.of(
                SESSION_ID, "sessionIdValue",
                PLAYER_ID, "playerIdValue"
        );
        WebSocketSession session = mock(WebSocketSession.class);
        when(session.getAttributes()).thenReturn(attributes);

        ConcurrentHashMap<String, WebSocketSession> inner = new ConcurrentHashMap<>();
        inner.put("0", session);
        frontendCommunicationHandler.getSessions().put("sessionIdValue", inner);

        // When
        frontendCommunicationHandler.removeConnection(session, "0");

        // Then
        ConcurrentHashMap<String, ConcurrentHashMap<String, WebSocketSession>> sessions = frontendCommunicationHandler.getSessions();
        assertThat(sessions).isNotNull();
        assertThat(sessions.size()).isEqualTo(0);
    }

    @Test
    void remove_connection_should_remove_connection_and_session_still_exists_when_other_player_exists() {
        // Given + Mock
        Map<String, Object> attributes = Map.of(
                SESSION_ID, "sessionIdValue",
                PLAYER_ID, "playerIdValue"
        );
        WebSocketSession session = mock(WebSocketSession.class);
        when(session.getAttributes()).thenReturn(attributes);

        ConcurrentHashMap<String, WebSocketSession> inner = new ConcurrentHashMap<>();
        inner.put("0", session);
        inner.put("1", mock(WebSocketSession.class));
        frontendCommunicationHandler.getSessions().put("sessionIdValue", inner);

        // When
        frontendCommunicationHandler.removeConnection(session, "0");

        // Then
        ConcurrentHashMap<String, ConcurrentHashMap<String, WebSocketSession>> sessions = frontendCommunicationHandler.getSessions();
        assertThat(sessions).isNotNull();
        assertThat(sessions.size()).isEqualTo(1);
        assertThat(sessions.get("sessionIdValue")).isNotNull();
        assertThat(sessions.get("sessionIdValue").get("0")).isNull();
        assertThat(sessions.get("sessionIdValue").get("1")).isNotNull();
    }

    @Test
    void remove_connection_should_do_nothing() {
        // Given + Mock
        Map<String, Object> attributes = Map.of(
                SESSION_ID, "sessionIdValue",
                PLAYER_ID, "playerIdValue"
        );
        WebSocketSession session = mock(WebSocketSession.class);
        when(session.getAttributes()).thenReturn(attributes);

        // When
        frontendCommunicationHandler.removeConnection(session, "0");

        // Then
        ConcurrentHashMap<String, ConcurrentHashMap<String, WebSocketSession>> sessions = frontendCommunicationHandler.getSessions();
        assertThat(sessions).isNotNull();
        assertThat(sessions.size()).isEqualTo(0);
    }

    @Test
    void send_request_should_send_request() throws IOException {
        // Given + Mocks
        Message message = createMessage();

        WebSocketSession session = mock(WebSocketSession.class);
        when(session.getAttributes()).thenReturn(Map.of(
                SESSION_ID, "sessionIdValue",
                "destination_player_id", "playerIdValue"
        ));
        doNothing().when(session).sendMessage(any());
        ConcurrentHashMap<String, WebSocketSession> inner = new ConcurrentHashMap<>();
        inner.put("playerIdValue", session);
        frontendCommunicationHandler.getSessions().put("sessionIdValue", inner);

        // When
        frontendCommunicationHandler.sendRequest(message);

        // Then
        ArgumentCaptor<TextMessage> argumentCaptor = ArgumentCaptor.forClass(TextMessage.class);
        verify(session, times(1)).sendMessage(argumentCaptor.capture());
        assertThat(argumentCaptor.getValue().getPayload()).isEqualTo("{\"test\":\"test\"}");
    }

    @Test
    void send_request_should_broadcast() throws Exception {
        // Given + Mocks
        Message message = createMessage();
        message.setHeaders(Map.of(
                "session_id", "sessionIdValue"
        ));

        WebSocketSession session = mock(WebSocketSession.class);
        WebSocketSession session2 = mock(WebSocketSession.class);
        when(session.getAttributes()).thenReturn(Map.of(
                SESSION_ID, "sessionIdValue"
        ));
        doNothing().when(session).sendMessage(any());
        doNothing().when(session2).sendMessage(any());
        ConcurrentHashMap<String, WebSocketSession> inner = new ConcurrentHashMap<>();
        inner.put("playerIdValue", session);
        inner.put("playerIdValue2", session2);
        frontendCommunicationHandler.getSessions().put("sessionIdValue", inner);

        // When
        frontendCommunicationHandler.sendRequest(message);

        // Then
        ArgumentCaptor<TextMessage> argumentCaptor = ArgumentCaptor.forClass(TextMessage.class);
        ArgumentCaptor<TextMessage> argumentCaptor2 = ArgumentCaptor.forClass(TextMessage.class);
        verify(session, times(1)).sendMessage(argumentCaptor.capture());
        verify(session2, times(1)).sendMessage(argumentCaptor2.capture());
        assertThat(argumentCaptor.getValue().getPayload()).isEqualTo("{\"test\":\"test\"}");
        assertThat(argumentCaptor2.getValue().getPayload()).isEqualTo("{\"test\":\"test\"}");
    }
}
