package at.ase.backend.socket;

import at.ase.backend.AbstractSpringBootTest;
import at.ase.backend.dto.SessionDto;
import at.ase.backend.exception.*;
import at.ase.backend.mapper.MessageMapper;
import at.ase.backend.messaging.Message;
import at.ase.backend.model.GameStatus;
import at.ase.backend.service.GameService;
import at.ase.backend.service.GameStatePusherService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.concurrent.ConcurrentException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.autoconfigure.metrics.MetricsProperties;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class SocketEndpointTest extends AbstractSpringBootTest {

    @Autowired
    ObjectMapper objectMapper;

    @Mock
    GameStatePusherService gameStatePusherService;

    @Mock
    FrontendCommunicationHandler frontendCommunicationHandler;

    @Mock
    GameService gameService;

    @Autowired
    MessageMapper messageMapper;

    SocketEndpoint socketEndpoint;

    @BeforeEach
    void beforeEach() {
        socketEndpoint = new SocketEndpoint(objectMapper, gameStatePusherService, frontendCommunicationHandler, gameService, messageMapper);
    }

    private ConcurrentHashMap<String, ConcurrentHashMap<String, WebSocketSession>> setupSessions() {
        WebSocketSession session = mock(WebSocketSession.class);
        when(session.getId()).thenReturn("idValue1");
        WebSocketSession otherSession = mock(WebSocketSession.class);
        when(otherSession.getId()).thenReturn("idValue2");
        when(session.getAttributes()).thenReturn(Map.of("session_id", "1", "player_id", "1"));
        when(otherSession.getAttributes()).thenReturn(Map.of("session_id", "1", "player_id", "0"));
        ConcurrentHashMap<String, ConcurrentHashMap<String, WebSocketSession>> sessions = new ConcurrentHashMap<>();
        ConcurrentHashMap<String, WebSocketSession> sessionHashmap = new ConcurrentHashMap<>();
        sessionHashmap.put("0", otherSession);
        sessionHashmap.put("1", session);
        sessions.put("1", sessionHashmap);

        return sessions;
    }

    @Test
    void after_connection_established_should_add_connection() throws Exception {
        doNothing().when(frontendCommunicationHandler).addConnection(any(), eq("0"));
        ConcurrentHashMap<String, WebSocketSession> innerHash = new ConcurrentHashMap<>();
        ConcurrentHashMap<String, ConcurrentHashMap<String, WebSocketSession>> sessions = new ConcurrentHashMap<>();
        sessions.put("1", innerHash);
        when(frontendCommunicationHandler.getSessions()).thenReturn(sessions);
        when(gameService.getSessionById(1L)).thenReturn(createSessionDto());
        WebSocketSession session = mock(WebSocketSession.class);
        when(session.getAttributes()).thenReturn(Map.of("session_id", "1", "player_id", "0"));

        socketEndpoint.afterConnectionEstablished(session);

        verify(frontendCommunicationHandler, times(1)).addConnection(any(WebSocketSession.class), eq("0"));
        verify(frontendCommunicationHandler, times(1)).sendRequest(any(Message.class));
    }

    @Test
    void after_connection_closed_should_remove_connection() throws Exception {
        doNothing().when(frontendCommunicationHandler).removeConnection(any(), eq("1"));
        ConcurrentHashMap<String, ConcurrentHashMap<String, WebSocketSession>> sessions = setupSessions();
        WebSocketSession session = sessions.get("1").get("1");

        when(frontendCommunicationHandler.getSessions()).thenReturn(sessions);
        when(gameService.getSessionById(1L)).thenReturn(createSessionDto());
        doNothing().when(frontendCommunicationHandler).sendRequest(any());

        socketEndpoint.afterConnectionClosed(session, mock(CloseStatus.class));

        verify(frontendCommunicationHandler, times(1)).removeConnection(any(WebSocketSession.class), eq("1"));
        verify(frontendCommunicationHandler, times(1)).sendRequest(any(Message.class));
    }

    @Test
    void after_connection_closed_should_remove_slot() throws Exception {
        doNothing().when(frontendCommunicationHandler).addConnection(any(), eq("0"));
        ConcurrentHashMap<String, ConcurrentHashMap<String, WebSocketSession>> sessions = setupSessions();
        when(frontendCommunicationHandler.getSessions()).thenReturn(sessions);
        when(gameService.getSessionById(1L)).thenReturn(createSessionDto());
        WebSocketSession session = sessions.get("1").get("0");
        doNothing().when(frontendCommunicationHandler).removeConnection(any(), eq("0"));
        when(gameService.removePlayer(1L, 1L)).thenReturn(createSessionDto());

        socketEndpoint.afterConnectionClosed(session, mock(CloseStatus.class));

        verify(frontendCommunicationHandler, times(1)).removeConnection(any(WebSocketSession.class), eq("0"));
        verify(gameService, times(1)).removePlayer(1L, 0L);
    }

    @Test
    void after_connection_closed_should_remove_slot_when_first_is_null() throws Exception {
        doNothing().when(frontendCommunicationHandler).addConnection(any(), eq("0"));
        ConcurrentHashMap<String, ConcurrentHashMap<String, WebSocketSession>> sessions = setupSessions();
        when(frontendCommunicationHandler.getSessions()).thenReturn(sessions);

        SessionDto sessionDto = createSessionDto();
        sessionDto.getSlots().get(0).setPlayer(null);

        when(gameService.getSessionById(1L)).thenReturn(sessionDto);
        WebSocketSession session = sessions.get("1").get("1");
        doNothing().when(frontendCommunicationHandler).removeConnection(any(), eq("0"));
        when(gameService.removePlayer(1L, 1L)).thenReturn(createSessionDto());

        socketEndpoint.afterConnectionClosed(session, mock(CloseStatus.class));

        verify(frontendCommunicationHandler, times(1)).removeConnection(any(WebSocketSession.class), eq("1"));
        verify(gameService, times(1)).removePlayer(1L, 1L);
    }

    @Test
    void after_connection_closed_should_shutdown_game_when_session_started() throws Exception, KubernetesException {
        doNothing().when(frontendCommunicationHandler).addConnection(any(), eq("0"));
        ConcurrentHashMap<String, ConcurrentHashMap<String, WebSocketSession>> sessions = setupSessions();
        when(frontendCommunicationHandler.getSessions()).thenReturn(sessions);

        SessionDto sessionDto = createSessionDto();
        sessionDto.setStatus(GameStatus.STARTED);

        when(gameService.getSessionById(1L)).thenReturn(sessionDto);
        WebSocketSession session = sessions.get("1").get("0");
        doNothing().when(frontendCommunicationHandler).removeConnection(any(), eq("0"));
        when(gameService.removePlayer(1L, 1L)).thenReturn(createSessionDto());

        socketEndpoint.afterConnectionClosed(session, mock(CloseStatus.class));

        verify(frontendCommunicationHandler, times(1)).removeConnection(any(WebSocketSession.class), eq("0"));
        verify(gameService, times(1)).removePlayer(1L, 0L);
        verify(gameStatePusherService, times(1)).shutdownSession(1L);
    }

    @Test
    void handle_text_message_should_call_service() throws Exception {
        // Given + Mocks
        doNothing().when(gameStatePusherService).publishMove(eq("1"), eq("1"), any());
        WebSocketSession session = mock(WebSocketSession.class);
        TextMessage message = mock(TextMessage.class);
        when(session.getAttributes()).thenReturn(Map.of("session_id", "sessionIdValue", "player_id", "1"));
        when(message.getPayload()).thenReturn("{ \"test\": \"test\" }");

        // When
        socketEndpoint.handleTextMessage(session, message);

        // Then
        ArgumentCaptor<JsonNode> argumentCaptor = ArgumentCaptor.forClass(JsonNode.class);
        verify(gameStatePusherService, times(1)).publishMove(eq("sessionIdValue"), eq("1"), argumentCaptor.capture());
        assertThat(argumentCaptor.getValue().get("test").asText()).isEqualTo("test");
        verifyNoMoreInteractions(gameStatePusherService);
        verifyNoInteractions(frontendCommunicationHandler);
    }

    @Test
    void handle_text_message_should_call_service_when_quoted() throws Exception {
        // Given + Mocks
        doNothing().when(gameStatePusherService).publishMove(eq("1"), eq("1"), any());
        WebSocketSession session = mock(WebSocketSession.class);
        TextMessage message = mock(TextMessage.class);
        when(session.getAttributes()).thenReturn(Map.of("session_id", "sessionIdValue", "player_id", "1"));
        when(message.getPayload()).thenReturn("\"{ \"test\": \"test\" }\"");

        // When
        socketEndpoint.handleTextMessage(session, message);

        // Then
        ArgumentCaptor<JsonNode> argumentCaptor = ArgumentCaptor.forClass(JsonNode.class);
        verify(gameStatePusherService, times(1)).publishMove(eq("sessionIdValue"), eq("1"), argumentCaptor.capture());
        assertThat(argumentCaptor.getValue().get("test").asText()).isEqualTo("test");
        verifyNoMoreInteractions(gameStatePusherService);
        verifyNoInteractions(frontendCommunicationHandler);
    }
}
