package at.ase.backend.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@AllArgsConstructor
@Data
@Builder
public class ImageResponseDto {
    private long gameId;
    private String imageName;
    private String imageType;
    private long size;
}
