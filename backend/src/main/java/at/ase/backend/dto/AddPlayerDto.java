package at.ase.backend.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AddPlayerDto {
    private Long playerIndex;
}
