package at.ase.backend.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GameDto {
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Long id;

    @NotBlank
    private String name;

    @JsonProperty("source_code")
    @NotBlank
    private String sourceCode;

    @NotNull
    @JsonProperty("min_capacity")
    private Integer minCapacity;

    @NotNull
    @JsonProperty("max_capacity")
    private Integer maxCapacity;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Set<ImageDto> images;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private PlayerDto creator;

    @NotNull
    private String description;
}
