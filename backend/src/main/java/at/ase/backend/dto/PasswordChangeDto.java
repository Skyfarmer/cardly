package at.ase.backend.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PasswordChangeDto {
    @NotBlank
    @NotNull
    private String currentPassword;

    @NotBlank
    @NotNull
    private String newPassword;
}
