package at.ase.backend.dto;

import at.ase.backend.model.GameStatus;
import at.ase.backend.model.SessionVisibility;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SessionDto {
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Long id;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private GameStatus status;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private List<SlotDto> slots;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private GameDto game;

    @NotNull
    @NonNull
    private SessionVisibility visibility;

    @JsonProperty(value = "join_token", access = JsonProperty.Access.READ_ONLY)
    private String joinToken;
}
