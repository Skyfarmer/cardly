package at.ase.backend.dto.messaging;

import at.ase.backend.dto.PlayerDto;
import at.ase.backend.dto.SlotDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PlayerLeaveDto {
    private final String type = "PlayerLeave";
    private SlotDto slotDto;
}
