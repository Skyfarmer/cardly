package at.ase.backend.dto.messaging;

import at.ase.backend.dto.SlotDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PlayerJoinDto {
    private final String type = "PlayerJoin";
    private SlotDto slotDto;
}
