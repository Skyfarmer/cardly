package at.ase.backend.dto.messaging;

import at.ase.backend.dto.SlotDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
public class PlayerAlreadyJoinedDto {
    private final String type = "PlayerAlreadyJoined";
}
