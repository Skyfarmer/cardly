package at.ase.backend.dto;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PlayerDto {
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Long id;

    @Email
    @NotNull
    private String email;

    @NotNull
    private String alias;
}
