package at.ase.backend.socket;

import at.ase.backend.dto.SessionDto;
import at.ase.backend.dto.SlotDto;
import at.ase.backend.exception.KubernetesException;
import at.ase.backend.exception.MessagingException;
import at.ase.backend.exception.NotFoundException;
import at.ase.backend.exception.PersistenceException;
import at.ase.backend.mapper.MessageMapper;
import at.ase.backend.messaging.Message;
import at.ase.backend.model.GameStatus;
import at.ase.backend.service.GameService;
import at.ase.backend.service.GameStatePusherService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.text.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.lang.Nullable;
import org.springframework.util.Assert;
import org.springframework.web.socket.*;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.IOException;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class SocketEndpoint extends TextWebSocketHandler {
    private final Logger logger = LoggerFactory.getLogger(SocketEndpoint.class);

    private final ObjectMapper objectMapper;
    private final GameStatePusherService gameStatePusherService;
    private final GameService gameService;
    private final FrontendCommunicationHandler frontendCommunicationHandler;
    private final MessageMapper messageMapper;

    public SocketEndpoint(ObjectMapper objectMapper,
                          GameStatePusherService gameStatePusherService,
                          FrontendCommunicationHandler frontendCommunicationHandler,
                          GameService gameService,
                          MessageMapper messageMapper) {
        Assert.notNull(objectMapper, "objectMapper must not be null");
        Assert.notNull(gameStatePusherService, "gameStatePusherService must not be null");
        Assert.notNull(frontendCommunicationHandler, "frontendCommunicationHandler must not be null");
        Assert.notNull(gameService, "gameService must not be null");
        Assert.notNull(messageMapper, "messageMapper must not be null");

        this.objectMapper = objectMapper;
        this.gameStatePusherService = gameStatePusherService;
        this.frontendCommunicationHandler = frontendCommunicationHandler;
        this.gameService = gameService;
        this.messageMapper = messageMapper;
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        String sessionId = getSessionId(session);
        String userJoinedId = getPlayerId(session);

        SessionDto sessionDto = gameService.getSessionById(Long.parseLong(sessionId));
        SlotDto slot = sessionDto
                .getSlots()
                .stream()
                .filter(x -> x.getPlayer() != null)
                .filter(x -> x.getPlayer().getId().toString().equals(userJoinedId))
                .findFirst()
                .get();
        String userJoinSlotIndex = slot.getSlotNumber().toString();

        if (frontendCommunicationHandler.getSessions().containsKey(sessionId) && frontendCommunicationHandler.getSessions().get(sessionId).containsKey(userJoinSlotIndex)) {
            Message message = messageMapper.toPlayerAlreadyJoined(sessionId);
            session.sendMessage(new TextMessage(objectMapper.writeValueAsString(message.getValue())));
            session.close();
            throw new Exception("Cannot connect with websocket because player has already a connection");
        }

        logger.info("Connection established for session '{}' for player id '{}'", sessionDto.getId(), userJoinedId);

        frontendCommunicationHandler.addConnection(session, userJoinSlotIndex);
        frontendCommunicationHandler.sendRequest(messageMapper.toPlayerJoinsGameMessage(sessionId, slot));

        super.afterConnectionEstablished(session);
    }

    @Override
    public void handleTextMessage(WebSocketSession session, TextMessage message) throws
            Exception {
        String sessionId = getSessionId(session);
        String playerId = getPlayerId(session);

        logger.info("Message received for session '{}' and player id '{}'", sessionId, playerId);

        String json = message.getPayload();
        logger.debug("Received json: " + json);

        if (json.startsWith("\"") && json.endsWith("\"")) {
            json = json.substring(1, json.length() - 1);
        }
        json = StringEscapeUtils.unescapeJson(json);
        logger.debug("Updated json: " + json);

        JsonNode node = objectMapper.readTree(json);

        gameStatePusherService.publishMove(sessionId, playerId, node);
    }

    private String getPlayerId(WebSocketSession session) {
        return (String) session.getAttributes().get("player_id");
    }

    private String getSessionId(WebSocketSession session) {
        return (String) session.getAttributes().get("session_id");
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        String sessionId = getSessionId(session);
        String userLeftId = getPlayerId(session);
        SessionDto sessionDto = gameService.getSessionById(Long.parseLong(sessionId));
        gameService.removePlayer(Long.parseLong(sessionId), Long.parseLong(userLeftId));

        SlotDto slot = sessionDto
                .getSlots()
                .stream()
                .filter(x -> x.getPlayer() != null)
                .filter(x -> x.getPlayer().getId().toString().equals(userLeftId))
                .findFirst()
                .get();
        logger.info("Connection dropped for session '{}' for player id '{}'", sessionDto.getId(), userLeftId);

        frontendCommunicationHandler.sendRequest(messageMapper.toPlayerLeavesGameMessage(sessionId, slot));

        if (sessionDto.getStatus().equals(GameStatus.STARTED)) {
            logger.info("Shutting down session with id '{}' because player with id '{}' left the game", sessionId, userLeftId);
            try {
                gameStatePusherService.shutdownSession(Long.valueOf(sessionId));
                frontendCommunicationHandler.sendRequest(messageMapper.toStoppedGameMessageForFrontend(sessionId));
            } catch (KubernetesException e) {
                logger.error("Cannot shutdown game", e);
            }
        }

        String userLeftSlotIndex = sessionDto
                .getSlots()
                .stream()
                .filter(x -> x.getPlayer() != null)
                .filter(x -> x.getPlayer().getId().toString().equals(userLeftId))
                .map(SlotDto::getSlotNumber)
                .findFirst()
                .get()
                .toString();

        frontendCommunicationHandler.removeConnection(session, userLeftSlotIndex);
        super.afterConnectionClosed(session, status);
    }
}
