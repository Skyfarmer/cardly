package at.ase.backend.socket;

import at.ase.backend.messaging.Message;
import at.ase.backend.service.GameStatePusherService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Main data structure for handling the communication between the execution engine and
 * frontend. The task of this class is to route messages from the websockets to the {@link GameStatePusherService}.
 */
@Component
public class FrontendCommunicationHandler {
    private static final String SESSION_ID = "session_id";
    private static final String PLAYER_ID = "player_id";
    private static FrontendCommunicationHandler instance;
    private final Logger logger = LoggerFactory.getLogger(FrontendCommunicationHandler.class);
    private final ObjectMapper objectMapper;

    // Key 1 = Session Id
    // Key 2 = Player Id
    private ConcurrentHashMap<String, ConcurrentHashMap<String, WebSocketSession>> sessions = new ConcurrentHashMap<>();

    public FrontendCommunicationHandler(ObjectMapper objectMapper) {
        Assert.notNull(objectMapper, "objectMapper must not be null");
        this.objectMapper = objectMapper;
    }

    /**
     * Get the sessions.
     */
    public ConcurrentHashMap<String, ConcurrentHashMap<String, WebSocketSession>> getSessions() {
        return sessions;
    }

    /**
     * Track the session for a player.
     *
     * @param session     must not be {@code null}.
     * @param playerIndex must not be {@code null}.
     */
    public void addConnection(WebSocketSession session, String playerIndex) {
        Assert.notNull(session, "session must not be null");
        Assert.notNull(playerIndex, "playerIndex must not be null");

        String sessionId = (String) session.getAttributes().get(SESSION_ID);
        String playerId = (String) session.getAttributes().get(PLAYER_ID);

        logger.info("Adding connection for session '{}' and player with the id '{}'", sessionId, playerId);

        Assert.hasText(sessionId, "sessionId must have text");
        Assert.hasText(playerId, "playerId must have text");

        logger.info("Adding connection for session " + sessionId + " with player " + playerId + " with player index " + playerIndex);

        if (sessions.containsKey(sessionId)) {
            ConcurrentHashMap<String, WebSocketSession> submap = sessions.get(sessionId);
            submap.put(playerIndex, session);
        } else {
            ConcurrentHashMap<String, WebSocketSession> submap = new ConcurrentHashMap<>();
            submap.put(playerIndex, session);
            sessions.put(sessionId, submap);
        }
    }

    /**
     * Removes a session for a player. This should be done when the websocket will be closed.
     *
     * @param session     must not be {@code null}.
     * @param playerIndex must not be {@code null}.
     */
    public void removeConnection(WebSocketSession session, String playerIndex) {
        Assert.notNull(session, "session must not be null");
        Assert.notNull(playerIndex, "playerIndex must not be null");

        String sessionId = (String) session.getAttributes().get(SESSION_ID);
        String playerId = (String) session.getAttributes().get(PLAYER_ID);

        logger.info("Removing connection for session '{}' and player with the id '{}'", sessionId, playerId);

        Assert.hasText(sessionId, "sessionId must have text");
        Assert.hasText(playerId, "playerId must have text");

        if (sessions.containsKey(sessionId)) {
            ConcurrentHashMap<String, WebSocketSession> subMap = sessions.get(sessionId);

            if (subMap.containsKey(playerIndex)) {
                subMap.remove(playerIndex);

                if (subMap.isEmpty()) {
                    sessions.remove(sessionId);
                }
            }
        }
    }

    /**
     * Send a request from the execution engine to the player over the websocket.
     *
     * @param message must not be {@code null}.
     */
    public void sendRequest(Message message) throws IOException {
        Assert.notNull(message, "message must not be null");

        String sessionId = message.getHeaders().get("session_id");
        String playerIndex = message.getHeaders().get("destination_player_id");
        Assert.hasText(sessionId, "sessionId must have text");

        if (playerIndex != null) {
            logger.info("Sending message to session '{}' for player index '{}': {}", sessionId, playerIndex, message.getValue());

            this.sessions.get(sessionId).get(playerIndex).sendMessage(new TextMessage(objectMapper.writeValueAsString(message.getValue())));
        } else {
            logger.info("Broadcasting message to session '{}'", sessionId);

            if (this.getSessions().get(sessionId) != null) {
                for (WebSocketSession session : this.sessions.get(sessionId).values()) {
                    try {
                        session.sendMessage(new TextMessage(objectMapper.writeValueAsString(message.getValue())));
                    } catch (IllegalStateException e) {
                        logger.debug("Will not send message to closed socket. Therefore skipping");
                    }
                }
            }
        }
    }
}
