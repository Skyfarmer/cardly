package at.ase.backend.socket.config;

import at.ase.backend.mapper.MessageMapper;
import at.ase.backend.service.GameService;
import at.ase.backend.service.GameStatePusherService;
import at.ase.backend.socket.FrontendCommunicationHandler;
import at.ase.backend.socket.SocketEndpoint;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.util.Assert;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.client.standard.WebSocketContainerFactoryBean;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;
import org.springframework.web.socket.server.HandshakeInterceptor;

import java.util.Map;

@Configuration
@EnableWebSocket
public class WebSocketServerConfiguration implements WebSocketConfigurer {

    private final ObjectMapper objectMapper;
    private final GameStatePusherService gameStatePusherService;
    private final GameService gameService;
    private final FrontendCommunicationHandler frontendCommunicationHandler;
    private final MessageMapper messageMapper;

    public WebSocketServerConfiguration(@Qualifier("wsObjectMapper") ObjectMapper objectMapper,
                                        GameStatePusherService gameStatePusherService,
                                        FrontendCommunicationHandler frontendCommunicationHandler,
                                        GameService gameService,
                                        MessageMapper messageMapper) {
        Assert.notNull(objectMapper, "objectMapper must not be null");
        Assert.notNull(gameStatePusherService, "gameStatePusherService must not be null");
        Assert.notNull(frontendCommunicationHandler, "frontendCommunicationHandler must not be null");
        Assert.notNull(gameService, "gameService must not be null");
        Assert.notNull(messageMapper, "messageMapper must not be null");

        this.objectMapper = objectMapper;
        this.gameStatePusherService = gameStatePusherService;
        this.frontendCommunicationHandler = frontendCommunicationHandler;
        this.gameService = gameService;
        this.messageMapper = messageMapper;
    }

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry.addHandler(new SocketEndpoint(objectMapper, gameStatePusherService, frontendCommunicationHandler, gameService, messageMapper), "/api/session/{sessionId}/player/{playerId}")
                .addInterceptors(sessionIntercepter())
                .setAllowedOrigins("*");
    }

    @Bean
    public WebSocketContainerFactoryBean createWebSocketContainer() {
        WebSocketContainerFactoryBean container = new WebSocketContainerFactoryBean();
        container.setMaxSessionIdleTimeout(1000 * 86400);
        return container;
    }

    @Bean
    public HandshakeInterceptor sessionIntercepter() {
        return new HandshakeInterceptor() {
            @Override
            public boolean beforeHandshake(org.springframework.http.server.ServerHttpRequest request, org.springframework.http.server.ServerHttpResponse response, WebSocketHandler wsHandler, Map<String, Object> attributes) throws Exception {
                String path = request.getURI().getPath();
                String[] parts = path.split("/");
                String sessionId = parts[3];
                String playerId = parts[5];

                attributes.put("session_id", sessionId);
                attributes.put("player_id", playerId);
                return true;
            }

            @Override
            public void afterHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler wsHandler, Exception exception) {

            }

        };
    }
}
