package at.ase.backend.exception;

/**
 * This exception will be thrown if something went wrong during the sending messages in Rabbitmq.
 */
public class MessagingException extends Exception {
}
