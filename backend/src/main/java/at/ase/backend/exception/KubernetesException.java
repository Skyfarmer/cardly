package at.ase.backend.exception;

public class KubernetesException extends Exception {
    public KubernetesException(String responseBody) {
        super(responseBody);
    }
}
