package at.ase.backend.exception;

/**
 * This exception will be thrown if something went wrong at the persistence layer.
 */
public class PersistenceException extends Exception{
}
