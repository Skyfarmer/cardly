package at.ase.backend.exception;

/**
 * This exception will be thrown if something went wrong during the setup of Rabbitmq.
 */
public class MessagingSetupException extends Exception {
}
