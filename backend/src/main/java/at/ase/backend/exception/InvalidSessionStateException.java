package at.ase.backend.exception;

import at.ase.backend.service.impl.GameServiceImpl;

/**
 * This exception is thrown if something goes wrong in the {@link GameServiceImpl}.
 */
public class InvalidSessionStateException extends RuntimeException {
    public InvalidSessionStateException(String message) {
        super(message);
    }
}

