package at.ase.backend.exception;

/**
 * This exception will be thrown if the user provided an invalid value for listing elements.
 */
public class InvalidListingConfigurationException extends Exception {
    public InvalidListingConfigurationException(String parameter) {
        super(String.format("Parameter '%s' is invalid.", parameter));
    }
}
