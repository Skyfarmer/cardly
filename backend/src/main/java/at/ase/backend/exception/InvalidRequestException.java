package at.ase.backend.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * The exception will be thrown if the request was invalid.
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidRequestException extends Exception {
    public InvalidRequestException(String message) {
        super(message);
    }

}
