package at.ase.backend.exception;

/**
 * This exception is thrown if something goes wrong for storing, loading or deleting images
 */
public class ImageStorageException extends RuntimeException {
    public ImageStorageException(String message) {
        super(message);
    }

    public ImageStorageException(String message, Throwable cause) {
        super(message, cause);
    }
}

