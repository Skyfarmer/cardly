package at.ase.backend.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class InvalidCreatorException extends Exception{

    public InvalidCreatorException() {
        super();
    }

    public InvalidCreatorException(String message) {
        super(message);
    }
}
