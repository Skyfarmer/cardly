package at.ase.backend.controller;


import at.ase.backend.dto.*;
import at.ase.backend.exception.InvalidRequestException;
import at.ase.backend.exception.NotFoundException;
import at.ase.backend.exception.PersistenceException;
import at.ase.backend.service.PlayerService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1")
public class AuthController {
    private final PlayerService playerService;

    public AuthController(PlayerService playerService) {
        this.playerService = playerService;
    }

    @PostMapping(path = "/register")
    @ResponseStatus(HttpStatus.CREATED)
    PlayerDto register(@RequestBody @Valid CreatePlayerDto dto) throws PersistenceException, InvalidRequestException {
        return playerService.registerPlayer(dto);
    }

    @PostMapping(path = "/requestEmail")
    @ResponseStatus(HttpStatus.OK)
    void requestEmailForPasswordChange(@RequestBody @Valid PasswordForgottenDto dto) throws PersistenceException {
        playerService.requestPasswordForgottenEmail(dto);
    }

    @PostMapping(path = "/resetPassword")
    @ResponseStatus(HttpStatus.OK)
    void resetPassword(@RequestBody @Valid PasswordResetDto dto) throws PersistenceException, NotFoundException, InvalidRequestException {
        playerService.resetPassword(dto);
    }
}
