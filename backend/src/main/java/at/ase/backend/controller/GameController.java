package at.ase.backend.controller;

import at.ase.backend.dto.GameDto;
import at.ase.backend.dto.SessionDto;
import at.ase.backend.exception.*;
import at.ase.backend.service.GameService;
import at.ase.backend.service.GameStatePusherService;
import io.kubernetes.client.openapi.ApiException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class GameController {
    private final GameService gameService;
    private final GameStatePusherService gameStatePusherService;

    public GameController(GameService gameService, GameStatePusherService gameStatePusherService) {
        Assert.notNull(gameService, "gameService must not be null");
        Assert.notNull(gameStatePusherService, "gameStatePusherService must not be null");
        this.gameService = gameService;
        this.gameStatePusherService = gameStatePusherService;
    }

    @GetMapping(path = "/game/{id}")
    @ResponseStatus(HttpStatus.OK)
    GameDto getGameById(@PathVariable Long id) throws PersistenceException, NotFoundException {
        return gameService.getGameById(id);
    }

    @GetMapping(path = "/games/{name}")
    @ResponseStatus(HttpStatus.OK)
    List<GameDto> getGameByName(@PathVariable String name) throws PersistenceException, NotFoundException {
        return gameService.getGamesByName(name);
    }

    @GetMapping(path = "/games/creator/{id}")
    @ResponseStatus(HttpStatus.OK)
    List<GameDto> getGamesByCreator(@PathVariable Long id) throws PersistenceException {
        return gameService.getGamesByCreator(id);
    }

    @PostMapping(path = "/game")
    @ResponseStatus(HttpStatus.CREATED)
    GameDto createGame(@RequestBody @Valid GameDto gameDto, Principal principal) throws PersistenceException, NotFoundException {
        return gameService.createGame(gameDto, principal.getName());
    }

    @PutMapping(path = "/game/{gameId}")
    @ResponseStatus(HttpStatus.OK)
    GameDto updateGame(@PathVariable Long gameId, @RequestBody @Valid GameDto gameDto, Principal principal) throws NotFoundException, PersistenceException, InvalidCreatorException {
        gameDto.setId(gameId);
        return gameService.updateGame(gameDto, principal.getName());
    }

    @GetMapping(path = "/games")
    List<GameDto> listGames(@RequestParam(required = false, defaultValue = "0") Integer page,
                            @RequestParam(required = false, defaultValue = "10") Integer size) throws InvalidListingConfigurationException, PersistenceException {
        return gameService.listGames(page, size);
    }

    @GetMapping(path = "/join")
    @ResponseStatus(HttpStatus.OK)
    SessionDto getSessionByJoinToken(@RequestParam String token) throws PersistenceException, NotFoundException {
        return gameService.getSessionByToken(token);
    }

    @GetMapping("/session/{sessionId}")
    SessionDto getSession(@PathVariable Long sessionId) throws PersistenceException, NotFoundException {
        return gameService.getSessionById(sessionId);
    }

    @PostMapping(path = "/game/{gameId}/session")
    @ResponseStatus(HttpStatus.CREATED)
    SessionDto createSession(@PathVariable Long gameId, @Valid @RequestBody SessionDto sessionDto) throws PersistenceException, NotFoundException {
        return gameService.createSession(gameId, sessionDto);
    }

    @PostMapping(path = "/session/{sessionId}/start")
    @ResponseStatus(HttpStatus.OK)
    SessionDto startGame(@PathVariable Long sessionId) throws IOException, PersistenceException, NotFoundException, KubernetesException {
        return gameService.startSession(sessionId);
    }

    @PostMapping(path = "/session/{sessionId}/player/{playerId}")
    @ResponseStatus(HttpStatus.OK)
    SessionDto addPlayer(@PathVariable Long sessionId, @PathVariable Long playerId) throws PersistenceException, NotFoundException, InvalidRequestException {
        return gameService.addPlayer(sessionId, playerId);
    }

    @DeleteMapping(path = "/session/{sessionId}/player/{playerId}")
    @ResponseStatus(HttpStatus.OK)
    ResponseEntity<SessionDto> removePlayer(@PathVariable Long sessionId, @PathVariable Long playerId) throws PersistenceException {
        try {
            return ResponseEntity.ok(gameService.removePlayer(sessionId, playerId));
        } catch (NotFoundException e) {
            return ResponseEntity.ok().build();
        }
    }

    @PostMapping(path = "/session/{id}/shutdown")
    @ResponseStatus(HttpStatus.OK)
    SessionDto shutdownGame(@PathVariable Long id) throws IOException, PersistenceException, NotFoundException, MessagingSetupException, KubernetesException {
        return gameStatePusherService.shutdownSession(id);
    }

    @PostMapping(path = "/game/{gameId}/player/{playerId}/matchmaking")
    @ResponseStatus(HttpStatus.OK)
    SessionDto matchMaking(@PathVariable Long gameId, @PathVariable Long playerId) throws PersistenceException, NotFoundException, InvalidRequestException {
        return gameService.matchMaking(gameId, playerId);
    }
}
