package at.ase.backend.controller;


import at.ase.backend.dto.PasswordChangeDto;
import at.ase.backend.dto.PlayerDto;
import at.ase.backend.exception.InvalidRequestException;
import at.ase.backend.exception.NotFoundException;
import at.ase.backend.exception.PersistenceException;
import at.ase.backend.service.PlayerService;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.security.Principal;

@RestController
@RequestMapping("/api/v1")
public class UserController {
    private final PlayerService playerService;

    public UserController(PlayerService playerService) {
        this.playerService = playerService;
    }

    @GetMapping(path = "/user/{id}")
    @ResponseStatus(HttpStatus.OK)
    PlayerDto findPlayerById(@PathVariable Long id) throws PersistenceException, NotFoundException {
        return playerService.findPlayerById(id);
    }

    @PutMapping(path = "/user")
    @ResponseStatus(HttpStatus.OK)
    PlayerDto update(Principal principal, @RequestBody @Valid PlayerDto dto) throws PersistenceException, NotFoundException, InvalidRequestException {
        return playerService.updatePlayer(principal.getName(), dto);
    }

    @PutMapping(path = "/user/password")
    @ResponseStatus(HttpStatus.OK)
    PlayerDto updatePassword(Principal principal, @RequestBody @Valid PasswordChangeDto dto) throws PersistenceException, NotFoundException, InvalidRequestException {
        return playerService.updatePassword(principal.getName(), dto);
    }
}
