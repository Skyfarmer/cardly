package at.ase.backend.controller;


import at.ase.backend.dto.ImageResponseDto;
import at.ase.backend.exception.ImageStorageException;
import at.ase.backend.exception.NotFoundException;
import at.ase.backend.exception.PersistenceException;
import at.ase.backend.model.ImageScope;
import at.ase.backend.service.ImageStorageService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/api/v1")
public class ImageController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ImageController.class);

    private final ImageStorageService imageStorageService;

    public ImageController(ImageStorageService imageStorageService) {
        Assert.notNull(imageStorageService, "imageStorageService must not be null");
        this.imageStorageService = imageStorageService;
    }

    /**
     * Used to upload a single image to the server.
     * Links for deleting and loading the file are returned.
     * These links contain the new internal filename which can be used to reference the image in the lua code.
     *
     * @param gameId The game the image belongs to
     * @param file   The image
     * @param scope  The {@link ImageScope} i.e. {@code CARD, THUMBNAIL, PRESENTATION}
     * @return Delete and load links
     */
    @PostMapping("/game/{gameId}/image/{scope}")
    @ResponseStatus(HttpStatus.OK)
    public ImageResponseDto uploadImage(@PathVariable Long gameId, @RequestParam("file") MultipartFile file,
                                        @PathVariable String scope) throws PersistenceException, NotFoundException, IOException {
        ImageScope sc;
        try {
            sc = ImageScope.valueOf(scope);
        } catch (IllegalArgumentException e) {
            sc = ImageScope.PRESENTATION;
            LOGGER.warn("Invalid image scope. Using presentation scope as default");
        }

        String imageName = imageStorageService.storeImage(gameId, file, sc);
        return ImageResponseDto.builder()
                .gameId(gameId)
                .imageName(imageName)
                .imageType(file.getContentType())
                .size(file.getSize())
                .build();
    }

    /**
     * Used to upload multiple images to the server.
     * Links for deleting and loading the files are returned.
     * These links contain the new internal filename which can be used to reference the image in the lua code.
     *
     * @param gameId The game the images belong to
     * @param files  A set of images
     * @param scope  The {@link ImageScope} i.e. {@code CARD, THUMBNAIL, PRESENTATION}
     * @return Delete and load links
     */
    @PostMapping("/game/{gameId}/images/{scope}")
    @ResponseStatus(HttpStatus.OK)
    public List<ImageResponseDto> uploadMultipleImages(@PathVariable Long gameId,
                                                       @RequestParam("files") MultipartFile[] files,
                                                       @PathVariable String scope) throws PersistenceException, NotFoundException, IOException {
        ArrayList<ImageResponseDto> dto = new ArrayList<>();

        for (MultipartFile file :
                files) {
            dto.add(uploadImage(gameId, file, scope));
        }

        return dto;
    }

    @GetMapping("/game/{gameId}/image/{fileName}")
    public ResponseEntity<Resource> loadImage(@PathVariable Long gameId, @PathVariable String fileName,
                                              HttpServletRequest request) throws NotFoundException, IOException, PersistenceException {
        // Load image as Resource
        Resource resource = imageStorageService.loadImage(gameId, fileName);
        String contentType = StringUtils.defaultString(request.getServletContext().getMimeType(resource.getFile().getAbsolutePath()), "image/png");

        return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION,
                        "attachment; filename=\"" + resource.getFilename() + "\"").body(resource);
    }

    @DeleteMapping("/game/{gameId}/image/{fileName}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteImage(@PathVariable Long gameId, @PathVariable String fileName) throws PersistenceException {
        imageStorageService.deleteImage(gameId, fileName);
    }
}
