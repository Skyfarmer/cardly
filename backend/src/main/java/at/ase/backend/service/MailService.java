package at.ase.backend.service;

/**
 * Handles the delivery of emails to a recipient.
 */
public interface MailService {
    /**
     * Email to the given `email` with a `refreshUrl` which is clickable.
     *
     * @param email      must not be {@code null}.
     * @param refreshUrl must not be {@code null}.
     */
    void sendPasswordForgottenEmail(String email, String refreshUrl);
}
