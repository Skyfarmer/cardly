package at.ase.backend.service.config;

import at.ase.backend.mapper.GameMapper;
import at.ase.backend.mapper.MessageMapper;
import at.ase.backend.mapper.SessionMapper;
import at.ase.backend.property.ImageStorageProperties;
import at.ase.backend.repository.GameRepository;
import at.ase.backend.repository.ImageRepository;
import at.ase.backend.repository.SessionRepository;
import at.ase.backend.service.GameService;
import at.ase.backend.service.GameStatePusherService;
import at.ase.backend.service.ImageStorageService;
import at.ase.backend.service.KubernetesService;
import at.ase.backend.mapper.*;
import at.ase.backend.repository.PlayerRepository;
import at.ase.backend.repository.SlotRepository;
import at.ase.backend.service.*;
import at.ase.backend.service.impl.GameServiceImpl;
import at.ase.backend.service.impl.GameStatePusherServiceImpl;
import at.ase.backend.service.impl.ImageStorageServiceImpl;
import at.ase.backend.service.impl.KubernetesServiceImpl;
import at.ase.backend.service.impl.PlayerServiceImpl;
import at.ase.backend.socket.FrontendCommunicationHandler;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.protobuf.Api;
import io.kubernetes.client.openapi.ApiClient;
import io.kubernetes.client.openapi.apis.AppsV1Api;
import io.kubernetes.client.util.Config;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ResourceLoader;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.filter.CommonsRequestLoggingFilter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.SafeConstructor;

import java.io.IOException;
import java.lang.reflect.Modifier;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;

@Configuration
public class ServiceConfiguration {
    @Value("${app.kubernetes.namespace:default}")
    private String namespace;

    @Value("${app.rabbitmq.exchange-requests:requests}")
    String exchangeRequests;

    @Value("${app.rabbitmq.exchange-responses:responses}")
    String exchangeResponses;

    @Value("${spring.rabbitmq.username:changeme}")
    String rabbitmqUsername;

    @Value("${spring.rabbitmq.password:changeme}")
    String rabbitmqPassword;

    @Value("${app.rabbitmq.service:rabbitmq-service}")
    String rabbitmqService;

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**").allowedMethods("HEAD", "GET", "POST", "PUT", "DELETE", "PATCH", "OPTIONS");
            }
        };
    }

    @Bean
    ImageStorageService imageStorageService(ImageStorageProperties properties, ImageRepository imageRepository, GameService gameService, GameMapper mapper) throws IOException {
        return new ImageStorageServiceImpl(properties, imageRepository, gameService, mapper);
    }

    @Bean
    GameService gameService(GameRepository gameRepository,
                            GameMapper gameMapper,
                            KubernetesService kubernetesService,
                            GameStatePusherService gameStatePusherService,
                            SessionMapper sessionMapper,
                            SessionRepository sessionRepository,
                            PlayerRepository playerRepository,
                            SlotRepository slotRepository,
                            FrontendCommunicationHandler frontendCommunicationHandler,
                            MessageMapper messageMapper) {
        return new GameServiceImpl(gameRepository,
                gameMapper,
                kubernetesService,
                gameStatePusherService,
                sessionMapper,
                sessionRepository,
                playerRepository,
                slotRepository,
                frontendCommunicationHandler,
                messageMapper);
    }

    @Bean
    PlayerService playerService(PlayerRepository playerRepository, PlayerMapper playerMapper, PasswordEncoder passwordEncoder, MailService mailService) {
        return new PlayerServiceImpl(playerRepository, playerMapper, passwordEncoder, mailService);
    }

    @Bean
    GameStatePusherService gameStatePusherService(@Qualifier("requestsExchange") Exchange exchangeRequests,
                                                  @Qualifier("responsesExchange") Exchange exchangeResponses,
                                                  MessageMapper messageMapper,
                                                  AmqpTemplate messageProducer,
                                                  RabbitAdmin rabbitAdmin,
                                                  SlotRepository slotRepository,
                                                  SlotMapper slotMapper,
                                                  SessionRepository sessionRepository,
                                                  SessionMapper sessionMapper,
                                                  KubernetesService kubernetesService) {
        return new GameStatePusherServiceImpl(exchangeRequests,
                exchangeResponses,
                messageMapper,
                messageProducer,
                rabbitAdmin,
                slotRepository,
                slotMapper,
                sessionRepository,
                sessionMapper,
                kubernetesService);
    }

    @Bean
    @Qualifier("wsObjectMapper")
    public ObjectMapper objectMapper() {
        return new ObjectMapper()
                .registerModule(new JavaTimeModule());
    }

    @Bean
    Gson gson() {
        return new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.STATIC, Modifier.TRANSIENT, Modifier.VOLATILE)
                .registerTypeAdapter(LocalDateTime.class, new GsonLocalDateTimeConverter())
                .registerTypeAdapter(OffsetDateTime.class, new GsonOffsetDateTimeConverter())
                .create();
    }

    @Bean
    Yaml yaml() {
        return new Yaml(new SafeConstructor());
    }

    @Bean
    ApiClient apiClient() throws IOException {
        return Config.defaultClient();
    }

    @Bean
    AppsV1Api appsV1Api(ApiClient apiClient) {
        return new AppsV1Api(apiClient);
    }

    @Bean
    KubernetesService kubernetesService(ApiClient apiClient, AppsV1Api appsV1Api, Yaml yaml, ResourceLoader resourceLoader, Gson gson) {
        return new KubernetesServiceImpl(apiClient, appsV1Api, yaml, resourceLoader, gson, namespace, rabbitmqUsername, rabbitmqPassword, rabbitmqService);
    }

    @Bean("requestsExchange")
    TopicExchange requestsExchange() {
        return new TopicExchange(exchangeRequests);
    }

    @Bean("responsesExchange")
    TopicExchange responsesExchange() {
        return new TopicExchange(exchangeResponses);
    }

    @Bean
    RabbitAdmin rabbitAdmin(RabbitTemplate rabbitTemplate) {
        return new RabbitAdmin(rabbitTemplate.getConnectionFactory());
    }

    @Bean("requestQueue")
    Queue requestQueue() {
        return QueueBuilder.durable("requests")
                .withArgument("x-dead-letter-exchange", "")
                .withArgument("x-dead-letter-routing-key", "dlq.requests")
                .build();
    }

    @Bean("dlqRequestQueue")
    Queue dlqRequestQueue() {
        return new Queue("dlq.requests", true);
    }

    @Bean("dlqResponseQueue")
    Queue dlqResponseQueue() {
        return new Queue("dlq.responses", true);
    }

    @Bean
    Binding requestBinding(@Qualifier("requestQueue") Queue requestQueue, @Qualifier("requestsExchange") Exchange exchangeRequests) {
        return BindingBuilder.bind(requestQueue)
                .to(exchangeRequests)
                .with("session.#")
                .noargs();
    }

    @Bean
    public RabbitTemplate rabbitTemplate(final ConnectionFactory connectionFactory) {
        final var rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(producerJackson2MessageConverter());
        return rabbitTemplate;
    }

    @Bean
    public Jackson2JsonMessageConverter producerJackson2MessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

}
