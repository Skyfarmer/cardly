package at.ase.backend.service.impl;

import at.ase.backend.exception.KubernetesException;
import at.ase.backend.service.KubernetesService;
import com.google.gson.Gson;
import io.kubernetes.client.openapi.ApiClient;
import io.kubernetes.client.openapi.ApiException;
import io.kubernetes.client.openapi.apis.AppsV1Api;
import io.kubernetes.client.openapi.apis.CoreV1Api;
import io.kubernetes.client.openapi.models.*;
import io.kubernetes.client.proto.V1Apps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ResourceLoader;
import org.springframework.util.Assert;
import org.yaml.snakeyaml.Yaml;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class KubernetesServiceImpl implements KubernetesService {
    public static final String AMQP_ADDR = "AMQP_ADDR";
    private final Logger logger = LoggerFactory.getLogger(KubernetesServiceImpl.class);
    private static final String SESSION_ID = "SESSION_ID";
    private static final String EXECUTION_ENGINE_YML = "execution-engine.yml";

    private final ApiClient apiClient;
    private final AppsV1Api appsV1Api;
    private final Yaml yaml;
    private final ResourceLoader resourceLoader;
    private final Gson gson;
    private final String namespace;
    private final String rabbitmqUsername;
    private final String rabbitmqPassword;
    private final String rabbitmqService;

    public KubernetesServiceImpl(
            ApiClient apiClient,
            AppsV1Api appsV1Api,
            Yaml yaml,
            ResourceLoader resourceLoader,
            Gson gson,
            String namespace,
            String rabbitmqUsername,
            String rabbitmqPassword,
            String rabbitmqService) {
        Assert.notNull(apiClient, "apiClient must not be null");
        Assert.notNull(appsV1Api, "appsV1Api must not be null");
        Assert.notNull(yaml, "yaml must not be null");
        Assert.notNull(resourceLoader, "resourceLoader must not be null");
        Assert.notNull(gson, "gson must not be null");
        Assert.notNull(namespace, "metadata must not be null");
        Assert.hasText(rabbitmqUsername, "rabbitmqUsername has text");
        Assert.hasText(rabbitmqPassword, "rabbitmqPassword has text");
        Assert.hasText(rabbitmqService, "rabbitmqService has text");

        this.apiClient = apiClient;
        this.appsV1Api = appsV1Api;
        this.yaml = yaml;
        this.resourceLoader = resourceLoader;
        this.gson = gson;
        this.namespace = namespace;
        this.rabbitmqUsername = rabbitmqUsername;
        this.rabbitmqPassword = rabbitmqPassword;
        this.rabbitmqService = rabbitmqService;
    }

    @Override
    public void startExecutionEngine(String gameName, String sessionId) throws IOException, KubernetesException {
        Assert.notNull(gameName, "name must not be null");
        Assert.notNull(sessionId, "sessionId must not be null");

        logger.info("Starting execution engine for game '{}' with id '{}'", gameName, sessionId);

        V1Deployment deployment = loadDeploymentFromYaml(EXECUTION_ENGINE_YML);

        V1EnvVar var = new V1EnvVar();
        var.setName(SESSION_ID);
        var.setValue(sessionId.toString());
        V1EnvVar varAddr = new V1EnvVar();
        varAddr.setName(AMQP_ADDR);
        varAddr.setValue(String.format("amqp://%s:%s@%s", rabbitmqUsername, rabbitmqPassword, rabbitmqService));
        deployment.getSpec().getTemplate().getSpec().getContainers().forEach(container -> {
            container.addEnvItem(var);
            container.addEnvItem(varAddr);
        });

        V1ObjectMeta metaData = deployment.getMetadata();
        metaData.setName(generateDeploymentName(sessionId, metaData.getName()));
        metaData.setNamespace(namespace);

        try {
            appsV1Api.createNamespacedDeployment(namespace, deployment, "true", null, null, null);
        } catch (ApiException e) {
            logger.error(e.getResponseBody());
            throw new KubernetesException(e.getResponseBody());
        }

        logger.info("Started execution engine for game '{}' with id '{}'", gameName, sessionId);
    }

    private String generateDeploymentName(String id, String existingName) {
        return String.format("%s-%s", id, existingName);
    }

    @Override
    public void stopExecutionEngine(String gameName, String sessionId) throws IOException, KubernetesException {
        Assert.notNull(gameName, "name must not be null");
        Assert.notNull(sessionId, "sessionId must not be null");

        logger.info("Stopping execution engine for game '{}' with id '{}'", gameName, sessionId);

        V1Deployment deployment = loadDeploymentFromYaml(EXECUTION_ENGINE_YML);
        V1ObjectMeta metaData = deployment.getMetadata();

        // Fact: this generation of the deployment is not migration safe.
        // When a deployment already exists and the name of the template changes then
        // this will not delete it.
        String deploymentName = generateDeploymentName(sessionId, metaData.getName());

        try {
            V1Status status = appsV1Api.deleteNamespacedDeployment(deploymentName, namespace, "true", null, 0, null, null, null);
        } catch (ApiException e) {
            logger.error(e.getResponseBody());
            throw new KubernetesException(e.getResponseBody());
        }
    }

    @Override
    public boolean isAlive(Long sessionId) throws ApiException {
        Assert.notNull(sessionId, "sessionId must not be null");

        CoreV1Api coreApi = new CoreV1Api(apiClient);
        var podList = coreApi.listNamespacedPod(namespace, "true", null, null, null, null, null, null, null, null, null);

        for (V1Pod pod :
                podList.getItems()) {
            for (V1Container container :
                    pod.getSpec().getContainers()) {
                if (container.getEnv() != null && container.getEnv().stream().anyMatch(env -> env.getName().equals(SESSION_ID) && env.getValue().equals(sessionId.toString()))) {
                    if (pod.getStatus().getContainerStatuses().stream().anyMatch(status -> !status.getRestartCount().equals(0))) {
                        logger.warn("Container for session '{}' is not alive", sessionId);
                        return false;
                    } else {
                        logger.info("Container for session '{}' is alive", sessionId);
                        return true;
                    }
                }
            }
        }

        return true;
    }

    private V1Deployment loadDeploymentFromYaml(String fileName) throws IOException {
        Map<Object, Object> jobManifest = yaml.load(
                resourceLoader.getResource(ResourceLoader.CLASSPATH_URL_PREFIX + fileName).getInputStream());

        return gson.fromJson(gson.toJson(jobManifest), V1Deployment.class);
    }
}
