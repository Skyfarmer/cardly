package at.ase.backend.service.impl;

import at.ase.backend.exception.ImageStorageException;
import at.ase.backend.exception.NotFoundException;
import at.ase.backend.exception.PersistenceException;
import at.ase.backend.mapper.GameMapper;
import at.ase.backend.model.ImageEntity;
import at.ase.backend.model.ImageScope;
import at.ase.backend.property.ImageStorageProperties;
import at.ase.backend.repository.ImageRepository;
import at.ase.backend.service.GameService;
import at.ase.backend.service.ImageStorageService;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.dao.DataAccessException;
import org.springframework.util.Assert;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

public class ImageStorageServiceImpl implements ImageStorageService {

    private final static Logger LOGGER = LoggerFactory.getLogger(ImageStorageServiceImpl.class);
    private final Path imageStorageLocation;
    private final ImageRepository imageRepository;
    private final GameService gameService;
    private final GameMapper gameMapper;

    private static final Character[] INVALID_WINDOWS_SPECIFIC_CHARS = {'"', '*', ':', '<', '>', '?', '\\', '|', 0x7F};
    private static final Character[] INVALID_UNIX_SPECIFIC_CHARS = {'\000'};

    public ImageStorageServiceImpl(ImageStorageProperties fileStorageProperties, ImageRepository imageRepository,
                                   GameService gameService, GameMapper gameMapper) {
        Assert.notNull(fileStorageProperties, "fileStorageProperties must not be null");
        Assert.notNull(imageRepository, "ImageRepository must not be null");
        Assert.notNull(gameService, "gameService must not be Null");
        Assert.notNull(gameMapper, "GameMapper must not be null");

        this.imageStorageLocation = Paths.get(fileStorageProperties.getUploadDir()).toAbsolutePath().normalize();
        this.imageRepository = imageRepository;
        this.gameService = gameService;
        this.gameMapper = gameMapper;
    }

    @PostConstruct
    void createStorage() throws IOException {
        Files.createDirectories(this.imageStorageLocation);
        LOGGER.info("Directory for image storage created");
    }

    @Override
    @Transactional
    public String storeImage(@NotNull Long gameId, @NotNull MultipartFile image, @NotNull ImageScope scope) throws ImageStorageException, PersistenceException, NotFoundException, IOException {
        Assert.notNull(image.getOriginalFilename(), "originalFileName must not be null");
        Assert.notNull(image.getContentType(), "contentType must not be null");

        gameService.getGameById(gameId);

        if (!image.getContentType().contains("image")) {
            throw new ImageStorageException("File is not an image");
        }

        String fileName = image.getOriginalFilename();
        checkForInvalidCharactersInFilename(fileName);

        try {
            Path location = this.imageStorageLocation.resolve(String.valueOf(gameId));
            createDirectoryIfDoesNotExist(gameId, location);
            Path fullPath = location.resolve(fileName);

            if (Files.deleteIfExists(fullPath)) {
                imageRepository.removeByNameAndGame(fileName, gameId);
                LOGGER.info("Image '{}' deleted at '{}' because it already exists", fileName, fullPath);
            }

            ImageEntity entity = ImageEntity.builder()
                    .game(gameMapper.map(gameService.getGameById(gameId)))
                    .name(fileName)
                    .scope(scope)
                    .build();
            imageRepository.save(entity);

            Files.copy(image.getInputStream(), fullPath);
            LOGGER.info("Image '{}' successfully stored at '{}'", fileName, fullPath);

            return fileName;
        } catch (DataAccessException e) {
            LOGGER.error(e.toString());
            throw new PersistenceException();
        }
    }

    private void createDirectoryIfDoesNotExist(Long gameId, Path location) {
        File directory = new File(location.toUri());
        if (!directory.exists()) {
            Assert.isTrue(directory.mkdir(), "Creation of directory failed");
            LOGGER.info("Created directory for game id " + gameId);
        }
    }

    private void checkForInvalidCharactersInFilename(String fileName) {
        if (Arrays.stream(INVALID_UNIX_SPECIFIC_CHARS).anyMatch(x -> fileName.contains(x.toString()))
                || Arrays.stream(INVALID_WINDOWS_SPECIFIC_CHARS).anyMatch(x -> fileName.contains(x.toString()))) {
            throw new ImageStorageException("Image contains invalid character");
        }
    }

    @Override
    public Resource loadImage(@NotNull Long gameId, String fileName) throws NotFoundException, PersistenceException {
        Validate.notBlank(fileName);

        try {
            gameService.getGameById(gameId);
            Path filePath = this.imageStorageLocation.resolve(String.valueOf(gameId)).resolve(fileName).normalize();
            Resource resource = new UrlResource(filePath.toUri());
            if (resource.exists()) {
                LOGGER.info("Loading image" + fileName);
                return resource;
            } else {
                throw new NotFoundException("Could not find image " + fileName);
            }
        } catch (MalformedURLException ex) {
            throw new NotFoundException("Could not find image " + fileName);
        }
    }

    @Override
    @Transactional
    public void deleteImage(@NotNull Long gameId, String fileName) throws ImageStorageException, PersistenceException {
        Validate.notBlank(fileName);

        deleteImageFromDbIfExists(gameId, fileName);
        deleteImageFromFileSystem(gameId, fileName);
    }


    private void deleteImageFromDbIfExists(Long gameId, String fileName) throws PersistenceException {
        try {
            ImageEntity image = imageRepository
                    .findByGameAndName(gameMapper.map(gameService.getGameById(gameId)), fileName)
                    .orElseThrow(NotFoundException::new);

            imageRepository.removeByNameAndGame(image.getName(), gameId);
        } catch (NotFoundException e) {
            LOGGER.warn("Cannot delete image {} from DB", fileName, e);
        }
    }

    private void deleteImageFromFileSystem(Long gameId, String fileName) {
        Path filePath = this.imageStorageLocation.resolve(String.valueOf(gameId)).resolve(fileName).normalize();
        if (Files.exists(filePath)) {
            try {
                Files.delete(filePath);
                LOGGER.info("Image '{}' for game '{}' successfully deleted", fileName, gameId);
            } catch (IOException e) {
                throw new ImageStorageException("Could not delete image " + fileName, e);
            }
        } else {
            LOGGER.warn("Could not find image at {}.", filePath);
        }
    }
}
