package at.ase.backend.service;

import at.ase.backend.dto.GameDto;
import at.ase.backend.dto.SessionDto;
import at.ase.backend.exception.*;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.rabbit.annotation.Queue;

import java.io.IOException;

/**
 * Pushes game state changes from the backend to the execution engine.
 */
public interface GameStatePusherService {
    /**
     * Creates a queue and binding for the game. Additionally, it publishes a start
     * message for the execution engine.
     *
     * @param sessionDto must not be {@code null}.
     * @throws MessagingSetupException if the {@link Queue} and {@link Binding} fails.
     * @throws MessagingException      if sending the start message fails. This will cause the removal of the {@link Queue} and {@link Binding}.
     */
    void startGame(SessionDto sessionDto) throws MessagingSetupException, MessagingException;

    /**
     * Sends messages from the backend to the execution with {@code data}.
     *
     * @param sessionId must not be {@code null}.
     * @param playerId  must not be {@code null}.
     * @param data      must not be {@code null}.
     * @throws MessagingException if sending the start message fails.
     */
    void publishMove(String sessionId, String playerId, JsonNode data) throws MessagingException, NotFoundException, PersistenceException, IOException, MessagingSetupException, KubernetesException;

    /**
     * Delete existing queues.
     *
     * @param sessionDto must not be {@code null}.
     * @throws MessagingSetupException if deleting of the queue fails.
     */
    void stopGame(SessionDto sessionDto) throws MessagingSetupException;

    /**
     * Shutdown the session.
     *
     * @param sessionId must not be {@code null}.
     * @return the updated game.
     */
    SessionDto shutdownSession(Long sessionId) throws NotFoundException, IOException, PersistenceException, MessagingSetupException, KubernetesException;
}
