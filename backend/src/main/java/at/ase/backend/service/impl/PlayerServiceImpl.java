package at.ase.backend.service.impl;

import at.ase.backend.dto.*;
import at.ase.backend.exception.InvalidRequestException;
import at.ase.backend.exception.NotFoundException;
import at.ase.backend.exception.PersistenceException;
import at.ase.backend.mapper.PlayerMapper;
import at.ase.backend.model.PlayerEntity;
import at.ase.backend.repository.PlayerRepository;
import at.ase.backend.service.MailService;
import at.ase.backend.service.PlayerService;
import io.jsonwebtoken.lang.Assert;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.Instant;
import java.util.Optional;

public class PlayerServiceImpl implements PlayerService {
    private static final int PASSWORD_RESET_TOKEN_LENGTH = 8;
    private final Logger logger = LoggerFactory.getLogger(PlayerServiceImpl.class);
    private final PlayerRepository playerRepository;
    private final PlayerMapper playerMapper;
    private final PasswordEncoder passwordEncoder;
    private final MailService mailService;

    public PlayerServiceImpl(PlayerRepository playerRepository, PlayerMapper playerMapper, PasswordEncoder passwordEncoder, MailService mailService) {
        Assert.notNull(playerRepository, "playerRepository must not be null");
        Assert.notNull(playerMapper, "playerMapper must not be null");
        Assert.notNull(passwordEncoder, "passwordEncoder must not be null");
        Assert.notNull(mailService, "mailService must not be null");

        this.playerRepository = playerRepository;
        this.playerMapper = playerMapper;
        this.passwordEncoder = passwordEncoder;
        this.mailService = mailService;
    }

    @Override
    public PlayerDto findPlayerByEmail(String email) throws NotFoundException, PersistenceException {
        Assert.notNull(email, "email must not be null");
        logger.debug("Get player by email '{}'", email);

        try {
            return playerMapper.map(playerRepository.findByEmail(email).orElseThrow(NotFoundException::new));
        } catch (DataAccessException exception) {
            logger.error("Error occurred during finding player", exception);
            throw new PersistenceException();
        }
    }

    @Override
    public PlayerDto findPlayerById(Long id) throws NotFoundException, PersistenceException {
        Assert.notNull(id, "id must not be null");
        logger.debug("Get player by id '{}'", id);

        try {
            return playerMapper.map(playerRepository.findById(id).orElseThrow(NotFoundException::new));
        } catch (DataAccessException exception) {
            logger.error("Error occurred during finding player", exception);
            throw new PersistenceException();
        }
    }

    @Override
    public void resetLoginAttempts(String email) throws NotFoundException, PersistenceException {
        Assert.notNull(email, "email must not be null");
        logger.debug("Resetting login attempts for player '{}'", email);

        try {
            PlayerEntity entity = playerRepository.findByEmail(email).orElseThrow(NotFoundException::new);
            entity.setLoginAttempts(0);
            playerRepository.save(entity);
        } catch (DataAccessException exception) {
            logger.error("Error occurred during updating player", exception);
            throw new PersistenceException();
        }
    }

    @Override
    public PlayerDto registerPlayer(CreatePlayerDto dto) throws PersistenceException, InvalidRequestException {
        Assert.notNull(dto, "dto must not be null");

        logger.debug("Registering player '{}'", dto.getEmail());

        try {
            if (playerRepository.findByEmail(dto.getEmail()).isPresent()) {
                throw new InvalidRequestException("User already exists");
            }

            if (playerRepository.findByAlias(dto.getAlias()).isPresent()) {
                throw new InvalidRequestException("Alias already exists");
            }

            PlayerEntity entity = new PlayerEntity();
            entity.setEmail(dto.getEmail());
            entity.setPassword(passwordEncoder.encode(dto.getPassword()));
            entity.setCreatedAt(Instant.now());
            entity.setUpdatedAt(Instant.now());
            entity.setBanned(false);
            entity.setAlias(dto.getAlias());
            return playerMapper.map(playerRepository.save(entity));
        } catch (DataAccessException exception) {
            logger.error("Error occurred during creating player", exception);
            throw new PersistenceException();
        }
    }

    @Override
    public void requestPasswordForgottenEmail(PasswordForgottenDto dto) throws PersistenceException {
        Assert.notNull(dto, "dto must not be null");

        try {
            Optional<PlayerEntity> playerEntity = playerRepository.findByEmail(dto.getEmail());

            if (playerEntity.isPresent()) {
                String token = RandomStringUtils.randomAlphabetic(PASSWORD_RESET_TOKEN_LENGTH);
                playerEntity.get().setPasswordResetToken(token);
                playerRepository.save(playerEntity.get());
                mailService.sendPasswordForgottenEmail(dto.getEmail(), "?token=" + token);
            }
        } catch (DataAccessException exception) {
            logger.error("Error occurred during creating player", exception);
            throw new PersistenceException();
        }
    }

    @Override
    public void resetPassword(PasswordResetDto dto) throws NotFoundException, PersistenceException, InvalidRequestException {
        try {
            PlayerEntity entity = playerRepository.findByPasswordResetToken(dto.getToken()).orElseThrow(NotFoundException::new);

            entity.setPassword(passwordEncoder.encode(dto.getNewPassword()));
            entity.setBanned(false);
            entity.setPasswordResetToken(null);
            playerRepository.save(entity);
        } catch (DataAccessException exception) {
            logger.error("Error occurred during resetting password", exception);
            throw new PersistenceException();
        }
    }

    @Override
    public PlayerDto updatePlayer(String email, PlayerDto dto) throws PersistenceException, NotFoundException, InvalidRequestException {
        Assert.notNull(email, "email must not be null");
        Assert.notNull(dto, "dto must not be null");

        try {
            PlayerEntity entity = playerRepository.findByEmail(email).orElseThrow(NotFoundException::new);

            if (!entity.getAlias().equals(dto.getAlias()) && playerRepository.findByAlias(dto.getAlias()).isPresent()) {
                throw new InvalidRequestException("Alias already exists");
            }

            entity.setAlias(dto.getAlias());
            entity.setEmail(dto.getEmail());
            return playerMapper.map(playerRepository.save(entity));
        } catch (DataAccessException exception) {
            logger.error("Error occurred during updating player", exception);
            throw new PersistenceException();
        }
    }

    @Override
    public PlayerDto updatePassword(String email, PasswordChangeDto dto) throws NotFoundException, InvalidRequestException, PersistenceException {
        Assert.notNull(email, "email must not be null");
        Assert.notNull(dto, "dto must not be null");

        try {
            PlayerEntity entity = playerRepository.findByEmail(email).orElseThrow(NotFoundException::new);

            if(!passwordEncoder.matches(dto.getCurrentPassword(), entity.getPassword())) {
                throw new InvalidRequestException("Old password is not correct");
            }

            entity.setPassword(passwordEncoder.encode(dto.getNewPassword()));
            return playerMapper.map(playerRepository.save(entity));
        } catch (DataAccessException exception) {
            logger.error("Error occurred during updating password", exception);
            throw new PersistenceException();
        }
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Assert.notNull(email, "email must not be null");

        PlayerEntity entity = playerRepository.findByEmail(email).orElseThrow(() -> new UsernameNotFoundException("User does not exist"));

        if (entity.getBanned()) {
            throw new LockedException("Account has been locked");
        }

        return new User(entity.getEmail(), entity.getPassword(), AuthorityUtils.createAuthorityList("ROLE_USER"));
    }
}
