package at.ase.backend.service;

import at.ase.backend.dto.AddPlayerDto;
import at.ase.backend.dto.GameDto;
import at.ase.backend.dto.SessionDto;
import at.ase.backend.exception.*;
import at.ase.backend.repository.GameRepository;

import java.io.IOException;
import java.util.List;

/**
 * This service is an abstraction for the {@link GameRepository}.
 */
public interface GameService {
    /**
     * Get game by id
     *
     * @param id must not be null.
     * @return {@link GameDto}.
     * @throws NotFoundException    if the entity does not exist.
     * @throws PersistenceException if an error at the persistence layer occurs.
     */
    GameDto getGameById(Long id) throws NotFoundException, PersistenceException;

    /**
     * Get games by name
     *
     * @param name must not be null
     * @return a {@link List} of {@link GameDto}.
     * @throws PersistenceException if an error at the persistence layer occurs.
     */
    List<GameDto> getGamesByName(String name) throws PersistenceException;

    /**
     * Get games by name
     *
     * @param id must not be null
     * @return a {@link List} of {@link GameDto}.
     * @throws PersistenceException if an error at the persistence layer occurs.
     */
    List<GameDto> getGamesByCreator(Long id) throws PersistenceException;

    /**
     * Get session by id
     *
     * @param id must not be null.
     * @return {@link SessionDto}.
     * @throws NotFoundException    if the entity does not exist.
     * @throws PersistenceException if an error at the persistence layer occurs.
     */
    SessionDto getSessionById(Long id) throws NotFoundException, PersistenceException;

    /**
     * Create a new game.
     *
     * @param game  must not {@code null}.
     * @param email must not {@code null}.
     * @return {@link GameDto}.
     * @throws PersistenceException if an error at the persistence layer occurs.
     */
    GameDto createGame(GameDto game, String email) throws PersistenceException, NotFoundException;

    /**
     * List the games with the given parameters.
     *
     * @param page must not be negative.
     * @param size must not be negative.
     * @return a {@link List} of {@link GameDto}.
     * @throws InvalidListingConfigurationException if the parameter {@code page} or {@code size} is negative or {@code size} is zero.
     * @throws PersistenceException                 if an error at the persistence layer occurs.
     * @throws InvalidSessionStateException         if session has an invalid state.
     */
    List<GameDto> listGames(int page, int size) throws InvalidListingConfigurationException, PersistenceException;

    /**
     * Start the game.
     *
     * @param sessionId must not be {@code null}.
     * @return a {@link SessionDto}.
     */
    SessionDto startSession(Long sessionId) throws IOException, NotFoundException, PersistenceException, KubernetesException;



    /**
     * Create a new session
     *
     * @param gameId     must not be {@code null}.
     * @param sessionDto must not be {@code null}.
     * @return a {@link SessionDto}.
     */
    SessionDto createSession(Long gameId, SessionDto sessionDto) throws NotFoundException, PersistenceException;

    /**
     * Update the entity with the given {@code gameDto}
     *
     * @param gameDto must not be {@¢ode null}.
     * @return an updated {@link GameDto}.
     */
    GameDto updateGame(GameDto gameDto, String email) throws NotFoundException, PersistenceException, InvalidCreatorException;

    /**
     * Add a player to the session.
     *
     * @param sessionId must not be {@code null}.
     * @param playerId  must not be {@code null}.
     * @return a {@link AddPlayerDto}.
     */
    SessionDto addPlayer(Long sessionId, Long playerId) throws PersistenceException, NotFoundException, InvalidRequestException;

    /**
     * Remove a player from the session.
     *
     * @param sessionId must not be {@code null}.
     * @param playerId  must not be {@code null}.
     * @return an updated {@link SessionDto}.
     */
    SessionDto removePlayer(Long sessionId, Long playerId) throws PersistenceException, NotFoundException;

    /**
     * Enter a session if it still has not enough players.
     *
     * @param gameId   must not be {@code null}.
     * @param playerId must not be {@code null}.
     * @return an updated {@link SessionDto}.
     */
    SessionDto matchMaking(Long gameId, Long playerId) throws NotFoundException, PersistenceException, InvalidRequestException;

    /**
     * Get session by the join token
     *
     * @param token must not be {@code null}.
     * @return a {@link SessionDto}.
     */
    SessionDto getSessionByToken(String token) throws NotFoundException, PersistenceException;

}
