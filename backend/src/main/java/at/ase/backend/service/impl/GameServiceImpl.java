package at.ase.backend.service.impl;

import at.ase.backend.dto.GameDto;
import at.ase.backend.dto.SessionDto;
import at.ase.backend.exception.*;
import at.ase.backend.mapper.GameMapper;
import at.ase.backend.mapper.MessageMapper;
import at.ase.backend.mapper.SessionMapper;
import at.ase.backend.model.*;
import at.ase.backend.repository.GameRepository;
import at.ase.backend.repository.PlayerRepository;
import at.ase.backend.repository.SessionRepository;
import at.ase.backend.repository.SlotRepository;
import at.ase.backend.service.GameService;
import at.ase.backend.service.GameStatePusherService;
import at.ase.backend.service.KubernetesService;
import at.ase.backend.socket.FrontendCommunicationHandler;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.util.Assert;

import javax.mail.Session;
import java.io.IOException;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

public class GameServiceImpl implements GameService {
    public static final int JOIN_TOKEN_LENGTH = 8;
    private final Logger logger = LoggerFactory.getLogger(GameServiceImpl.class);
    private final GameRepository gameRepository;
    private final GameMapper gameMapper;
    private final KubernetesService kubernetesService;
    private final GameStatePusherService gameStatePusherService;
    private final SessionMapper sessionMapper;
    private final SessionRepository sessionRepository;
    private final PlayerRepository playerRepository;
    private final SlotRepository slotRepository;
    private final FrontendCommunicationHandler frontendCommunicationHandler;
    private final MessageMapper messageMapper;

    public GameServiceImpl(GameRepository gameRepository,
                           GameMapper gameMapper,
                           KubernetesService kubernetesService,
                           GameStatePusherService gameStatePusherService,
                           SessionMapper sessionMapper,
                           SessionRepository sessionRepository,
                           PlayerRepository playerRepository,
                           SlotRepository slotRepository,
                           FrontendCommunicationHandler frontendCommunicationHandler,
                           MessageMapper messageMapper) {
        Assert.notNull(gameRepository, "gameRepository must not null");
        Assert.notNull(gameMapper, "gameMapper must not be null");
        Assert.notNull(kubernetesService, "kubernetesService must not be null");
        Assert.notNull(gameStatePusherService, "gameStatePusherService must not be null");
        Assert.notNull(sessionMapper, "sessionMapper must not be null");
        Assert.notNull(sessionRepository, "sessionRepository must not be null");
        Assert.notNull(playerRepository, "playerRepository must not be null");
        Assert.notNull(slotRepository, "slotRepository must not be null");
        Assert.notNull(frontendCommunicationHandler, "frontEndCommunicationHandler must not be null");
        Assert.notNull(messageMapper, "messageMapper must not be null");

        this.gameRepository = gameRepository;
        this.gameMapper = gameMapper;
        this.kubernetesService = kubernetesService;
        this.gameStatePusherService = gameStatePusherService;
        this.sessionMapper = sessionMapper;
        this.sessionRepository = sessionRepository;
        this.playerRepository = playerRepository;
        this.slotRepository = slotRepository;
        this.frontendCommunicationHandler = frontendCommunicationHandler;
        this.messageMapper = messageMapper;
    }

    @Override
    public GameDto getGameById(Long id) throws NotFoundException, PersistenceException {
        logger.debug("Get game with the '{}'", id);
        try {
            return gameMapper.map(gameRepository.findById(id).orElseThrow(NotFoundException::new));
        } catch (DataAccessException exception) {
            logger.error("Error occurred during fetching game", exception);
            throw new PersistenceException();
        }
    }

    @Override
    public List<GameDto> getGamesByName(String name) throws PersistenceException {
        logger.debug("Get game with the '{}'", name);
        try {
            return gameRepository
                    .findByNameContainingIgnoreCase(name)
                    .stream()
                    .map(gameMapper::map)
                    .toList();
        } catch (DataAccessException exception) {
            logger.error("Error occurred during fetching games", exception);
            throw new PersistenceException();
        }
    }

    @Override
    public List<GameDto> getGamesByCreator(Long id) throws PersistenceException {
        logger.debug("Get game with the creator '{}'", id);
        try {
            return gameRepository
                    .findByCreator_Id(id)
                    .stream()
                    .map(gameMapper::map)
                    .toList();
        } catch (DataAccessException exception) {
            logger.error("Error occurred during fetching games by creator", exception);
            throw new PersistenceException();
        }
    }

    @Override
    public SessionDto getSessionById(Long id) throws NotFoundException, PersistenceException {
        logger.debug("Get session with the '{}'", id);
        try {
            return sessionMapper.map(sessionRepository.findById(id).orElseThrow(NotFoundException::new));
        } catch (DataAccessException exception) {
            logger.error("Error occurred during fetching session", exception);
            throw new PersistenceException();
        }
    }

    @Override
    public GameDto createGame(GameDto game, String email) throws PersistenceException, NotFoundException {
        GameEntity entity = gameMapper.map(game);
        entity.setCreatedAt(Instant.now());
        entity.setUpdatedAt(Instant.now());
        entity.setDescription(game.getDescription());

        PlayerEntity creator = playerRepository.findByEmail(email).orElseThrow(NotFoundException::new);
        entity.setCreator(creator);

        GameVersionEntity versionEntity = new GameVersionEntity();
        versionEntity.setSourceCode(game.getSourceCode());
        versionEntity.setCreatedAt(Instant.now());
        versionEntity.setUpdatedAt(Instant.now());
        entity.addVersion(versionEntity);

        try {
            return gameMapper.map(gameRepository.save(entity));
        } catch (DataAccessException exception) {
            logger.error("Error occurred during creating game: {}", exception.toString());
            throw new PersistenceException();
        }
    }

    @Override
    public List<GameDto> listGames(int page, int size) throws InvalidListingConfigurationException, PersistenceException {
        if (page < 0) {
            throw new InvalidListingConfigurationException("page");
        }
        if (size <= 0) {
            throw new InvalidListingConfigurationException("size");
        }

        Pageable pageable = PageRequest.of(page, size);

        try {
            return gameRepository
                    .findAll(pageable)
                    .stream()
                    .map(gameMapper::map)
                    .toList();
        } catch (DataAccessException exception) {
            logger.error("Error occurred during listing games", exception);
            throw new PersistenceException();
        }
    }

    @Override
    public SessionDto startSession(Long sessionId) throws IOException, NotFoundException, PersistenceException, KubernetesException {
        try {
            SessionEntity sessionEntity = sessionRepository.findById(sessionId).orElseThrow(NotFoundException::new);

            if (sessionEntity.getStatus() != GameStatus.NEW) {
                throw new InvalidSessionStateException("Game is not in status 'NEW'");
            }

            if (sessionEntity.getGame().getMinCapacity() > sessionEntity.getSlots().stream().filter(x -> x.getPlayer() != null).count()) {
                throw new InvalidSessionStateException("Minimum of players not reached");
            }

            sessionEntity.setStatus(GameStatus.STARTED);
            sessionEntity.setUpdatedAt(Instant.now());
            sessionRepository.save(sessionEntity);

            // Start the container
            kubernetesService.startExecutionEngine(sessionEntity.getGame().getName(), sessionEntity.getId().toString());

            // Inform the execution engine
            try {
                gameStatePusherService.startGame(sessionMapper.map(sessionEntity));
            } catch (MessagingSetupException | MessagingException exception) {
                logger.error("Error occurred during rabbitmq setup of game", exception);

                // Setup failed, therefore changing status
                sessionEntity.setStatus(GameStatus.NEW);

                kubernetesService.stopExecutionEngine(sessionEntity.getGame().getName(), sessionEntity.getId().toString());

                return sessionMapper.map(sessionEntity);
            }

            SessionDto sessionDto = sessionMapper.map(sessionRepository.save(sessionEntity));
            frontendCommunicationHandler.sendRequest(messageMapper.toStartGameMessageForFrontend(sessionId.toString()));

            return sessionDto;
        } catch (DataAccessException exception) {
            logger.error("Error occurred during starting session", exception);
            throw new PersistenceException();
        }
    }

    @Override
    public SessionDto createSession(Long gameId, SessionDto sessionDto) throws NotFoundException, PersistenceException {
        Assert.notNull(gameId, "gameId must not be null");
        Assert.notNull(sessionDto, "sessionDto must not be null");

        GameEntity gameEntity = gameRepository.findById(gameId).orElseThrow(NotFoundException::new);
        SessionEntity sessionEntity = new SessionEntity();
        sessionEntity.setGame(gameEntity);
        sessionEntity.setStatus(GameStatus.NEW);
        sessionEntity.setCreatedAt(Instant.now());
        sessionEntity.setUpdatedAt(Instant.now());
        sessionEntity.setSlots(Collections.emptySet());
        sessionEntity.setJoinToken(RandomStringUtils.randomAlphabetic(JOIN_TOKEN_LENGTH));
        sessionEntity.setVisibility(sessionDto.getVisibility());

        try {
            return sessionMapper.map(sessionRepository.save(sessionEntity));
        } catch (DataAccessException exception) {
            logger.error("Error occurred during adding player", exception);
            throw new PersistenceException();
        }
    }

    @Override
    public GameDto updateGame(GameDto gameDto, String email) throws NotFoundException, PersistenceException, InvalidCreatorException {
        Assert.notNull(gameDto, "gameDto must not be null");

        try {
            GameEntity entity = gameRepository.findById(gameDto.getId()).orElseThrow(NotFoundException::new);
            PlayerEntity creator = playerRepository.findByEmail(email).orElseThrow(NotFoundException::new);
            if (!Objects.equals(creator.getId(), entity.getCreator().getId())) {
                logger.error("User is not creator of updating game");
                throw new InvalidCreatorException("User not allowed to update Game");
            }
            entity.setName(gameDto.getName());
            entity.setDescription(gameDto.getDescription());
            entity.setMaxCapacity(gameDto.getMaxCapacity());
            entity.setMinCapacity(gameDto.getMinCapacity());

            GameVersionEntity versionEntity = new GameVersionEntity();
            versionEntity.setSourceCode(gameDto.getSourceCode());
            versionEntity.setCreatedAt(Instant.now());
            versionEntity.setUpdatedAt(Instant.now());
            entity.addVersion(versionEntity);

            gameRepository.save(entity);

            return gameMapper.map(entity);
        } catch (DataAccessException exception) {
            logger.error("Error occurred during updating game", exception);
            throw new PersistenceException();
        }
    }

    @Override
    public SessionDto addPlayer(Long sessionId, Long playerId) throws PersistenceException, NotFoundException, InvalidRequestException {
        Assert.notNull(sessionId, "sessionId must not be null");
        Assert.notNull(playerId, "playerId must not be null");

        try {
            SessionEntity entity = sessionRepository.findById(sessionId).orElseThrow(NotFoundException::new);
            PlayerEntity playerEntity = playerRepository.findById(playerId).orElseThrow(NotFoundException::new);

            if (entity.getSlots().stream().filter(x -> x.getPlayer() != null).anyMatch(x -> x.getPlayer().getId().equals(playerEntity.getId()))) {
                throw new InvalidRequestException("Player already in the list");
            }

            if (entity.getSlots().stream().allMatch(x -> x.getPlayer() != null)) {
                if (entity.getSlots().size() >= entity.getGame().getMaxCapacity()) {
                    throw new InvalidRequestException("Game already full");
                }

                // Add new slot because all are full
                SlotEntity slotEntity = new SlotEntity();
                slotEntity.setPlayer(playerEntity);
                slotEntity.setSession(entity);
                slotEntity.setCreatedAt(Instant.now());
                slotEntity.setUpdatedAt(Instant.now());
                slotEntity.setSlotNumber((long) entity.getSlots().size());

                slotRepository.save(slotEntity);

                entity.getSlots().add(slotEntity);
            } else {
                // Fill next
                Optional<SlotEntity> slot = entity.getSlots().stream().filter(x -> x.getPlayer() == null).findFirst();
                assert slot.isPresent();

                slot.get().setPlayer(playerEntity);
            }

            sessionRepository.save(entity);

            return sessionMapper.map(entity);
        } catch (DataAccessException exception) {
            logger.error("Error occurred during adding player", exception);
            throw new PersistenceException();
        }
    }

    @Override
    public SessionDto removePlayer(Long sessionId, Long playerId) throws PersistenceException, NotFoundException {
        Assert.notNull(sessionId, "sessionId must not be null");
        Assert.notNull(playerId, "playerId must not be null");
        try {
            SessionEntity entity = sessionRepository.findById(sessionId).orElseThrow(NotFoundException::new);
            playerRepository.findById(playerId).orElseThrow(NotFoundException::new);

            for (SlotEntity slot : entity.getSlots()) {
                if (slot.getPlayer() != null && slot.getPlayer().getId().equals(playerId)) {
                    slot.setPlayer(null);
                    slotRepository.save(slot);
                    break;
                }
            }

            sessionRepository.save(entity);

            return sessionMapper.map(entity);
        } catch (DataAccessException exception) {
            logger.error("Error occurred during adding player", exception);
            throw new PersistenceException();
        }
    }

    @Override
    public SessionDto matchMaking(Long gameId, Long playerId) throws NotFoundException, PersistenceException {
        Assert.notNull(gameId, "gameId must not be null");
        Assert.notNull(playerId, "playerId must not be null");

        gameRepository.findById(gameId).orElseThrow(NotFoundException::new);

        List<SessionEntity> sessions = sessionRepository.findSessionEntitiesByStatus(GameStatus.NEW)
                .stream()
                .filter(sessionEntity -> sessionEntity.getGame().getId().equals(gameId))
                .filter(sessionEntity -> sessionEntity.getVisibility().equals(SessionVisibility.PUBLIC))
                .filter(sessionEntity -> sessionEntity.getSlots().stream().filter(y -> y.getPlayer() != null).count() < sessionEntity.getGame().getMaxCapacity())
                .collect(Collectors.toList());

        sessions.sort(Comparator.comparing(SessionEntity::getCreatedAt));

        if (sessions.isEmpty()) {
            SessionDto dto = SessionDto.builder()
                    .visibility(SessionVisibility.PUBLIC)
                    .build();

            return this.createSession(gameId, dto);
        } else {
            return sessionMapper.map(sessions.get(0));
        }
    }

    @Override
    public SessionDto getSessionByToken(String token) throws NotFoundException, PersistenceException {
        Assert.notNull(token, "token must not be null");

        try {
            SessionEntity entity = sessionRepository.findByJoinToken(token).orElseThrow(NotFoundException::new);
            return sessionMapper.map(entity);
        } catch (DataAccessException exception) {
            logger.error("Error occurred during fetching session by token", exception);
            throw new PersistenceException();
        }
    }
}
