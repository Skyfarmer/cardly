package at.ase.backend.service.impl;

import at.ase.backend.dto.GameDto;
import at.ase.backend.dto.PlayerDto;
import at.ase.backend.dto.SessionDto;
import at.ase.backend.exception.*;
import at.ase.backend.mapper.MessageMapper;
import at.ase.backend.mapper.SessionMapper;
import at.ase.backend.mapper.SlotMapper;
import at.ase.backend.messaging.Message;
import at.ase.backend.model.GameStatus;
import at.ase.backend.model.SessionEntity;
import at.ase.backend.model.SlotEntity;
import at.ase.backend.repository.SessionRepository;
import at.ase.backend.repository.SlotRepository;
import at.ase.backend.service.GameService;
import at.ase.backend.service.GameStatePusherService;
import at.ase.backend.service.KubernetesService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.dao.DataAccessException;
import org.springframework.util.Assert;

import java.io.IOException;

public class GameStatePusherServiceImpl implements GameStatePusherService {
    private static final String SESSION_WITH_ID_PREFIX = "session_with_id_";
    private final Logger logger = LoggerFactory.getLogger(GameStatePusherServiceImpl.class);
    private final Exchange exchangeRequests;
    private final Exchange exchangeResponses;
    private final MessageMapper messageMapper;
    private final AmqpTemplate messageProducer;
    private final RabbitAdmin rabbitAdmin;
    private final SlotRepository slotRepository;
    private final SlotMapper slotMapper;
    private final SessionRepository sessionRepository;
    private final SessionMapper sessionMapper;
    private final KubernetesService kubernetesService;

    public GameStatePusherServiceImpl(Exchange exchangeRequests,
                                      Exchange exchangeResponses,
                                      MessageMapper messageMapper,
                                      AmqpTemplate messageProducer,
                                      RabbitAdmin rabbitAdmin,
                                      SlotRepository slotRepository,
                                      SlotMapper slotMapper,
                                      SessionRepository sessionRepository,
                                      SessionMapper sessionMapper,
                                      KubernetesService kubernetesService) {
        Assert.notNull(exchangeRequests, "exchangeRequests must not be null");
        Assert.notNull(exchangeResponses, "exchangeResponses must not be null");
        Assert.notNull(messageMapper, "messageMapper must not be null");
        Assert.notNull(messageProducer, "messageProducer must not be null");
        Assert.notNull(rabbitAdmin, "rabbitAdmin must not be null");
        Assert.notNull(slotRepository, "slotRepository must not be null");
        Assert.notNull(slotMapper, "slotMapper must not be null");
        Assert.notNull(sessionRepository, "sessionRepository must not be null");
        Assert.notNull(sessionMapper, "sessionMapper must not be null");
        Assert.notNull(kubernetesService, "kubernetesService must not be null");
        this.exchangeRequests = exchangeRequests;
        this.exchangeResponses = exchangeResponses;
        this.messageMapper = messageMapper;
        this.messageProducer = messageProducer;
        this.rabbitAdmin = rabbitAdmin;
        this.slotRepository = slotRepository;
        this.slotMapper = slotMapper;
        this.sessionRepository = sessionRepository;
        this.sessionMapper = sessionMapper;
        this.kubernetesService = kubernetesService;
    }

    @Override
    public void startGame(SessionDto sessionDto) throws MessagingSetupException, MessagingException {
        Assert.notNull(sessionDto, "sessionDto must not be null");

        // Setup the binding and queue on rabbitmq
        try {
            setupQueueAndBinding(exchangeRequests, exchangeResponses, sessionDto);
        } catch (AmqpException e) {
            logger.error(e.toString());
            throw new MessagingSetupException();
        }

        // Send the first message to the exchange which will be
        // routed to the correct queue based on the `routingKey`.
        // The execution engine can then consume the message.

        try {
            String routingKey = "session." + sessionDto.getId().toString() + ".#";
            Message message = messageMapper.toStartGameMessage(sessionDto);
            ObjectMapper objectMapper = new ObjectMapper();
            String json = objectMapper.writeValueAsString(message);
            logger.info("Sending json with routing key '{}': {}", routingKey, json);
            messageProducer.convertAndSend(exchangeResponses.getName(), routingKey, json);
        } catch (AmqpException | JsonProcessingException e) {
            logger.error(e.toString());
            removeQueues(sessionDto);
            throw new MessagingException();
        }
    }

    @Override
    public void publishMove(String sessionId, String playerId, JsonNode data) throws MessagingException, NotFoundException, PersistenceException, IOException, MessagingSetupException, KubernetesException {
        String routingKey = "session." + sessionId + "." + playerId;

        SlotEntity entity = slotRepository.findBySessionIdAndPlayerId(Long.parseLong(sessionId), Long.parseLong(playerId))
                .orElseThrow(NotFoundException::new);

        if (data.at("/value/type").asText().contains("GameOver")) {
            logger.info("Win message detected, therefore ending game");
            shutdownSession(Long.parseLong(sessionId));
        }

        try {
            Message message = messageMapper.toUserMoveGameMessage(sessionId, slotMapper.map(entity), data);
            ObjectMapper objectMapper = new ObjectMapper();
            String json = objectMapper.writeValueAsString(message);
            logger.info("Sending json: {}", json);
            messageProducer.convertAndSend(exchangeResponses.getName(), routingKey, json);
        } catch (AmqpException | JsonProcessingException e) {
            logger.error(e.toString());
            throw new MessagingException();
        }
    }

    @Override
    public void stopGame(SessionDto dto) throws MessagingSetupException {
        logger.info("Stopping game with the id '{}'", dto.getGame().getId());

        try {
            removeQueues(dto);
        } catch (AmqpException exception) {
            logger.error(exception.toString());
            throw new MessagingSetupException();
        }
    }

    @Override
    public SessionDto shutdownSession(Long sessionId) throws NotFoundException, IOException, PersistenceException, MessagingSetupException, KubernetesException {
        SessionDto dto;
        try {
            SessionEntity entity = sessionRepository.findById(sessionId).orElseThrow(NotFoundException::new);
            entity.setJoinToken(null);
            entity.setStatus(GameStatus.FINISHED);
            dto = sessionMapper.map(sessionRepository.save(entity));
        } catch (DataAccessException exception) {
            logger.error("Error occurred during stopping session", exception);
            throw new PersistenceException();
        }

        stopGame(dto);
        kubernetesService.stopExecutionEngine(dto.getGame().getName(), dto.getId().toString());

        return dto;

    }

    /**
     * When a game is created, then also a queue will be created.
     * Every queue has a routing key which is the id of {@link GameDto}.
     * Messages are then published to an exchange. Based on a {@link Binding} (the binding has a routing key), the
     * exchange can route the message to the right queue.
     */
    private void setupQueueAndBinding(Exchange exchangeRequests, Exchange exchangeResponses, SessionDto sessionDto) {
        Queue queue_responses = QueueBuilder.durable(SESSION_WITH_ID_PREFIX + sessionDto.getId() + "_responses")
                .withArgument("x-dead-letter-exchange", "")
                .withArgument("x-dead-letter-routing-key", "dlq.responses")
                .build();
        Binding binding_response = BindingBuilder.bind(queue_responses).to(exchangeResponses).with("session." + sessionDto.getId().toString() + ".#").noargs();
        rabbitAdmin.declareQueue(queue_responses);
        rabbitAdmin.declareBinding(binding_response);
    }

    private void removeQueues(SessionDto sessionDto) {
        rabbitAdmin.deleteQueue(SESSION_WITH_ID_PREFIX + sessionDto.getGame().getId() + "_responses");
    }
}
