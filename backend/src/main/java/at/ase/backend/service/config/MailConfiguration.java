package at.ase.backend.service.config;

import at.ase.backend.service.MailService;
import at.ase.backend.service.impl.MailServiceImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

@Configuration
public class MailConfiguration {
    @Value( "${mail.host:127.0.0.1}" )
    private String mailHost;

    @Value( "${mail.port:1025}" )
    private String mailPort;

    @Bean
    JavaMailSender javaMailSender() {
        JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();
        javaMailSender.setHost(mailHost);
        javaMailSender.setPort(Integer.parseInt(mailPort));
        return javaMailSender;
    }

    @Bean
    MailService mailService(JavaMailSender sender) {
        return new MailServiceImpl(sender);
    }
}
