package at.ase.backend.service.impl;

import at.ase.backend.service.MailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.util.Assert;

import java.lang.invoke.MethodHandles;

public class MailServiceImpl implements MailService {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(MethodHandles.lookup().lookupClass());
    private final JavaMailSender sender;

    public MailServiceImpl(JavaMailSender sender) {
        Assert.notNull(sender, "sender must not be null");

        this.sender = sender;
    }

    @Override
    public void sendPasswordForgottenEmail(String email, String refreshUrl) {
        LOGGER.info("Sending email for the password update");
        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setFrom("noreply@gameplatform.at");
        mail.setTo(email);
        mail.setSubject("Reset your Password");
        mail.setText("Reset your password at https://aseloves.us/#/reset-password" + refreshUrl);
        sender.send(mail);
    }
}
