package at.ase.backend.service;

import at.ase.backend.exception.KubernetesException;
import io.kubernetes.client.openapi.ApiException;

import java.io.IOException;

/**
 * The service interacts with the kubernetes api.
 */
public interface KubernetesService {
    /**
     * Creates a deployment for execution engine.
     *
     * @param gameName  must not be {@code null}.
     * @param sessionId must not be {@code null}.
     */
    void startExecutionEngine(String gameName, String sessionId) throws IOException, KubernetesException;

    /**
     * Deletes the deployment.
     *
     * @param gameName  must not be {@code null}.
     * @param sessionId must not be {@code null}.
     */
    void stopExecutionEngine(String gameName, String sessionId) throws IOException, KubernetesException;

    /**
     * Check if the pod has no restart counter.
     *
     * @param sessionId must not be {@code null}.
     * @return if the pod is alive
     */
    boolean isAlive(Long sessionId) throws ApiException;
}
