package at.ase.backend.service;

import at.ase.backend.dto.*;
import at.ase.backend.exception.InvalidRequestException;
import at.ase.backend.exception.NotFoundException;
import at.ase.backend.exception.PersistenceException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface PlayerService extends UserDetailsService {
    /**
     * Find a player email
     *
     * @param email must not be {@code null}.
     * @return a {@link PlayerDto}.
     * @throws NotFoundException    when user does not exist.
     * @throws PersistenceException when persisting the entity fails.
     */
    PlayerDto findPlayerByEmail(String email) throws NotFoundException, PersistenceException;

    /**
     *
     * @param id must not be {@code null}.
     * @return a {@link PlayerDto}.
     * @throws NotFoundException when user does not exist.
     * @throws PersistenceException when persisting the entity fails.
     */
    PlayerDto findPlayerById(Long id) throws NotFoundException, PersistenceException;

    /**
     * Resetting the login attempt counter for the given user.
     *
     * @param email must not be {@code null}.
     * @throws NotFoundException    when user does not exist.
     * @throws PersistenceException when persisting the entity fails.
     */
    void resetLoginAttempts(String email) throws NotFoundException, PersistenceException;

    /**
     * Creating a new player
     *
     * @param dto must not be {@code null}.
     * @return the created player
     * @throws NotFoundException    when user does not exist.
     * @throws PersistenceException when persisting the entity fails.
     */
    PlayerDto registerPlayer(CreatePlayerDto dto) throws PersistenceException, InvalidRequestException;

    /**
     * Request to send an email with a token to change the password.
     *
     * @param dto must not be {@code null}.
     */
    void requestPasswordForgottenEmail(PasswordForgottenDto dto) throws PersistenceException;

    /**
     * Change the password of the user.
     *
     * @param dto must not be {@code null}.
     */
    void resetPassword(PasswordResetDto dto) throws NotFoundException, PersistenceException, InvalidRequestException;

    /**
     * Change the player in the persistence layer.
     *
     * @param user must not be {@code null}.
     * @param dto  must not be {@code null}.
     * @return a {@link PlayerDto}.
     */
    PlayerDto updatePlayer(String user, PlayerDto dto) throws PersistenceException, NotFoundException, InvalidRequestException;

    /**
     * Change the player's password.
     *
     * @param name mut not be {@code null}.
     * @param dto  must not be {@code null}.
     * @return a {@link PlayerDto}.
     */
    PlayerDto updatePassword(String name, PasswordChangeDto dto) throws NotFoundException, InvalidRequestException, PersistenceException;
}
