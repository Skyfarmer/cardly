package at.ase.backend.service;

import at.ase.backend.exception.ImageStorageException;
import at.ase.backend.exception.NotFoundException;
import at.ase.backend.exception.PersistenceException;
import at.ase.backend.model.ImageScope;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.core.io.Resource;

import java.io.IOException;

public interface ImageStorageService {

    /**
     * Stores an image in the directory of the associated game.
     *
     * @param id    The id of the game the image belongs to
     * @param image An image that is to be stored
     * @return The relative path of the image (id/filename)
     */
    String storeImage(Long id, MultipartFile image, ImageScope scope) throws ImageStorageException, PersistenceException, NotFoundException, IOException;

    /**
     * Returns the specified image.
     *
     * @param id The id of the game the image belongs to
     * @param fileName The image that should be loaded
     * @return The image as Spring Resource
     */
    Resource loadImage(Long id, String fileName) throws NotFoundException, PersistenceException;

    /**
     * Deletes a stored image.
     *
     * @param id The id of the game the image belongs to
     * @param fileName The image that should be deleted
     */
    void deleteImage(Long id, String fileName) throws ImageStorageException, PersistenceException;
}
