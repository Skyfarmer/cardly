package at.ase.backend;

import at.ase.backend.mapper.MessageMapper;
import at.ase.backend.messaging.Message;
import at.ase.backend.model.GameStatus;
import at.ase.backend.model.SessionEntity;
import at.ase.backend.repository.SessionRepository;
import at.ase.backend.service.KubernetesService;
import at.ase.backend.socket.FrontendCommunicationHandler;
import io.jsonwebtoken.lang.Assert;
import io.kubernetes.client.openapi.ApiException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.Instant;
import java.util.List;

@Component
public class HealthCheckTask {
    private final Logger logger = LoggerFactory.getLogger(HealthCheckTask.class);

    private final SessionRepository sessionRepository;
    private final KubernetesService kubernetesService;
    private final FrontendCommunicationHandler frontendCommunicationHandler;
    private final MessageMapper messageMapper;

    public HealthCheckTask(SessionRepository sessionRepository,
                           KubernetesService kubernetesService,
                           FrontendCommunicationHandler frontendCommunicationHandler,
                           MessageMapper messageMapper) {
        Assert.notNull(sessionRepository, "sessionRepository must not be null");
        Assert.notNull(kubernetesService, "kubernetesService must not be null");
        Assert.notNull(frontendCommunicationHandler, "frontendCommunicationHandler must not be null");
        Assert.notNull(messageMapper, "messageMapper must not be null");
        this.sessionRepository = sessionRepository;
        this.kubernetesService = kubernetesService;
        this.frontendCommunicationHandler = frontendCommunicationHandler;
        this.messageMapper = messageMapper;
    }

    @Scheduled(fixedDelay = 60000)
    public void checkActiveSessions() throws IOException, ApiException {
        List<SessionEntity> sessions = sessionRepository.findSessionEntitiesByStatus(GameStatus.STARTED);

        List<SessionEntity> survived = sessions.stream()
                .peek(sessionEntity -> logger.debug("Received session: " + sessionEntity.getId()))
                .filter(sessionEntity -> sessionEntity.getUpdatedAt().isBefore(Instant.now().minusSeconds(30)))
                .peek(sessionEntity -> logger.debug("Filtered session: " + sessionEntity.getId())).toList();

        for (SessionEntity session :
                survived) {
            if(!kubernetesService.isAlive(session.getId())) {
                logger.warn("Session '{}' is not alive", session.getId());
                Message message = messageMapper.toGameCrashedMessageForFrontend(session.getId().toString());
                frontendCommunicationHandler.sendRequest(message);
            } else {
                logger.debug("Session '{}' is alive", session.getId());
            }
        }
    }
}
