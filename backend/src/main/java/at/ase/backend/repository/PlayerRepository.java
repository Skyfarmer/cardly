package at.ase.backend.repository;

import at.ase.backend.model.PlayerEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PlayerRepository extends JpaRepository<PlayerEntity, Long> {
    Optional<PlayerEntity> findByEmail(String email);
    Optional<PlayerEntity> findByPasswordResetToken(String token);
    Optional<PlayerEntity> findByAlias(String alias);
    Optional<PlayerEntity> findById(Long id);
}
