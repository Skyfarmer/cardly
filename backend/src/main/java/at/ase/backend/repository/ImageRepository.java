package at.ase.backend.repository;

import at.ase.backend.model.GameEntity;
import at.ase.backend.model.ImageEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

public interface ImageRepository extends JpaRepository<ImageEntity, Long> {

    Optional<ImageEntity> findByGameAndName(GameEntity game, String name);

    List<ImageEntity> findByGame(GameEntity game);

    @Modifying
    @Query("DELETE FROM ImageEntity i WHERE i.name = :name AND i.game.id = :gameId")
    void removeByNameAndGame(String name, Long gameId);
}
