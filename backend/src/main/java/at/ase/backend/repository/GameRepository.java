package at.ase.backend.repository;
import at.ase.backend.model.GameEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import java.util.List;

public interface GameRepository extends JpaRepository<GameEntity, Long> {
    List<GameEntity> findByNameContainingIgnoreCase(@Param("name") String name);

    List<GameEntity> findByCreator_Id(Long id);
}
