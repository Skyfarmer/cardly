package at.ase.backend.repository;

import at.ase.backend.model.GameStatus;
import at.ase.backend.model.SessionEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface SessionRepository extends JpaRepository<SessionEntity, Long> {
    List<SessionEntity> findSessionEntitiesByStatus(GameStatus status);

    Optional<SessionEntity> findByJoinToken(String token);
}
