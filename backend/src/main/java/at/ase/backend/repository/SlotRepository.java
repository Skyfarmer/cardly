package at.ase.backend.repository;

import at.ase.backend.model.SlotEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface SlotRepository extends JpaRepository<SlotEntity, Long> {
    Optional<SlotEntity> findBySessionIdAndPlayerId(Long sessionId, Long PlayerId);

}
