package at.ase.backend.messaging;

import com.fasterxml.jackson.databind.JsonNode;
import com.sun.istack.NotNull;
import lombok.*;

import java.io.Serializable;
import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Message implements Serializable {
    private Map<String, String> headers;

    private String key;

    @NotNull
    @NonNull
    private JsonNode value;
}
