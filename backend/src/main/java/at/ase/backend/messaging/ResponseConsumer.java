package at.ase.backend.messaging;

import at.ase.backend.socket.FrontendCommunicationHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class ResponseConsumer {
    private final Logger logger = LoggerFactory.getLogger(ResponseConsumer.class);

    private final FrontendCommunicationHandler frontendCommunicationHandler;

    public ResponseConsumer(FrontendCommunicationHandler frontendCommunicationHandler) {
        this.frontendCommunicationHandler = frontendCommunicationHandler;
    }

    @RabbitListener(queues="requests")
    public void receivedMessage(Message message) throws IOException {
        logger.debug("received message: " + message);

        try {
            frontendCommunicationHandler.sendRequest(message);
        } catch (Exception e) {
            logger.error(e.toString());
            throw new AmqpRejectAndDontRequeueException(e);
        }
    }
}
