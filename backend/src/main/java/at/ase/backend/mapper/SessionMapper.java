package at.ase.backend.mapper;

import at.ase.backend.dto.SessionDto;
import at.ase.backend.model.SessionEntity;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {GameMapper.class, PlayerMapper.class, SlotMapper.class}, injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface SessionMapper {
    @Mapping(source = "id", target = "id")
    @Mapping(source = "game", target = "game")
    @Mapping(source = "slots", target = "slots")
    @Mapping(source = "visibility", target = "visibility")
    @Mapping(source = "joinToken", target = "joinToken")
    SessionDto map(SessionEntity entity);

    @Mapping(source = "id", target = "id")
    @Mapping(source = "game", target = "game")
    @Mapping(source = "slots", target = "slots")
    @Mapping(source = "visibility", target = "visibility")
    @Mapping(source = "joinToken", target = "joinToken")
    SessionEntity map(SessionDto player);
}
