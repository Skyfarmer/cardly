package at.ase.backend.mapper;

import at.ase.backend.dto.GameDto;
import at.ase.backend.model.GameEntity;
import at.ase.backend.model.GameVersionEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.Comparator;
import java.util.Optional;

@Mapper(componentModel = "spring")
public interface GameMapper {
    @Mapping(target = "sourceCode", expression = "java(computeSourceCode(entity))")
    @Mapping(source = "minCapacity", target = "minCapacity")
    @Mapping(source = "maxCapacity", target = "maxCapacity")
    @Mapping(source = "images", target = "images")
    @Mapping(source = "creator", target = "creator")
    @Mapping(source = "description", target = "description")
    GameDto map(GameEntity entity);

    default String computeSourceCode(GameEntity entity) {
        Comparator<GameVersionEntity> comparing = Comparator
                .comparing(GameVersionEntity::getCreatedAt);
        Optional<GameVersionEntity> version = entity.getVersions().stream().max(comparing).stream().findFirst();

        if (version.isPresent()) {
            return version.get().getSourceCode();
        } else {
            return "";
        }
    }

    @Mapping(source = "minCapacity", target = "minCapacity")
    @Mapping(source = "maxCapacity", target = "maxCapacity")
    @Mapping(source = "images", target = "images")
    @Mapping(source = "creator", target = "creator")
    @Mapping(source = "description", target = "description")
    GameEntity map(GameDto game);
}
