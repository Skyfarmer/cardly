package at.ase.backend.mapper;

import at.ase.backend.dto.SlotDto;
import at.ase.backend.model.SlotEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface SlotMapper {
    @Mapping(source = "slotNumber", target = "slotNumber")
    SlotDto map(SlotEntity entity);
    SlotEntity map(SlotDto player);
    List<SlotEntity> map(List<SlotDto> players);
}
