package at.ase.backend.mapper;

import at.ase.backend.dto.PlayerDto;
import at.ase.backend.model.PlayerEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PlayerMapper {
    @Mapping(source = "id", target = "id")
    @Mapping(source = "email", target = "email")
    @Mapping(source = "alias", target = "alias")
    PlayerDto map(PlayerEntity entity);

    @Mapping(source = "id", target = "id")
    @Mapping(source = "email", target = "email")
    @Mapping(source = "alias", target = "alias")
    PlayerEntity map(PlayerDto player);
    List<PlayerEntity> map(List<PlayerDto> players);
}
