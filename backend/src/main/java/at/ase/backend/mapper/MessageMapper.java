package at.ase.backend.mapper;

import at.ase.backend.dto.PlayerDto;
import at.ase.backend.dto.SessionDto;
import at.ase.backend.dto.SlotDto;
import at.ase.backend.dto.messaging.*;
import at.ase.backend.messaging.Message;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.mapstruct.Mapper;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;

import java.util.HashMap;
import java.util.Map;

@Mapper(componentModel = "spring")
@Validated
public abstract class MessageMapper {

    private final ObjectMapper objectMapper;

    protected MessageMapper() {
        this.objectMapper = new ObjectMapper();
    }



    static final class MessageHeaders {
        public static final String DESTINATION_PLAYER_ID = "destination_player_id";
        public static String SESSION_ID = "session_id";
        public static String SOURCE_PLAYER_ID = "source_player_id";
    }

    /**
     * Creates a message that the game started.
     *
     * @param sessionDto must not be {@code null}.
     * @return a message.
     */
    public Message toStartGameMessage(SessionDto sessionDto) {
        Assert.notNull(sessionDto, "sessionDto must not be null");

        return Message.builder()
                .headers(createHeaders(sessionDto.getId().toString()))
                .key(sessionDto.getId().toString())
                .value(objectMapper.valueToTree(sessionDto))
                .build();
    }

    /**
     * Creates a message that indicates that the game has been started.
     *
     * @param sessionId must not be {@code null}.
     * @return a message.
     */
    public Message toStartGameMessageForFrontend(String sessionId) {
        Assert.notNull(sessionId, "sessionId must not be null");
        GameStartedDto dto = new GameStartedDto();

        return Message.builder()
                .headers(createHeaders(sessionId))
                .key(sessionId)
                .value(objectMapper.valueToTree(dto))
                .build();
    }

    /**
     * Creates a message that indicates that the game has crashed.
     *
     * @param sessionId must not be {@code null}.
     * @return a message.
     */
    public Message toGameCrashedMessageForFrontend(String sessionId) {
        Assert.notNull(sessionId, "sessionId must not be null");
        GameCrashedDto dto = new GameCrashedDto();

        return Message.builder()
                .headers(createHeaders(sessionId))
                .key(sessionId)
                .value(objectMapper.valueToTree(dto))
                .build();
    }

    /**
     * Creates a message that indicates that the game has been stopped.
     *
     * @param sessionId must not be {@code null}.
     * @return a message.
     */
    public Message toStoppedGameMessageForFrontend(String sessionId) {
        Assert.notNull(sessionId, "sessionId must not be null");
        GameStoppedDto dto = new GameStoppedDto();

        return Message.builder()
                .headers(createHeaders(sessionId))
                .key(sessionId)
                .value(objectMapper.valueToTree(dto))
                .build();
    }

    /**
     * Creates a message that indicates that the user has made a move.
     *
     * @param sessionId must not be {@code null}.
     * @param slotDto   must not be {@code null}.
     * @param data      must not be {@code null}.
     * @return a message.
     */
    public Message toUserMoveGameMessage(String sessionId, SlotDto slotDto, JsonNode data) {
        Assert.notNull(sessionId, "sessionId must not be null");
        Assert.notNull(slotDto, "slotDto must not be null");
        Assert.notNull(data, "data must not be null");

        Map<String, String> headers = new HashMap<>();
        headers.put(MessageHeaders.SESSION_ID, sessionId);
        headers.put(MessageHeaders.SOURCE_PLAYER_ID, slotDto.getSlotNumber().toString());

        return Message.builder()
                .headers(headers)
                .key(sessionId)
                .value(data)
                .build();
    }

    /**
     * Creates a message that indicates that a player has joined the game.
     *
     * @param sessionId  must not be {@code null}.
     * @param slotDto  must not be {@code null}.
     * @return a message.
     */
    public Message toPlayerJoinsGameMessage(String sessionId, SlotDto slotDto) {
        Assert.notNull(sessionId, "sessionId must not be null");
        Assert.notNull(slotDto, "slotDto must not be null");

        Map<String, String> headers = new HashMap<>();
        headers.put(MessageHeaders.SESSION_ID, sessionId);

        PlayerJoinDto dto = new PlayerJoinDto();
        dto.setSlotDto(slotDto);

        return Message.builder()
                .headers(headers)
                .key(sessionId)
                .value(objectMapper.valueToTree(dto))
                .build();
    }

    public Message toPlayerAlreadyJoined(String sessionId) {
        Assert.notNull(sessionId, "sessionId must not be null");

        Map<String, String> headers = new HashMap<>();
        headers.put(MessageHeaders.SESSION_ID, sessionId);

        PlayerAlreadyJoinedDto dto = new PlayerAlreadyJoinedDto();

        return Message.builder()
                .headers(headers)
                .key(sessionId)
                .value(objectMapper.valueToTree(dto))
                .build();
    }

    /**
     * Creates a message that indicates that a player has left the game.
     *
     * @param sessionId  must not be {@code null}.
     * @param slotDto  must not be {@code null}.
     * @return a message.
     */
    public Message toPlayerLeavesGameMessage(String sessionId, SlotDto slotDto) {
        Assert.notNull(sessionId, "sessionId must not be null");
        Assert.notNull(slotDto, "slotDto must not be null");

        Map<String, String> headers = new HashMap<>();
        headers.put(MessageHeaders.SESSION_ID, sessionId);

        PlayerLeaveDto dto = new PlayerLeaveDto();
        dto.setSlotDto(slotDto);

        return Message.builder()
                .headers(headers)
                .key(sessionId)
                .value(objectMapper.valueToTree(dto))
                .build();
    }

    /**
     * Create a message text that indicates that the game has stopped because someone left the session.
     *
     * @param sessionId must no be {@code null}.
     * @return a message.
     */
    public Message toGameStoppedMessage(String sessionId) {
        Assert.notNull(sessionId, "sessionId must not be null");

        Map<String, String> headers = new HashMap<>();
        headers.put(MessageHeaders.SESSION_ID, sessionId);
        headers.put(MessageHeaders.DESTINATION_PLAYER_ID, String.valueOf(-1));

        GameStoppedDto dto = new GameStoppedDto();

        return Message.builder()
                .headers(headers)
                .key(sessionId)
                .value(objectMapper.valueToTree(dto))
                .build();
    }

    private Map<String, String> createHeaders(String sessionId) {
        Map<String, String> headers = new HashMap<>();
        headers.put(MessageHeaders.SESSION_ID, sessionId);

        return headers;
    }
}
