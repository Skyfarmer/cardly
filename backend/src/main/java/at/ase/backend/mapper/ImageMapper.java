package at.ase.backend.mapper;


import at.ase.backend.dto.ImageDto;
import at.ase.backend.model.ImageEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ImageMapper {
    @Mapping(source = "id", target = "id")
    @Mapping(source = "name", target = "name")
    @Mapping(source = "scope", target = "scope")
    ImageDto map(ImageEntity imageEntity);

    @Mapping(source = "id", target = "id")
    @Mapping(source = "name", target = "name")
    @Mapping(source = "scope", target = "scope")
    ImageEntity map(ImageDto imageDto);
    List<ImageDto> map(List<ImageEntity> images);
}
