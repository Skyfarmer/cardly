package at.ase.backend.property;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Getter
@Setter
@ConfigurationProperties(prefix = "file")
public class ImageStorageProperties {
    // binds file properties to POJO
    private String uploadDir;
}
