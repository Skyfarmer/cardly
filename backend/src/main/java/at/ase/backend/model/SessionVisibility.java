package at.ase.backend.model;

/**
 * Defines if players can join with a link or with the matchmaking.
 */
public enum SessionVisibility {
    PRIVATE,
    PUBLIC
}
