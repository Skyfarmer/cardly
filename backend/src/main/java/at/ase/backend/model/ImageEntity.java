package at.ase.backend.model;


import lombok.*;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;

@Table(name = "image")
@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ImageEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private GameEntity game;

    @Column(nullable = false)
    private String name;


    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private ImageScope scope;

}
