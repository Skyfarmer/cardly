package at.ase.backend.model;

/**
 * Represents the state of a {@link GameEntity}.
 */
public enum GameStatus {
    NEW,
    STARTED,
    FINISHED
}
