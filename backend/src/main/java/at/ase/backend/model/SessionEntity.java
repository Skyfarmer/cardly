package at.ase.backend.model;

import lombok.*;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.Instant;
import java.util.Collection;
import java.util.Set;

@Table(name = "session")
@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SessionEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "created_at", nullable = false)
    @CreatedDate
    private Instant createdAt;

    @OneToMany(mappedBy = "session", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @OrderBy(value = "slotNumber ASC")
    private Set<SlotEntity> slots;

    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.STRING)
    private GameStatus status;

    @ManyToOne
    private GameEntity game;

    @Column(name = "visibility")
    @Enumerated(EnumType.STRING)
    private SessionVisibility visibility;

    @Column(name = "join_token", unique = true)
    private String joinToken;

    @Column(name = "updated_at", nullable = false)
    @UpdateTimestamp
    private Instant updatedAt;
}