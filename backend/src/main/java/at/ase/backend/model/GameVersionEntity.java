package at.ase.backend.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.Instant;

/**
 * {@code GameVersionEntity} stores the information for a certain version of the game.
 * The most important property of this entity is {@sourceCode} which stores the
 * lua executable which the {@code code-executor} will run asynchronously.
 */
@Table(name = "game_versions")
@Entity
@Getter
@Setter
public class GameVersionEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "version", nullable = false)
    private Long version = 0L;

    @Column(name = "source_code")
    private String sourceCode;

    @Column(name = "created_at", nullable = false)
    @CreatedDate
    private Instant createdAt;

    @Column(name = "updated_at", nullable = false)
    @UpdateTimestamp
    private Instant updatedAt;

    @ManyToOne(fetch = FetchType.LAZY)
    private GameEntity game;
}
