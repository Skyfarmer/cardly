package at.ase.backend.model;

/**
 * Represents the scope of a {@link ImageEntity}.
 */
public enum ImageScope {
    CARD,
    THUMBNAIL,
    PRESENTATION
}
