package at.ase.backend.model;

import lombok.*;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.Instant;
import java.util.*;

/**
 * {@code GameEntity} is the main data structure for a game. It will store
 * information which belongs to every version of the game.
 * If the user decides to update the game, we don't want to lose the information about
 * the old versions because there might be a running game.
 */
@Table(name = "game")
@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GameEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true, mappedBy = "game")
    @OrderBy("createdAt DESC")
    private Set<GameVersionEntity> versions = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true, mappedBy = "game")
    private Set<ImageEntity> images = new HashSet<>();

    @Column(name = "created_at", nullable = false)
    @CreatedDate
    private Instant createdAt;

    @Column(name = "updated_at", nullable = false)
    @UpdateTimestamp
    private Instant updatedAt;

    @Column(name = "min_capacity")
    private Integer minCapacity;

    @Column(name = "max_capacity")
    private Integer maxCapacity;

    @ManyToOne(fetch = FetchType.EAGER)
    private PlayerEntity creator;

    @Column(name = "description")
    private String description;

    public void addVersion(GameVersionEntity v) {
        if(this.versions == null) {
            this.versions = new HashSet<>();
        }

        this.versions.add(v);
        v.setGame(this);
    }
}
