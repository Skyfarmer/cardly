package at.ase.backend.errors;

import at.ase.backend.exception.*;
import io.kubernetes.client.openapi.ApiException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.time.Instant;

@ControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler({ApiException.class})
    protected ResponseEntity<ErrorResponse> handleKubernetesException(ApiException ex, WebRequest req) {
        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        ErrorResponse response = ErrorResponse
                .builder()
                .timestamp(Instant.now())
                .error(ex.getResponseBody())
                .message(null)
                .path(req.getContextPath())
                .status(status.value())
                .build();

        return new ResponseEntity<>(response, new HttpHeaders(), status);
    }

    @ExceptionHandler({ImageStorageException.class})
    protected ResponseEntity<ErrorResponse> handleImageStoreException(ImageStorageException ex, WebRequest req) {
        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        ErrorResponse response = ErrorResponse
                .builder()
                .timestamp(Instant.now())
                .error(ex.getMessage())
                .message(null)
                .path(req.getContextPath())
                .status(status.value())
                .build();

        return new ResponseEntity<>(response, new HttpHeaders(), status);
    }

    @ExceptionHandler({InvalidListingConfigurationException.class})
    protected ResponseEntity<ErrorResponse> handleInvalidListingConfigurationException(InvalidListingConfigurationException ex, WebRequest req) {
        HttpStatus status = HttpStatus.BAD_REQUEST;
        ErrorResponse response = ErrorResponse
                .builder()
                .timestamp(Instant.now())
                .error(ex.getMessage())
                .message(null)
                .path(req.getContextPath())
                .status(status.value())
                .build();

        return new ResponseEntity<>(response, new HttpHeaders(), status);
    }

    @ExceptionHandler({InvalidSessionStateException.class})
    protected ResponseEntity<ErrorResponse> handleInvalidSessionStateException(InvalidSessionStateException ex, WebRequest req) {
        HttpStatus status = HttpStatus.BAD_REQUEST;
        ErrorResponse response = ErrorResponse
                .builder()
                .timestamp(Instant.now())
                .error(ex.getMessage())
                .message(null)
                .path(req.getContextPath())
                .status(status.value())
                .build();

        return new ResponseEntity<>(response, new HttpHeaders(), status);
    }

    @ExceptionHandler({NotFoundException.class})
    protected ResponseEntity<ErrorResponse> handleNotFoundException(NotFoundException ex, WebRequest req) {
        HttpStatus status = HttpStatus.NOT_FOUND;
        ErrorResponse response = ErrorResponse
                .builder()
                .timestamp(Instant.now())
                .error(ex.getMessage())
                .message(null)
                .path(req.getContextPath())
                .status(status.value())
                .build();

        return new ResponseEntity<>(response, new HttpHeaders(), status);
    }

    @ExceptionHandler({InvalidRequestException.class})
    protected ResponseEntity<ErrorResponse> handleInvalidRequest(InvalidRequestException ex, WebRequest req) {
        HttpStatus status = HttpStatus.BAD_REQUEST;
        ErrorResponse response = ErrorResponse
                .builder()
                .timestamp(Instant.now())
                .error(ex.getMessage())
                .message(null)
                .path(req.getContextPath())
                .status(status.value())
                .build();

        return new ResponseEntity<>(response, new HttpHeaders(), status);
    }

    @ExceptionHandler({KubernetesException.class})
    protected ResponseEntity<ErrorResponse> handleKubernetesException(KubernetesException ex, WebRequest req) {
        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        ErrorResponse response = ErrorResponse
                .builder()
                .timestamp(Instant.now())
                .error(ex.getMessage())
                .message(null)
                .path(req.getContextPath())
                .status(status.value())
                .build();

        return new ResponseEntity<>(response, new HttpHeaders(), status);
    }
}
