package at.ase.backend.security;

import at.ase.backend.dto.PlayerDto;
import at.ase.backend.dto.PlayerLoginDto;
import at.ase.backend.service.PlayerService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Validation;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.List;
import java.util.stream.Collectors;

public class JwtAuthenticationFilter extends
        UsernamePasswordAuthenticationFilter {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private final AuthenticationManager authenticationManager;
    private final JwtTokenizer jwtTokenizer;
    private final PlayerService playerService;

    /**
     * Attempts authorization with a jwt token.
     *
     * @param authenticationManager used to check token
     * @param jwtTokenizer          creates jwt tokens
     * @param playerService         used to track login attempts
     */
    public JwtAuthenticationFilter(AuthenticationManager authenticationManager,
                                   JwtTokenizer jwtTokenizer, PlayerService playerService) {
        this.authenticationManager = authenticationManager;
        this.jwtTokenizer = jwtTokenizer;
        setFilterProcessesUrl("/api/v1/login");
        this.playerService = playerService;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request,
                                                HttpServletResponse response)
            throws AuthenticationException {
        try {
            PlayerLoginDto user = new ObjectMapper()
                    .readValue(request.getInputStream(), PlayerLoginDto.class);

            logger.info(String.format("Trying to authenticate user '%s'", user.getEmail()));

            if (!Validation.buildDefaultValidatorFactory().getValidator().validate(user).isEmpty()) {
                throw new InsufficientAuthenticationException("invalid login data");
            }

            return authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(user.getEmail(), user.getPassword()));
        } catch (IOException error) {
            throw new BadCredentialsException("Wrong API request or JSON schema", error);
        }
    }

    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request,
                                              HttpServletResponse response,
                                              AuthenticationException failed) throws IOException {
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        response.getWriter().write(failed.getMessage());
        LOGGER.debug("Invalid authentication attempt: {}", failed.getMessage());
    }

    @SneakyThrows
    @Override
    protected void successfulAuthentication(HttpServletRequest request,
                                            HttpServletResponse response,
                                            FilterChain chain,
                                            Authentication authResult) throws IOException {
        User user = ((User) authResult.getPrincipal());
        PlayerDto applicationUser = playerService.findPlayerByEmail(user.getUsername());

        List<String> roles = user.getAuthorities()
                .stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());

        playerService.resetLoginAttempts(user.getUsername());
        String token = jwtTokenizer.getAuthToken(user.getUsername(), roles, applicationUser.getId());
        response.addHeader("token", token);
        response.getWriter()
                .write(token);
        LOGGER.info("Successfully authenticated user {}", user.getUsername());
    }
}
