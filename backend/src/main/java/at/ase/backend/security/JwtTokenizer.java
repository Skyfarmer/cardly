package at.ase.backend.security;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
public class JwtTokenizer {

    private final SecurityConfiguration securityConfiguration;

    public JwtTokenizer(SecurityConfiguration securityConfiguration) {
        this.securityConfiguration = securityConfiguration;
    }

    /**
     * create a jwt token for a given user with set of roles.
     *
     * @param user  User
     * @param roles roles that apply to user
     * @param id    of the user
     * @return Jwt Token
     */
    public String getAuthToken(String user, List<String> roles, Long id) {
        byte[] signingKey = securityConfiguration.getJwt().getSecret().getBytes();
        String token = Jwts.builder()
                .signWith(Keys.hmacShaKeyFor(signingKey), SignatureAlgorithm.HS512)
                .setHeaderParam("type", securityConfiguration.getJwt().getType())
                .setIssuer(securityConfiguration.getJwt().getIssuer())
                .setAudience(securityConfiguration.getJwt().getAudience())
                .setSubject(user)
                .setExpiration(new Date(System.currentTimeMillis() + securityConfiguration.getJwt().getExpirationTime()))
                .claim("rol", roles)
                .claim("id", id)
                .compact();
        return token;
    }
}
