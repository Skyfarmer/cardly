package at.ase.backend.security;

import at.ase.backend.service.PlayerService;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.lang.Assert;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    private final PlayerService playerService;
    private final PasswordEncoder passwordEncoder;
    private final JwtTokenizer jwtTokenizer;
    private final JwtParser jwtParser;

    public WebSecurityConfig(PlayerService playerService,
                             PasswordEncoder passwordEncoder,
                             JwtTokenizer jwtTokenizer,
                             JwtParser jwtParser) {
        Assert.notNull(playerService, "playerService must not be null");
        Assert.notNull(passwordEncoder, "passwordEncoder must not be null");
        Assert.notNull(jwtTokenizer, "jwtTokenizer must not be null");
        Assert.notNull(jwtParser, "jwtParser must not be null");

        this.playerService = playerService;
        this.passwordEncoder = passwordEncoder;
        this.jwtTokenizer = jwtTokenizer;
        this.jwtParser = jwtParser;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .cors().and()
                .csrf().disable()
                .authorizeRequests()
                .antMatchers(HttpMethod.POST, "/api/v1/login", "/api/v1/register", "/api/v1/requestEmail", "/api/v1/resetPassword")
                .permitAll()
                .antMatchers(HttpMethod.OPTIONS, "**")
                .permitAll()
                .antMatchers(HttpMethod.GET, "/api/v1/game/*/image/**")
                .permitAll()
                .antMatchers("/api/session/*/player/*")
                .permitAll()
                .antMatchers(HttpMethod.GET, "/api/v1/games")
                .permitAll()
                .antMatchers(HttpMethod.GET, "/api/v1/game/**")
                .permitAll()
                .and()
                .authorizeRequests()
                .anyRequest()
                .authenticated()
                .and()
                .addFilter(new JwtAuthenticationFilter(authenticationManager(), jwtTokenizer, playerService))
                .addFilter(new JwtAuthorizationFilter(authenticationManager(), jwtParser))
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(playerService).passwordEncoder(passwordEncoder);
    }
}
