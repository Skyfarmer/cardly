package at.ase.backend.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.List;
import java.util.stream.Collectors;

public class JwtAuthorizationFilter extends BasicAuthenticationFilter {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(MethodHandles.lookup().lookupClass());
    private static final String AUTHENTICATION_HEADER = "Authentication";
    public static final String BEARER_PREFIX = "Bearer ";
    private final JwtParser jwtParser;

    public JwtAuthorizationFilter(AuthenticationManager authenticationManager,
                                  JwtParser jwtParser) {
        super(authenticationManager);
        this.jwtParser = jwtParser;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        try {
            UsernamePasswordAuthenticationToken authToken = getAuthToken(request);
            SecurityContextHolder.getContext().setAuthentication(authToken);
        } catch (IllegalArgumentException | JwtException error) {
            LOGGER.debug("Invalid authorization attempt: {}", error.getMessage());
        }
        chain.doFilter(request, response);
    }

    private UsernamePasswordAuthenticationToken getAuthToken(
            HttpServletRequest request)
            throws JwtException, IllegalArgumentException {
        String token = request.getHeader(AUTHENTICATION_HEADER);

        if (token == null || token.isEmpty() || !token
                .startsWith(BEARER_PREFIX)) {
            throw new IllegalArgumentException(
                    "Authorization header is malformed or missing");
        }

        Claims claims = jwtParser
                .parseClaimsJws(
                        token.replace(BEARER_PREFIX, ""))
                .getBody();

        String username = claims.getSubject();

        if (username == null || username.isEmpty()) {
            throw new IllegalArgumentException("Token contains no user");
        }

        List<SimpleGrantedAuthority> authorities = ((List<?>) claims
                .get("rol")).stream()
                .map(authority -> new SimpleGrantedAuthority((String) authority))
                .collect(Collectors.toList());


        return new UsernamePasswordAuthenticationToken(username,
                null, authorities);
    }
}
