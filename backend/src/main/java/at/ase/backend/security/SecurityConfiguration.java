package at.ase.backend.security;

import at.ase.backend.security.properties.JwtProperties;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;
import lombok.Getter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.nio.charset.StandardCharsets;

@Configuration
@Getter
public class SecurityConfiguration {
    private final JwtProperties jwt;

    public SecurityConfiguration(JwtProperties jwt) {
        this.jwt = jwt;
    }

    @Bean
    public static PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public JwtParser jwtParser() {
        return Jwts.parser()
                .setSigningKey(this.jwt.getSecret().getBytes(StandardCharsets.UTF_8));
    }
}
