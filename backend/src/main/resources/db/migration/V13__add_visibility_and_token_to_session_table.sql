ALTER TABLE session ADD COLUMN visibility TEXT NOT NULL DEFAULT 'PUBLIC';
ALTER TABLE session ADD COLUMN join_token TEXT;
ALTER TABLE session ADD CONSTRAINT session_join_token_unique UNIQUE (join_token);
