ALTER TABLE player ADD COLUMN password_reset_token TEXT;
ALTER TABLE player ADD CONSTRAINT player_password_reset_token UNIQUE (password_reset_token);
