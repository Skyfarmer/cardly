DROP TABLE IF EXISTS session_player;

CREATE TABLE slot
(
    id          SERIAL    NOT NULL,
    player_id   BIGINT    NOT NULL,
    session_id  BIGINT    NOT NULL,
    slot_number INT       NOT NULL,
    created_at  TIMESTAMP NOT NULL,
    updated_at  TIMESTAMP NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_session_player_player_id
        FOREIGN KEY (player_id)
            REFERENCES player (id)
            ON UPDATE CASCADE
            ON DELETE CASCADE,
    CONSTRAINT fk_session_player_session_id
        FOREIGN KEY (session_id)
            REFERENCES session (id)
            ON UPDATE CASCADE
            ON DELETE CASCADE
);