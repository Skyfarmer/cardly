DROP TABLE image;

CREATE TABLE image
(
  id            serial PRIMARY KEY,
  game_id       BIGINT REFERENCES game (id) ON UPDATE CASCADE ON DELETE CASCADE,
  name          TEXT NOT NULL,
  scope         TEXT NOT NULL
);