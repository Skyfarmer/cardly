ALTER TABLE player
    ADD COLUMN banned boolean DEFAULT FALSE;
ALTER TABLE player
    ADD COLUMN loginAttempts INTEGER DEFAULT 0;
ALTER TABLE player
    ADD COLUMN email TEXT;
ALTER TABLE player
    ADD COLUMN password TEXT;
ALTER TABLE player
    ADD UNIQUE (email);