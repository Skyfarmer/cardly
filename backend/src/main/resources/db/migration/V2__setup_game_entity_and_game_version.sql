CREATE TABLE game
(
    id         serial PRIMARY KEY,
    name       VARCHAR(255) NOT NULL,
    created_at TIMESTAMP    NOT NULL,
    updated_at TIMESTAMP    NOT NULL
);

CREATE TABLE game_versions
(
    id          serial PRIMARY KEY,
    source_code TEXT      NOT NULL,
    version     INT       NOT NULL DEFAULT 0,
    game_id     INT       NOT NULL,
    created_at  TIMESTAMP NOT NULL,
    updated_at  TIMESTAMP NOT NULL,
    CONSTRAINT fk_game_version_game_id
        FOREIGN KEY (game_id)
            REFERENCES game (id)
            ON UPDATE CASCADE
            ON DELETE CASCADE
);