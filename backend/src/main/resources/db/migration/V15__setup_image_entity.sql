CREATE TABLE image
(
  id            serial PRIMARY KEY,
  game          INT REFERENCES game (id) ON UPDATE CASCADE ON DELETE CASCADE,
  name          VARCHAR(255) NOT NULL,
  scope         TEXT NOT NULL
 );