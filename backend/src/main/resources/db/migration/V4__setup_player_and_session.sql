CREATE TABLE player
(
    id         serial PRIMARY KEY,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL
);

CREATE TABLE session
(
    id         serial PRIMARY KEY,
    status     TEXT      NOT NULL,
    game_id    BIGINT NOT NULL REFERENCES game (id) ON UPDATE CASCADE ON DELETE CASCADE,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL
);

CREATE TABLE session_player
(
    player_id  BIGINT NOT NULL,
    session_id BIGINT NOT NULL,
    PRIMARY KEY (player_id, session_id),
    CONSTRAINT fk_session_player_player_id
        FOREIGN KEY (player_id)
            REFERENCES player (id)
            ON UPDATE CASCADE
            ON DELETE CASCADE,
    CONSTRAINT fk_session_player_session_id
        FOREIGN KEY (session_id)
            REFERENCES session (id)
            ON UPDATE CASCADE
            ON DELETE CASCADE
);

ALTER TABLE game
    DROP COLUMN status;
