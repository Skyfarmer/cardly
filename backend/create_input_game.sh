#!/usr/bin/env bash

ID=100
NAME=my_game
uno=`cat ../execution-engine/games/uno.lua | base64`

JSON_STRING=$( jq -R -n \
                  --arg id "$ID" \
                  --arg name "$NAME" \
                  --arg min_capacity 2 \
                  --arg max_capacity 2 \
                  --arg description "description" \
                  --arg source_code "$uno" \
                  '{id: $id | tonumber, name: $name, source_code: $source_code, min_capacity: $min_capacity, max_capacity: $max_capacity, description: $description }' )

echo $JSON_STRING | tr -d '\n' > input_game.json
