export const environment = {
  production: true,
  backendBaseUrl: 'https://aseloves.us/api/v1',
  backendWsUrl: 'wss://aseloves.us/api/',
  joinSessionBaseUrl: 'https://aseloves.us/#/?gameToken='
};
