import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {GameService} from "../services/game.service";
import {CommunicationService} from "../services/communication.service";
import {GamePlayService} from "../services/game-play.service";
import {ActivatedRoute, Router} from "@angular/router";
import {SessionDTO} from "../models/sessionDTO";
import {ImageService} from "../services/image.service";
import {AuthenticationService} from "../services/authentication.service";
import {LobbyStateService} from "../services/lobby.state.service";
import {ToastService} from "angular-toastify";

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit, OnDestroy {

  socketMode: boolean = true;
  session: SessionDTO | undefined;
  playerId: string = "";
  gameId: number | undefined;
  sessionId: number | undefined;

  constructor(private gameService: GameService,
              private communicationService: CommunicationService,
              private route: ActivatedRoute,
              private router: Router,
              private toastService: ToastService,
              private lobbyStateService: LobbyStateService,
              private gamePlayService: GamePlayService,
              private imageService: ImageService,
              private authService: AuthenticationService) {
  }

  ngOnDestroy(): void {
    console.log("Stopping socket connection");
    this.gamePlayService.stopSocketConnection();

    //Turn scrolling back on
    document.body.classList.remove("no-scroll-body")

    this.lobbyStateService.sessionId = -1;
  }

  ngOnInit(): void {
    this.initGame()
  }

  initGame() {
    //Turn off scrolling
    if (!document.getElementById("no-scroll-body")) {
      let style = document.createElement("style")
      style.id = "no-scroll-body";
      style.innerHTML = ".no-scroll-body { height: 100%; overflow: hidden; }"
      document.getElementsByTagName("head")[0].appendChild(style);
    }
    document.body.classList.add("no-scroll-body")


    if (!this.authService.isLoggedIn()) {
      throw new Error("User is not logged in");
    } else {
      this.playerId = this.authService.getPlayerId()!;
    }

    if(this.lobbyStateService.sessionId == -1)  {
      this.toastService.dismissAll();
      this.toastService.info("Game has ended");
      this.router.navigate(["/"]);
    }

    this.gameService.getSession(this.lobbyStateService.sessionId).subscribe((session) => {
      this.session = session;
    });
  }
}
