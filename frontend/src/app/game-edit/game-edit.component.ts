import {Component, OnInit} from '@angular/core';
import {GameService} from "../services/game.service";
import {ActivatedRoute, Router} from "@angular/router";
import {ImageService} from "../services/image.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {PlayerService} from "../services/player.service";
import {GameDTO} from "../models/gameDTO";
import {PlayerDTO} from "../models/playerDTO";
import {environment} from "../../environments/environment";
import {CARD_IMAGE_EXTENSION} from "../elements/card";
import {ImageScopes} from "../services/ImageScopes";
import {ImageDTO} from "../models/imageDTO";
import {ToastService} from "angular-toastify";

@Component({
  selector: 'app-game-edit',
  templateUrl: './game-edit.component.html',
  styleUrls: ['./game-edit.component.scss']
})
export class GameEditComponent implements OnInit {

  game: GameDTO | undefined;
  gameId: number | undefined;
  player: PlayerDTO | undefined;
  editable: boolean = false
  images: any[] = []
  cards: any[] = []
  thumbnail: any
  sourceCode: any
  luaEnding: string = ".lua"

  gameEditForm: FormGroup = this.formBuilder.group({
    name: ['', [Validators.required]],
    description: ['', [Validators.required]],
    playerCount: ['', [Validators.required]],
    sourceCode: [''],
  });
  thumbForm: FormGroup = this.formBuilder.group({
    thumbnail: ['']
  })

  constructor(private gameService: GameService, private route: ActivatedRoute, private imageService: ImageService,
              private formBuilder: FormBuilder, private router: Router,
              private playerService: PlayerService,
              private toastService: ToastService) {
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.gameId = parseInt(<string>params["gameId"]);
      if (!Number.isNaN(this.gameId)) {
        this.editable = true
        this.gameService.getGameById(this.gameId).subscribe((game) => {
            this.game = game;
            this.initForm()
            this.getGameImages(this.game)
            this.playerService.getPlayerInformation(Number(localStorage.getItem("playerId"))).subscribe((player) => {
              if(player.id !== this.game?.creator.id) {
                this.toastService.error("User not allowed to update game")
                this.router.navigate(['/profile/user-data'])
              }
            }, error => {
              console.log(error)
              this.toastService.error("Error fetching user information")
              this.router.navigate(['/profile/user-data'])
            })
          },
          error => {
            console.error(error)
            this.toastService.error("Error fetching game")
            this.router.navigate(['/profile/user-data'])
          })
      }
    })
  }

  initForm() {
    this.gameEditForm.controls['name'].setValue(this.game!.name)
    this.gameEditForm.controls['description'].setValue(this.game!.description)
    this.gameEditForm.controls['playerCount'].setValue(this.game!.max_capacity)
  }

  getGameImages(game: GameDTO) {
    game.images.forEach((image) => {
      let url = environment.backendBaseUrl + "/game/" + game.id + "/image/" + image.name
      fetch(url).then(async response => {
        const blob = await response.blob()
        const file = new File([blob], image.name, {type: "image/png"})
        if (image.scope === ImageScopes.CARD) {
          this.cards.push(file)
        } else if (image.scope === ImageScopes.PRESENTATION) {
          this.images.push(file)
        }
      })
    })
  }

  getGameThumbnailUrl(gameId: number) {
    return environment.backendBaseUrl + "/game/" + gameId + "/image/" + "thumbnail" + CARD_IMAGE_EXTENSION
  }

  onSelectImages(event: any) {
    this.images.push(...event.addedFiles);
  }

  onSelectCards(event: any) {
    this.cards.push(...event.addedFiles);
  }

  onRemoveCard(event: any) {
    if (this.game !== undefined) {
      this.cards.splice(this.cards.indexOf(event), 1);
      this.imageService.deleteImage(this.game.id, event.name).subscribe((data) => {
        console.log("Deleted image: ", event.name)
      }, error => console.error(error))
    } else {
      this.cards.splice(this.cards.indexOf(event), 1);
    }
  }

  onRemoveImage(event: any) {
    if (this.game !== undefined) {
      this.images.splice(this.images.indexOf(event), 1);
      this.imageService.deleteImage(this.game.id, event.name).subscribe((data) => {
        console.log("Deleted image: ", event.name)
      }, error => console.error(error))
    } else {
      this.images.splice(this.images.indexOf(event), 1);
    }
  }

  onFileChangedThumbnail(event: any) {
    let file = event.target.files[0]
    if (file !== null) {
      this.thumbnail = file
    } else {
      console.log("No file selected")
    }
  }

  onFileChangedSourceCode(event: any) {
    let file = event.target.files[0]
    if (file !== null) {
      this.sourceCode = file
    } else {
      console.log("No file selected")
    }
  }

  saveGameDetails() {
    const name = this.gameEditForm.controls['name']?.value;
    const description = this.gameEditForm.controls['description']?.value;
    const playerCount = this.gameEditForm.controls['playerCount']?.value;
    if(this.sourceCode.name.substring(this.sourceCode.name.length - this.luaEnding.length) === this.luaEnding) {
      if (this.sourceCode !== undefined) {
        this.toBase64(this.sourceCode).then((data) => {
          this.updateGame(data as string, name, description, playerCount)
        })
      } else {
        if (this.game !== undefined) {
          this.updateGame(this.game.source_code, name, description, playerCount)
        } else {
          console.log("Error updating fetched game")
          this.toastService.error("Error updating game")
        }
      }
    } else {
      this.toastService.error("File is not a lua code file")
    }


  }

  updateGame(data: string, name: string, description: string, playerCount: number) {
    this.playerService.getPlayerInformation(Number(localStorage.getItem("playerId"))).subscribe((player) => {
      this.player = player
      if (this.game !== undefined) {
        let tmpGame = this.createTmpGame(name, data, playerCount, description,
          this.player.id, player.email, player.alias)
        if (this.game.creator.id === this.player.id) {
          tmpGame.id = this.game.id
          this.gameService.updateGame(tmpGame).subscribe((game) => {
            this.game = game
            this.toastService.success("Game update successful")
          }, (error) => {
            console.error(error)
            this.toastService.error("Game update failed")
          })
        }
      } else {
        this.toastService.error("User not allowed to update game")
      }
    }, error => {
      console.log(error)
      this.toastService.error("Game update failed")
    })
  }

  saveImageResources() {
    this.uploadThumbnail()
    this.uploadCards()
    this.uploadImages()
  }

  createTmpGame(gameName: string, sourceCode: string, capacity: number, description: string,
                creatorId: number, creatorEmail: string, creatorAlias: string) {
    let tmpGame: GameDTO = {
      id: 1,
      name: gameName,
      source_code: sourceCode,
      min_capacity: capacity,
      max_capacity: capacity,
      description: description,
      creator: {
        id: creatorId,
        email: creatorEmail,
        alias: creatorAlias
      },
      images: new Set<ImageDTO>()
    }
    return tmpGame
  }

  uploadCards() {
    if (this.cards.length !== 0 && this.game !== undefined) {
      let formData: FormData = this.appendFilesToFormData(this.cards)
      this.imageService.uploadMultipleImages(this.game.id, formData, ImageScopes.CARD).subscribe((imageResponse) => {
        this.toastService.success("Card upload successful")
      }, (error) => {
        console.log(error)
        this.toastService.error("Card upload failed")
      })
    }
  }

  uploadImages() {
    if (this.images.length !== 0 && this.game !== undefined) {
      let formData: FormData = this.appendFilesToFormData(this.images)
      this.imageService.uploadMultipleImages(this.game.id, formData, ImageScopes.PRESENTATION).subscribe((imageResponse) => {
        this.toastService.success("Image upload successful")
      }, (error) => {
        console.log(error)
        this.toastService.error("Image upload failed")
      })
    }
  }

  uploadThumbnail() {
    if (this.thumbnail !== undefined && this.game !== undefined) {
      const formData: FormData = new FormData();
      formData.append("file", this.thumbnail)
      this.imageService.uploadImage(this.game.id, formData, ImageScopes.THUMBNAIL).subscribe((image) => {
        this.toastService.success("Thumbnail upload successful")
      }, (error) => {
        console.log(error)
        this.toastService.error("Thumbnail upload failed")
      })
    }
  }


  toBase64(file: any) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      // @ts-ignore
      reader.onload = () => resolve(reader.result.split(',')[1]);
      reader.onerror = error => reject(error);
    })
  }

  appendFilesToFormData(files: any[]): FormData {
    const formData: FormData = new FormData();
    if (files.length !== 0) {
      for (let i = 0; i < files.length; i++) {
        formData.append("files", files[i]);
      }
    } else {
      console.log("No files to upload")
    }
    return formData
  }
}
