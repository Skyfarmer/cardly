import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestEmailComponent } from './request-email.component';
import {FormBuilder} from "@angular/forms";
import {Router} from "@angular/router";
import {AuthenticationService} from "../services/authentication.service";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {RouterTestingModule} from "@angular/router/testing";

describe('RequestEmailComponent', () => {
  let component: RequestEmailComponent;
  let fixture: ComponentFixture<RequestEmailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RequestEmailComponent ],
      imports: [HttpClientTestingModule,RouterTestingModule],
      providers: [FormBuilder, AuthenticationService]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestEmailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
