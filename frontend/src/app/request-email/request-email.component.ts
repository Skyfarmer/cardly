import {Component} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthenticationService} from "../services/authentication.service";
import {RequestEmailDto} from "../models/requestEmailDto";
import {Router} from "@angular/router";
import {ToastService} from "angular-toastify";

@Component({
  selector: 'app-request-email',
  templateUrl: './request-email.component.html',
  styleUrls: ['./request-email.component.scss']
})
export class RequestEmailComponent {

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private toastService: ToastService,
              private authenticationService: AuthenticationService) {
  }

  form: FormGroup = this.formBuilder.group({
    email: ['', [Validators.required, Validators.email]],
  });


  onSubmit() {
    const email = this.form.controls['email']?.value;

    let dto: RequestEmailDto = {
      email: email,
    };

    this.authenticationService.requestEmail(dto).subscribe((data) => {
      console.log('Requesting email', data);
      this.router.navigate(['/'], {});
      this.toastService.info("Email has been sent if the account exists")
    }, error => console.error(error));
  }
}
