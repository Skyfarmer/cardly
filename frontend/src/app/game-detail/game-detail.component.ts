import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {GameService} from "../services/game.service";
import {GameDTO} from "../models/gameDTO";
import {environment} from "../../environments/environment";
import {CARD_IMAGE_EXTENSION} from "../elements/card";
import {AuthenticationService} from "../services/authentication.service";
import {ImageScopes} from "../services/ImageScopes";
import {SessionDTO, SessionVisibility} from "../models/sessionDTO";
import {ToastService} from "angular-toastify";
import {LobbyStateService} from "../services/lobby.state.service";
import {saveAs} from 'file-saver';

@Component({
  selector: 'app-game-detail',
  templateUrl: './game-detail.component.html',
  styleUrls: ['./game-detail.component.scss']
})
export class GameDetailComponent implements OnInit {

  gameId: number | undefined;
  game: GameDTO | undefined;
  thumbnail: string = "thumbnail"
  images: any[] = []
  imageUrls: any[] = []
  cards: any[] = []
  correctDesc: string = ""

  constructor(private route: ActivatedRoute,
              private router: Router,
              private gameService: GameService,
              private authService: AuthenticationService,
              private lobbyState: LobbyStateService,
              private toastService: ToastService) {
  }

  ngOnInit() {
    this.initGame();
  }

  initGame() {
    this.route.params.subscribe(params => {
      this.gameId = parseInt(<string>params["gameId"]);
      this.gameService.getGameById(this.gameId).subscribe((game) => {
          this.game = game
          this.correctDesc = this.game.description.replace(/-r-n/g, "\r\n")
          this.getGameImages(this.game)
        },
        error => console.error(error))
    });
  }

  getGameImages(game: GameDTO) {
    game.images.forEach((image) => {
      let url = environment.backendBaseUrl + "/game/" + game.id + "/image/" + image.name
      fetch(url).then(async response => {
        const blob = await response.blob()
        const file = new File([blob], image.name, {type: "image/png"})
        if (image.scope === ImageScopes.CARD) {
          this.cards.push(file)
        } else if (image.scope === ImageScopes.PRESENTATION) {
          this.images.push(file)
          this.imageUrls.push(url)
        }
      })
    })
  }

  matchmaking() {
    let playerId: string;
    if (!this.authService.isLoggedIn()) {
      this.toastService.info("Please log in first")
      throw new Error("User is not logged in");
    } else {
      playerId = this.authService.getPlayerId()!;
    }

    this.gameService.matchMaking(this.gameId!, parseInt(playerId!)).subscribe((session) => {
      console.log("Matchmaking session", session);
      this.gameService.addPlayer(session.id, parseInt(String(playerId!))).subscribe((session) => {
        this.lobbyState.sessionId = session.id;
        this.router.navigateByUrl('/game-lobby', {});
      });
    });
  }

  getGameThumbnailUrl(gameId: number) {
    return environment.backendBaseUrl + "/game/" + gameId + "/image/" + this.thumbnail + CARD_IMAGE_EXTENSION
  }

  imageClick(event: any) {
    let preview_image = document.getElementById(event.target.id)
    let active_image = document.getElementById("active-image") as HTMLImageElement
    let image_slides = document.getElementsByClassName("container-slides")
    for (let i = 0; i < image_slides.length; i++) {
      image_slides[i].classList.remove("active")
    }
    if (active_image !== null && preview_image !== null && preview_image.parentElement !== null) {
      active_image.src = event.target.src
      preview_image.parentElement.classList.add("active")
    }
  }

  createPrivateSession() {
    if (!this.authService.isLoggedIn()) {
      this.toastService.info("Please log in first")
      throw new Error("User is not logged in");
    } else {
      const session = {
        visibility: SessionVisibility.PRIVATE
      };

      if (this.gameId != null) {
        this.gameService.createSession(session as SessionDTO, this.gameId!).subscribe((session) => {
          const playerId = this.authService.getPlayerId()!;
          this.gameService.addPlayer(session.id, parseInt(String(playerId!))).subscribe((session) => {
            this.lobbyState.sessionId = session.id;
            this.router.navigateByUrl('/game-lobby', {});
          });
        });
      } else {
        this.toastService.error("Could not create private Session")
        console.error("Cannot create session when there is no game id");
      }
    }
  }

  saveGameCode() {
    if (this.game !== undefined) {
      let blob = new Blob([atob(this.game.source_code)], {type: 'text/plain'})
      saveAs(blob, this.game.name + '.lua');
    }
  }
}
