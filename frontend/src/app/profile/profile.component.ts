import { Component, OnInit } from '@angular/core';
import {PlayerService} from "../services/player.service";
import {PlayerDTO} from "../models/playerDTO";
import {GameDTO} from "../models/gameDTO";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  player: PlayerDTO | undefined
  componentElements = ['User Data','Change Password','Created Games']
  componentUrls = ['user-data','change-password','created-games']

  constructor(private playerService: PlayerService) { }

  ngOnInit(): void {
    let playerId = localStorage.getItem("playerId")
    this.playerService.getPlayerInformation(Number(playerId)).subscribe( (player) => {
      this.player = player;
    })
  }
}
