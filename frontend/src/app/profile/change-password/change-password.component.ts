import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {AuthenticationService} from "../../services/authentication.service";
import {changePasswordDTO} from "../../models/changePasswordDTO";
import {ToastService} from "angular-toastify";

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  email: string = ""

  // @ts-ignore
  checkPasswords: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
    const currentPassword = control.get('currentPassword')?.value
    const pass = control.get('password')?.value;
    const confirmPass = control.get('confirmPassword')?.value;
    return pass !== confirmPass ? {notSame: true} : null;
  };

  form: FormGroup = this.formBuilder.group({
    currentPassword: [''],
    password: ['', [Validators.required, Validators.minLength(8)]],
    confirmPassword: [''],
  }, {validator: this.checkPasswords});

  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private authService: AuthenticationService,
              private toastService: ToastService) {
  }

  ngOnInit(): void {
    const routeParams = this.route.snapshot.paramMap;
    const email = routeParams.get('email');
    this.email = email + "";
  }

  changePassword() {
    let change: changePasswordDTO = {
      currentPassword: this.form.controls['currentPassword']?.value,
      newPassword: this.form.controls['password']?.value
    }
    this.authService.updatePassword(change).subscribe((player) => {
        this.toastService.success("Password updated")
      },
      error => {
        console.error(error)
        if (error.status === 400) {
          this.toastService.error("Current Password is not correct")
        } else {
          this.toastService.error("Could not update password")
        }
      })
  }

}
