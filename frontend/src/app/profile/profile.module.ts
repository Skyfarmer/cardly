import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileComponent} from "./profile.component";
import {ChangePasswordComponent} from "./change-password/change-password.component";
import {CreatedGamesComponent} from "./created-games/created-games.component";
import {UserDataComponent} from "./user-data/user-data.component";
import {ProfileRoutingModule} from "./profile-routing.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatIconModule} from "@angular/material/icon";
import {MatSidenavModule} from "@angular/material/sidenav";


@NgModule({
  declarations: [ProfileComponent, ChangePasswordComponent, CreatedGamesComponent, UserDataComponent],
  imports: [
    CommonModule,
    ProfileRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatIconModule,
    MatSidenavModule
  ]
})
export class ProfileModule { }
