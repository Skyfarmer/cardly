import {Component, OnInit} from '@angular/core';
import {PlayerService} from "../../services/player.service";
import {PlayerDTO} from "../../models/playerDTO";
import {FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators} from '@angular/forms';
import Swal from "sweetalert2";
import {AuthenticationService} from "../../services/authentication.service";
import {Router} from "@angular/router";
import {ToastService} from "angular-toastify";

@Component({
  selector: 'app-user-data',
  templateUrl: './user-data.component.html',
  styleUrls: ['./user-data.component.scss']
})
export class UserDataComponent implements OnInit {

  player: PlayerDTO | undefined

  constructor(private playerService: PlayerService, private formBuilder: FormBuilder,
              private authenticationService: AuthenticationService, private router: Router,
              private toastService: ToastService) {
  }

  userDataForm: FormGroup = this.formBuilder.group({
    alias: ['', [Validators.required]],
    email: ['', {validators: [Validators.required, Validators.email], updateOn: "blur"}],
  });

  ngOnInit(): void {
    let playerId = localStorage.getItem("playerId")
    if (playerId !== null) {
      this.playerService.getPlayerInformation(Number(playerId)).subscribe((player) => {
        this.player = player
        this.userDataForm.controls['email'].setValue(player.email)
        this.userDataForm.controls['alias'].setValue(player.alias)

      }, (error) => {
        console.log(error)
      })
    }
  }

  onSave() {
    const email = this.userDataForm.controls['email']?.value;
    const alias = this.userDataForm.controls['alias']?.value;
    let updatePlayer: PlayerDTO = {
      id: Number(localStorage.getItem("playerId")),
      email: email,
      alias: alias
    }
    this.playerService.updatePlayer(updatePlayer).subscribe((player) => {
      if(this.userDataForm.controls['email']?.dirty) {
        this.toastService.success("Email updated")
      } else {
        this.toastService.success("Alias updated")
      }
      this.authenticationService.logout()
      this.router.navigate(["/"])
    }, (error) => {
      if(error.status === 400) {
        this.toastService.error("Alias already exists")
      } else {
        this.toastService.error("Could not update user data")
      }
      console.log(error)
    })
  }
}
