import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatedGamesComponent } from './created-games.component';
import {ProfileComponent} from "../profile.component";
import {RouterTestingModule} from "@angular/router/testing";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {FormBuilder} from "@angular/forms";

describe('CreatedGamesComponent', () => {
  let component: CreatedGamesComponent;
  let fixture: ComponentFixture<CreatedGamesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreatedGamesComponent ],
      imports: [RouterTestingModule, HttpClientTestingModule],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatedGamesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
