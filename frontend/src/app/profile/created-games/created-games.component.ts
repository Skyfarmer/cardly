import {Component, Input, OnInit} from '@angular/core';
import {GameService} from "../../services/game.service";
import {GameDTO} from "../../models/gameDTO";
import {Router} from "@angular/router";

@Component({
  selector: 'app-created-games',
  templateUrl: './created-games.component.html',
  styleUrls: ['./created-games.component.scss']
})
export class CreatedGamesComponent implements OnInit {

  games: GameDTO[] = []
  headElements = ['Name', ''];

  constructor(private gameService: GameService, private router: Router) { }

  ngOnInit(): void {
    let playerId = localStorage.getItem("playerId")
    this.gameService.getGamesByCreator(Number(playerId)).subscribe( (games) => {
      this.games = games
      this.games.sort((a,b) => {
        return a.id - b.id
      })
    },
      (error) => {
      console.log(error)
      })
  }

  editGame(game:GameDTO) {
    this.router.navigate(['/game-edit',game.id])
  }
}
