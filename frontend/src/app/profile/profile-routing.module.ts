import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ProfileComponent} from "./profile.component";
import {ChangePasswordComponent} from "./change-password/change-password.component";
import {CreatedGamesComponent} from "./created-games/created-games.component";
import {UserDataComponent} from "./user-data/user-data.component";

const routes: Routes = [
  {
    path: 'profile',
    component: ProfileComponent,
    children: [
      {
        path: 'change-password',
        component: ChangePasswordComponent
      },
      {
        path: 'created-games',
        component: CreatedGamesComponent
      },
      {
        path: 'user-data',
        component: UserDataComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfileRoutingModule{ }
