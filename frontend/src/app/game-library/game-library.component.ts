import {Component, OnInit} from '@angular/core';

import {GameDTO} from "../models/gameDTO";
import {GameService} from "../services/game.service";
import {Router} from "@angular/router";
import {environment} from "../../environments/environment";
import {CARD_IMAGE_EXTENSION} from "../elements/card";
import {AuthenticationService} from "../services/authentication.service";
import {ToastService} from "angular-toastify";
import {LobbyStateService} from "../services/lobby.state.service";
import {GamePlayService} from "../services/game-play.service";

@Component({
  selector: 'app-game-library',
  templateUrl: './game-library.component.html',
  styleUrls: ['./game-library.component.scss']
})
export class GameLibraryComponent implements OnInit {

  games: GameDTO[] = []
  thumbnail: string = "thumbnail"
  gameName: any

  constructor(private gameService: GameService,
              private authService: AuthenticationService,
              private router: Router,
              private lobbyState: LobbyStateService,
              private gamePlayService: GamePlayService,
              private toastService: ToastService) {
  }

  ngOnInit(): void {
    this.gamePlayService.stopSocketConnection();
    this.gameService.getGames().subscribe((games) => {
      this.games = games;
    });
  }

  goToGameDetail(gameId: number) {
    this.router.navigate(['/games', gameId]);
  }

  matchmaking(gameId: number) {
    if (!this.authService.isLoggedIn()) {
      this.toastService.info("Please log in first")
      throw new Error("Cannot do matchmaking when not logged in");
    }

    let playerId = this.authService.getPlayerId();
    console.log('Player id for matchmaking is ' + playerId);
    this.gameService.matchMaking(gameId, parseInt(playerId!)).subscribe((session) => {
      this.gameService.addPlayer(session.id, parseInt(playerId!)).subscribe((session) => {
        console.log('Player has been added to session ' + session.id + ' for player ' + playerId);
        this.lobbyState.sessionId = session.id;
        this.router.navigateByUrl('/game-lobby', {});
      });
    });
  }

  addThumbnailUrlToGames(gameId: number): string {
    return environment.backendBaseUrl + "/game/" + gameId + "/image/" + this.thumbnail + CARD_IMAGE_EXTENSION
  }

  searchForGamesByName() {
    this.gameService.getGamesByName(this.gameName).subscribe((games) => {
      this.games = games;
    })
  }

}
