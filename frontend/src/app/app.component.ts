import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from "./services/authentication.service";
import {ActivatedRoute, NavigationStart, Router} from "@angular/router";
import {GameService} from "./services/game.service";
import {ToastService} from "angular-toastify";
import {LobbyStateService} from "./services/lobby.state.service";
import {GamePlayService} from "./services/game-play.service";

export let browserRefresh = false;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  sessionToken: string = "";
  sessionTokenError: string = "";
  userLoggedIn: boolean = false;
  subject = "";

  constructor(private authService: AuthenticationService, private router: Router, private gameService: GameService,
              private lobbyStateService: LobbyStateService,
              private route: ActivatedRoute, private toastService: ToastService,
              private gamePlayService: GamePlayService) {
    router.events.subscribe((val) => {
      if (this.authService.isLoggedIn()) {
        this.userLoggedIn = Boolean(localStorage.getItem("loggedIn"))
        this.subject = this.authService.getSub() as string;
      }
      if (val instanceof NavigationStart) {
        browserRefresh = !router.navigated
      }
    });
  }

  ngOnInit(): void {
    this.userLoggedIn = Boolean(localStorage.getItem("loggedIn"))
    this.authService.loggedIn.subscribe((loggedIn) => {
      this.userLoggedIn = loggedIn as boolean;
    })
    this.route.queryParams.subscribe( (params) => {
      let gameToken = params['gameToken']
      if (gameToken !== undefined) {
        this.sessionToken = gameToken
        this.enterSession()
      }
    })
  }

  logout() {
    this.authService.logout();
    this.userLoggedIn = false;
    this.toastService.info("Logged out")
    this.gamePlayService.stopSocketConnection()
    this.router.navigate(['/'])
  }

  enterSession() {
    this.gameService.join(this.sessionToken).subscribe((session) => {
      const playerId = this.authService.getPlayerId()!;
      this.gameService.addPlayer(session.id, parseInt(String(playerId!))).subscribe((session) => {
        this.lobbyStateService.sessionId = session.id;
        this.router.navigateByUrl('/game-lobby', {});
      }, error => {
        this.toastService.error("Error joining private Lobby")
      });
    }, error => {
      console.error(error);
      this.toastService.error("Error joining private Lobby")
      this.sessionTokenError = "Cannot find game";
    });
  }

  redirectToHome() {
    this.gamePlayService.stopSocketConnection();
    this.router.navigate(['/'])
  }
}
