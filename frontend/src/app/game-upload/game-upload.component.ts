import {Component, OnInit} from '@angular/core';
import {GameService} from "../services/game.service";
import {GameDTO} from "../models/gameDTO";
import {ActivatedRoute, Router} from "@angular/router";
import {ImageService} from "../services/image.service";
import {environment} from "../../environments/environment";
import {CARD_IMAGE_EXTENSION} from "../elements/card";
import {ImageScopes} from "../services/ImageScopes";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ImageDTO} from "../models/imageDTO";
import {PlayerService} from "../services/player.service";
import {PlayerDTO} from "../models/playerDTO";
import {ToastService} from "angular-toastify";

@Component({
  selector: 'app-game-upload',
  templateUrl: './game-upload.component.html',
  styleUrls: ['./game-upload.component.scss']
})
export class GameUploadComponent implements OnInit {

  game: GameDTO | undefined;
  gameId: number | undefined;
  player: PlayerDTO | undefined;
  editable: boolean = false
  images: any[] = []
  cards: any[] = []
  thumbnail: any
  sourceCode: any
  luaEnding: string = ".lua"

  gameUploadForm: FormGroup = this.formBuilder.group({
    name: ['', [Validators.required]],
    description: ['', [Validators.required]],
    playerCount: ['', [Validators.required]],
    sourceCode: ['', [Validators.required]],
  });

  constructor(private gameService: GameService, private route: ActivatedRoute, private imageService: ImageService,
              private formBuilder: FormBuilder, private router: Router,
              private playerService: PlayerService, private toastService: ToastService) {
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.gameId = parseInt(<string>params["gameId"]);
      if (!Number.isNaN(this.gameId)) {
        this.editable = true
        this.gameService.getGameById(this.gameId).subscribe((game) => {
            this.game = game;
            this.initForm()
            this.getGameImages(this.game)
          },
          error => console.error(error))
      }
    })
  }

  initForm() {
    this.gameUploadForm.controls['name'].setValue(this.game!.name)
    this.gameUploadForm.controls['description'].setValue(this.game!.description)
    this.gameUploadForm.controls['playerCount'].setValue(this.game!.max_capacity)
  }

  getGameImages(game: GameDTO) {
    game.images.forEach((image) => {
      let url = environment.backendBaseUrl + "/game/" + game.id + "/image/" + image.name
      fetch(url).then(async response => {
        const blob = await response.blob()
        const file = new File([blob], image.name, {type: "image/png"})
        if (image.scope === ImageScopes.CARD) {
          this.cards.push(file)
        } else if (image.scope === ImageScopes.PRESENTATION) {
          this.images.push(file)
        }
      })
    })
  }

  getGameThumbnailUrl(gameId: number) {
    return environment.backendBaseUrl + "/game/" + gameId + "/image/" + "thumbnail" + CARD_IMAGE_EXTENSION
  }

  onFileChangedSourceCode(event: any) {
    let file = event.target.files[0]
    if (file !== null) {
      this.sourceCode = file
    } else {
      console.log("No file selected")
    }
  }

  onSave() {
    const name = this.gameUploadForm.controls['name']?.value;
    const description = this.gameUploadForm.controls['description']?.value;
    const playerCount = this.gameUploadForm.controls['playerCount']?.value;
    if(this.sourceCode.name.substring(this.sourceCode.name.length - this.luaEnding.length) === this.luaEnding) {
      this.toBase64(this.sourceCode).then((data) => {
        this.playerService.getPlayerInformation(Number(localStorage.getItem("playerId"))).subscribe((player) => {
          this.player = player
          let tmpGame = this.createTmpGame(name, data as string, playerCount, description,
            this.player.id, player.email, player.alias)
          this.gameService.createGame(tmpGame).subscribe((game) => {
            this.game = game
            this.toastService.success("Game created")
            this.router.navigate(["/game-edit", this.game.id])
          }, (error) => {
            console.error(error)
            this.toastService.error("Error creating game")
          })
        })
      })
    } else {
      this.toastService.error("File is not a lua code file")
    }

  }

  createTmpGame(gameName: string, sourceCode: string, capacity: number, description: string,
                creatorId: number, creatorEmail: string, creatorAlias: string) {
    let tmpGame: GameDTO = {
      id: 1,
      name: gameName,
      source_code: sourceCode,
      min_capacity: capacity,
      max_capacity: capacity,
      description: description,
      creator: {
        id: creatorId,
        email: creatorEmail,
        alias: creatorAlias
      },
      images: new Set<ImageDTO>()
    }
    return tmpGame
  }

  toBase64(file: any) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      // @ts-ignore
      reader.onload = () => resolve(reader.result.split(',')[1]);
      reader.onerror = error => reject(error);
    })
  }

  appendFilesToFormData(files: any[]): FormData {
    const formData: FormData = new FormData();
    if (files.length !== 0) {
      for (let i = 0; i < files.length; i++) {
        formData.append("files", files[i]);
      }
    } else {
      console.log("No files to upload")
    }
    return formData
  }
}
