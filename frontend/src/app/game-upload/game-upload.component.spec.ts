import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GameUploadComponent } from './game-upload.component';
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {GameService} from "../services/game.service";
import { RouterModule } from '@angular/router';
import {FormBuilder, FormsModule, ReactiveFormsModule} from "@angular/forms";
import {RouterTestingModule} from "@angular/router/testing";

describe('GameUploadComponent', () => {
  let component: GameUploadComponent;
  let fixture: ComponentFixture<GameUploadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GameUploadComponent ],
      imports: [HttpClientTestingModule, RouterModule.forRoot([]),FormsModule, ReactiveFormsModule,
        RouterTestingModule],
      providers: [GameService]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GameUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
