import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {AuthenticationService} from "../services/authentication.service";
import {ResetPasswordDto} from "../models/resetPasswordDto";
import {ToastService} from "angular-toastify";

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  private token: string | undefined;

  // @ts-ignore
  checkPasswords: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
    const pass = control.get('password')?.value;
    const confirmPass = control.get('confirmPassword')?.value;
    return pass !== confirmPass ? {notSame: true} : null;
  };

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private route: ActivatedRoute,
              private toastService: ToastService,
              private authService: AuthenticationService,
  ) {
  }

  ngOnInit(): void {
    this.route.queryParamMap.subscribe(params => {
      this.token = params.get("token") + "";
    });
  }

  form: FormGroup = this.formBuilder.group({
    password: ['', [Validators.required, Validators.minLength(8)]],
    confirmPassword: [''],
  }, {validator: this.checkPasswords});

  reset() {
    const password = this.form.controls['password']?.value;

    const request: ResetPasswordDto = {
      newPassword: password,
      token: this.token!
    };

    this.authService.resetPassword(request).subscribe(
      () => {
        console.log("Reset password")
        this.router.navigateByUrl('/').then(() => {
          this.toastService.info("Password has been resetted");
        });
      },
      (error) => {
        console.error(error);
      }
    );

  }
}
