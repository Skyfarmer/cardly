export interface ImageResponseDTO {
  gameId: number,
  imageName: string,
  imageType: string,
  size: number
}
