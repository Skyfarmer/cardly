export interface changePasswordDTO {
  currentPassword: string;
  newPassword: string;
}
