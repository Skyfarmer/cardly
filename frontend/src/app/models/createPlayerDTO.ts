export interface CreatePlayerDTO {
  email: string;
  password: string;
  alias: string;
}
