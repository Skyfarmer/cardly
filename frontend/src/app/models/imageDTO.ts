export interface ImageDTO {
  id: number;
  name: string;
  scope: string;
}
