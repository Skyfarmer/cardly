import {PlayerDTO} from "./playerDTO";
import {ImageDTO} from "./imageDTO";

export interface GameDTO {
  id: number;
  name: string;
  source_code: string;
  min_capacity: number;
  max_capacity: number;
  images: Set<ImageDTO>;
  creator: PlayerDTO;
  description: string;

}

export interface Deck {
  id: string;
  layout: DeckLayout;
  set: Card[];
}

export enum DeckLayout {
  Stacked = "Stacked",
  Spreaded = "Spreaded",
}

export interface Card {
  value: number;
  color: string;
}
