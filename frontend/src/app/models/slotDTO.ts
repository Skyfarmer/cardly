import {PlayerDTO} from "./playerDTO";

export interface SlotDTO {
  id: number;
  slotNumber: number;
  player: PlayerDTO
}
