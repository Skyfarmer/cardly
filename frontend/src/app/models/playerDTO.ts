export interface PlayerDTO {
  id : number;
  email: string;
  alias: string;
}
