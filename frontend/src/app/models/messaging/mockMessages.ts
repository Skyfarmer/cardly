// These Messages will be replaces by the real ones once everything is running
export let cardFlipped: string = "{\"type\": \"CardFlipped\", \"deck\":{\"id\":3,\"set\":[{\"value\":3,\"color\":\"Green\"}],\"visibility\":\"Hidden\"}}"
export let userSelection: string = "{\"type\": \"UserSelection\",\"options\":[\"option1\",\"option2\",\"option3\",\"option4\"]}"
export let userSelection2: string = "{\"type\": \"UserSelection\",\"title\": \"Auction\",\"options\":[{\"option1\":[\"2\",\"3\",\"4\",\"5\",\"6\",\"7\"]}" +
  ",{\"option2\":[\"Hearts\",\"Diamonds\",\"Spades\",\"Clubs\"]},\"option3\"]}"

export let userSelection3: string = "{\"type\": \"UserSelection\",\"title\": \"Auction\",\"options\":[{\"option\":[\"2\",\"3\",\"4\",\"5\",\"6\",\"7\"]}" +
  ",{\"option\":[\"Hearts\",\"Diamonds\",\"Spades\",\"Clubs\"]},\"option3\"]}"

export let winMessage: string = "{\"type\" : \"WinMessage\",\"winner\":[1,2,3,4]}"
export let errorMessage: string = "{\"type\": \"ErrorMessage\",\"message\": \"Error\"}"
export let individualText: string = "{\"type\": \"individualText\",\"text\": \"Text\"}"

export let cardMoved: string = "{\"type\": \"GameEvent\",\"event\": {\"type\": \"CardMoved\",\"card\": {" +
  "\"value\": 5,\"color\": \"Red\"},\"from\": 1705559955964132900,\"to\": 16065188778612410000}}"



export let selectedOption: string = "{\"type\" : \"SelectedOptions\",\"option\" : 1}"
export let tryCardMoved: string = "{\"type\" : \"TryCardMoved\",\"card\" : {\"value\" : 6,\"color\" : \"Red\"}," +
  "\"fromStack\" : 123123123,\"toStack\" : 123123123}"

export let objectClicked: string = "{\"type\" : \"ObjectClicked\",\"deckId\" : 123123123," +
  "\"card\" : {\"value\" : 6,\"color\" : \"Red\"}}"


