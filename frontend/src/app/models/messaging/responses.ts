import {Card} from "../gameDTO";

export type Response = SelectedState | SelectedOptions | TryDeckSelection | TryCardMoved;

export interface SelectedState {
  type: 'SelectedState';
  option: number;
}

export interface SelectedOptions {
  type: 'SelectedOptions';
  options: number[];
}

export interface TryDeckSelection {
  type: 'TryDeckSelection';
  deck: string
}

export interface TryCardMoved {
  type: 'TryCardMoved';
  card: Card;
  from: string,
  to: string,
}

export function serialize(payload: Response): String {
  return JSON.stringify(payload);
}
