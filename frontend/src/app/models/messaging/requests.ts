import {Card, Deck} from "../gameDTO";
import {PlayerDTO} from "../playerDTO";
import {SlotDTO} from "../slotDTO";

export type Request =
  | GameConfiguration
  | DeckReordered
  | CardFlipped
  | CardMoved
  | CurrentPlayer
  | UserSelection
  | GameOver
  | Info
  | Error
  | IndividualText
  | GameStopped
  | GameStarted
  | GameCrashed
  | PlayerAlreadyJoined
  | PlayerJoin
  | PlayerLeave;

export interface GameConfiguration {
  global_decks: Deck[];
  player_decks: {
    [key: string]: Deck[]
  };
}

export interface DeckReordered {
  deck: Deck,
}

export interface CardFlipped {
  deck_id: string
  old_card: Card
  new_card: Card
}

export interface CardMoved {
  card: Card,
  from: string,
  to: string
}

export interface CurrentPlayer {
  player_id: string,
  additional_options: string[],
  other_options_exist: boolean
}

export interface UserSelection {
  options: string[][]
}

export interface GameOver {
  winners: number[]
}

export interface Info {
  message: string
}

export interface Error {
  message: string
}

export interface IndividualText {
  text: string
}

export interface PlayerJoin {
  slotDto: SlotDTO
}

export interface PlayerAlreadyJoined {

}

export interface PlayerLeave {
  slotDto: SlotDTO
}

export interface GameStopped {

}

export interface GameStarted {

}

export interface GameCrashed {

}

export function deserialize(payload: string): Request {
  const object = JSON.parse(JSON.stringify(payload));

  if (object.type == 'GameConfiguration') {
    return object as GameConfiguration;
  } else if (object.type == 'DeckReordered') {
    return object as DeckReordered;
  } else if (object.type == 'CardFlipped') {
    return object as CardFlipped;
  } else if (object.type == 'CardMoved') {
    return object as CardMoved;
  } else if (object.type == 'CurrentPlayer') {
    return object as CurrentPlayer;
  } else if (object.type == 'UserSelection') {
    return object as UserSelection;
  } else if (object.type == 'GameOver') {
    return object as GameOver;
  } else if (object.type == 'Info') {
    return object as Info;
  } else if (object.type == 'Error') {
    return object as Error;
  } else if (object.type == 'IndividualText') {
    return object as IndividualText;
  } else if (object.type == 'PlayerJoin') {
    return object as PlayerJoin;
  } else if (object.type == 'GameStopped') {
    return object as GameStopped;
  } else if (object.type == 'PlayerLeave') {
    return object as PlayerLeave;
  } else if (object.type == 'GameStarted') {
    return object as GameStarted;
  } else if (object.type == 'GameCrashed') {
    return object as GameCrashed;
  } else if (object.type == 'PlayerAlreadyJoined') {
    return object as PlayerAlreadyJoined;
  }

  throw 'Cannot deserialize object';
}
