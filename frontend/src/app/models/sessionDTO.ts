import {GameDTO} from "./gameDTO";
import {GameStatus} from "./gameStatus";
import {SlotDTO} from "./slotDTO";

export interface SessionDTO {
  id: number;
  status: GameStatus;
  slots: SlotDTO[];
  game: GameDTO;
  visibility: SessionVisibility;
  join_token: string;
}

export enum SessionVisibility {
  PRIVATE = "PRIVATE" ,
  PUBLIC = "PUBLIC",
}
