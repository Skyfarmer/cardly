export interface LoginPlayerDTO {
  email: string;
  password: string;
}
