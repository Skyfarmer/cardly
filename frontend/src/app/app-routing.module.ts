import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {GameLibraryComponent} from './game-library/game-library.component';
import {GameDetailComponent} from './game-detail/game-detail.component';
import {GameUploadComponent} from './game-upload/game-upload.component';
import {GameLobbyComponent} from './game-lobby/game-lobby.component';
import {GameComponent} from './game/game.component';
import {RegisterComponent} from "./register/register.component";
import {LoginComponent} from "./login/login.component";
import {ResetPasswordComponent} from "./reset-password/reset-password.component";
import {RequestEmailComponent} from "./request-email/request-email.component";
import {GameEditComponent} from "./game-edit/game-edit.component";

const routes: Routes = [
  {path: '', component: GameLibraryComponent},
  {path: 'games/:gameId', component: GameDetailComponent},
  {path: 'game-upload', component: GameUploadComponent},
  {path: 'game-edit/:gameId', component: GameEditComponent},
  {path: 'game-lobby', component: GameLobbyComponent},
  {path: 'session', component: GameComponent},
  {path: 'reset-password', component: ResetPasswordComponent},
  {path: 'request-email', component: RequestEmailComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'login', component: LoginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
