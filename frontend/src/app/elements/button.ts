import * as createjs from "createjs-module";

export class Button<T> extends createjs.Container {
  label: T
  lineWidth: number | undefined = undefined
  clicked = false

  override draw(ctx: CanvasRenderingContext2D, ignoreCache?: boolean): boolean {
    if (this.lineWidth !== undefined) {
      this.updateGraphics(this.lineWidth)
    }
    return super.draw(ctx, ignoreCache);
  }

  updateGraphics(lineWidth: number) {
    let text_form = this.children[1] as createjs.Text
    text_form.lineWidth = lineWidth
    let base = this.children[0] as createjs.Shape
    let base_width = text_form.getBounds().width * 1.2
    let base_height = text_form.getBounds().height * 1.2
    if (this.clicked) {
      base.graphics = new createjs.Graphics()
        .beginStroke("black")
        .beginFill("lightgreen")
        .drawRoundRect(0, 0, base_width, base_height, 30)
    } else {
      base.graphics = new createjs.Graphics()
        .beginStroke("black")
        .beginFill("white")
        .drawRoundRect(0, 0, base_width, base_height, 30)
    }


    text_form.x = base_width / 2;
    text_form.y = base_height / ((text_form.getMeasuredHeight() / text_form.getMeasuredLineHeight()) + 1)
    this.setBounds(0, 0, base_width, base_height)
  }

  constructor(text: string, label: T) {
    super();
    this.label = label

    let text_form = new createjs.Text(text);
    text_form.textAlign = "center";
    text_form.textBaseline = "middle";
    text_form.font = "bold 40px Arial"
    let base = new createjs.Shape();

    this.addChild(base, text_form);
  }
}
