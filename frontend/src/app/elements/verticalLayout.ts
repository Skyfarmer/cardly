import * as createjs from "createjs-module";

export class VerticalLayout extends createjs.Container {
  padding: number

  override draw(ctx: CanvasRenderingContext2D, ignoreCache?: boolean): boolean {
    this.updateObjects()
    return super.draw(ctx, ignoreCache);
  }

  getChildrenHeight(): number {
    let acc = 0;
    for (let c of this.children) {
      acc += c.getBounds().height
    }
    return acc + this.padding * (this.children.length - 1)
  }

  getChildrenHeightUntil(until: number): number {
    let acc = 0;
    for (let i = 0; i < this.children.length && i < until; i++) {
      acc += this.children[i].getBounds().height
    }
    return acc + this.padding * (until - 1)
  }

  getMaxChildWidth(): number {
    let max = 0;
    for (let c of this.children) {
      if (c.getBounds().width > max) {
        max = c.getBounds().width
      }
    }
    return max
  }

  scale(): number {
    if (this.getBounds() === null) {
      return 1
    }
    let height = this.getChildrenHeight()
    let expectedHeight = 1
    if (height / this.getBounds().height > 0.8) {
      expectedHeight = (this.getBounds().height * 0.8) / height
    }
    let width = this.getMaxChildWidth()
    let expectedWidth = 1
    if (width / this.getBounds().width > 0.8) {
      expectedWidth = (this.getBounds().width * 0.8) / width
    }
    if(expectedWidth < expectedHeight) {
      return expectedWidth
    } else {
      return expectedHeight
    }
  }

  calcX(row: number, scale: number): number {
    let midPoint = (this.children[row].getBounds().width * scale) / 2
    return (this.getBounds().width / 2) - midPoint
  }

  calcY(row: number, scale: number): number {
    let posY = this.getChildrenHeightUntil(row) * scale
    let midPoint = (this.getChildrenHeight() * scale) / 2
    let relPos = posY - midPoint
    return (this.getBounds().height / 2) + relPos
  }

  updateObjects() {
    let scale = this.scale();
    for (let i = 0; i < this.children.length; i++) {
      let child = this.children[i];
      let x = this.calcX(i, scale)
      let y = this.calcY(i, scale)
      child.setTransform(x, y, scale, scale);
    }
  }


  constructor() {
    super();
    this.padding = 20
  }
}
