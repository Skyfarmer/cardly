import * as createjs from "createjs-module";
import {environment} from "../../environments/environment";
import {Deck, DeckType} from "./deck";
import {Tween} from "createjs-module";

export const CARD_WIDTH = 100;
export const CARD_HEIGHT = 180;
export const BACK_FACE = "backFace"
export const CARD_IMAGE_EXTENSION = ".png"

export class Card extends createjs.Shape {
  value: number = 0
  color: string = ""
  gameid: number;
  image: HTMLImageElement
  isMoving: boolean = false
  movingTween: Tween | undefined = undefined
  targetProps: any = {}


  constructor(value: number, color: string, gameid: number) {
    super()
    this.gameid = gameid
    this.image = new Image()
    this.image.onload = () => {
      if (this.parent instanceof Deck) {
        // Skip animation if not spreaded deck
        if (this.parent.deckType === DeckType.Spreaded
        || this.parent.getChildIndex(this) === this.parent.numChildren - 1) {
          this.skewY = 0
          createjs.Tween.get(this)
            .to({skewY: 90}, 300)
            .call(() => {
              this.updateImage()
              this.skewY = 270
              createjs.Tween.get(this).to({skewY: 360}, 300)
            })
        } else {
          this.updateImage()
        }
      }

    }

    this.update(value, color)
  }

  removeTweens() {
    if(this.movingTween === undefined) {
      createjs.Tween.removeTweens(this)
    } else {
      this.movingTween.setPaused(true)
    }
    this.isMoving = false
  }

  private updateImage() {
    let m = new createjs.Matrix2D();
    m.translate(0, 0);
    m.scale(CARD_WIDTH / this.image.width, CARD_HEIGHT / this.image.height);
    this.graphics = new createjs.Graphics()
      .beginBitmapFill(this.image, "repeat", m)
      .drawRoundRect(0, 0, CARD_WIDTH, CARD_HEIGHT, 10)
  }

  update(value: number, color: string) {
    this.value = value;
    this.color = color
    let url
    if (this.color === "") {
      url = environment.backendBaseUrl + "/game/" + this.gameid + "/image/" + BACK_FACE + CARD_IMAGE_EXTENSION
    } else {
      url = environment.backendBaseUrl + "/game/" + this.gameid + "/image/" + this.value + this.color + CARD_IMAGE_EXTENSION
    }

    this.image.crossOrigin = "anonymous"
    this.image.src = url
  }

  setShadow(setting: boolean) {
    if (setting) {
      this.shadow = new createjs.Shadow("#000000", 5, 5, 10);
    } else {
      // @ts-ignore
      this.shadow = null
    }
  }
}
