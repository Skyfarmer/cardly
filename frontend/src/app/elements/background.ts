import * as createjs from "createjs-module";
import {Bitmap, Graphics} from "createjs-module";

export class Background extends createjs.Shape {
  bitmap: HTMLImageElement
  isValid = false

  updateGraphics() {
    if (this.isValid) {
      let m = new createjs.Matrix2D()
      m.scale(0.6, 0.6);
      this.graphics = new Graphics()
        .beginBitmapFill(this.bitmap, "repeat", m)
        .drawRect(0, 0, this.getBounds().width, this.getBounds().height)
      let blurFilter = new createjs.BlurFilter(3, 1, 1);
      this.filters = [blurFilter]
      this.cache(0, 0, this.getBounds().width, this.getBounds().height)
    }
  }


  constructor() {
    super();
    this.bitmap = new Image()
    this.bitmap.onload = () => {
      this.isValid = true
      this.updateGraphics()
    }
    this.bitmap.src = "assets/texture.bmp"

    this.setBounds(0, 0, 0, 0)
  }
}
