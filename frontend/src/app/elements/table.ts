import * as createjs from "createjs-module";
import {Deck, DeckType} from "./deck";
import {DeckRow} from "./deckRow";
import {VerticalLayout} from "./verticalLayout";

export class Table extends createjs.Container {
  layout: DeckRow[] = []

  partX: number
  partY: number

  override draw(ctx: CanvasRenderingContext2D, ignoreCache?: boolean): boolean {
    let canvas = ctx.canvas
    let layout = this.children[0]
    if(this.rotation == 90 || this.rotation == -90) {
      layout.setBounds(0, 0, canvas.height * this.partY, canvas.width * this.partX)
    } else {
      layout.setBounds(0, 0, canvas.width * this.partX, canvas.height * this.partY)
    }

    return super.draw(ctx, ignoreCache);
  }

  findPos(size: number): number {
    for (let i = 0; i < this.layout.length; i++) {
      if (this.layout[i].hasRoom(size)) {
        return i
      }
    }
    this.layout.push(new DeckRow())
    return this.layout.length - 1
  }

  constructor(decks: Deck[], partX: number, partY: number) {
    super();
    this.partX = partX
    this.partY = partY

    let layout = new VerticalLayout()
    this.addChild(layout)

    for (let d of decks) {
      switch (d.deckType) {
        case DeckType.Stacked: {
          let row = this.findPos(1)
          this.layout[row].addDeck(d, 1)
          layout.addChild(this.layout[row])
          break;
        }
        case DeckType.Spreaded: {
          let row = this.findPos(5)
          this.layout[row].addDeck(d, 5)
          layout.addChild(this.layout[row])
          break;
        }
      }
    }
  }
}
