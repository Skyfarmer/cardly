import * as createjs from "createjs-module";

export class Tooltip extends createjs.Container {
  tick = 0

  override draw(ctx: CanvasRenderingContext2D, ignoreCache?: boolean): boolean {
    return super.draw(ctx, ignoreCache);
  }

  constructor(text: string) {
    super();

    let text_form = new createjs.Text(text);
    text_form.textAlign = "center";
    text_form.textBaseline = "middle";
    text_form.font = "bold 20px Arial"
    let base = new createjs.Shape();

    let base_width = text_form.getBounds().width * 1.2
    let base_height = text_form.getBounds().height * 1.2

    base.graphics = new createjs.Graphics()
      .beginStroke("black")
      .beginFill("white")
      .drawRect(0, 0, base_width, base_height)

    text_form.x = base_width / 2;
    text_form.y = base_height / ((text_form.getMeasuredHeight() / text_form.getMeasuredLineHeight()) + 1)
    this.setBounds(0, 0, base_width, base_height)
    this.addChild(base, text_form);
  }
}
