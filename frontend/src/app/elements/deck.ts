import * as createjs from "createjs-module";
import {Card, CARD_HEIGHT, CARD_WIDTH} from "./card";
import {Graphics} from "createjs-module";


export enum DeckType {
  Spreaded,
  Stacked
}

export class Deck extends createjs.Container {
  deckId: string
  deckType: DeckType
  dropHint = false
  sort = false

  customGraphics = (graphics: Graphics) => {
    return graphics
  }
  background: createjs.Shape = new createjs.Shape()

  constructor(id: string, deck_type?: DeckType) {
    super()

    if (deck_type !== undefined) {
      this.deckType = deck_type
    } else {
      this.deckType = DeckType.Stacked
    }

    this.graphicsForDeck(false);
    this.addChild(this.background);
    this.deckId = id
  }

  override draw(ctx: CanvasRenderingContext2D, ignoreCache?: boolean): boolean {
    this.graphicsForDeck(this.dropHint);
    this.transformCards()

    return super.draw(ctx, ignoreCache);
  }

  setDropHint(setting: boolean) {
    this.dropHint = setting
  }

  addCardAt(card: Card, index?: number) {
    if (!this.contains(card)) {
      this.transformCards(card, index)
    }
  }

  addCard(card: Card) {
    this.addCardAt(card)
  }

  private animateCard(card: Card, x: number, y: number, scaleX: number, scaleY: number) {
    let props: any = {}
    if (card.x !== x || card.y !== y) {
      if (card.parent !== null && card.parent !== this) {
        let old = card.parent.localToLocal(card.x, card.y, this)
        card.x = old.x
        card.y = old.y
      }
      props.x = x
      props.y = y
    }
    if (card.scaleX !== scaleX) {
      props.scaleX = scaleX
    }
    if (card.scaleY !== scaleY) {
      props.scaleY = scaleY
    }

    let same_props = props.x === card.targetProps.x
      && props.y === card.targetProps.y
      && props.scaleX === card.targetProps.scaleX
      && props.scaleY === card.targetProps.scaleY

    if (Object.keys(props).length !== 0 && (!card.isMoving || !same_props)) {
      card.removeTweens()
      card.isMoving = true
      card.targetProps = props
      card.movingTween = createjs.Tween
        .get(card)
        .to(props, 1000, createjs.Ease.getPowInOut(4))
        .call(() => card.removeTweens())
    }
  }

  removeAllTweens() {
    for (let c of this.children) {
      if (c instanceof Card) {
        c.removeTweens()
      }
    }
  }

  private transformCards(card?: Card, index?: number) {
    let cards = this.children.filter(c => c instanceof Card).map(c => c as Card);
    if (card !== undefined) {
      if (index !== undefined) {
        cards.splice(index, 0, card)
      } else {
        cards.push(card)
      }
    }

    if (this.sort) {
      cards.sort((l, r) => {
        if (l.color === r.color) {
          return l.value - r.value
        } else {
          return l.color < r.color ? -1 : 1
        }
      })
    }

    if (cards.length === 0) {
      return;
    }

    let i = 0;
    let base = 10;
    let max = this.getBounds().width - 20 - CARD_WIDTH * 0.9;
    let step = 0;
    if (cards.length > 1) {
      step = Math.floor(max / (cards.length - 1));
    }

    for (let c of cards) {
      let bounds = this.getBounds();
      if (this.deckType === DeckType.Spreaded) {
        let height = bounds.height
        let y = Math.floor((height - height * 0.9) / 2)
        this.animateCard(c, base + step * i, y, 0.9, 0.9)
      } else {
        let width = bounds.width
        let height = bounds.height
        this.animateCard(c, (width - width * 0.9) / 2,
          (height - height * 0.9) / 2, 0.9, 0.9)
      }
      this.addChild(c)

      i += 1;
    }
  }

  private graphicsForDeck(selected: boolean) {
    let width;
    let height = CARD_HEIGHT;
    if (this.deckType === DeckType.Stacked) {
      width = CARD_WIDTH;
    } else {
      width = CARD_WIDTH * 8
    }
    this.setBounds(0, 0, width, height);
    if (selected) {
      this.background.graphics = new createjs.Graphics()
        .beginStroke("lightblue")
        .beginFill("lightblue")
        .drawRoundRect(0, 0, width, height, 20);
    } else {
      this.background.graphics = this.customGraphics(new createjs.Graphics())
        .beginStroke("lightblue")
        .drawRoundRect(0, 0, width, height, 20);
    }
  }
}
