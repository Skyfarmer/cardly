import Swal from "sweetalert2";
import {UserSelection} from "../models/messaging/requests";
import {SelectedOptions} from "../models/messaging/responses";
import {GamePlayService} from "../services/game-play.service";

export function yourTurnAlertWithImage() {
  Swal.fire({
    imageUrl: "../../assets/yourTurn_RobotoBold_Gold.png",
    imageWidth: 400,
    imageHeight: 200,
    imageAlt: 'Custom image',
    background: 'transparent',
    showConfirmButton: false,
    timer: 1000
  })
}

export function messageAlert(title: string, color: string,
                             showConfirm: boolean, timer: number, background: string, callback: () => void) {
  Swal.fire({
    position: 'center', // IntelliJ why are you trolling me...
    title: title,
    color: color,
    showConfirmButton: showConfirm,
    background: background,
    customClass: 'swal-title',
    timer: timer
  }).then(callback)
}

export function winAlert(players: string[], callback: () => void) {
  let text;
  if(players.length === 0) {
    text = "Everyone lost"
  } else if (players.length === 1) {
    text = players[0] + " won"
  } else {
    text = "Players "
    for (let i = 0; i < players.length; i++) {
      text += players[i]
      if(i === players.length - 2) {
        text += " and "
      } else if (i !== players.length - 1) {
        text += ", "
      }
    }
    text += " won the game"
  }

  messageAlert(text, "gold", false, 4000, "Transparent", callback)
}

export async function confirmAlert() {
  let confirmed = false;
  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: 'btn btn-success',
      cancelButton: 'btn btn-danger'
    },
    buttonsStyling: false
  })
  swalWithBootstrapButtons.fire({
    title: 'Are you sure you want to change your details?',
    text: "This action will log you out",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes',
    cancelButtonText: 'No',
  }).then((result) => {
    confirmed = result.isConfirmed;
  })
  return confirmed
}

export function userSelectionDialog(service: GamePlayService, userSelection: UserSelection) {
  let customHtmlString: string = "<div class=box--full style='margin: auto'>"

  for (let i = 0; i < userSelection.options.length; i++) {
    let tmpArray = []
    let tmpDiv = ""
    for (let j = 0; j < userSelection.options[i].length; j++) {
      tmpArray.push(userSelection.options[i][j])
      tmpDiv += "<h3>" +
        "<input style='vertical-align: baseline; margin: 5px' type='radio' name='" + i + "' class='check" + i + "'/>" +
        userSelection.options[i][j] +
        "</h3>"
    }
    if(i != userSelection.options.length - 1) {
      tmpDiv += "<hr>"
    }

    customHtmlString += tmpDiv
  }
  customHtmlString += "</div>"

  Swal.fire({
    title: "Select options",
    html: customHtmlString,
    showConfirmButton: true,
    allowOutsideClick: false,
    allowEscapeKey: false,
    preConfirm: () => {
      let selected: number[] = []

      for (let i = 0; i < userSelection.options.length; i++) {
        let checkBoxes = Swal.getPopup()?.getElementsByClassName('check' + i)
        if (checkBoxes !== undefined) {
          let s = false;
          for (let j = 0; j < checkBoxes.length; j++) {
            // @ts-ignore
            let cb = checkBoxes[j] as Checkbox
            if (cb.checked) {
              if (s) {
                Swal.showValidationMessage("Too many options selected")
                return undefined
              }
              s = true;
              selected.push(j);
            }
          }
          if (!s) {
            Swal.showValidationMessage("Missing option")
            return undefined
          }
        }
      }
      return selected
    }
  }).then((result) => {
    if (result.isConfirmed && result.value !== undefined) {
      const obj: SelectedOptions = {
        type: 'SelectedOptions',
        options: result.value as number[],
      };
      service.sendSocketMessageToBackend(obj)
    }
  })
}
