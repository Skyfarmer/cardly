import * as createjs from "createjs-module";
import {Deck} from "./deck";
import {CARD_WIDTH} from "./card";

export class DeckRow extends createjs.Container {
  size: number = 0

  hasRoom(size: number): boolean {
    return this.size + size <= 5
  }

  addDeck(deck: Deck, size: number) {
    this.addChild(deck)
    this.size += size
  }

  override draw(ctx: CanvasRenderingContext2D, ignoreCache?: boolean): boolean {
    for(let i = 0; i < this.children.length; i++) {
      let deck = this.children[i] as Deck
      deck.setTransform((CARD_WIDTH + 20) * i)
    }
    return super.draw(ctx, ignoreCache);
  }
}
