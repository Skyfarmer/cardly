import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {GameLibraryComponent} from './game-library/game-library.component';
import {GameDetailComponent} from './game-detail/game-detail.component';
import {GameUploadComponent} from './game-upload/game-upload.component';
import {GameLobbyComponent} from './game-lobby/game-lobby.component';
import {GameComponent} from './game/game.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {GameService} from "./services/game.service";
import {CommunicationService} from "./services/communication.service";
import {RegisterComponent} from './register/register.component';
import {LoginComponent} from './login/login.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import {TokenInterceptorService} from "./services/token.interceptor";
import { RequestEmailComponent } from './request-email/request-email.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSlideToggleModule } from '@angular/material/slide-toggle'
import {MatListModule} from "@angular/material/list";
import {MatIconModule} from "@angular/material/icon";
import {MatFormFieldModule} from "@angular/material/form-field";
import {NgxDropzoneModule} from "ngx-dropzone";
import { ToastService, AngularToastifyModule } from 'angular-toastify';
import {HashLocationStrategy, LocationStrategy} from "@angular/common";
import {ProfileModule} from "./profile/profile.module";
import { GameEditComponent } from './game-edit/game-edit.component';
import {ClipboardModule} from "ngx-clipboard";
import {LobbyStateService} from "./services/lobby.state.service";
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';

@NgModule({
  declarations: [
    AppComponent,
    GameLibraryComponent,
    GameDetailComponent,
    GameUploadComponent,
    GameLobbyComponent,
    GameComponent,
    RegisterComponent,
    LoginComponent,
    ResetPasswordComponent,
    RequestEmailComponent,
    GameEditComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    ProfileModule,
    ReactiveFormsModule,
    FormsModule,
    BrowserAnimationsModule,
    MatListModule,
    MatIconModule,
    MatFormFieldModule,
    MatSlideToggleModule,
    NgxDropzoneModule,
    AngularToastifyModule,
    ClipboardModule,
    MatProgressSpinnerModule,
  ],
  providers: [
    GameService,
    CommunicationService,
    ToastService,
    LobbyStateService,
    {provide: LocationStrategy, useClass: HashLocationStrategy},
    {provide: HTTP_INTERCEPTORS, useClass: TokenInterceptorService, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
