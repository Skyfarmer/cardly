import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from "@angular/router";
import {AuthenticationService} from "../services/authentication.service";
import {CreatePlayerDTO} from "../models/createPlayerDTO";
import {ToastService} from "angular-toastify";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {
  nonWhiteSpaceRegExp: RegExp = new RegExp('\\S');
  // @ts-ignore
  checkPasswords: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
    const pass = control.get('password')?.value;
    const confirmPass = control.get('confirmPassword')?.value;
    return pass !== confirmPass ? {notSame: true} : null;
  };

  registrationForm: FormGroup = this.formBuilder.group({
    alias: ['',[Validators.required]],
    email: ['', {validators: [Validators.required, Validators.email], updateOn: "blur"}],
    password: ['', [Validators.required, Validators.minLength(8)]],
    confirmPassword: [''],
  }, {validator: this.checkPasswords});


  constructor(private route: ActivatedRoute,
              private formBuilder: FormBuilder,
              private router: Router,
              private authenticationService: AuthenticationService,
              private toastService: ToastService) {
  }

  onSubmit() {
    const email = this.registrationForm.controls['email']?.value;
    const password = this.registrationForm.controls['password']?.value;
    const alias = this.registrationForm.controls['alias']?.value;

    let player: CreatePlayerDTO = {
      email: email,
      password: password,
      alias: alias
    };

    this.authenticationService.register(player).subscribe((data) => {
      console.log('Registered', data);
      this.toastService.success("Registration successful")
      this.router.navigate(['/login'], {});
    }, error => {
      console.error(error)
      if(error.status === 400) {
        this.toastService.error("User already exists")
      }
    });
  }
}
