import {EventEmitter, Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {PlayerDTO} from "../models/playerDTO";
import {CreatePlayerDTO} from "../models/createPlayerDTO";
import {LoginPlayerDTO} from "../models/loginPlayerDto";
import {catchError, Observable, of, tap, throwError} from "rxjs";
import {JwtHelperService} from "@auth0/angular-jwt";
import {RequestEmailDto} from "../models/requestEmailDto";
import {ResetPasswordDto} from "../models/resetPasswordDto";
import {changePasswordDTO} from "../models/changePasswordDTO";
import {AppComponent} from "../app.component";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  readonly baseURL: string;
  private jwtHelper = new JwtHelperService();
  loggedIn = new EventEmitter<boolean>();

  constructor(private http: HttpClient, private router: Router) {
    this.baseURL = environment.backendBaseUrl;
  }

  requestEmail(dto: RequestEmailDto): Observable<any> {
    return this.http.post(`${this.baseURL}/requestEmail`, dto)
      .pipe(catchError(this.errorStrategy));
  }

  resetPassword(dto: ResetPasswordDto): Observable<any> {
    return this.http.post(`${this.baseURL}/resetPassword`, dto)
      .pipe(catchError(this.errorStrategy));
  }

  updatePassword(dto: changePasswordDTO): Observable<any> {
    return this.http.put<PlayerDTO>(`${this.baseURL}/user/password`, dto)
      .pipe(catchError(this.errorStrategy));
  }

  register(cpDTO: CreatePlayerDTO): Observable<PlayerDTO> {
    return this.http.post<PlayerDTO>(`${this.baseURL}/register`, cpDTO)
      .pipe(catchError(this.errorStrategy));
  }

  login(dto: LoginPlayerDTO): Observable<string> {
    const url = this.baseURL + "/login";
    return this.http.post(url, dto, {responseType: 'text'})
      .pipe(catchError(this.errorStrategy))
      .pipe(
        tap((authResponse: string) => this.setToken(authResponse))
      );
  }

  logout() {
    localStorage.clear();
    this.loggedIn.emit(false)
  }

  isLoggedIn(): boolean {
    if (localStorage.getItem("jwt") != null && !this.jwtHelper.isTokenExpired(localStorage.getItem("jwt")!, 0)) {
      return true;
    } else {
      this.logout();
      return false;
    }
  }

  getPlayerId(): string | null {
    return localStorage.getItem("playerId");
  }

  getSub(): string | null {
    return localStorage.getItem("sub");
  }

  private setToken(authResponse: string) {
    const x = this.jwtHelper.decodeToken(authResponse);

    console.log(x);

    localStorage.setItem("loggedIn", "true")
    localStorage.setItem("playerId", x.id);
    localStorage.setItem("jwt", authResponse);
    localStorage.setItem("sub", x.sub);
  }

  private errorStrategy(err: any) {
    console.log('An error has occurred', err);
    return throwError(err);
  }
}
