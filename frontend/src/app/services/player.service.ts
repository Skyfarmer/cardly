import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {PlayerDTO} from "../models/playerDTO";
import {environment} from "../../environments/environment";
import {catchError, throwError} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class PlayerService {

  readonly baseURL: string

  constructor(private http: HttpClient) {
    this.baseURL = environment.backendBaseUrl;
  }

  getPlayerInformation(id: number) {
    return this.http.get<PlayerDTO>(`${this.baseURL}/user/${id}`)
      .pipe(catchError(this.errorStrategy));
  }

  updatePlayer(player: PlayerDTO) {
    return this.http.put<PlayerDTO>(`${this.baseURL}/user/`, player)
      .pipe(catchError(this.errorStrategy));
  }

  private errorStrategy(err: any) {
    console.log('An error has occurred', err);
    return throwError(err);
  }
}
