import {TestBed} from '@angular/core/testing';

import {GamePlayService} from './game-play.service';
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {RouterTestingModule} from "@angular/router/testing";

describe('GamePlayService', () => {
  let service: GamePlayService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [GamePlayService]
    });
    service = TestBed.inject(GamePlayService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
