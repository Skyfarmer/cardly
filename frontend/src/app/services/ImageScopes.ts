export enum ImageScopes {
  CARD = "CARD",
  THUMBNAIL = "THUMBNAIL",
  PRESENTATION = "PRESENTATION"
}
