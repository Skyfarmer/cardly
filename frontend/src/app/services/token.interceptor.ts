import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";
import {AuthenticationService} from "./authentication.service";
import { Injectable } from '@angular/core';

@Injectable()
export class TokenInterceptorService implements HttpInterceptor {
  constructor(private authService: AuthenticationService) {

  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let newHeaders = req.headers;
    if (this.authService.isLoggedIn()) {
      newHeaders = newHeaders.append('Authentication', "Bearer " + localStorage.getItem("jwt")!);
    } else {
      console.log('Not appending bearer token to request');
    }

    const authReq = req.clone({headers: newHeaders});
    return next.handle(authReq);
  }
}
