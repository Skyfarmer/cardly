import { Injectable } from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class LobbyStateService {
  sessionId: number = -1;
}
