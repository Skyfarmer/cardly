import {Injectable} from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {catchError, Observable, of, throwError} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  readonly baseURL: string;

  constructor(private http: HttpClient) {
    this.baseURL = environment.backendBaseUrl;
  }

  loadImage(gameId: number, fileName: string) {
    return this.http.get(`${this.baseURL}/game/${gameId}/image/${fileName}`)
      .pipe(catchError(this.errorStrategy));
  }

  uploadImage(gameId: number, file: FormData, scope: string) {
    return this.http.post(`${this.baseURL}/game/${gameId}/image/${scope}`, file)
      .pipe(catchError(this.errorStrategy));
  }

  uploadMultipleImages(gameId: number, files: FormData, scope: string) {
    return this.http.post(`${this.baseURL}/game/${gameId}/images/${scope}`, files)
      .pipe(catchError(this.errorStrategy));
  }

  deleteImage(gameId: number, fileName: string) {
    return this.http.delete(`${this.baseURL}/game/${gameId}/image/${fileName}`)
      .pipe(catchError(this.errorStrategy));
  }

  private errorStrategy(err: any) {
    console.log('An error has occurred', err);
    return throwError(err);
  }
}
