import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {GameDTO} from "../models/gameDTO";
import {environment} from "../../environments/environment";
import {catchError, map, Observable, of, throwError} from "rxjs";
import {SessionDTO} from "../models/sessionDTO";

@Injectable({
  providedIn: 'root'
})
export class GameService {

  readonly baseURL: string;

  constructor(private http: HttpClient) {
    this.baseURL = environment.backendBaseUrl;
  }

  getGameById(id: number): Observable<GameDTO> {
    return this.http.get<GameDTO>(`${this.baseURL}/game/${id}`)
      .pipe(catchError(this.errorStrategy));
  }

  join(token: string): Observable<SessionDTO> {
    return this.http.get<SessionDTO>(`${this.baseURL}/join?token=${token}`)
      .pipe(catchError(this.errorStrategy));
  }

  getGamesByName(name: string): Observable<GameDTO[]> {
    return this.http.get<GameDTO[]>(`${this.baseURL}/games/${name}`)
      .pipe(catchError(this.errorStrategy));
  }

  getGamesByCreator(id: number): Observable<GameDTO[]> {
    return this.http.get<GameDTO[]>(`${this.baseURL}/games/creator/${id}`)
      .pipe(catchError(this.errorStrategy));
  }

  getGames(page = 0, size = 100): Observable<GameDTO[]> {
    return this.http.get<GameDTO[]>(`${this.baseURL}/games?page=${page}&size=${size}`)
      .pipe(catchError(this.errorStrategy));
  }

  createGame(game: GameDTO): Observable<GameDTO> {
    return this.http.post<GameDTO>(`${this.baseURL}/game`, game)
      .pipe(catchError(this.errorStrategy));
  }

  updateGame(game: GameDTO): Observable<GameDTO> {
    return this.http.put<GameDTO>(`${this.baseURL}/game/${game.id}`, game)
      .pipe(catchError(this.errorStrategy));
  }

  createSession(session: SessionDTO, gameId: number): Observable<SessionDTO> {
    return this.http.post<SessionDTO>(`${this.baseURL}/game/${gameId}/session`, session)
      .pipe(catchError(this.errorStrategy));
  }

  getSession(sessionId: number): Observable<SessionDTO> {
    return this.http.get<SessionDTO>(`${this.baseURL}/session/${sessionId}`)
      .pipe(catchError(this.errorStrategy));
  }

  matchMaking(gameId: number, playerId: number): Observable<SessionDTO> {
    return this.http.post<SessionDTO>(`${this.baseURL}/game/${gameId}/player/${playerId}/matchmaking`, gameId)
      .pipe(catchError(this.errorStrategy));
  }

  startGame(session: SessionDTO): Observable<SessionDTO> {
    return this.http.post<SessionDTO>(`${this.baseURL}/session/${session.id}/start`, session)
      .pipe(catchError(this.errorStrategy));
  }

  addPlayer(sessionId: number, playerId: number): Observable<SessionDTO> {
    return this.http.post<SessionDTO>(`${this.baseURL}/session/${sessionId}/player/${playerId}`, sessionId)
      .pipe(catchError(this.errorStrategy));
  }

  removePlayer(sessionId: number, playerId: number): Observable<any> {
    return this.http.delete(`${this.baseURL}/session/${sessionId}/player/${playerId}`)
      .pipe(catchError(this.errorStrategy));
  }

  shutdownGame(session: SessionDTO): Observable<SessionDTO> {
    return this.http.post<SessionDTO>(`${this.baseURL}/session/${session.id}/shutdown`, session)
      .pipe(catchError(this.errorStrategy));
  }

  private errorStrategy(err: any) {
    console.log('An error has occurred', err);
    return throwError(err);
  }
}
