import {Injectable} from "@angular/core";
import {environment} from "../../environments/environment";
import {webSocket, WebSocketSubject} from "rxjs/webSocket";
import {catchError, EMPTY, Subject, tap} from "rxjs";
import {serialize, Response} from "../models/messaging/responses";
import {deserialize} from "../models/messaging/requests";

@Injectable({
  providedIn: 'root'
})
export class CommunicationService {
  private baseURL: string;
  private socket$: WebSocketSubject<any> | undefined;
  public messages$ = new Subject();

  constructor() {
    this.baseURL = environment.backendWsUrl;
  }

  public connect(session_id: number, playerId: number): void {
    console.log("Connecting websocket with session " + session_id + " and player id " + playerId);
    if (!this.socket$ || this.socket$.closed) {
      this.socket$ = webSocket(this.baseURL + `session/${session_id}/player/${playerId}`);
      this.socket$.pipe(
        tap({
          error: error => console.log(error),
        }), catchError(_ => EMPTY)
      ).subscribe((value) => {
        console.log('Message received', value);
        this.messages$.next(deserialize(value));
      });
      console.log("Web socket connection established");
    }
  }

  sendMessage(msg: Response) {
    this.socket$?.next(serialize(msg));
  }

  close() {
    if(this.socket$ != undefined) {
      this.socket$.complete();
      this.socket$ = undefined;
    }

    this.messages$.complete()
    this.messages$ = new Subject()
  }

}
