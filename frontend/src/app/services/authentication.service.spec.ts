import {TestBed} from '@angular/core/testing';
import {AuthenticationService} from './authentication.service';
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {RouterTestingModule} from "@angular/router/testing";
import {JWT_OPTIONS, JwtHelperService} from "@auth0/angular-jwt";

describe('AuthenticationService', () => {
  let service: AuthenticationService;

  beforeEach(async () => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule,RouterTestingModule],
      providers: [
        AuthenticationService,
        {provide: JWT_OPTIONS, useValue: JWT_OPTIONS},
        JwtHelperService
      ]
    });
    service = TestBed.inject(AuthenticationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
