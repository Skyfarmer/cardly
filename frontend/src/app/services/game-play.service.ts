import {EventEmitter, Injectable} from '@angular/core';
import {CommunicationService} from "./communication.service";
import {Response, SelectedOptions, SelectedState, TryCardMoved, TryDeckSelection} from "../models/messaging/responses";
import {
  CardFlipped,
  CardMoved,
  CurrentPlayer,
  DeckReordered,
  Error,
  GameConfiguration,
  GameStarted,
  PlayerJoin,
  PlayerLeave,
  IndividualText,
  Request,
  UserSelection, Info, GameOver, GameStopped, GameCrashed, PlayerAlreadyJoined
} from "../models/messaging/requests";
import * as createjs from 'createjs-module';
import {messageAlert, userSelectionDialog, winAlert, yourTurnAlertWithImage} from "../elements/alertNotifications";
import {Deck, DeckType} from "../elements/deck";
import {Card} from "../elements/card";
import {Card as CardDTO, Deck as DeckDTO, DeckLayout} from "../models/gameDTO"
import {Button} from "../elements/button";
import {SessionDTO} from "../models/sessionDTO";
import {GameService} from "./game.service";
import {firstValueFrom} from "rxjs";
import {GameStatus} from "../models/gameStatus";
import {Table} from "../elements/table";
import {SlotDTO} from "../models/slotDTO";
import {VerticalLayout} from "../elements/verticalLayout";
import {Background} from "../elements/background";
import {Router} from "@angular/router";
import {Tooltip} from "../elements/tooltip";
import {ToastService} from "angular-toastify";
import Swal from "sweetalert2";

enum Location {
  Up,
  Down,
  Left,
  Right
}

@Injectable({
  providedIn: 'root'
})
export class GamePlayService {
  allowedToPlay: boolean = false;
  mainPlayerDecks: Deck[] = []
  stage: createjs.Stage;
  selectedCard: Card | null = null
  selectedCardTimeoutId = -1
  decks: Deck[] = [];
  session: SessionDTO | null = null
  playerId: string | null = null
  buttons: createjs.Container | null = null
  eventQueue: Request[] = []
  isStarted: boolean = false;

  playerEvents = new EventEmitter();


  constructor(private gameService: GameService,
              private router: Router,
              private communicationService: CommunicationService,
              private toastService: ToastService) {
    this.stage = new createjs.Stage("demoCanvas1")
    createjs.Ticker.framerate = 60
    createjs.Ticker.on("tick", (e: any) => this.stage.update(e))

  }

  clear() {
    this.allowedToPlay = false;
    this.mainPlayerDecks = []
    this.stage.clear()
    this.decks = []
    this.selectedCard = null
    this.session = null
    this.buttons = null
    this.eventQueue = []
    this.isStarted = false;
    this.communicationService.close()
  }

  startSocketConnectionBackend(sessionId: number, playerId: number) {
    this.communicationService.connect(sessionId, playerId);
    this.communicationService.messages$.subscribe((value) => {
      let v = value as any
      if (v.type === "GameStarted") {
        this.handleRequest(v, sessionId)
      } else {
        this.eventQueue.push(value as Request)
        while (this.eventQueue.length !== 0) {
          this.handleRequest(this.eventQueue[0])
          this.eventQueue = this.eventQueue.slice(1)
        }
      }
    })
  }

  stopSocketConnection() {
    this.communicationService.close();
  }

  private getAliasForExecId(id: number): string | undefined {
    let player = this.session?.slots.find(slot => {
      return slot.slotNumber === id
    })
    if (player === undefined) {
      console.error("Cannot find player with execution-engine id", id)
      return undefined
    } else {
      return player.player.alias
    }
  }

  private substitutePlayerText(input: string): string {
    for (let i = 0; i < input.length; i++) {
      let start = input.indexOf('{', i)
      let end = input.indexOf('}', i)
      if (start === -1 || end === -1) {
        return input
      }
      let exec_id = parseInt(input.substring(start + 1, end))
      let alias = this.getAliasForExecId(exec_id)
      if (alias === undefined) {
        return ""
      } else {
        input = input.substring(0, start).concat(alias, input.substring(end + 1))
      }
    }
    return input
  }

  handleRequest(request: any, sessionId?: number) {
    switch (request.type) {
      case "GameConfiguration":
        let gameConfig = request as GameConfiguration;
        this.initGame(gameConfig)
        break;
      case "DeckReordered": {
        let req = request as DeckReordered;
        let deck = this.decks.find(d => d.deckId === req.deck.id) as Deck;
        let cards = this.initCards(req.deck.set);
        deck.removeAllChildren();
        cards.forEach(c => deck.addCard(c))
        break;
      }
      case "CardFlipped": {
        let req = request as CardFlipped

        let deck = this.decks.find(d => d.deckId === req.deck_id) as Deck;
        let c = undefined;
        for (let card of deck.children) {
          if (card instanceof Card) {
            if (card.value === req.old_card.value && card.color === req.old_card.color) {
              c = card
            }
          }
        }
        if (c === undefined) {
          console.warn("Could not find card to flip, it is likely already visible to the player")
        } else {
          c.update(req.new_card.value, req.new_card.color)
        }

        break;
      }
      case "CardMoved": {
        let req = request as CardMoved

        let fromDeck = this.decks.find(d => d.deckId === req.from) as Deck;
        let toDeck = this.decks.find(d => d.deckId === req.to) as Deck;

        let claimOtherCard = true
        if (this.selectedCard !== null) {
          //Assert that this is the same card
          if (this.selectedCard.value == req.card.value && this.selectedCard.color == req.card.color) {
            //Assert that this card originates from the same deck
            let card = this.selectedCard as any;
            if (card.old_parent == fromDeck) {
              toDeck.addCard(card)
              window.clearTimeout(this.selectedCardTimeoutId)
              this.selectedCard = null
              claimOtherCard = false
            } else {
              console.warn("Super weird, the player moved a card but the engine says that it originates from" +
                "somewhere else")
              this.putBackSelectedCard()
            }
          } else {
            //Interestingly, another card gets moved, just place this one back
            this.putBackSelectedCard()
          }
        }
        if (claimOtherCard) {
          let card = fromDeck.children.find(c => {
            if (c instanceof Card) {
              return c.value === req.card.value && c.color === req.card.color
            } else {
              return false
            }
          }) as Card;
          toDeck.addCard(card)
        }
        break;
      }
      case "CurrentPlayer": {
        Swal.close()
        let req = request as CurrentPlayer

        // Find the position of the current player
        let myIndex = -1
        let execId = this.session?.slots.find((slot) => {
          myIndex += 1
          return slot.player.id.toString() === this.playerId
        })?.slotNumber.toString()

        let slots = this.session?.slots as SlotDTO[]
        let winnerIndex = 0;
        for (let i = 0; i < slots.length; i++) {
          if (slots[(i + myIndex) % slots.length].slotNumber.toString() === req.player_id) {
            winnerIndex = i
            break;
          }
        }

        // Indicate the current player
        for (let i = 0; i < this.mainPlayerDecks.length; i++) {
          let playerDeck = this.mainPlayerDecks[i]
          if (i === winnerIndex) {
            playerDeck.customGraphics = (graphics) => {
              return graphics.beginFill("lightgreen")
            }
          } else {
            playerDeck.customGraphics = (graphics => graphics)
          }
        }

        if (execId === req.player_id) {
          console.log("It's my turn!")
          if (!this.allowedToPlay) {
            yourTurnAlertWithImage()
          }

          this.allowedToPlay = true
        } else if (execId !== req.player_id) {
          console.log("It's player's", req.player_id, "turn")
          // Another player is currently playing
          this.allowedToPlay = false
        }
        if (req.additional_options !== null) {
          if (this.buttons !== null) {
            this.stage.removeChild(this.buttons)
          }

          if (req.additional_options.length == 1 && !req.other_options_exist) {
            // Immediately send selection message because the player does not have any other options
            const msg: SelectedState = {
              type: "SelectedState",
              option: 0
            }
            this.sendSocketMessageToBackend(msg)
          } else {
            this.buttons = this.addContainerWithButtons(req.additional_options)
            this.stage.addChild(this.buttons)
          }
        }

        break;
      }
      case "UserSelection": {
        let req = request as UserSelection
        let isSingleton = true
        for (let o of req.options) {
          if (o.length !== 1) {
            isSingleton = false
            userSelectionDialog(this, req);
            break
          }
        }
        // If only one option is possible, don't open the dialog
        if (isSingleton) {
          let result = new Array(req.options.length)
          result.fill(0)
          const obj: SelectedOptions = {
            type: 'SelectedOptions',
            options: result,
          };
          this.sendSocketMessageToBackend(obj)
        }
        break;
      }
      case "GameStarted": {
        this.showLoadingAlert()
        if (sessionId === undefined) {
          console.warn("Player has not yet joined a session")
          break
        }
        firstValueFrom(this.gameService.getSession(sessionId)).then((session) => {
          this.session = session
          if (this.session.status !== GameStatus.STARTED) {
            console.warn("Expected session to be started")
          } else {
            this.isStarted = true;
            this.router.navigate(["/session"]).then(() => {
              console.log("Navigating to game view")
            })
          }
        })
        break;
      }
      case "GameStopped": {
        messageAlert("A player has left the game", "red", false, 4000, "transparent", () => {
          this.stopSocketConnection();
          this.router.navigate(["/"]).then(() => {
            this.clear()
          })
        })
        break;
      }
      case "GameCrashed": {
        let req = request as GameCrashed
        console.log("Received GameCrashed", req)
        messageAlert("The game crashed unexpectedly", "red", false, 4000, "transparent", () => {
          this.stopSocketConnection();
          this.router.navigate(["/"]).then(() => {
            this.clear()
          })
        })
        break;
      }
      case "GameOver": {
        let req = request as GameOver
        let winners = req.winners.map(id => {
          let alias = this.getAliasForExecId(id)
          if (alias === undefined) {
            return ""
          } else {
            return alias
          }
        })
        this.isStarted = false
        winAlert(winners, () => {
          this.stopSocketConnection();
          this.router.navigate(["/"]).then(() => {
            this.clear()
          })
        })
        break;
      }
      case "Info": {
        let req = request as Info
        this.toastService.default(this.substitutePlayerText(req.message))
        break
      }
      case "Error": {
        let req = request as Error;
        this.toastService.error(this.substitutePlayerText(req.message))
        break;
      }
      case "PlayerAlreadyJoined": {
        let req = request as PlayerAlreadyJoined;
        this.playerEvents.emit(req)
        break;
      }
      case "PlayerJoin": {
        let req = request as PlayerJoin;
        if (req.slotDto.player.id.toString() != this.playerId) {
          this.playerEvents.emit(req)
        } else {
          console.log("Skipping PlayerJoin message because self join")
        }
        break;
      }
      case "PlayerLeave": {
        let req = request as PlayerLeave;
        if (req.slotDto.player.id.toString() != this.playerId && this.isStarted) {
          this.playerEvents.emit(req)
        } else {
          console.log("Skipping PlayerLeave message because self join")
        }
        break;
      }
      case "IndividualText": {
        let req = request as IndividualText;
        this.stage.removeChild(this.stage.getChildByName("scoreboard"))

        let score = this.addScoreBoard(this.substitutePlayerText(req.text))
        let tmp = () => {
          let canvas = this.stage.canvas as HTMLCanvasElement
          score.setBounds(0, 0, canvas.width * 0.2, canvas.height * 0.32)
          score.setTransform(canvas.width * 0.8, canvas.height * 0.68)
          for (let c of score.children) {
            let b = c as Button<any>
            b.lineWidth = canvas.width * 0.2
            b.updateGraphics(b.lineWidth)
          }
        }
        this.stage.addChild(score)
        tmp()
        window.addEventListener("resize", tmp)
        break;
      }
      default:
        console.error("Received request is unknown", request)
    }
  }

  // Sends Message to Server Socket, is converted to a Response beforehand
  sendSocketMessageToBackend(msg: Response) {
    if (this.allowedToPlay) {
      console.log("Sending socket message", msg)
      this.communicationService.sendMessage(msg);
    }
  }

  initGame(gameConfig: GameConfiguration) {
    // create stage and point it to the canvas:
    let canvas = document.getElementById("demoCanvas1") as HTMLCanvasElement;

    canvas.oncontextmenu = (e) => {
      e.preventDefault();
      return false;
    };

    let tmp = () => {
      let canvas = document.getElementById("demoCanvas1") as HTMLCanvasElement;
      let height = document.getElementById("navbar") as HTMLElement
      canvas.width = window.innerWidth
      canvas.height = window.innerHeight - height.offsetHeight
    }
    tmp()
    window.addEventListener("resize", tmp);


    this.stage = new createjs.Stage(canvas);

    // enable touch interactions if supported on the current device:
    createjs.Touch.enable(this.stage);

    // enabled mouse over / out events
    this.stage.enableMouseOver(10);
    this.stage.mouseMoveOutside = true; // keep tracking the mouse even when it leaves the canvas

    // Init background
    this.initBackground()

    // Init global decks with the main table
    this.decks = this.initDecks(gameConfig.global_decks);
    this.initMainTable(this.decks.slice())

    // Find my slot number which is equal to the internal player number
    // of the execution engine
    let slots = this.session?.slots as SlotDTO[]
    let me = slots.find((slot) => {
      return slot.player.id.toString() === this.playerId
    })?.slotNumber as number
    for (let i = 0; i < slots.length; i++) {
      let p = (me + i) % slots.length
      let playerDecks = this.initDecks(gameConfig.player_decks[p]);
      if (playerDecks.length == 0) {
        // Don't setup any decks if there aren't any
        continue;
      }
      // Sort cards for player decks
      for (let d of playerDecks) {
        d.sort = true
      }

      this.mainPlayerDecks[i] = playerDecks[0];
      (this.mainPlayerDecks[i] as any).slotNumber = p

      switch (i) {
        case 0: {
          this.stage.addChild(this.getTableForPlayer(Location.Down, playerDecks, 0.8, 0.34))
          break;
        }
        case 1:
          if (this.session?.slots.length === 2) {
            this.stage.addChild(this.getTableForPlayer(Location.Up, playerDecks, 0.8, 0.2))
          } else {
            this.stage.addChild(this.getTableForPlayer(Location.Left, playerDecks, 0.1, 0.66))
          }
          break;
        case 2:
          this.stage.addChild(this.getTableForPlayer(Location.Up, playerDecks, 0.8, 0.2))
          break;
        case 3:
          this.stage.addChild(this.getTableForPlayer(Location.Right, playerDecks, 0.1, 0.66))
          break;
        default:
          console.error("Dynamic player layout not yet supported")
      }

      this.decks.push(...playerDecks);
    }

    this.stage.on("stagemousemove", (evt: any) => {
      let tooltipUsed = false
      this.mainPlayerDecks.slice(1).forEach(d => {
        let bounds = d.getBounds()
        let local = d.globalToLocal(evt.stageX, evt.stageY)
        if (local.x > 0 && local.x < bounds.width && local.y > 0 && local.y < bounds.height) {
          let tooltip = this.stage.getChildByName("tooltip")
          if (tooltip === null) {
            let alias = this.session?.slots[(d as any).slotNumber].player.alias as string
            tooltip = new Tooltip(alias)
            tooltip.name = "tooltip"
            this.stage.addChild(tooltip)
          }
          let canvas = this.stage.canvas as HTMLCanvasElement
          if (evt.stageX < canvas.width / 2) {
            tooltip.setTransform(evt.stageX, evt.stageY);
          } else {
            tooltip.setTransform(evt.stageX - tooltip.getBounds().width, evt.stageY)
          }
          tooltipUsed = true
        }
      })
      if (!tooltipUsed) {
        let tooltip = this.stage.getChildByName("tooltip")
        if (tooltip !== null) {
          this.stage.removeChild(tooltip)
        }
      }
    })
  }

  initBackground() {
    let bg = new Background()
    let tmp = () => {
      let canvas = this.stage.canvas as HTMLCanvasElement
      bg.setBounds(0, 0, canvas.width, canvas.height)
      bg.updateGraphics()
    }
    this.stage.addChild(bg)
    window.addEventListener("resize", tmp)
    tmp()
  }

  initMainTable(globalDecks: Deck[]) {
    let table = new Table(globalDecks, 0.8, 0.56)
    let tmp = () => {
      let canvas = this.stage.canvas as HTMLCanvasElement
      table.setTransform(canvas.width * 0.1, canvas.height * 0.2)
    }
    window.addEventListener("resize", tmp)
    tmp()
    this.stage.addChild(table)
  }

  initCards(cards: CardDTO[]): Card[] {
    return cards.map(c => {
      let card = new Card(c.value, c.color, this.session?.game.id as number);
      let self = this;

      let ignoreProcessing = false
      card.on("mousedown", function (this: any, evt: any) {
        if(evt.nativeEvent.defaultPrevented) {
          console.log("Detecting right click")
          self.putBackSelectedCard()
        }

        if (self.selectedCard !== null) {
          ignoreProcessing = true
          console.log("Card is still being processed")
          return
        }

        ignoreProcessing = false
        window.clearTimeout(self.selectedCardTimeoutId)


        this.setShadow(true);

        this.old_parent = this.parent;
        this.old_index = this.parent.getChildIndex(this) - 1;

        this.removeTweens()
        let local_offset = this.globalToLocal(evt.stageX, evt.stageY)
        this.x = evt.stageX - local_offset.x;
        this.y = evt.stageY - local_offset.y;
        this.offset = {x: this.x - evt.stageX, y: this.y - evt.stageY};

        self.stage.addChild(this);
        self.selectedCard = this
      });

      card.on("pressup", function (this: any, evt: any) {
        if (ignoreProcessing) {
          return
        }

        console.log(this)
        this.setShadow(false);
        self.decks.forEach(d => d.setDropHint(false));
        let nothingSelected = true

        self.decks.forEach(d => {
          let bounds = d.getBounds()
          let local = d.globalToLocal(evt.stageX, evt.stageY)
          if (local.x > 0 && local.x < bounds.width && local.y > 0 && local.y < bounds.height) {
            nothingSelected = false
            if (d.deckId === this.old_parent.deckId) {
              //Trigger as deck selection
              let msg: TryDeckSelection = {
                type: "TryDeckSelection",
                deck: d.deckId
              };
              self.sendSocketMessageToBackend(msg)
            } else {
              let c: CardDTO = {
                value: card.value,
                color: card.color
              };
              let msg: TryCardMoved = {
                type: 'TryCardMoved',
                card: c,
                from: this.old_parent.deckId,
                to: d.deckId
              };
              self.sendSocketMessageToBackend(msg)
            }

            self.selectedCardTimeoutId = window.setTimeout(() => {
              self.putBackSelectedCard()
            }, 1000)
          }
        })

        if (nothingSelected) {
          self.putBackSelectedCard()
        }
        this.old_parent.transformCards();
      });

      // the pressmove event is dispatched when the mouse moves after a mousedown on the target until the mouse is released.
      card.on("pressmove", function (this: any, evt: any) {
        if (ignoreProcessing) {
          return
        }

        this.x = evt.stageX + this.offset.x;
        this.y = evt.stageY + this.offset.y;

        self.decks.forEach(d => {
          let bounds = d.getBounds()
          let local = d.globalToLocal(evt.stageX, evt.stageY)
          let cond = local.x > 0 && local.x < bounds.width && local.y > 0 && local.y < bounds.height
          d.setDropHint(cond)
        })
      });

      return card;
    });
  }

  initDecks(global_decks: DeckDTO[]): Deck[] {
    let decks: Deck[] = [];

    let i = 0;
    for (let d of global_decks) {
      let deck_type = DeckType.Spreaded;
      switch (d.layout) {
        case DeckLayout.Spreaded:
          deck_type = DeckType.Spreaded;
          break;
        case DeckLayout.Stacked:
          deck_type = DeckType.Stacked;
          break;
      }
      let cards = this.initCards(d.set);
      let new_deck = new Deck(d.id, deck_type);
      cards.forEach(c => new_deck.addCard(c));

      decks.push(new_deck);

      i += 1;
    }

    decks.forEach(d => this.stage.addChild(d))

    return decks;
  }

  getTableForPlayer(location: Location, playerDecks: Deck[], partX: number, partY: number): Table {
    let container = new Table(playerDecks, partX, partY)
    switch (location) {
      case Location.Down: {
        let tmp = () => {
          let canvas = this.stage.canvas as HTMLCanvasElement
          container.setTransform(canvas.width * 0.1, canvas.height * 0.68)
        }
        window.addEventListener("resize", tmp)
        tmp()
        break;
      }
      case Location.Left: {
        let tmp = () => {
          let canvas = this.stage.canvas as HTMLCanvasElement
          container.setTransform(canvas.width * 0.1, canvas.height * 0.1, undefined, undefined, 90)
        }
        window.addEventListener("resize", tmp)
        tmp()
        break
      }
      case Location.Up: {
        let tmp = () => {
          let canvas = this.stage.canvas as HTMLCanvasElement
          container.setTransform(canvas.width * 0.1, 0)
        }
        window.addEventListener("resize", tmp)
        tmp()
        break
      }
      case Location.Right: {
        let tmp = () => {
          let canvas = this.stage.canvas as HTMLCanvasElement
          container.setTransform(canvas.width * 0.9, canvas.height * 0.76, undefined, undefined, -90)
        }
        window.addEventListener("resize", tmp)
        tmp()
        break
      }
    }
    return container
  }

  addScoreBoard(message: string) {
    let score = new Button(message, 0);
    let layout = new VerticalLayout()
    layout.name = "scoreboard"
    layout.addChild(score)
    return layout
  }

  addContainerWithButtons(additionalOptions: string[]) {
    // additional Options from socket message
    let layout = new VerticalLayout()

    for (let i = 0; i < additionalOptions.length; i++) {
      let button = new Button(additionalOptions[i], i);
      let self = this;

      button.on("click", function (this: any) {
        const msg: SelectedState = {
          type: "SelectedState",
          option: this.label
        }
        self.sendSocketMessageToBackend(msg)
      })

      button.on("mousedown", () => {
        button.clicked = true
      });

      button.on("pressup", () => {
        button.clicked = false
      });

      layout.addChild(button)
    }
    let tmp = () => {
      let canvas = this.stage.canvas as HTMLCanvasElement
      layout.setBounds(0, 0, canvas.width * 0.2, canvas.height * 0.32)
      layout.setTransform(0, canvas.height * 0.68)
      for (let c of layout.children) {
        let b = c as Button<any>
        b.lineWidth = canvas.width * 0.3
        b.updateGraphics(b.lineWidth)
      }
    }
    window.addEventListener("resize", tmp)
    tmp()
    return layout
  }

  private putBackSelectedCard() {
    if (this.selectedCard !== null) {
      let card = this.selectedCard as any;

      card.old_parent.removeAllTweens()
      card.old_parent.addCardAt(card, card.old_index)
      this.selectedCard = null
    }
  }

  setPlayerId(playerId: string) {
    this.playerId = playerId
  }

  showLoadingAlert() {
    Swal.fire({
      background: 'transparent'
    })
    Swal.showLoading(null)
  }
}
