import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GameLobbyComponent } from './game-lobby.component';
import {RouterTestingModule} from "@angular/router/testing";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {Router} from "@angular/router";
import {SessionDTO, SessionVisibility} from "../models/sessionDTO";
import {GameStatus} from "../models/gameStatus";
import {GameDTO} from "../models/gameDTO";
import {ImageDTO} from "../models/imageDTO";

describe('GameLobbyComponent', () => {
  let component: GameLobbyComponent;
  let fixture: ComponentFixture<GameLobbyComponent>;
  let router: jasmine.SpyObj<Router>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GameLobbyComponent ],
      imports: [RouterTestingModule, HttpClientTestingModule],
    })
    .compileComponents();
  });

  beforeEach(() => {
    router = TestBed.get(Router);
    let tmpGame: GameDTO = {
      id: 1,
      name: "my_game",
      source_code: "test",
      min_capacity: 2,
      max_capacity: 2,
      description: "The Game description",
      creator: {
        id: 1,
        email: "test@test.test",
        alias: "tester"
      },
      images: new Set<ImageDTO>()
    }
    let session: SessionDTO = {
      id: 1,
      status: GameStatus.NEW,
      slots: [],
      game: tmpGame,
      visibility: SessionVisibility.PUBLIC,
      join_token: ""
    }
    spyOn(router, 'getCurrentNavigation').and.returnValue({ extras: { state: { SessionDTO: session} } } as any);
    fixture = TestBed.createComponent(GameLobbyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
