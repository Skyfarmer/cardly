import {AfterViewChecked, Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {GameService} from "../services/game.service";
import {SessionDTO, SessionVisibility} from "../models/sessionDTO";
import {GameDTO} from "../models/gameDTO";
import {GamePlayService} from "../services/game-play.service";
import {SlotDTO} from "../models/slotDTO";
import {AuthenticationService} from "../services/authentication.service";
import {PlayerDTO} from "../models/playerDTO";
import {environment} from "../../environments/environment";
import {CARD_IMAGE_EXTENSION} from "../elements/card";
import {PlayerJoin, PlayerLeave} from "../models/messaging/requests";
import {browserRefresh} from "../app.component";
import {ToastService} from "angular-toastify";
import {ClipboardService} from "ngx-clipboard";
import {LobbyStateService} from "../services/lobby.state.service";


@Component({
  selector: 'app-game-lobby',
  templateUrl: './game-lobby.component.html',
  styleUrls: ['./game-lobby.component.scss']
})
export class GameLobbyComponent implements OnInit, OnDestroy, AfterViewChecked {

  session: SessionDTO | undefined;
  slots: SlotDTO[] = [];
  players: SlotDTO[] = []
  tableHeaders = ['#', 'Name']
  game: GameDTO | null = null;
  sessionLink: string = ""
  startAllowed: boolean = false;
  playerCanStart: boolean = false;
  correctDesc: string = ""

  constructor(private route: ActivatedRoute,
              private router: Router,
              private gameService: GameService,
              private gamePlayService: GamePlayService,
              private authService: AuthenticationService,
              private toastService: ToastService,
              private lobbyStateService: LobbyStateService,
              private clipBoardService: ClipboardService) {

  }

  ngOnDestroy(): void {
    if (!this.gamePlayService.isStarted) {
      console.log("Stopping socket connection");
      this.gamePlayService.stopSocketConnection();
    }
  }

  setup(id: number) {
    if(id == -1) {
      this.toastService.dismissAll();
      this.toastService.info("You left the session");
      this.router.navigate(["/"]);
    }

    this.gameService.getSession(id).subscribe((session) => {
      this.session = session as SessionDTO;
      this.sessionLink = environment.joinSessionBaseUrl + this.session.join_token
      if (browserRefresh) {
        this.router.navigate(['/games', session.game.id])
      }
      this.initPlayerTable()
      this.canPlayerStartGame()
      this.gamePlayService.setPlayerId(this.authService.getPlayerId()!);
      this.gamePlayService.startSocketConnectionBackend(this.session.id, Number(this.authService.getPlayerId()));
      this.gameService.getGameById(session.game.id).subscribe((game) => {
        this.game = game
        this.correctDesc = this.game.description.replace(/-r-n/g, "\r\n")
      })
      if (this.players.length >= this.session.game.min_capacity) {
        this.startAllowed = true;
      }
      this.handlePlayerEvents()
    }, error => {
      console.log(error)
      this.toastService.info("Please log in first")
    });
  }

  ngOnInit() {
    console.log('SessionId', this.lobbyStateService.sessionId);
    this.gamePlayService.stopSocketConnection();
    this.setup(this.lobbyStateService.sessionId);
  }

  ngAfterViewChecked() {
    let startButton = document.getElementById('start-button')
    if (startButton !== null) {
      if (this.startAllowed) {
        if (this.playerCanStart) {
          startButton.title = "Start"
        } else {
          startButton.title = "Only Player 1 can start the Game"
        }
      } else {
        startButton.title = "Not Enough Players"
      }
    }
  }

  getGameThumbnailUrl(gameId: number) {
    return environment.backendBaseUrl + "/game/" + gameId + "/image/" + "thumbnail" + CARD_IMAGE_EXTENSION
  }

  handlePlayerEvents() {
    this.gamePlayService.playerEvents.subscribe((playerEvent) => {
      if (playerEvent.type === 'PlayerJoin') {
        const slots = this.players;
        slots.push(playerEvent.slotDto);

        slots.sort((a, b) => {
          if (a.slotNumber > b.slotNumber) {
            return 1;
          }

          if (a.slotNumber < b.slotNumber) {
            return -1;
          }

          return 0;
        });

        this.players = slots
        this.toastService.dismissAll()
        this.toastService.info("Player joined the lobby")
        if (this.session !== undefined) {
          if (this.players.length >= this.session.game.min_capacity) {
            this.startAllowed = true;
          }
        }
      } else if (playerEvent.type === "PlayerLeave") {
        this.removePlayer(playerEvent.slotDto)
        this.startAllowed = false;
        this.canPlayerStartGame()
        this.toastService.dismissAll()
        this.toastService.info("Player left the lobby")
      } else if (playerEvent.type == "PlayerAlreadyJoined") {
        this.toastService.dismissAll()
        this.toastService.info("Player has already an open websocket connection")
        this.router.navigate(["/"]);
      }
    })
  }

  canPlayerStartGame() {
    if (this.players[0].slotNumber === 0 && this.players[0].player.id === Number(localStorage.getItem("playerId"))) {
      this.playerCanStart = true
    } else {
      this.playerCanStart = false
    }

  }

  removePlayer(player: SlotDTO) {
    const index = this.players.findIndex(object => {
      return object.player.id === player.player.id
    })
    if (index !== -1) {
      this.players.splice(index, 1)
    }
  }

  initPlayerTable() {
    this.session!.slots.forEach((slot) => {
      if (slot.player !== null) {
        this.players.push(slot)
      }
    })
  }

  startGame() {
    if (this.session !== undefined) {
      this.lobbyStateService.sessionId = this.session.id;
      this.gameService.startGame(this.session).subscribe((session) => {
        this.session = session;
        console.log("Started Game", session);
      });
    } else {
      throw new Error("Game session is undefined")
    }
  }

  numSequence(n: number): Array<number> {
    return Array(n);
  }

  copySessionLink() {
    this.clipBoardService.copyFromContent(this.sessionLink)
  }
}
