import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {AuthenticationService} from "../services/authentication.service";
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LoginPlayerDTO} from "../models/loginPlayerDto";
import {ToastService} from "angular-toastify";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  constructor(private route: ActivatedRoute,
              private formBuilder: FormBuilder,
              private authService: AuthenticationService,
              private toastService: ToastService,
              private router: Router) {
  }

  submitted: boolean = false;

  loginForm: FormGroup = this.formBuilder.group({
      email: ['', {validators: [Validators.required, Validators.email], updateOn: "blur"}],
      password: ['', [Validators.required]]
    }
  );

  @ViewChild('password') passwordElement: ElementRef | undefined;

  ResetPassword() {
    this.router.navigate(['/request-email'])
  }

  login() {
    if (this.loginForm.valid) {
      const authRequest: LoginPlayerDTO = {
        email: this.loginForm.controls['email'].value,
        password: this.loginForm.controls['password'].value,
      };
      this.authService.login(authRequest).subscribe(
        () => {
          console.log("logged in")
          this.router.navigate(['/'], {});
        },
        (error) => {
          console.log(error)

          if(error.status == 401) {
            this.toastService.warn("Login failed");
          } else {
            this.toastService.error("An error occurred");
          }
        }
      );
    } else {
      console.log('Invalid input');

      if (this.passwordElement == null) {
        this.passwordElement!.nativeElement.focus();
        this.passwordElement!.nativeElement.value = '';
      }
    }
  }

}
