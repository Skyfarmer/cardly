use crate::components::draw_type::DrawType;
use crate::components::event::Event;
use crate::components::{Card, Deck};
use crate::lua_game::deck_visibility::DeckVisibility;
use crate::lua_game::{DeckCustom, DeckVisibilityMapping};
use crate::state_machine::states::GlobalState;
use log::{debug, trace, warn};
use rlua::{Context, Scope};
use std::borrow::Borrow;
use std::cell::RefCell;
use std::collections::HashMap;

/// Insert a function into a given Lua scope and context.
// This function looks really complicated but it's basically just an imitation of
// scope.create_function so have a look over there if you want to understand this art.
pub fn insert_method<'lua, 'callback, 'scope, A, R, F>(
    lua_ctx: &'callback Context<'lua>,
    scope: &'callback Scope<'lua, 'scope>,
    name: &'scope str,
    f: F,
) -> rlua::Result<()>
where
    A: rlua::FromLuaMulti<'callback>,
    R: rlua::ToLuaMulti<'callback>,
    F: 'scope + Fn(Context<'callback>, A) -> rlua::Result<R>,
{
    lua_ctx.globals().set(
        name,
        scope.create_function(move |s, params| {
            trace!("{} invoked", name);
            f(s, params)
        })?,
    )
}

/// Insert a mutable function into a given Lua scope and context.
// This function looks really complicated but it's basically just an imitation of
// scope.create_function_mut so have a look over there if you want to understand this art.
pub fn insert_method_mut<'lua, 'callback, 'scope, A, R, F>(
    lua_ctx: &'callback Context<'lua>,
    scope: &'callback Scope<'lua, 'scope>,
    name: &'scope str,
    mut f: F,
) -> rlua::Result<()>
where
    A: rlua::FromLuaMulti<'callback>,
    R: rlua::ToLuaMulti<'callback>,
    F: 'scope + FnMut(Context<'callback>, A) -> rlua::Result<R>,
{
    lua_ctx.globals().set(
        name,
        scope.create_function_mut(move |s, params| {
            trace!("{} invoked", name);
            f(s, params)
        })?,
    )
}

/// Convert a string to the corresponding draw type.
pub fn string_to_draw_type(draw_type: Option<String>) -> rlua::Result<DrawType> {
    match draw_type {
        Some(v) if v == "top" => Ok(DrawType::Top),
        Some(v) if v == "bottom" => Ok(DrawType::Bottom),
        Some(v) if v == "random" => Ok(DrawType::Random),
        None => Ok(DrawType::Top),
        Some(v) => lua_error!("Draw type {} not supported", v),
    }
}

pub fn get_deck_id<T: AsRef<str>>(state: &GlobalState<DeckCustom>, name: T) -> rlua::Result<usize> {
    state
        .custom()
        .1
        .get(name.as_ref())
        .copied()
        .ok_or_else(|| lua_error!(no_err, "Could find deck id for {}", name.as_ref()))
}

pub fn get_deck_name<T: Into<usize>>(
    state: &GlobalState<DeckCustom>,
    id: T,
) -> rlua::Result<String> {
    let id = id.into();
    state
        .custom()
        .1
        .iter()
        .find_map(|(name, i)| if *i == id { Some(name.clone()) } else { None })
        .ok_or_else(|| lua_error!(no_err, "Could not find name for deck with id {}", id))
}

pub fn get_global_deck<T: AsRef<str>>(
    state: &GlobalState<DeckCustom>,
    name: T,
) -> rlua::Result<&RefCell<Deck>> {
    let deck_id = get_deck_id(state, name)?;
    state
        .get_deck(deck_id)
        .ok_or_else(|| lua_error!(no_err, "Deck with id {} not found", deck_id))
}

pub fn get_player_deck<T: AsRef<str>>(
    state: &GlobalState<DeckCustom>,
    player: usize,
    name: T,
) -> rlua::Result<&RefCell<Deck>> {
    let name = format!("{}{}", name.as_ref(), player);
    let deck_id = get_deck_id(state, name)?;
    state
        .player_by_id(player)
        .and_then(|p| p.get_deck(deck_id))
        .ok_or_else(|| lua_error!(no_err, "Deck with id {} not found", deck_id))
}

pub fn parse_player_id<T: AsRef<str>>(id: T) -> rlua::Result<usize> {
    id.as_ref().parse().map_err(|e| lua_error!(no_err, "{}", e))
}

/// Apply the deck visibility to the given deck by adjusting the `Card` restrictions.
pub fn apply_visibility(
    deck: &mut Deck,
    mapping: &mut DeckVisibilityMapping,
    player: Option<usize>,
) -> rlua::Result<()> {
    let v = *mapping.get(&deck.id()).ok_or_else(|| {
        debug!("{:?}", mapping);
        lua_error!(
            no_err,
            "Cannot find a mapped visibility for deck {}",
            deck.id()
        )
    })?;
    let deck_id = deck.id();
    let mut events = Vec::new();

    let mut handle_card = |v: DeckVisibility, c: &mut Card| {
        let prev_vis = c.restrictions().map(Vec::from);
        match v {
            DeckVisibility::Hidden | DeckVisibility::TopOnly => {
                c.update_restrictions([]);
            }
            DeckVisibility::Global => {
                c.clear_restrictions();
            }
            DeckVisibility::Player if player.is_some() => c.update_restrictions([player.unwrap()]),
            DeckVisibility::Player => {
                return lua_error!("Cannot apply visibility for player if no player is given");
            }
        }
        let curr_vis = c.restrictions().map(Vec::from);
        if prev_vis != curr_vis {
            // Trigger event only if restrictions have actually changed
            events.push(Event::CardVisibilityChanged {
                card: c.clone(),
                deck_id,
                old_visibility: prev_vis,
            });
        }
        Ok(())
    };
    let deck_length = deck.len();
    if deck_length > 0 {
        for c in deck.cards_iter_mut().take(deck_length - 1) {
            handle_card(v, c)?;
        }
    }

    deck.peek_top_mut().map_or(Ok(()), |c| match v {
        e @ DeckVisibility::Hidden | e @ DeckVisibility::Global | e @ DeckVisibility::Player => {
            handle_card(e, c)
        }
        DeckVisibility::TopOnly => handle_card(DeckVisibility::Global, c),
    })?;

    if let Some(event_sink) = deck.clone_event_sink() {
        for e in events {
            event_sink.send(e).unwrap();
        }
    }
    Ok(())
}

pub fn card_to_map<B: Borrow<Card>>(card: B) -> HashMap<String, String> {
    let mut tmp = HashMap::new();
    tmp.insert("value".to_string(), card.borrow().value().to_string());
    tmp.insert("color".to_string(), card.borrow().color().to_string());

    tmp
}

/// Map multiple `Card` to a `HashMap` so that it can be used inside Lua.
pub fn cards_to_map<B: Borrow<Card>, T: IntoIterator<Item = B>>(
    cards: T,
) -> Vec<HashMap<String, String>> {
    cards
        .into_iter()
        .map(|c| card_to_map(c))
        .collect::<Vec<_>>()
}

#[cfg(test)]
pub mod tests {
    use crate::components::DeckLayout;
    use crate::lua_game::deck_visibility::DeckVisibility;
    use crate::lua_game::DeckCustom;
    use crate::state_machine::states::test::{test_global_state, test_player_state};
    use std::cell::RefCell;
    use std::collections::HashMap;

    pub fn initial_global_state() -> crate::state_machine::states::GlobalState<DeckCustom> {
        use crate::components::{Card, Deck};

        let mut visibility_mapping = HashMap::new();
        let mut name_mapping = HashMap::new();

        let deck = Deck::new_empty(DeckLayout::Stacked);
        visibility_mapping.insert(deck.id(), DeckVisibility::Player);
        name_mapping.insert("player1".to_string(), deck.id());
        let player1 = test_player_state(1, vec![deck]);

        let deck = Deck::new_empty(DeckLayout::Spreaded);
        visibility_mapping.insert(deck.id(), DeckVisibility::Player);
        name_mapping.insert("player2".to_string(), deck.id());
        let player2 = test_player_state(2, vec![deck]);

        let deck = Deck::new(
            vec![
                Card::new(0, "red"),
                Card::new(1, "red"),
                Card::new(2, "red"),
            ]
            .into(),
            DeckLayout::Stacked,
            None,
        );
        let mut decks = Vec::new();

        visibility_mapping.insert(deck.id(), DeckVisibility::Global);
        name_mapping.insert("main".to_string(), deck.id());

        decks.push(RefCell::new(deck));

        let deck = Deck::new_empty(DeckLayout::Spreaded);
        visibility_mapping.insert(deck.id(), DeckVisibility::Hidden);
        name_mapping.insert("side".to_string(), deck.id());

        decks.push(RefCell::new(deck));

        test_global_state(
            decks,
            vec![player1, player2],
            (visibility_mapping, name_mapping),
        )
    }
}
