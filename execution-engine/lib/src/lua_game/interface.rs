use crate::components::event::Event;
use crate::components::{Card, Deck};
use crate::lua_game::interface_common::{
    card_to_map, cards_to_map, get_global_deck, get_player_deck, parse_player_id,
};
use crate::lua_game::DeckCustom;
use crate::state_machine::states::GlobalState;
use log::{info, warn};
use rand::Rng;
use rlua::Variadic;
use std::collections::HashMap;

/// Helper macro to track invalid input because the Lua runtime won't tell us.
macro_rules! lua_error {
    (no_err, $($arg:tt)+) => {{
        let formatted = format!($($arg)*);
        warn!("{}", formatted);
        rlua::prelude::LuaError::RuntimeError(formatted)
    }};
    ($($arg:tt)+) => {
        Err(lua_error!(no_err, $($arg)*))
    }
}

/// Retrieve the player id of the player who would take the next turn after the given player.
pub fn get_player_after<Custom>(
    global_state: &GlobalState<Custom>,
    (current, amount): (String, Option<usize>),
) -> rlua::Result<String> {
    let amount = amount.unwrap_or(1) % global_state.players().len();
    let current = current
        .parse()
        .map_err(|_| lua_error!(no_err, "Player id {} is not valid", current))?;

    global_state
        .players()
        .iter()
        // Cycle because the player we seek might be back in the beginning
        .cycle()
        .take(global_state.players().len() + amount)
        .skip_while(|p| p.id() != current)
        .nth(amount)
        .map(|p| p.id().to_string())
        .ok_or_else(|| lua_error!(no_err, "Player {} does not exist", current))
}

pub fn get_player_before<Custom>(
    global_state: &GlobalState<Custom>,
    (current, amount): (String, Option<usize>),
) -> rlua::Result<String> {
    let amount = amount.unwrap_or(global_state.players().len() - 1) % global_state.players().len();
    get_player_after(global_state, (current, Some(amount)))
}

/// Return the amount of cards on a player `Deck`.
pub fn amount_cards_on_player_deck(
    global_state: &GlobalState<DeckCustom>,
    (player, deck_name): (String, String),
) -> rlua::Result<usize> {
    let player = parse_player_id(player)?;
    get_player_deck(global_state, player, deck_name).map(|d| d.borrow().len())
}

/// Return the cards on a player `Deck`.
pub fn cards_on_player_deck(
    global_state: &GlobalState<DeckCustom>,
    (player, deck_name): (String, String),
) -> rlua::Result<Vec<HashMap<String, String>>> {
    let player = parse_player_id(player)?;
    get_player_deck(global_state, player, deck_name).map(|d| cards_to_map(d.borrow().cards()))
}

/// Return the amount of cards on a global `Deck`.
pub fn amount_cards_on_deck(
    global_state: &GlobalState<DeckCustom>,
    deck_name: String,
) -> rlua::Result<usize> {
    get_global_deck(global_state, deck_name).map(|d| d.borrow().len())
}

/// Return the cards on a global `Deck`.
pub fn cards_on_deck(
    global_state: &GlobalState<DeckCustom>,
    deck_name: String,
) -> rlua::Result<Vec<HashMap<String, String>>> {
    get_global_deck(global_state, deck_name).map(|d| cards_to_map(d.borrow().cards()))
}

/// Helper method to apply a specific peek method on the given `Deck`.
fn peek<T: FnOnce(&Deck) -> Option<&Card>>(
    global_state: &GlobalState<DeckCustom>,
    deck_name: String,
    t: T,
) -> rlua::Result<Option<HashMap<String, String>>> {
    get_global_deck(global_state, deck_name).map(|d| t(&d.borrow()).map(card_to_map))
}

/// Return the topmost card in a Lua-understandable way.
pub fn peek_top(
    global_state: &GlobalState<DeckCustom>,
    deck_name: String,
) -> rlua::Result<Option<HashMap<String, String>>> {
    peek(global_state, deck_name, Deck::peek_top)
}

/// Return the bottommost card in a Lua-understandable way.
pub fn peek_bottom(
    global_state: &GlobalState<DeckCustom>,
    deck_name: String,
) -> rlua::Result<Option<HashMap<String, String>>> {
    peek(global_state, deck_name, Deck::peek_bottom)
}

/// Print a message from the Lua-code.
pub fn print(message: Variadic<Option<String>>) -> rlua::Result<()> {
    info!(
        "Message from game:{}",
        message
            .into_iter()
            .fold(String::new(), |mut current, next| {
                if let Some(next) = next {
                    current.push(' ');
                    current.push_str(&next);
                }

                current
            })
    );
    Ok(())
}

pub fn player_score_message<CUSTOM>(
    global_state: &GlobalState<CUSTOM>,
    (player_id, message): (usize, Variadic<String>),
) -> rlua::Result<()> {
    let message = message.into_iter().fold(String::new(), |mut acc, s| {
        acc.push_str(&s);
        acc.push(' ');
        acc
    });
    global_state.publish_event(Event::ScoreMessage { player_id, message });
    Ok(())
}

pub fn player_info_message<CUSTOM>(
    global_state: &GlobalState<CUSTOM>,
    (player_id, message): (usize, Variadic<String>),
) -> rlua::Result<()> {
    let message = message.into_iter().fold(String::new(), |mut acc, s| {
        acc.push_str(&s);
        acc.push(' ');
        acc
    });
    global_state.publish_event(Event::InfoMessage { player_id, message });
    Ok(())
}

pub fn player_error_message<CUSTOM>(
    global_state: &GlobalState<CUSTOM>,
    (player_id, message): (usize, Variadic<String>),
) -> rlua::Result<()> {
    let message = message.into_iter().fold(String::new(), |mut acc, s| {
        acc.push_str(&s);
        acc.push(' ');
        acc
    });
    global_state.publish_event(Event::ErrorMessage { player_id, message });
    Ok(())
}

pub fn score_message<CUSTOM>(
    global_state: &GlobalState<CUSTOM>,
    message: Variadic<String>,
) -> rlua::Result<()> {
    for p in global_state.players() {
        player_score_message(global_state, (p.id(), message.clone()))?;
    }
    Ok(())
}

pub fn info_message<CUSTOM>(
    global_state: &GlobalState<CUSTOM>,
    message: Variadic<String>,
) -> rlua::Result<()> {
    for p in global_state.players() {
        player_info_message(global_state, (p.id(), message.clone()))?;
    }
    Ok(())
}

pub fn error_message<CUSTOM>(
    global_state: &GlobalState<CUSTOM>,
    message: Variadic<String>,
) -> rlua::Result<()> {
    for p in global_state.players() {
        player_error_message(global_state, (p.id(), message.clone()))?;
    }
    Ok(())
}

/// Generate a random number in the given range.
pub fn random((from, to): (isize, Option<isize>)) -> rlua::Result<isize> {
    if let Some(to) = to {
        Ok(rand::thread_rng().gen_range(from..=to))
    } else {
        Ok(rand::thread_rng().gen_range(1..=from))
    }
}

#[cfg(test)]
mod tests {
    use crate::components::Card;
    use crate::lua_game::interface::{
        amount_cards_on_deck, amount_cards_on_player_deck, cards_to_map, get_player_after,
        get_player_before, peek_bottom, peek_top,
    };
    use crate::lua_game::interface_common::tests::initial_global_state;

    #[test]
    fn get_player_after_invalid_id() {
        let state = initial_global_state();

        assert!(get_player_after(&state, ("sd".to_string(), None)).is_err());
    }

    #[test]
    fn get_player_before_invalid_id() {
        let state = initial_global_state();

        assert!(get_player_before(&state, ("sd".to_string(), None)).is_err());
    }

    #[test]
    fn get_player_after_unknown_id() {
        let state = initial_global_state();

        assert!(get_player_after(&state, ("3".to_string(), None)).is_err());
    }

    #[test]
    fn get_player_before_unknown_id() {
        let state = initial_global_state();

        assert!(get_player_before(&state, ("3".to_string(), None)).is_err());
    }

    #[test]
    fn get_player_after_direct_successor() {
        let state = initial_global_state();

        let successor = get_player_after(&state, ("1".to_string(), None));
        assert!(successor.is_ok());
        assert_eq!(successor.unwrap(), "2");
    }

    #[test]
    fn get_player_before_direct_predecessor() {
        let state = initial_global_state();

        let successor = get_player_before(&state, ("1".to_string(), None));
        assert!(successor.is_ok());
        assert_eq!(successor.unwrap(), "2");
    }

    #[test]
    fn get_player_after_two_successors_is_same_player_for_two_players() {
        let state = initial_global_state();

        let successor = get_player_after(&state, ("1".to_string(), Some(2)));
        assert!(successor.is_ok());
        assert_eq!(successor.unwrap(), "1");
    }

    #[test]
    fn get_player_before_two_successors_is_same_player_for_two_players() {
        let state = initial_global_state();

        let successor = get_player_before(&state, ("1".to_string(), Some(2)));
        assert!(successor.is_ok());
        assert_eq!(successor.unwrap(), "1");
    }

    #[test]
    fn get_player_after_three_successors_wraps_around() {
        let state = initial_global_state();

        let successor = get_player_after(&state, ("1".to_string(), Some(3)));
        assert!(successor.is_ok());
        assert_eq!(successor.unwrap(), "2");
    }

    #[test]
    fn get_player_before_three_successors_wraps_around() {
        let state = initial_global_state();

        let successor = get_player_before(&state, ("1".to_string(), Some(3)));
        assert!(successor.is_ok());
        assert_eq!(successor.unwrap(), "2");
    }

    #[test]
    fn test_cards_on_player_deck() {
        let state = initial_global_state();
        let p1 = state.players()[0].id();
        assert_eq!(
            amount_cards_on_player_deck(&state, (p1.to_string(), "player".to_string())).unwrap(),
            0
        );
    }

    #[test]
    fn test_cards_on_deck() {
        let state = initial_global_state();
        assert_eq!(amount_cards_on_deck(&state, "main".to_string()).unwrap(), 3);
    }

    #[test]
    fn test_peek_top_empty() {
        let state = initial_global_state();
        let top = peek_top(&state, "side".to_string()).unwrap();
        assert!(top.is_none());
    }

    #[test]
    fn test_peek_top() {
        let state = initial_global_state();
        let top = peek_top(&state, "main".to_string()).unwrap().unwrap();
        assert_eq!(top.get("value"), Some(&"2".to_string()));
        assert_eq!(top.get("color"), Some(&"red".to_string()));
    }

    #[test]
    fn test_peek_bottom() {
        let state = initial_global_state();
        let top = peek_bottom(&state, "main".to_string()).unwrap().unwrap();
        assert_eq!(top.get("value"), Some(&"0".to_string()));
        assert_eq!(top.get("color"), Some(&"red".to_string()));
    }

    #[test]
    fn cards_on_map() {
        let cards = vec![Card::new(0, "0"), Card::new(1, "1"), Card::new(1, "2")];
        let mapped = cards_to_map(&cards);
        for (c, m) in cards.iter().zip(mapped.iter()) {
            assert_eq!(m.len(), 2);
            assert_eq!(Some(c.color().to_string()).as_ref(), m.get("color"));
            assert_eq!(Some(c.value().to_string()).as_ref(), m.get("value"));
        }
    }
}
