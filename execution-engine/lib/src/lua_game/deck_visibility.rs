#[derive(Copy, Clone, PartialEq, Debug)]
/// This enum describes how a deck is presented to other players.
pub enum DeckVisibility {
    /// Deck is 'turned around' and no one can see any of the cards.
    Hidden,
    /// All the cards are visible to all players.
    Global,
    /// Cards are only visible to the player owning the cards.
    Player,
    /// Only the top card of the deck is visible.
    TopOnly,
}
