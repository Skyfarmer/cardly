use std::collections::HashMap;

use log::{debug, warn};

use crate::components::draw_type::DrawType;
use crate::components::Card;
use crate::lua_game::interface_common::{
    apply_visibility, get_global_deck, get_player_deck, parse_player_id, string_to_draw_type,
};
use crate::lua_game::{DeckCustom, LuaGame};
use crate::state_machine::states::GlobalState;

pub fn shuffle_deck(global_state: &mut GlobalState<DeckCustom>, name: String) -> rlua::Result<()> {
    get_global_deck(global_state, name).map(|d| d.borrow_mut().shuffle())
}

pub fn player_deck_transfer(
    global_state: &mut GlobalState<DeckCustom>,
    (id, player_deck, global_deck): (String, String, String),
) -> rlua::Result<()> {
    let player_id = parse_player_id(id)?;

    let from = get_player_deck(global_state, player_id, &player_deck)?;
    let to = get_global_deck(global_state, global_deck)?;
    while !from.borrow().is_empty() {
        to.borrow_mut()
            .draw_from(&mut from.borrow_mut(), DrawType::Top);
    }
    Ok(())
}

pub fn player_deck_draw_from(
    global_state: &mut GlobalState<DeckCustom>,
    (global_deck, id, player_deck, draw_type): (String, String, String, Option<String>),
) -> rlua::Result<()> {
    let draw_type = string_to_draw_type(draw_type)?;
    let player_id = parse_player_id(id)?;

    let from = get_global_deck(global_state, &global_deck)?;
    let to = get_player_deck(global_state, player_id, &player_deck)?;
    let mut to = to.borrow_mut();
    to.draw_from(&mut from.borrow_mut(), draw_type)
        .map(|_| apply_visibility(&mut to, &mut global_state.custom_mut().0, Some(player_id)))
        .unwrap_or_else(|| {
            lua_error!(
                "Either decks {} or {} are empty or do not exist or player {} does not exist",
                global_deck,
                player_deck,
                player_id
            )
        })
}

pub fn deck_change_visibility(
    global_state: &mut GlobalState<DeckCustom>,
    (global_deck, visibility): (String, String),
) -> rlua::Result<()> {
    get_global_deck(global_state, global_deck).and_then(|d| {
        let mut v = global_state.custom_mut();
        v.0.insert(d.borrow().id(), LuaGame::string_to_visibility(&visibility)?);
        apply_visibility(&mut d.borrow_mut(), &mut v.0, None)
    })
}

pub fn player_deck_change_visibility(
    global_state: &mut GlobalState<DeckCustom>,
    (id, player_deck, visibility): (String, String, String),
) -> rlua::Result<()> {
    let player_id = parse_player_id(id)?;
    get_player_deck(global_state, player_id, player_deck).and_then(|d| {
        let mut v = global_state.custom_mut();
        v.0.insert(d.borrow().id(), LuaGame::string_to_visibility(&visibility)?);
        apply_visibility(&mut d.borrow_mut(), &mut v.0, Some(player_id))
    })
}

pub fn deck_draw(
    global_state: &mut GlobalState<DeckCustom>,
    (deck_from, deck_to, draw_type): (String, String, Option<String>),
) -> rlua::Result<()> {
    let draw_type = string_to_draw_type(draw_type)?;
    let from = get_global_deck(global_state, &deck_from)?;
    let to = get_global_deck(global_state, deck_to)?;
    let mut to = to.borrow_mut();
    to.draw_from(&mut from.borrow_mut(), draw_type)
        .ok_or_else(|| lua_error!(no_err, "Deck {} was empty", deck_from))
        .and_then(|_| apply_visibility(&mut to, &mut global_state.custom_mut().0, None))
}

pub fn deck_put_on(
    global_state: &mut GlobalState<DeckCustom>,
    (player, player_deck, card, global_deck, put_type): (
        String,
        String,
        HashMap<String, String>,
        String,
        Option<String>,
    ),
) -> rlua::Result<()> {
    let value = match card["value"].parse() {
        Ok(v) => v,
        Err(_) => {
            warn!("Card value {} is not a number", card["value"]);
            return Ok(());
        }
    };

    let card = Card::new(value, card["color"].clone());
    let put_type = string_to_draw_type(put_type)?;

    let player_id = parse_player_id(player)?;
    let mut to = get_global_deck(global_state, global_deck)?.borrow_mut();
    let from = get_player_deck(global_state, player_id, &player_deck)?;

    let ok = to
        .draw_card_from(&mut from.borrow_mut(), &card, put_type)
        .ok_or_else(|| lua_error!(no_err, "Deck {} is empty", player_deck))
        .and_then(|_| apply_visibility(&mut to, &mut global_state.custom_mut().0, None));

    if ok.is_ok() {
        debug!("card {:#?} played for player {}", card, player_id);
    }

    ok
}

#[cfg(test)]
mod tests {
    use crate::components::draw_type::DrawType;
    use crate::components::Card;
    use crate::lua_game::interface_common::tests::initial_global_state;
    use crate::lua_game::interface_mutable::{
        deck_draw, deck_put_on, player_deck_draw_from, shuffle_deck,
    };
    use std::collections::HashMap;

    #[test]
    fn test_shuffle_deck() {
        let mut state = initial_global_state();
        assert!(shuffle_deck(&mut state, "main".to_string()).is_ok());
    }

    #[test]
    fn test_player_deck_draw_from_top() {
        let mut state = initial_global_state();
        let p1 = state.players()[0].id();

        // Draw a top card from a global deck and put it on the player's deck.
        assert!(player_deck_draw_from(
            &mut state,
            (
                "main".to_string(),
                p1.to_string(),
                "player".to_string(),
                Some("top".to_string()),
            ),
        )
        .is_ok());

        assert_eq!(
            state.players()[0].decks()[0].borrow().cards_iter().last(),
            Some(&Card::new(2, "red").with_restriction([p1]))
        );
    }

    #[test]
    fn test_player_deck_draw_from_bottom() {
        let mut state = initial_global_state();
        let p1 = state.players()[0].id();

        // Draw a bottom card from a global deck and put it on the player's deck.
        assert!(player_deck_draw_from(
            &mut state,
            (
                "main".to_string(),
                p1.to_string(),
                "player".to_string(),
                Some("bottom".to_string()),
            ),
        )
        .is_ok());

        assert_eq!(
            state.players()[0].decks()[0].borrow().cards_iter().last(),
            Some(&Card::new(0, "red").with_restriction([p1]))
        );
    }

    #[test]
    fn test_player_deck_draw_from_random() {
        let mut state = initial_global_state();
        let p1 = state.players()[0].id().to_string();

        // Draw a random card from a global deck and put it on the player's deck.
        assert!(player_deck_draw_from(
            &mut state,
            (
                "main".to_string(),
                p1,
                "player".to_string(),
                Some("random".to_string()),
            ),
        )
        .is_ok());

        assert_eq!(state.players()[0].decks()[0].borrow().cards().len(), 1);
    }

    #[test]
    fn test_deck_draw_from_top() {
        let mut state = initial_global_state();

        // Draw a top card from a global deck and put it on the player's deck.
        assert!(deck_draw(
            &mut state,
            (
                "main".to_string(),
                "side".to_string(),
                Some("top".to_string()),
            ),
        )
        .is_ok());

        assert_eq!(
            state.decks()[1].borrow().cards_iter().last(),
            Some(&Card::new(2, "red").with_restriction([]))
        );
    }

    #[test]
    fn test_deck_draw_from_bottom() {
        let mut state = initial_global_state();

        // Draw a bottom card from a global deck and put it on the player's deck.
        assert!(deck_draw(
            &mut state,
            (
                "main".to_string(),
                "side".to_string(),
                Some("bottom".to_string()),
            ),
        )
        .is_ok());

        assert_eq!(
            state.decks()[1].borrow().cards_iter().last(),
            Some(&Card::new(0, "red").with_restriction([]))
        );
    }

    #[test]
    fn test_deck_draw_from_random() {
        let mut state = initial_global_state();

        // Draw a random card from a global deck and put it on the player's deck.
        assert!(deck_draw(
            &mut state,
            (
                "main".to_string(),
                "side".to_string(),
                Some("random".to_string()),
            ),
        )
        .is_ok());

        assert_eq!(state.decks()[1].borrow().cards().len(), 1);
    }

    #[test]
    fn test_deck_put_on_top() {
        let mut state = initial_global_state();

        // Move a card to a players deck for preparation
        let player_deck = &state.players()[0].decks()[0];
        player_deck
            .borrow_mut()
            .draw_from(&mut state.decks()[0].borrow_mut(), DrawType::Top);

        let mut mapped_card = HashMap::new();
        mapped_card.insert("value".to_string(), 2.to_string());
        mapped_card.insert("color".to_string(), "red".to_string());

        let id = state.players()[0].id();
        assert!(deck_put_on(
            &mut state,
            (
                id.to_string(),
                "player".to_string(),
                mapped_card,
                "main".to_string(),
                Some("top".to_string())
            )
        )
        .is_ok());

        let main_deck = state.decks()[0].borrow();
        assert_eq!(main_deck.peek_top(), Some(&Card::new(2, "red")))
    }
}
