use std::cell::RefCell;
use std::collections::{HashMap, VecDeque};
use std::fmt::Display;
use std::fs::File;
use std::io::{BufReader, Read};
use std::path::Path;
use std::rc::Rc;
use std::time::Duration;

use log::{debug, error, info, trace, warn};
use rlua::prelude::{LuaError, LuaResult};
use rlua::{Context, FromLua, Function, Lua, Table, Value};

use interface_common::*;

use crate::components::{Card, Deck, DeckLayout};
use crate::config::{GameConfiguration, PlayerConfiguration};
use crate::lua_game::deck_visibility::DeckVisibility;
use crate::state_machine::states::GlobalState;
use crate::state_machine::traits::{Game, Input, InputDescription, State, UserOption};
use crate::state_machine::{PlayerMove, StateTransition};

#[macro_use]
/// This module contains all functions callable by Lua which do not require global state modification.
mod interface;
mod deck_visibility;
/// Common functions for managing interface functions.
mod interface_common;
/// This module contains all functions callable by Lua which require global state modification.
mod interface_mutable;

type DeckVisibilityMapping = HashMap<usize, DeckVisibility>;
type DeckNameMapping = HashMap<String, usize>;
type DeckCustom = (DeckVisibilityMapping, DeckNameMapping);

/// Insert the common interface into the given Lua context and scope.
/// The interface includes the following functions:
/// * `players` - Returns a list of all players.
/// * `cardsOnPlayerDeck` - Returns the number of cards of a player's deck.
/// * `cardsOnDeck` - Returns the number of cards on a given global deck.
/// * `peekTop` - Returns the card on top of the given deck. Color and value can be accessed with
/// the keys 'color' and 'value' respectively.
macro_rules! insert_methods {
    ( $lua_ctx:expr, $scope:expr, $global_state:expr, $self:expr ) => {
        insert_method($lua_ctx, $scope, "enginePlayers", |_, ()| {
            Ok($global_state
                .players()
                .iter()
                .map(|p| p.id().to_string())
                .collect::<Vec<String>>())
        })?;

        insert_method(
            $lua_ctx,
            $scope,
            "enginePlayerAmountCardsOnDeck",
            |_, params| interface::amount_cards_on_player_deck($global_state, params),
        )?;

        insert_method($lua_ctx, $scope, "engineCardsOnPlayerDeck", |_, params| {
            interface::cards_on_player_deck($global_state, params)
        })?;

        insert_method($lua_ctx, $scope, "engineAmountCardsOnDeck", |_, params| {
            interface::amount_cards_on_deck($global_state, params)
        })?;

        insert_method($lua_ctx, $scope, "engineCardsOnDeck", |_, params| {
            interface::cards_on_deck($global_state, params)
        })?;

        insert_method($lua_ctx, $scope, "engineGetPlayerAfter", |_, params| {
            interface::get_player_after($global_state, params)
        })?;

        insert_method($lua_ctx, $scope, "engineGetPlayerBefore", |_, params| {
            interface::get_player_before($global_state, params)
        })?;

        insert_method($lua_ctx, $scope, "enginePeekTop", |_, params| {
            interface::peek_top($global_state, params)
        })?;

        insert_method($lua_ctx, $scope, "enginePeekBottom", |_, params| {
            interface::peek_bottom($global_state, params)
        })?;

        insert_method($lua_ctx, $scope, "print", |_, params| {
            interface::print(params)
        })?;

        insert_method($lua_ctx, $scope, "enginePlayerScoreMessage", |_, params| {
            interface::player_score_message($global_state, params)
        })?;

        insert_method($lua_ctx, $scope, "engineScoreMessage", |_, params| {
            interface::score_message($global_state, params)
        })?;

        insert_method($lua_ctx, $scope, "enginePlayerInfoMessage", |_, params| {
            interface::player_info_message($global_state, params)
        })?;

        insert_method($lua_ctx, $scope, "engineInfoMessage", |_, params| {
            interface::info_message($global_state, params)
        })?;

        insert_method($lua_ctx, $scope, "enginePlayerErrorMessage", |_, params| {
            interface::player_error_message($global_state, params)
        })?;

        insert_method($lua_ctx, $scope, "engineErrorMessage", |_, params| {
            interface::error_message($global_state, params)
        })?;

        insert_method($lua_ctx, $scope, "engineRandom", |_, params| {
            interface::random(params)
        })?;
    };
}

/// Insert the mutable common interface into the given Lua context and scope. These functions
/// modify the global state.
/// The interface includes the following functions:
/// * `nextPlayer` - Deque the next player.
/// * `shuffleDeck` - Shuffle the given deck.
/// * `playerDeckDrawFrom` - Draw a random card from a global deck and put it onto a player deck.
/// * `deckDrawFrom` - Draw a random card from a global deck and put it onto another global deck.
/// * `playerDeckDrawFromTop` - Draw the top card from a global deck and put it onto a player deck.
/// * `deckPutOnTop` - Put a card from a player's deck onto a global deck.
/// * All functions from the `insert_methods` macro.
macro_rules! insert_methods_mut {
    ( $lua_ctx:expr, $scope:expr, $global_state:expr, $self:expr ) => {
        insert_methods!($lua_ctx, $scope, &*$global_state, $self);

        insert_method($lua_ctx, $scope, "engineShuffleDeck", |_, params| {
            interface_mutable::shuffle_deck($global_state, params)
        })?;

        insert_method($lua_ctx, $scope, "enginePlayerDeckDrawFrom", |_, params| {
            interface_mutable::player_deck_draw_from($global_state, params)
        })?;

        insert_method($lua_ctx, $scope, "enginePlayerDeckTransfer", |_, params| {
            interface_mutable::player_deck_transfer($global_state, params)
        })?;

        insert_method($lua_ctx, $scope, "engineDeckDraw", |_, params| {
            interface_mutable::deck_draw($global_state, params)
        })?;

        insert_method($lua_ctx, $scope, "engineDeckSetVisibility", |_, params| {
            interface_mutable::deck_change_visibility($global_state, params)
        })?;

        insert_method(
            $lua_ctx,
            $scope,
            "enginePlayerDeckSetVisibility",
            |_, params| interface_mutable::player_deck_change_visibility($global_state, params),
        )?;

        insert_method(
            $lua_ctx,
            $scope,
            "enginePlayerDeckDrawFromTop",
            |_, params| interface_mutable::player_deck_draw_from($global_state, params),
        )?;

        insert_method($lua_ctx, $scope, "engineDeckPutOnTop", |_, params| {
            interface_mutable::deck_put_on($global_state, params)
        })?;
    };
}

/// Call a lua function by name and the given parameters. Returns error if function does not exist.
macro_rules! call_lua {
    ($ctx:expr, $name:expr, $args:expr) => {{
        trace!("Calling lua function {} with parameters {:?}", $name, $args);
        let f: Function = $ctx.get($name)?;
        f.call::<_, _>($args)
    }};
}

/// Call a lua function by name and the given parameters. Returns default value if function does not
/// exist.
macro_rules! call_lua_optional {
    ($ctx:expr, $name:expr, $args:expr, $default:expr) => {{
        let f: Option<Function> = $ctx.get($name)?;

        if let Some(f) = f {
            trace!("Calling Lua function {} with parameters {:?}", $name, $args);
            f.call::<_, _>($args)
        } else {
            Ok($default)
        }
    }};
}

/// `LuaGame` contains the game code and maintains the lua runtime. This is needed for the `StateMachine`.
pub struct LuaGame {
    code: String,
    lua: Lua,
    visiblity_mapping: RefCell<DeckVisibilityMapping>,
    name_mapping: RefCell<DeckNameMapping>,
}

impl LuaGame {
    /// Load the game from the given path.
    pub fn load<P: AsRef<Path> + Display>(path: P) -> Result<Self, anyhow::Error> {
        info!("Loading game {}", path);
        let f = File::open(path)?;
        let mut buffered = BufReader::new(f);
        let mut code = Vec::new();
        buffered.read_to_end(&mut code)?;

        Self::try_from(String::from_utf8(code)?)
    }

    /// Run the closure in the given context. Exits the process if an `Err` is returned.
    fn run_lua_context<R, F: FnOnce(Context) -> rlua::Result<R>>(lua: &Lua, f: F) -> R {
        let has_finished = std::sync::Arc::new(std::sync::atomic::AtomicBool::new(false));
        let has_finished_cpy = has_finished.clone();
        std::thread::spawn(move || {
            std::thread::sleep(Duration::new(10, 0));

            if !has_finished.load(std::sync::atomic::Ordering::SeqCst) {
                error!("The execution of the lua game took too long. Terminating.");
                std::process::exit(-1);
            }
        });

        let result = match lua.context(f) {
            Ok(s) => s,
            Err(e) => {
                error!("{}", e);
                panic!("{}", e);
            }
        };

        has_finished_cpy.swap(true, std::sync::atomic::Ordering::SeqCst);

        result
    }

    fn string_to_layout(visibility: &str) -> rlua::Result<DeckLayout> {
        match visibility {
            "stacked" => Ok(DeckLayout::Stacked),
            "spreaded" => Ok(DeckLayout::Spreaded),
            _ => {
                return Err(LuaError::RuntimeError(format!(
                    "{} is an invalid deck layout",
                    visibility
                )));
            }
        }
    }

    fn string_to_visibility(visibility: &str) -> rlua::Result<DeckVisibility> {
        match visibility {
            "hidden" => Ok(DeckVisibility::Hidden),
            "toponly" => Ok(DeckVisibility::TopOnly),
            "global" => Ok(DeckVisibility::Global),
            "player" => Ok(DeckVisibility::Player),
            _ => {
                return Err(LuaError::RuntimeError(format!(
                    "{} is an invalid deck visibility",
                    visibility
                )));
            }
        }
    }

    fn add_deck(
        visibility_mapping: &mut DeckVisibilityMapping,
        name_mapping: &mut DeckNameMapping,
        decks: &mut Vec<Deck>,
        (name, layout, visibility): (String, Option<String>, Option<String>),
    ) -> rlua::Result<()> {
        let layout = layout
            .map(|l| Self::string_to_layout(&l))
            .unwrap_or_else(|| Ok(DeckLayout::Stacked))?;
        let visibility = visibility
            .map(|s| Self::string_to_visibility(&s))
            .unwrap_or_else(|| Ok(DeckVisibility::Global))?;

        debug!(
            "Adding new deck with name {}, layout {:?} and visibility {:?}",
            name, layout, visibility
        );
        let deck = Deck::new_empty(layout);
        visibility_mapping.insert(deck.id(), visibility);
        name_mapping.insert(name, deck.id());
        decks.push(deck);

        Ok(())
    }
}

impl TryFrom<String> for LuaGame {
    type Error = anyhow::Error;

    fn try_from(code: String) -> Result<Self, Self::Error> {
        let g = LuaGame {
            code,
            lua: Lua::new(),
            visiblity_mapping: RefCell::new(HashMap::new()),
            name_mapping: RefCell::new(HashMap::new()),
        };

        g.lua.context(|lua_ctx| -> rlua::Result<()> {
            info!("Parsing game");

            lua_ctx.load(&g.code).exec()?;

            Ok(())
        })?;

        Ok(g)
    }
}

/// All implemented functions are passed on to their corresponding Lua counterpart. If a Lua function
/// fails for an arbitrary reason, the entire process exits.
impl Game for LuaGame {
    type Custom = DeckCustom;

    /// Invokes `defineCardSet` Lua function.
    /// Provides the following functions:
    /// * `addCard` - Adds a card with the given name and value to the card set.
    fn define_card_set(&self) -> Vec<Card> {
        Self::run_lua_context(&self.lua, |lua_ctx| -> rlua::Result<Vec<Card>> {
            let mut deck = Vec::new();

            lua_ctx.scope(|scope| {
                insert_method_mut(
                    &lua_ctx,
                    scope,
                    "engineAddCard",
                    |_, (value, color): (isize, String)| {
                        if color.is_empty() {
                            lua_error!("Card color cannot be empty")
                        } else {
                            deck.push(Card::new(value, color));
                            Ok(())
                        }
                    },
                )?;

                call_lua!(lua_ctx.globals(), "defineCardSet", ())
            })?;

            Ok(deck)
        })
    }

    /// Invokes `defineGlobalConfiguration` Lua function.
    /// Provides the following functions:
    /// * `setNumPlayers` - Set the number of players required by the game.
    fn define_configuration(&self) -> GameConfiguration {
        Self::run_lua_context(&self.lua, |lua_ctx| -> rlua::Result<GameConfiguration> {
            let config = GameConfiguration::default();
            lua_ctx.scope(|_scope| {
                call_lua_optional!(lua_ctx.globals(), "defineGlobalConfiguration", (), ())
            })?;

            Ok(config)
        })
    }

    /// Invokes `definePlayerConfiguration` Lua function.
    fn define_player_configuration(&self) -> PlayerConfiguration {
        Self::run_lua_context(&self.lua, |lua_ctx| -> rlua::Result<PlayerConfiguration> {
            call_lua_optional!(lua_ctx.globals(), "definePlayerConfiguration", (), ())?;
            Ok(PlayerConfiguration::default())
        })
    }

    /// Invokes `defineGlobalDecks` Lua function.
    /// Provides the following functions:
    /// * `addMainDeck` - Add a new deck with the given name and put the entire card set
    /// defined by `define_card_set` onto it. Can optionally contain the visibility of the deck
    /// as a second argument.
    /// * `addDeck` - Add a new deck with the given name. Can optionally contain the visibility of the deck
    /// as a second argument.
    fn define_global_decks(&self, card_set: Vec<Card>) -> Vec<Deck> {
        Self::run_lua_context(&self.lua, |lua_ctx| -> rlua::Result<_> {
            let decks = RefCell::new(Vec::new());
            let mut card_set = Some(card_set);

            lua_ctx.scope(|scope| {
                insert_method_mut(
                    &lua_ctx,
                    scope,
                    "engineAddMainDeck",
                    |_, (name, layout, visibility): (String, Option<String>, Option<String>)| {
                        let layout = layout
                            .map(|l| Self::string_to_layout(&l))
                            .unwrap_or(Ok(DeckLayout::Stacked))?;
                        let visibility = visibility
                            .map(|s| Self::string_to_visibility(&s))
                            .unwrap_or_else(|| Ok(DeckVisibility::Global))?;

                        if let Some(card_set) = card_set.take() {
                            debug!(
                                "Adding main deck with name {}, layout {:?} and visibility {:?}",
                                name, layout, visibility
                            );
                            let mut deck = Deck::new(VecDeque::from(card_set), layout, None);
                            self.visiblity_mapping
                                .borrow_mut()
                                .insert(deck.id(), visibility);
                            apply_visibility(
                                &mut deck,
                                &mut self.visiblity_mapping.borrow_mut(),
                                None,
                            )?;
                            self.name_mapping.borrow_mut().insert(name, deck.id());
                            decks.borrow_mut().push(deck);

                            Ok(())
                        } else {
                            Err(LuaError::RuntimeError(
                                "Cannot call addMainDeck more than once".to_string(),
                            ))
                        }
                    },
                )?;

                insert_method_mut(&lua_ctx, scope, "engineAddDeck", |_, params| {
                    Self::add_deck(
                        &mut self.visiblity_mapping.borrow_mut(),
                        &mut self.name_mapping.borrow_mut(),
                        &mut decks.borrow_mut(),
                        params,
                    )
                })?;

                call_lua!(lua_ctx.globals(), "defineGlobalDecks", ())
            })?;

            Ok(decks.into_inner())
        })
    }

    /// Invokes `definePlayerDecks` Lua function.
    /// Provides the following functions:
    /// * `addDeck` - Add a new deck with the given name.
    fn define_player_decks(&self, player: usize) -> Vec<Deck> {
        Self::run_lua_context(&self.lua, |lua_ctx| -> rlua::Result<_> {
            let mut decks = Vec::new();

            lua_ctx.scope(|scope| {
                insert_method_mut(
                    &lua_ctx,
                    scope,
                    "engineAddDeck",
                    |_, (mut name, layout, deck_visibility): (String, _, _)| {
                        name.push_str(&player.to_string());
                        Self::add_deck(
                            &mut self.visiblity_mapping.borrow_mut(),
                            &mut self.name_mapping.borrow_mut(),
                            &mut decks,
                            (name, layout, deck_visibility),
                        )
                    },
                )?;

                call_lua!(lua_ctx.globals(), "definePlayerDecks", ())
            })?;

            Ok(decks)
        })
    }

    fn init_state(&self) -> Box<dyn State<'_, Custom = Self::Custom> + '_> {
        Box::new(LuaGameState::from(&self.lua))
    }

    fn init_custom_data(&self) -> Self::Custom {
        (self.visiblity_mapping.take(), self.name_mapping.take())
    }
}

#[derive(Clone)]
/// This struct handles the functions required by the `State` trait and passes them on to the Lua code.
///
/// Since sharing data structures between Rust and Lua is difficult and thus, should be avoided at all costs,
/// we don't actually share any data structures (apart from 'primitives' such as strings, numbers, etc.),
/// the interaction occurs over an interface of global functions. These functions must be first injected
/// via the `insert_methods` or `insert_methods_mut` macros.
///
/// A particular edge case are the successor states. Since we can't transfer Lua tables out of a Lua context,
/// we store the tables inside a special variable named '__current_state'. If `index` is `None`,
/// then there is only one successor state. Otherwise, '__current_state' consists of a list of successor
/// states and `index` represents the according state within that list.
pub struct LuaGameState<'a> {
    lua: &'a Lua,
    index: Option<i64>,
}

impl LuaGameState<'_> {
    /// Retrieve the Lua state, this particular instance of `LuaGameState` corresponds to.
    fn get_lua_state<'a>(&self, lua_ctx: Context<'a>) -> rlua::Result<Table<'a>> {
        let state: Table = lua_ctx.globals().get("__current_state")?;

        if let Some(index) = self.index {
            state.get(index)
        } else {
            Ok(state)
        }
    }
}

impl<'a> From<&'a Lua> for LuaGameState<'a> {
    /// Create a new LuaGame state and start with the initial state defined by the lua function
    /// `defineInitState`.
    fn from(lua: &'a Lua) -> Self {
        match lua.context(|lua_ctx: Context| -> rlua::Result<()> {
            let init_state: Table = call_lua!(lua_ctx.globals(), "defineInitState", ())?;

            lua_ctx.globals().set("__current_state", init_state)?;
            Ok(())
        }) {
            Ok(_) => {}
            Err(e) => panic!("{}", e),
        }

        LuaGameState { lua, index: None }
    }
}

impl<'a> From<(&'a Lua, i64)> for LuaGameState<'a> {
    fn from(other: (&'a Lua, i64)) -> Self {
        LuaGameState {
            lua: other.0,
            index: Some(other.1),
        }
    }
}

impl UserOption for LuaGameState<'_> {
    /// Calls the lua function `description`.
    fn description(&self) -> String {
        LuaGame::run_lua_context(self.lua, |lua_ctx| {
            let state = self.get_lua_state(lua_ctx)?;

            call_lua!(state, "stateDescription", state)
        })
    }
}

impl<'a> State<'a> for LuaGameState<'a> {
    type Custom = DeckCustom;

    /// Calls the lua function `stateEndsGame`.
    fn ends_game(&self, global_state: &GlobalState<Self::Custom>) -> Option<Vec<usize>> {
        LuaGame::run_lua_context(self.lua, |lua_ctx| {
            let state = self.get_lua_state(lua_ctx)?;

            lua_ctx.scope(|scope| {
                insert_methods!(&lua_ctx, scope, global_state, self);
                let winners: Option<Value> =
                    call_lua_optional!(state, "stateEndsGame", state, None)?;
                let r: Option<Vec<usize>> = match winners {
                    Some(Value::String(str)) => {
                        let winner = str.to_str()?.parse().unwrap();
                        Some(vec![winner])
                    }
                    Some(winners @ Value::Table(_)) => Some(Vec::from_lua(winners, lua_ctx)?),
                    None => None,
                    _ => unimplemented!("This type is not supported"),
                };

                Ok(r)
            })
        })
    }

    /// Calls the lua function `stateValid`. The common interface is provided.
    fn valid(&self, global_state: &GlobalState<Self::Custom>) -> bool {
        LuaGame::run_lua_context(self.lua, |lua_ctx| {
            lua_ctx.scope(|scope| {
                insert_methods!(&lua_ctx, scope, global_state, self);
                let state = self.get_lua_state(lua_ctx)?;

                call_lua_optional!(state, "stateValid", state, true)
            })
        })
    }

    /// Calls the lua function `statePriority`.
    /// Higher value means state must be preferred over others with lower priority.
    fn priority(&self) -> isize {
        LuaGame::run_lua_context(self.lua, |lua_ctx| {
            let state = self.get_lua_state(lua_ctx)?;

            call_lua_optional!(state, "statePriority", state, 0)
        })
    }

    /// Calls the lua function `stateAskPlayer`.
    /// If a user option should be presented (because either the state requests
    /// input or multiple successor states are available), return
    /// the player that should be asked.
    fn ask_player(&self) -> Option<usize> {
        LuaGame::run_lua_context(self.lua, |lua_ctx| {
            let state = self.get_lua_state(lua_ctx)?;

            let val = call_lua_optional!(state, "stateAskPlayer", state, None)?;

            Ok(val)
        })
    }

    /// Calls the lua function `stateExpectedInput`. The following special interface is provided:
    /// * `expectCardsFromPlayerDeck` - Request a card from the player's deck.
    fn input(
        &self,
        state: &GlobalState<Self::Custom>,
    ) -> Option<HashMap<String, InputDescription>> {
        LuaGame::run_lua_context(self.lua, |lua_ctx| {
            let expected_input = RefCell::new(HashMap::new());
            lua_ctx.scope(|scope| {
                insert_methods!(&lua_ctx, scope, state, self);

                insert_method_mut(
                    &lua_ctx,
                    scope,
                    "engineExpectCardsFromPlayerDeck",
                    |_, (key, player, amount, mut deck_name): (String, String, usize, String)| {
                        deck_name.push_str(&player);
                        let deck_id = get_deck_id(state, deck_name)?;
                        expected_input.borrow_mut().insert(
                            key,
                            InputDescription::CardsFromPlayerDeck {
                                player: player.parse().unwrap(),
                                amount,
                                deck_id,
                            },
                        );

                        Ok(())
                    },
                )?;

                insert_method_mut(
                    &lua_ctx,
                    scope,
                    "engineExpectDeck",
                    |_, (key, allowed): (String, Vec<String>)| {
                        if allowed.is_empty() {
                            warn!("Allowed decks is empty");
                        }
                        let decks: Vec<_> = allowed
                            .into_iter()
                            .map(|deck| state.custom().1.get(&deck).copied())
                            .collect::<Option<Vec<_>>>()
                            .ok_or_else(|| lua_error!(no_err, "A deck could not be found"))?;

                        expected_input
                            .borrow_mut()
                            .insert(key, InputDescription::Deck(decks));
                        Ok(())
                    },
                )?;

                insert_method_mut(
                    &lua_ctx,
                    scope,
                    "engineExpectCustomSelection",
                    |_, (key, options): (String, Vec<String>)| {
                        expected_input
                            .borrow_mut()
                            .insert(key, InputDescription::Custom(options));
                        Ok(())
                    },
                )?;

                let state = self.get_lua_state(lua_ctx)?;
                call_lua_optional!(state, "stateExpectedInput", state, ())
            })?;

            if expected_input.borrow().is_empty() {
                Ok(None)
            } else {
                Ok(Some(expected_input.into_inner()))
            }
        })
    }

    /// Calls the lua function `stateTransition`. The entire interface is provided.
    /// Returns itself as a successor state if only one Lua successor state is available.
    /// Otherwise, `self` is cloned with the corresponding index if multiple states are possible.
    fn transition(
        mut self: Box<Self>,
        global_state: &mut GlobalState<Self::Custom>,
        input: Option<PlayerMove>,
    ) -> StateTransition<'a, Self::Custom> {
        LuaGame::run_lua_context(self.lua, |lua_ctx| -> rlua::Result<_> {
            fn input_helper<T>(
                input: Option<&PlayerMove>,
                key: String,
                f: impl FnOnce(&PlayerMove, &str) -> Option<T>,
            ) -> LuaResult<T> {
                match input.and_then(|input| f(input, &key)) {
                    Some(v) => Ok(v),
                    None => lua_error!(
                        "Either no input is available or the key {} does not exist",
                        key
                    ),
                }
            }

            let global_state = Rc::new(RefCell::new(global_state));

            let next_state: Table = lua_ctx.scope(|scope| -> rlua::Result<_> {
                insert_methods_mut!(&lua_ctx, scope, &mut global_state.borrow_mut(), self);

                insert_method(&lua_ctx, scope, "engineInputCards", |_, key: String| {
                    input_helper(input.as_ref(), key, |input, key| {
                        if let Some(Input::Cards(cards)) = input.get(key) {
                            let c = cards
                                .iter()
                                .map(|c| {
                                    let mut t: HashMap<String, String> = HashMap::new();
                                    t.insert("color".to_string(), c.color().to_string());
                                    t.insert("value".to_string(), c.value().to_string());

                                    t
                                })
                                .collect::<Vec<_>>();
                            Some(c)
                        } else {
                            None
                        }
                    })
                })?;

                insert_method(
                    &lua_ctx,
                    scope,
                    "engineInputDeck",
                    |_, key: String| -> LuaResult<(Option<usize>, String)> {
                        input_helper(input.as_ref(), key, |input, key| {
                            if let Some(Input::Deck(selection)) = input.get(key) {
                                let (p, i) = RefCell::borrow(&global_state)
                                    // Check global decks
                                    .decks()
                                    .iter()
                                    .find(|&d| d.borrow().id() == *selection)
                                    // Return deck name and indicate with None that it is a global deck
                                    .map(|d| (None, d.borrow().id()))
                                    // Otherwise, look for a player deck
                                    .or_else(|| {
                                        RefCell::borrow(&global_state).players().iter().find_map(
                                            |p| {
                                                p.decks()
                                                    .iter()
                                                    .find(|&d| d.borrow().id() == *selection)
                                                    .map(|d| (Some(p.id()), d.borrow().id()))
                                            },
                                        )
                                    })
                                    .unwrap();
                                let i = get_deck_name(&RefCell::borrow(&global_state), i).ok()?;
                                Some((p, i))
                            } else {
                                None
                            }
                        })
                    },
                )?;

                insert_method(&lua_ctx, scope, "engineInputCustom", |_, key: String| {
                    input_helper(input.as_ref(), key, |input, key| {
                        if let Some(Input::CustomSelection(selection)) = input.get(key) {
                            Some(*selection + 1)
                        } else {
                            None
                        }
                    })
                })?;

                let state = self.get_lua_state(lua_ctx)?;

                call_lua!(state, "stateTransition", state)
            })?;

            let successors = if next_state.len()? != 0 {
                //Multiple successors available
                let mut s: Vec<Box<dyn State<Custom = _>>> = (1..next_state.len()?)
                    .map(|i| Box::new(Self::from((self.lua, i))) as Box<dyn State<Custom = _>>)
                    .collect();
                self.index = Some(s.len() as i64 + 1);
                s.push(self);

                StateTransition::Multiple(s)
            } else {
                self.index = None;
                StateTransition::Single(self)
            };

            lua_ctx.globals().set("__current_state", next_state)?;

            Ok(successors)
        })
    }
}

#[cfg(test)]
mod tests {
    use std::collections::HashMap;

    use crate::components::{Card, DeckLayout};
    use crate::lua_game::{DeckCustom, LuaGame};
    use crate::state_machine::states::GlobalState;
    use crate::state_machine::traits::{Game, Input, InputDescription};
    use crate::state_machine::StateTransition;

    #[test]
    fn load_game() {
        let _game = LuaGame::load("../games/test.lua".to_string()).unwrap();
    }

    #[test]
    fn define_set() {
        let game = LuaGame::load("../games/test.lua".to_string()).unwrap();
        let cards = game.define_card_set();
        assert_eq!(cards.len(), 1);
        assert_eq!(cards.first(), Some(&Card::new(0, "red")));
    }

    #[test]
    fn define_global_config() {
        let game = LuaGame::load("../games/test.lua".to_string()).unwrap();
        let config = game.define_configuration();
        assert_eq!(config.components().len(), 0);
    }

    #[test]
    fn define_player_config() {
        let game = LuaGame::load("../games/test.lua".to_string()).unwrap();
        let _config = game.define_player_configuration();
    }

    #[test]
    fn define_global_decks() {
        let game = LuaGame::load("../games/test.lua".to_string()).unwrap();
        let cards = game.define_card_set();
        let decks = game.define_global_decks(cards.clone());

        assert_eq!(decks.len(), 3);
        // Main deck receives all the cards
        assert_eq!(decks[0].layout(), DeckLayout::Stacked);
        assert_eq!(decks[0].cards_iter().cloned().collect::<Vec<_>>(), cards);

        assert_eq!(decks[1].layout(), DeckLayout::Spreaded);

        assert_eq!(decks[2].layout(), DeckLayout::Stacked);
    }

    #[test]
    fn define_player_decks() {
        let game = LuaGame::load("../games/test.lua".to_string()).unwrap();
        let decks = game.define_player_decks(0);

        assert_eq!(decks.len(), 1);
        // Main deck receives all the cards
        assert_eq!(decks[0].layout(), DeckLayout::Stacked);
    }

    #[test]
    // See lua file `test_description.lua`.
    fn test_description_of_game() {
        let game = LuaGame::load("../games/test_description.lua").unwrap();

        let state = game.init_state();

        assert_eq!(state.description(), "Test State");
    }

    #[test]
    // See lua file `test_valid.lua`.
    fn test_validity_of_game() {
        let game = LuaGame::load("../games/test_valid.lua").unwrap();

        let global = GlobalState::with_decks_and_custom_data(Vec::new(), Default::default());
        let state = game.init_state();

        assert!(!state.valid(&global));
    }

    #[test]
    // See lua file `test_description.lua`.
    fn test_validity_of_game_if_undefined() {
        let game = LuaGame::load("../games/test_description.lua").unwrap();

        let global = GlobalState::with_decks_and_custom_data(Vec::new(), Default::default());
        let state = game.init_state();

        assert!(state.valid(&global));
    }

    #[test]
    // See lua file `test_priority.lua`.
    fn test_priority_of_game() {
        let game = LuaGame::load("../games/test_priority.lua").unwrap();

        let state = game.init_state();

        assert_eq!(state.priority(), 4);
    }

    #[test]
    // See lua file `test_description.lua`.
    fn test_priority_of_game_if_undefined() {
        let game = LuaGame::load("../games/test_description.lua").unwrap();

        let state = game.init_state();

        assert_eq!(state.priority(), 0);
    }

    #[test]
    // See lua file `test_input.lua`.
    fn test_expected_input_of_game() {
        let game = LuaGame::load("../games/test_input.lua").unwrap();
        let deck_id = rand::random();

        let state = game.init_state();
        let mut expected_input = HashMap::new();
        expected_input.insert(
            "playedCard".to_string(),
            InputDescription::CardsFromPlayerDeck {
                player: 4,
                amount: 3,
                deck_id,
            },
        );

        let global_state: GlobalState<DeckCustom> = Default::default();
        global_state
            .custom_mut()
            .1
            .insert("handCards4".to_string(), deck_id);

        assert_eq!(state.input(&global_state), Some(expected_input));
    }

    #[test]
    // See lua file `test_description.lua`.
    fn test_expected_input_of_game_if_undefined() {
        let game = LuaGame::load("../games/test_description.lua").unwrap();

        let state = game.init_state();

        assert_eq!(state.input(&Default::default()), None);
    }

    #[test]
    fn test_single_transition() {
        let game = LuaGame::load("../games/test_single_transition.lua").unwrap();

        let mut global_state =
            GlobalState::with_decks_and_custom_data(Vec::new(), Default::default());
        let state = game.init_state();

        matches!(
            state.transition(&mut global_state, None),
            StateTransition::Single(..)
        );
    }

    #[test]
    fn test_multiple_transition() {
        let game = LuaGame::load("../games/test_multiple_transition.lua").unwrap();

        let mut global_state =
            GlobalState::with_decks_and_custom_data(Vec::new(), Default::default());
        let state = game.init_state();

        matches!(
            state.transition(&mut global_state, None),
            StateTransition::Multiple(..)
        );
    }

    #[test]
    fn test_single_transition_with_input() {
        let game = LuaGame::load("../games/test_single_transition_with_input.lua").unwrap();

        let mut global_state =
            GlobalState::with_decks_and_custom_data(Vec::new(), Default::default());
        let mut input = HashMap::new();
        input.insert("input".to_string(), Input::Cards(vec![Card::new(2, "red")]));

        let state = game.init_state();

        matches!(
            state.transition(&mut global_state, Some(input)),
            StateTransition::Single(..)
        );
    }
}
