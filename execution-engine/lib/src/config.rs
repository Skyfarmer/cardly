use std::collections::HashSet;
use std::hash::Hash;

#[derive(Eq, PartialEq, Hash, Debug)]
/// Configuration elements describing general properties of the game.
pub enum GlobalConfigElements {}

#[derive(Debug, Default)]
/// Stores general configuration elements.
pub struct GameConfiguration {
    components: HashSet<GlobalConfigElements>,
}

impl GameConfiguration {
    /// Set a given property. Note that each property can only exist once. Thus, if a certain
    /// configuration element already exists, it will be overriden by the new setting.
    pub fn set(&mut self, config: GlobalConfigElements) {
        self.components.insert(config);
    }

    /// Return the configuration elements.
    pub fn components(&self) -> &HashSet<GlobalConfigElements> {
        &self.components
    }
}

#[derive(Hash, std::cmp::Eq, std::cmp::PartialEq, Debug)]
/// Configuration elements describing properties of the players. The enum is currently empty and thus,
/// not usable.
pub enum PlayerConfigElements {}

#[derive(Debug, Default)]
/// Stores player-specific configuration elements. Currently, a player configuration applies to all
/// players. Thus, special roles for a specific player are unsupported.
pub struct PlayerConfiguration {
    components: HashSet<PlayerConfigElements>,
}

impl PlayerConfiguration {
    #[allow(dead_code)]
    /// Set a given property. Note that each property can only exist once. Thus, if a certain
    /// configuration element already exists, it will be overriden by the new setting.
    pub fn set(&mut self, config: PlayerConfigElements) {
        self.components.insert(config);
    }

    /// Return the configuration elements.
    pub fn components(&self) -> &HashSet<PlayerConfigElements> {
        &self.components
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn global_empty_config() {
        let config = GameConfiguration::default();
        assert_eq!(config.components.len(), 0);
    }

    #[test]
    fn player_empty_config() {
        let config = PlayerConfiguration::default();
        assert_eq!(config.components.len(), 0);
    }
}
