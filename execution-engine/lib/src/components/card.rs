use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Serialize, Deserialize)]
/// Represents a card of a card set. It currently consists of a value and a color. There is no
/// assumption about the order of the cards.
pub struct Card {
    value: isize,
    color: String,
    #[serde(skip)]
    visibility: Option<Vec<usize>>,
}

impl Card {
    /// Create a new card with the given value and color.
    pub fn new<T: ToString>(value: isize, color: T) -> Self {
        Card {
            value,
            color: color.to_string(),
            visibility: None,
        }
    }

    /// Add restrictions regarding visibility to a `Card`.
    pub fn with_restriction<T: IntoIterator<Item = usize>>(mut self, players: T) -> Self {
        self.update_restrictions(players);
        self
    }

    /// Return the value of the `Card`.
    pub fn value(&self) -> isize {
        self.value
    }

    /// Return the color of the `Card`.
    pub fn color(&self) -> &str {
        &self.color
    }

    /// Return the player ids who can see this `Card`.
    pub fn restrictions(&self) -> Option<&[usize]> {
        self.visibility.as_deref()
    }

    /// Update the players ids who can see this `Card`.
    pub fn update_restrictions<T: IntoIterator<Item = usize>>(&mut self, players: T) {
        self.visibility = Some(players.into_iter().collect());
    }

    /// Remove all restrictions regarding visibility.
    pub fn clear_restrictions(&mut self) {
        self.visibility = None;
    }
}

impl PartialEq for Card {
    fn eq(&self, other: &Self) -> bool {
        self.value == other.value && self.color == other.color
    }
}

#[cfg(test)]
mod tests {
    use crate::components::Card;

    #[test]
    fn card() {
        let card = Card::new(5, String::from("Test"));

        assert_eq!(card.value(), 5);
        assert_eq!(card.color(), "Test");
    }

    #[test]
    fn card_equal_if_color_and_value_equal() {
        let card1 = Card::new(5, String::from("Test"));
        let card2 = Card::new(5, String::from("Test"));

        assert_eq!(card1, card2);
    }

    #[test]
    fn card_nequal_if_color_nequal() {
        let card1 = Card::new(5, String::from("Test"));
        let card2 = Card::new(5, String::from("Nequal"));

        assert_ne!(card1, card2);
    }

    #[test]
    fn card_nequal_if_value_nequal() {
        let card1 = Card::new(5, String::from("Test"));
        let card2 = Card::new(4, String::from("Test"));

        assert_ne!(card1, card2);
    }

    #[test]
    fn card_equal_restrictions_irrelevant() {
        let card1 = Card::new(5, String::from("Test")).with_restriction([]);
        let card2 = Card::new(5, String::from("Test")).with_restriction([1]);
        let card3 = Card::new(5, String::from("Test"));

        assert_eq!(card1, card2);
        assert_eq!(card1, card3);
        assert_eq!(card2, card3);
    }
}
