use crate::components::Card;
use serde::Deserialize;
use serde::Serialize;

#[derive(Debug, PartialEq, Clone, Serialize, Deserialize)]
#[serde(tag = "type")]
/// These events are triggered by a `Deck` and are designed to notify the user interface
/// in case something has changed.
pub enum Event {
    /// A deck was completely reordered (e.g. shuffled). The user interface should query the entire
    /// deck.
    DeckReordered {
        deck_id: usize,
    },
    /// The given card was moved from one deck to another.
    CardMoved {
        card: Card,
        from: usize,
        to: usize,
    },
    /// Whether the visibility of a card has changed
    CardVisibilityChanged {
        card: Card,
        deck_id: usize,
        old_visibility: Option<Vec<usize>>,
    },
    ScoreMessage {
        player_id: usize,
        message: String,
    },
    InfoMessage {
        player_id: usize,
        message: String,
    },
    ErrorMessage {
        player_id: usize,
        message: String,
    },
    GameOver {
        winners: Vec<usize>,
    },
}
