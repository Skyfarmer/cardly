#[derive(Clone, Copy)]
/// Describes how to draw/put a card from/on a deck.
pub enum DrawType {
    Top,
    Bottom,
    Random,
}
