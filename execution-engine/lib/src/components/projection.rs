use crate::components::*;
use std::collections::VecDeque;

/// This is a helper struct which allows to reduce
/// the visibility of decks.
pub struct Projection;

impl Projection {
    /// Apply the visibility of the deck to a given player id by removing all related card
    /// information.
    pub fn apply(d: &Deck, player: usize) -> Deck {
        let cards: VecDeque<_> = d
            .cards_iter()
            .map(|c| {
                if c.restrictions().map_or(true, |v| v.contains(&player)) {
                    Card::new(c.value(), c.color())
                } else {
                    Card::new(0, "")
                }
            })
            .collect();

        Deck::new(cards, DeckLayout::Stacked, None)
    }
}

#[cfg(test)]
mod tests {
    use crate::components::{Card, Deck, DeckLayout, Projection};
    use std::collections::VecDeque;

    #[test]
    fn projection_should_return_none() {
        let cards: VecDeque<_> =
            vec![Card::new(1, "1"), Card::new(2, "2"), Card::new(3, "3")].into();
        let deck = Deck::new(cards.clone(), DeckLayout::Stacked, None);

        assert_eq!(&cards, Projection::apply(&deck, 1).cards());
    }

    #[test]
    fn projection_should_return_equal_player() {
        let mut cards: VecDeque<_> = vec![
            Card::new(1, "1"),
            Card::new(2, "2").with_restriction([1]),
            Card::new(3, "3").with_restriction([2]),
            Card::new(4, "4").with_restriction([]),
        ]
        .into();
        let deck = Deck::new(cards.clone(), DeckLayout::Stacked, None);

        cards[2] = Card::new(0, "");
        cards[3] = Card::new(0, "");

        assert_eq!(&cards, Projection::apply(&deck, 1).cards());
    }
}
