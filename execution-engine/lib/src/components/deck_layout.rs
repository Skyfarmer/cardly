use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Copy, PartialEq, Serialize, Deserialize)]
/// Represents how a `Deck` should be presented by the user interface.
pub enum DeckLayout {
    /// A deck where only the topmost card can be seen.
    Stacked,
    /// A deck where all the cards can be seen.
    Spreaded,
}
