use crate::components::draw_type::DrawType;
use crate::components::event::Event;
use crate::components::*;
use crate::utilities::string;
use crossbeam_channel::Sender;
use log::error;
use rand::prelude::SliceRandom;
use rand::Rng;
use serde::{Deserialize, Serialize};
use std::collections::vec_deque::{Iter, IterMut};
use std::collections::VecDeque;
use std::fmt::{Display, Formatter};

#[derive(Debug, Clone, Serialize, Deserialize)]
/// Represents a deck consisting of a (potentially empty) set of cards. Also has a name in order to
/// identify the deck in the game code.
pub struct Deck {
    #[serde(with = "string")]
    id: usize,
    set: VecDeque<Card>,
    layout: DeckLayout,
    #[serde(skip)]
    event_sink: Option<Sender<Event>>,
}

impl Deck {
    /// Create a new deck with the provided properties.
    pub fn new(set: VecDeque<Card>, layout: DeckLayout, event_sink: Option<Sender<Event>>) -> Self {
        Deck {
            id: rand::random(),
            set,
            layout,
            event_sink,
        }
    }

    /// Create an empty deck with the provided properties.
    pub fn new_empty(layout: DeckLayout) -> Self {
        Self::new(VecDeque::new(), layout, None)
    }

    /// Return the id of the `Deck`.
    pub fn id(&self) -> usize {
        self.id
    }

    /// Get the cards.
    pub fn cards(&self) -> &VecDeque<Card> {
        &self.set
    }

    /// Get an iterator over all cards contained in the deck.
    pub fn cards_iter(&self) -> Iter<Card> {
        self.set.iter()
    }

    /// Get a mutable iterator over all cards contained in the deck.
    pub fn cards_iter_mut(&mut self) -> IterMut<Card> {
        self.set.iter_mut()
    }

    /// Retrieve the layout of the deck.
    pub fn layout(&self) -> DeckLayout {
        self.layout
    }

    /// Retrieve the set of cards.
    pub fn set(&self) -> &VecDeque<Card> {
        &self.set
    }

    /// Clone the event sink used by the deck.
    pub fn clone_event_sink(&self) -> Option<Sender<Event>> {
        self.event_sink.clone()
    }

    /// Set the event sink to receive notifications when something has changed in a deck.
    pub fn set_event_sink(&mut self, event_sink: Sender<Event>) {
        self.event_sink = Some(event_sink);
    }

    /// Retrieve the number of cards on the deck.
    pub fn len(&self) -> usize {
        self.set.len()
    }

    /// Checks if the deck is empty.
    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }

    /// Randomly shuffle the cards on the deck.
    pub fn shuffle(&mut self) {
        self.set.make_contiguous().shuffle(&mut rand::thread_rng());
        self.send_event(Event::DeckReordered { deck_id: self.id });
    }

    /// Return a random card from the deck. Returns None if deck is empty.
    fn take_random(&mut self) -> Option<Card> {
        if !self.is_empty() {
            let mut rng = rand::thread_rng();
            self.set.remove(rng.gen_range(0..self.set.len()))
        } else {
            None
        }
    }

    /// Return a card that is equal to the `other`. Returns `None` if none could be found.
    fn take(&mut self, other: &Card) -> Option<Card> {
        let (i, _) = self.set.iter().enumerate().find(|(_, c)| c == &other)?;
        self.set.remove(i)
    }

    /// Return a card from the top of the deck. Returns None if deck is empty.
    fn take_top(&mut self) -> Option<Card> {
        self.set.pop_back()
    }

    /// Return a card from the bottom of the deck. Returns None if deck is empty.
    fn take_bottom(&mut self) -> Option<Card> {
        self.set.pop_front()
    }

    /// Put a card on top of the deck.
    fn put_top(&mut self, card: Card) {
        self.set.push_back(card);
    }

    /// Insert a card on the bottom of the deck.
    fn put_bottom(&mut self, card: Card) {
        self.set.push_front(card);
    }

    /// Draw a card from a deck using the given `DrawType`. Returns
    /// `None` if *other* was empty.
    pub fn draw_from(&mut self, other: &mut Self, draw_type: DrawType) -> Option<()> {
        let card = match draw_type {
            DrawType::Top => other.take_top(),
            DrawType::Bottom => other.take_bottom(),
            DrawType::Random => other.take_random(),
        }?;
        self.put_top(card.clone());

        self.send_event(Event::CardMoved {
            card,
            from: other.id,
            to: self.id,
        });

        Some(())
    }

    /// Draw a card from a deck which is equal to the card given. Returns
    /// `None` if *other* was empty.
    pub fn draw_card_from(
        &mut self,
        other: &mut Self,
        card: &Card,
        put_type: DrawType,
    ) -> Option<()> {
        let card = other.take(card)?;

        match put_type {
            DrawType::Top => self.put_top(card.clone()),
            DrawType::Bottom => self.put_bottom(card.clone()),
            DrawType::Random => {
                todo!()
            }
        }

        self.send_event(Event::CardMoved {
            card,
            from: other.id,
            to: self.id,
        });

        Some(())
    }

    /// Look at the topmost card but don't take it from the deck. Returns `None` if deck is empty.
    pub fn peek_top(&self) -> Option<&Card> {
        self.set.back()
    }

    /// Look at the topmost card but don't take it from the deck. Returns `None` if deck is empty.
    pub fn peek_top_mut(&mut self) -> Option<&mut Card> {
        self.set.back_mut()
    }

    /// Look at the bottommost card but don't take it from the deck. Returns `None` if deck is empty.
    pub fn peek_bottom(&self) -> Option<&Card> {
        self.set.front()
    }

    /// Sends the given `Event` to the event queue.
    fn send_event(&mut self, event: Event) {
        if let Some(sink) = self.event_sink.as_ref() {
            if let Err(e) = sink.send(event) {
                error!("Could not send event: {}", e);
            }
        }
    }
}

impl Display for Deck {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "Card Deck {} ({} cards)", self.id, self.set.len())
    }
}

impl PartialEq for Deck {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id && self.layout == other.layout && self.set == other.set
    }
}

#[cfg(test)]
mod tests {
    use crate::components::draw_type::DrawType;
    use crate::components::event::Event;
    use crate::components::{Card, Deck, DeckLayout};
    use std::collections::VecDeque;

    fn create_test_deck() -> Deck {
        let cards: VecDeque<_> = vec![
            Card::new(1, "1"),
            Card::new(2, "2"),
            Card::new(3, "3"),
            Card::new(4, "4"),
            Card::new(5, "5"),
        ]
        .into();
        Deck::new(cards, DeckLayout::Stacked, None)
    }

    #[test]
    fn empty_deck() {
        // Create empty deck
        let deck = Deck::new_empty(DeckLayout::Spreaded);
        assert_eq!(deck.len(), 0);
        assert_eq!(deck.len(), deck.set().len());

        assert_eq!(deck.layout(), DeckLayout::Spreaded);
    }

    #[test]
    fn deck_with_start_set() {
        // Create deck with name and cards
        let deck = create_test_deck();

        assert_eq!(deck.len(), deck.set().len());

        assert_eq!(deck.layout(), DeckLayout::Stacked);
        for (l, r) in deck.cards_iter().zip(deck.set.iter()) {
            assert_eq!(l, r);
        }
    }

    #[test]
    fn take_top() {
        // Create deck with a single card
        let cards: VecDeque<_> =
            vec![Card::new(1, "1"), Card::new(2, "2"), Card::new(3, "3")].into();
        let mut deck = Deck::new(cards.clone(), DeckLayout::Stacked, None);

        // Contains two cards
        assert_eq!(deck.len(), cards.len());
        assert_eq!(deck.take_top().as_ref(), cards.back());

        assert!(deck.take_top().is_some());
        assert!(deck.take_top().is_some());

        // Deck must be empty now
        assert!(deck.take_top().is_none());
        assert_eq!(deck.len(), 0);
    }

    #[test]
    fn take_random() {
        // Create deck with three cards
        let cards: VecDeque<_> =
            vec![Card::new(1, "1"), Card::new(2, "2"), Card::new(3, "3")].into();
        let mut deck = Deck::new(cards, DeckLayout::Spreaded, None);

        // Deck must return three cards and
        assert!(deck.take_random().is_some());
        assert!(deck.take_random().is_some());
        assert!(deck.take_random().is_some());

        // Deck must be empty now
        assert!(deck.take_random().is_none());
        assert_eq!(deck.len(), 0);
    }

    #[test]
    fn take_bottom() {
        // Create deck with a single card
        let cards: VecDeque<_> =
            vec![Card::new(1, "1"), Card::new(2, "2"), Card::new(3, "3")].into();
        let mut deck = Deck::new(cards.clone(), DeckLayout::Stacked, None);

        // Contains two cards
        assert_eq!(deck.len(), cards.len());
        assert_eq!(deck.take_bottom().as_ref(), cards.front());

        // Two more cards
        assert!(deck.take_bottom().is_some());
        assert!(deck.take_bottom().is_some());

        // Deck must be empty now
        assert!(deck.take_bottom().is_none());
        assert_eq!(deck.len(), 0);
    }

    #[test]
    fn peek_top() {
        // Create deck with three cards
        let cards: VecDeque<_> =
            vec![Card::new(1, "1"), Card::new(2, "2"), Card::new(3, "3")].into();
        let deck = Deck::new(cards, DeckLayout::Stacked, None);

        // Deck must always return the top card but not consume it
        assert_eq!(deck.peek_top(), Some(Card::new(3, "3")).as_ref());
        assert_eq!(deck.peek_top(), Some(Card::new(3, "3")).as_ref());
        assert_eq!(deck.len(), 3);
    }

    #[test]
    fn peek_top_empty() {
        // Create deck with three cards
        let deck = Deck::new_empty(DeckLayout::Stacked);

        // Deck must always return the top card but not consume it
        assert!(deck.peek_top().is_none());
    }

    #[test]
    fn put_top() {
        let mut deck = Deck::new_empty(DeckLayout::Stacked);
        let card = Card::new(1, "1");
        deck.put_top(card.clone());

        assert_eq!(deck.peek_top(), Some(card).as_ref());
    }

    #[test]
    fn peek_bottom() {
        // Create deck with three cards
        let cards: VecDeque<_> =
            vec![Card::new(1, "1"), Card::new(2, "2"), Card::new(3, "3")].into();
        let deck = Deck::new(cards, DeckLayout::Stacked, None);

        // Deck must always return the top card but not consume it
        assert_eq!(deck.peek_bottom(), Some(Card::new(1, "1")).as_ref());
        assert_eq!(deck.peek_bottom(), Some(Card::new(1, "1")).as_ref());
        assert_eq!(deck.len(), 3);
    }

    #[test]
    fn peek_bottom_empty() {
        // Create deck with three cards
        let deck = Deck::new_empty(DeckLayout::Stacked);

        // Deck must always return the top card but not consume it
        assert!(deck.peek_bottom().is_none());
    }

    #[test]
    fn put_bottom() {
        let mut deck = Deck::new_empty(DeckLayout::Stacked);
        let card = Card::new(1, "1");
        deck.put_bottom(card.clone());

        assert_eq!(deck.peek_bottom(), Some(card).as_ref());
    }

    #[test]
    fn shuffle_triggers_event() {
        let (s, r) = crossbeam_channel::unbounded();
        let mut deck = create_test_deck();
        deck.set_event_sink(s);

        deck.shuffle();
        assert_eq!(r.recv().unwrap(), Event::DeckReordered { deck_id: deck.id });
        assert!(r.is_empty());
    }

    #[test]
    fn draw_deck_from_top() {
        let mut source = create_test_deck();
        let mut target = create_test_deck();
        target.draw_from(&mut source, DrawType::Top);

        assert_eq!(target.peek_top(), Some(Card::new(5, "5")).as_ref());
    }

    #[test]
    fn draw_deck_from_bottom() {
        let mut source = create_test_deck();
        let mut target = create_test_deck();
        target.draw_from(&mut source, DrawType::Bottom);

        assert_eq!(target.peek_top(), Some(Card::new(1, "1")).as_ref());
    }

    #[test]
    fn draw_deck_triggers_event() {
        let (s, r) = crossbeam_channel::unbounded();
        let mut source = create_test_deck();
        let mut target = create_test_deck();
        target.set_event_sink(s);
        target.draw_from(&mut source, DrawType::Bottom);

        assert_eq!(
            r.recv().unwrap(),
            Event::CardMoved {
                card: Card::new(1, "1"),
                from: source.id,
                to: target.id,
            }
        );
        assert!(r.is_empty());
    }

    #[test]
    fn draw_specific_card_put_top() {
        let mut source = create_test_deck();
        let mut target = create_test_deck();
        let card = Card::new(3, "3");
        target.draw_card_from(&mut source, &card, DrawType::Top);

        assert_eq!(target.peek_top(), Some(card).as_ref());
    }

    #[test]
    fn draw_specific_card_put_bottom() {
        let mut source = create_test_deck();
        let mut target = create_test_deck();
        let card = Card::new(3, "3");
        target.draw_card_from(&mut source, &card, DrawType::Bottom);

        assert_eq!(target.peek_bottom(), Some(card).as_ref());
    }

    #[test]
    fn draw_specific_card_put_triggers_event() {
        let (s, r) = crossbeam_channel::unbounded();
        let mut source = create_test_deck();
        let mut target = create_test_deck();
        let card = Card::new(3, "3");

        target.set_event_sink(s);
        target.draw_card_from(&mut source, &card, DrawType::Bottom);

        assert_eq!(
            r.recv().unwrap(),
            Event::CardMoved {
                card,
                from: source.id,
                to: target.id,
            }
        );
        assert!(r.is_empty());
    }
}
