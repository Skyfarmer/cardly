use crate::components::event::Event;
use crate::components::Deck;
use crossbeam_channel::Sender;
use log::warn;
use std::cell::{Ref, RefCell, RefMut};

#[derive(Debug, Default)]
/// This struct defines the overall state of the game. This includes global decks as well as the state
/// of each individual player.
pub struct GlobalState<Custom> {
    decks: Vec<RefCell<Deck>>,
    /// The first `PlayerState` in the list represents the current player.
    players: Vec<PlayerState>,
    event_sink: Option<Sender<Event>>,
    custom: RefCell<Custom>,
}

impl<Custom> GlobalState<Custom> {
    pub fn decks(&self) -> &[RefCell<Deck>] {
        &self.decks
    }

    pub fn custom(&self) -> Ref<Custom> {
        self.custom.borrow()
    }

    pub fn custom_mut(&self) -> RefMut<Custom> {
        self.custom.borrow_mut()
    }

    /// Add player states to a global state
    pub fn add_players(self, players: Vec<PlayerState>) -> Self {
        GlobalState { players, ..self }
    }

    /// Add custom data to a global state
    pub fn add_custom_data(self, custom: Custom) -> Self {
        GlobalState {
            custom: RefCell::new(custom),
            ..self
        }
    }

    pub fn add_event_sink(self, sink: Sender<Event>) -> Self {
        GlobalState {
            event_sink: Some(sink),
            ..self
        }
    }

    /// Create a new state with the given global decks and custom data.
    pub fn with_decks_and_custom_data<T: IntoIterator<Item = Deck>>(
        decks: T,
        custom: Custom,
    ) -> Self {
        GlobalState {
            decks: decks.into_iter().map(RefCell::new).collect(),
            players: Vec::new(),
            custom: RefCell::new(custom),
            event_sink: None,
        }
    }

    /// Return a reference to the deck with the given id. Only considers global decks
    /// Returns None if deck does not exist.
    pub fn get_deck<T: Into<usize>>(&self, id: T) -> Option<&RefCell<Deck>> {
        let id = id.into();
        self.decks.iter().find(|&d| d.borrow().id() == id)
    }

    pub fn publish_event(&self, event: Event) {
        if let Some(sink) = &self.event_sink {
            sink.send(event).unwrap();
        } else {
            warn!("Global state does not have an event sink");
        }
    }

    /// Return a slice to all player states.
    pub fn players(&self) -> &[PlayerState] {
        &self.players
    }

    /// Return a player state by id. Return `None` if player does not exist.
    pub fn player_by_id(&self, id: usize) -> Option<&PlayerState> {
        self.players.iter().find(|p| p.id() == id)
    }

    pub fn find_deck_by_id(&self, id: usize) -> Option<&RefCell<Deck>> {
        self.decks
            .iter()
            .find(|&d| d.borrow().id() == id)
            .or_else(|| {
                self.players
                    .iter()
                    .find_map(|p| p.decks.iter().find(|&d| d.borrow().id() == id))
            })
    }
}

#[derive(Debug, Default)]
/// This struct defines the state of an individual player. This includes player-specific decks, a unique id as well
/// as custom properties.
pub struct PlayerState {
    id: usize,
    decks: Vec<RefCell<Deck>>,
}

impl PlayerState {
    /// Create a state with given decks.
    pub fn with_decks<T: IntoIterator<Item = Deck>>(id: usize, decks: T) -> Self {
        Self {
            id,
            decks: decks.into_iter().map(RefCell::new).collect(),
        }
    }

    /// Return the player id.
    pub fn id(&self) -> usize {
        self.id
    }

    /// Get the decks of the player.
    pub fn decks(&self) -> &[RefCell<Deck>] {
        &self.decks
    }

    /// Return a reference to the deck with the given id. Only considers player decks.
    /// Returns None if deck does not exist.
    pub fn get_deck<T: Into<usize>>(&self, id: T) -> Option<&RefCell<Deck>> {
        let id = id.into();
        self.decks.iter().find(|&d| d.borrow().id() == id)
    }
}

#[cfg(test)]
pub mod test {
    use super::*;
    use crate::components::DeckLayout;
    use rand::random;

    pub fn test_player_state(id: usize, decks: Vec<Deck>) -> PlayerState {
        PlayerState::with_decks(id, decks)
    }

    pub fn test_global_state<Custom>(
        decks: Vec<RefCell<Deck>>,
        players: Vec<PlayerState>,
        custom: Custom,
    ) -> GlobalState<Custom> {
        GlobalState {
            decks,
            players,
            custom: RefCell::new(custom),
            event_sink: None,
        }
    }

    fn create_decks() -> Vec<Deck> {
        vec![
            Deck::new_empty(DeckLayout::Stacked),
            Deck::new_empty(DeckLayout::Spreaded),
        ]
    }

    fn create_players() -> Vec<PlayerState> {
        vec![
            PlayerState::with_decks(random(), Vec::new()),
            PlayerState::with_decks(random(), Vec::new()),
            PlayerState::with_decks(random(), Vec::new()),
        ]
    }

    #[test]
    fn test_global_state_get_players() {
        let players = create_players();
        let ids = players.iter().map(|p| p.id()).collect::<Vec<_>>();
        let state =
            GlobalState::with_decks_and_custom_data(create_decks(), ()).add_players(players);

        assert_eq!(
            state.players().iter().map(|p| p.id()).collect::<Vec<_>>(),
            ids
        );
        assert_eq!(
            state.players().iter().map(|p| p.id()).collect::<Vec<_>>(),
            ids
        );
    }

    #[test]
    fn test_global_state_get_player_bi_id() {
        let players = create_players();
        let id = players[1].id();
        let state =
            GlobalState::with_decks_and_custom_data(create_decks(), ()).add_players(players);

        assert_eq!(state.player_by_id(id).map(|p| p.id()), Some(id));
    }

    #[test]
    fn test_global_state_unknown_deck() {
        let state = GlobalState::with_decks_and_custom_data(create_decks(), ());
        let id = loop {
            let id = rand::random();
            if state.find_deck_by_id(id).is_none() {
                break id;
            }
        };
        assert!(state.get_deck(id).is_none());
    }

    #[test]
    fn test_global_state_known_deck() {
        let state = GlobalState::with_decks_and_custom_data(create_decks(), ());
        let id = state.decks()[0].borrow().id();
        assert!(state.get_deck(id).is_some());
    }

    #[test]
    fn test_player_state_unknown_deck() {
        let state = PlayerState::with_decks(random(), create_decks());
        let id = loop {
            let id = rand::random();
            if state.decks().iter().all(|d| d.borrow().id() != id) {
                break id;
            }
        };
        assert!(state.get_deck(id).is_none());
    }

    #[test]
    fn test_player_state_known_deck() {
        let state = PlayerState::with_decks(random(), create_decks());
        let id = state.decks()[0].borrow().id();
        assert!(state.get_deck(id).is_some());
    }
}
