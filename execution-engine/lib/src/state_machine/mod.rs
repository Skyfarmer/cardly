pub mod states;
pub mod traits;

use anyhow::{anyhow, bail, Context, Result};
use crossbeam_channel::{unbounded, Receiver, Sender};
use log::Level::{Debug, Warn};
use log::{debug, error, info, log_enabled, warn};
use std::cell::RefCell;
use std::collections::HashMap;

use crate::components::event::Event;
use crate::components::Deck;
use crate::state_machine::states::{GlobalState, PlayerState};
use crate::state_machine::traits::{Game, Input, InputDescription, Interface, State, UserOption};

pub type PlayerMove = HashMap<String, Input>;

/// This enum tells the engine whether there is only one successor state or multiple ones.
pub enum StateTransition<'a, Custom> {
    Single(Box<dyn State<'a, Custom = Custom> + 'a>),
    Multiple(Vec<Box<dyn State<'a, Custom = Custom> + 'a>>),
}

struct NextState<'a, T: Game, Input> {
    input: Option<HashMap<String, Input>>,
    state: Box<dyn State<'a, Custom = T::Custom> + 'a>,
}

impl<'a, T: Game, Input> NextState<'a, T, Input> {
    fn new(
        input: Option<HashMap<String, Input>>,
        state: Box<dyn State<'a, Custom = T::Custom> + 'a>,
    ) -> Self {
        NextState { input, state }
    }
}

/// The `StateMachine` controls the flow of the given game.
pub struct StateMachine<T: Game> {
    game: T,
}

impl<T: Game> StateMachine<T> {
    /// Construct a new `StateMachine` with the given game and interface.
    pub fn new(game: T) -> Self {
        StateMachine { game }
    }
}

impl<T: Game> StateMachine<T>
where
    <T as Game>::Custom: Default,
{
    /// Initialize the initial `GlobalState`.
    fn init(&self, num_players: usize) -> GlobalState<T::Custom> {
        let card_set = self.game.define_card_set();
        let global_config = self.game.define_configuration();
        debug!("Game configuration: {:#?}", global_config);
        let player_config = self.game.define_player_configuration();
        debug!("Player configuration: {:#?}", player_config);

        let global_decks = self.game.define_global_decks(card_set);

        if log_enabled!(Debug) {
            let concatenated = global_decks
                .iter()
                .fold(String::new(), |s, d| s + " " + &d.to_string());
            debug!("Global decks: {}", concatenated);
        }

        let players: Vec<_> = (0..num_players)
            .map(|i| {
                let p = PlayerState::with_decks(i, self.game.define_player_decks(i));
                player_config.components().iter().fold(p, |p, _c| p)
            })
            .collect();

        GlobalState::with_decks_and_custom_data(global_decks, self.game.init_custom_data())
            .add_players(players)
    }

    fn insert_sinks<'a, R: IntoIterator<Item = &'a RefCell<Deck>>>(decks: R, sink: Sender<Event>) {
        for d in decks {
            d.borrow_mut().set_event_sink(sink.clone());
        }
    }

    /// Execute the state machine. Note that this might never return if the underlying game does
    /// not terminate.
    pub fn run(&self, mut interface: impl Interface<T::Custom>) -> Result<()> {
        let num_players = interface.get_num_players();
        debug!("User interface expects {} players", num_players);
        let mut global_state = self.init(num_players);
        let (s, r) = unbounded();
        global_state = global_state.add_event_sink(s.clone());
        // Provide the interface with the initial state.
        interface.init(&global_state);
        Self::insert_sinks(global_state.decks(), s.clone());
        for p in global_state.players() {
            Self::insert_sinks(p.decks(), s.clone());
        }

        let mut next_transition = StateTransition::Single(self.game.init_state());

        loop {
            let NextState { input, state } =
                self.get_next_state(&mut global_state, next_transition, &mut interface, &r)?;

            info!("Switching to state {}", state.description());

            if let Some(winners) = state.ends_game(&global_state) {
                if winners.is_empty() {
                    warn!("There are no winners")
                }
                if let Err(e) =
                    interface.update_ui(&global_state, vec![Event::GameOver { winners }])
                {
                    warn!("User interface reports error: {}", e);
                }
                break;
            } else {
                next_transition = state.transition(&mut global_state, input);
                let events = r.try_iter().collect();
                if let Err(e) = interface.update_ui(&global_state, events) {
                    warn!("User interface reports error: {}", e);
                }
            }
        }

        Ok(())
    }

    /// Compute the next possible state, which is possible for the user.
    fn get_next_state<'a, 'b>(
        &'a self,
        global_state: &'b mut GlobalState<T::Custom>,
        next_transition: StateTransition<'a, T::Custom>,
        interface: &mut impl Interface<T::Custom>,
        event_sink: &Receiver<Event>,
    ) -> Result<NextState<T, Input>> {
        let states = match next_transition {
            StateTransition::Single(state) => vec![state],
            StateTransition::Multiple(states) => states,
        };
        let max_prio = Self::get_max_prio(&states, global_state)?;

        // Get the next possible states if they are valid
        // and the priority is highest.
        let next_states: Vec<_> = states
            .into_iter()
            .filter(|s| s.valid(global_state) && s.priority() == max_prio)
            .map(|s| NextState::new(s.input(global_state), s))
            .collect();

        if next_states.len() == 1 && next_states[0].input.is_none() {
            // We only have one next state and no input from the user interface is required,
            // so transition to it.
            next_states
                .into_iter()
                .next()
                .map(|s| NextState::new(None, s.state))
                .context("Next state does not exist")
        } else {
            info!("Multiple equal successor states available or state requires user input, delegate to user interface");

            let ask_player = match next_states.first().unwrap().state.ask_player() {
                Some(p) => p,
                None => {
                    bail!("There is no player to ask")
                }
            };

            if log_enabled!(Warn)
                && next_states.iter().any(|s| match s.state.ask_player() {
                    Some(v) => v != ask_player,
                    None => true,
                })
            {
                warn!("Inconsistency between who to ask for the next state");
            }
            let player_state = match global_state.player_by_id(ask_player) {
                Some(p) => p,
                None => bail!("Player {} does not exist", ask_player),
            };
            let events = event_sink.try_iter().collect();
            if let Err(e) = interface.update_ui(global_state, events) {
                warn!("User interface reports error: {}", e);
            }
            self.ask_user_for_next_state(global_state, player_state, next_states, interface)
                .context("Asking for next state failed")
        }
    }

    /// Get the maximal priority over the set of `states`.
    fn get_max_prio(
        states: &[Box<dyn State<Custom = T::Custom> + '_>],
        global_state: &GlobalState<T::Custom>,
    ) -> Result<isize> {
        match states
            .iter()
            .filter(|state| state.valid(global_state))
            .map(|state| state.priority())
            .max()
        {
            Some(m) => Ok(m),
            None => Err(anyhow!("Missing successor state")),
        }
    }

    /// Ask the user to provide a valid next state.
    /// This function will block until the user provides a valid input.
    /// This function will return the state which was selected.
    fn ask_user_for_next_state<'a>(
        &self,
        global_state: &GlobalState<T::Custom>,
        player_state: &PlayerState,
        mut next_states: Vec<NextState<'a, T, InputDescription>>,
        interface: &mut impl Interface<T::Custom>,
    ) -> Result<NextState<'a, T, Input>> {
        info!("Telling user interface to ask player {}", player_state.id());
        let states: Vec<_> = next_states
            .iter()
            .map(|s| (s.input.clone(), s.state.description()))
            .collect();
        loop {
            let (input, selected_option) = interface
                .ask_for_option(global_state, player_state, &states)
                .context("Asking for option failed")?;
            if selected_option >= next_states.len() {
                error!(
                    "Selected option ({}) out-of-bounds, only {} options available",
                    selected_option,
                    next_states.len()
                );
                continue;
            } else {
                // Get owned object from `next_states` and return it.
                return Ok(NextState::new(
                    input,
                    next_states.remove(selected_option).state,
                ));
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use test_log::test;

    use crate::components::event::Event;
    use crate::components::{Card, Deck};
    use crate::config::{GameConfiguration, PlayerConfiguration};
    use crate::state_machine::states::{GlobalState, PlayerState};
    use crate::state_machine::traits::{
        SuccessorIndexWithInput, SuccessorStateWithInputDescription,
    };
    use crate::state_machine::{
        Game, Interface, PlayerMove, State, StateMachine, StateTransition, UserOption,
    };

    #[derive(Clone, PartialEq)]
    enum TestCase {
        SingleValid,
        SingleInvalid,
        MultipleInvalid(usize),
        MultipleDescendingPriority(usize, usize, bool),
        MultipleSamePriority {
            count: usize,
            target: usize,
            me: Option<usize>,
            ask: Vec<usize>,
        },
    }

    struct TestGame {
        test_case: TestCase,
    }

    impl TestGame {
        fn new(test_case: TestCase) -> Self {
            TestGame { test_case }
        }
    }

    impl Game for TestGame {
        type Custom = ();

        fn define_card_set(&self) -> Vec<Card> {
            Vec::new()
        }

        fn define_configuration(&self) -> GameConfiguration {
            GameConfiguration::default()
        }

        fn define_player_configuration(&self) -> PlayerConfiguration {
            PlayerConfiguration::default()
        }

        fn define_global_decks(&self, _card_set: Vec<Card>) -> Vec<Deck> {
            Vec::new()
        }

        fn define_player_decks(&self, _player: usize) -> Vec<Deck> {
            Vec::new()
        }

        fn init_state(&self) -> Box<dyn State<Custom = Self::Custom> + '_> {
            Box::new(TestState::new(self.test_case.clone()))
        }

        fn init_custom_data(&self) -> Self::Custom {}
    }

    struct DummyInterface {
        //players: Option<usize>,
    }

    impl DummyInterface {
        fn new() -> Self {
            DummyInterface {}
        }
    }

    impl<Custom> Interface<Custom> for DummyInterface {
        fn ask_for_option(
            &mut self,
            _global_state: &GlobalState<Custom>,
            _player_state: &PlayerState,
            _states: &[SuccessorStateWithInputDescription],
        ) -> anyhow::Result<SuccessorIndexWithInput> {
            todo!()
        }

        fn get_num_players(&mut self) -> usize {
            1
        }

        fn update_ui(
            &mut self,
            _state: &GlobalState<Custom>,
            _events: Vec<Event>,
        ) -> anyhow::Result<()> {
            Ok(())
        }
    }

    struct FixedSelectionInterface {
        select: usize,
        players: Option<usize>,
    }

    impl FixedSelectionInterface {
        fn new(select: usize) -> Self {
            FixedSelectionInterface {
                select,
                players: None,
            }
        }
        fn players(self, players: usize) -> Self {
            FixedSelectionInterface {
                players: Some(players),
                ..self
            }
        }
    }

    impl<Custom> Interface<Custom> for FixedSelectionInterface {
        fn ask_for_option(
            &mut self,
            _global_state: &GlobalState<Custom>,
            _player_state: &PlayerState,
            _states: &[SuccessorStateWithInputDescription],
        ) -> anyhow::Result<SuccessorIndexWithInput> {
            Ok((None, self.select))
        }

        fn get_num_players(&mut self) -> usize {
            self.players.unwrap_or(1)
        }

        fn update_ui(
            &mut self,
            _state: &GlobalState<Custom>,
            _event: Vec<Event>,
        ) -> anyhow::Result<()> {
            Ok(())
        }
    }

    struct TestState(TestCase);

    impl TestState {
        fn new(case: TestCase) -> Self {
            TestState(case)
        }
    }

    impl UserOption for TestState {
        fn description(&self) -> String {
            "Test state".to_string()
        }
    }

    impl<'a> State<'a> for TestState {
        type Custom = ();

        fn ends_game(&self, _state: &GlobalState<Self::Custom>) -> Option<Vec<usize>> {
            use TestCase::*;
            match self.0 {
                SingleValid | SingleInvalid | MultipleDescendingPriority(_, _, true) => {
                    Some(vec![0])
                }
                MultipleSamePriority { target, me, .. } if Some(target) == me => Some(vec![0]),
                _ => None,
            }
        }

        fn valid(&self, _state: &GlobalState<Self::Custom>) -> bool {
            use TestCase::*;
            match self.0 {
                SingleValid | MultipleDescendingPriority(_, _, _) | MultipleSamePriority { .. } => {
                    true
                }
                _ => false,
            }
        }

        fn priority(&self) -> isize {
            use TestCase::*;
            match self.0 {
                MultipleDescendingPriority(_, count, _) => count as isize,
                _ => 0,
            }
        }

        fn ask_player(&self) -> Option<usize> {
            use TestCase::*;

            match &self.0 {
                MultipleSamePriority { me, ask, .. } => ask.get(me.unwrap()).copied(),
                _ => None,
            }
        }

        fn transition(
            self: Box<Self>,
            _state: &mut GlobalState<Self::Custom>,
            _input: Option<PlayerMove>,
        ) -> StateTransition<'a, Self::Custom> {
            use TestCase::*;
            match &self.0 {
                SingleValid | SingleInvalid => StateTransition::Single(self),
                MultipleInvalid(count) => StateTransition::Multiple(
                    (0..*count)
                        .map(|_| {
                            Box::new(TestState::new(self.0.clone())) as Box<dyn State<Custom = _>>
                        })
                        .collect(),
                ),
                MultipleDescendingPriority(max, count, false) if max == count => {
                    StateTransition::Multiple(
                        (0..*count)
                            .map(|i| {
                                Box::new(TestState::new(MultipleDescendingPriority(
                                    *max,
                                    *count - i,
                                    true,
                                ))) as Box<dyn State<Custom = _>>
                            })
                            .collect(),
                    )
                }
                MultipleDescendingPriority(_, _, _) => {
                    //Panic if a transition was taken without the correctly prioritized function
                    panic!("Wrong transition chosen");
                }
                // Initialize test case
                MultipleSamePriority {
                    count,
                    target,
                    me,
                    ask,
                } if me.is_none() => {
                    let c = (0..*count)
                        .map(|i| {
                            Box::new(TestState::new(MultipleSamePriority {
                                count: *count,
                                target: *target,
                                me: Some(i),
                                ask: ask.clone(),
                            })) as Box<dyn State<Custom = _>>
                        })
                        .collect();
                    StateTransition::Multiple(c)
                }
                // The state machine must pick the state selected by the user interface.
                MultipleSamePriority { target, me, .. } if Some(*target) == *me => {
                    StateTransition::Single(self)
                }
                MultipleSamePriority { .. } => {
                    //Panic if the wrong state has been selected
                    panic!("Wrong transition chosen");
                }
            }
        }
    }

    #[test]
    fn correct_initialization() {
        let players = 3;
        let game = TestGame::new(TestCase::SingleValid);
        let machine = StateMachine::new(game);
        let state = machine.init(players);

        assert_eq!(state.players().len(), players);

        for (i, p) in state.players().iter().enumerate() {
            assert_eq!(p.id(), i);
        }
    }

    #[test]
    fn start_and_halt_machine() {
        let game = TestGame::new(TestCase::SingleValid);

        let machine = StateMachine::new(game);

        assert!(machine.run(DummyInterface::new()).is_ok());
    }

    #[test]
    fn single_ignore_valid_property() {
        let game = TestGame::new(TestCase::SingleInvalid);

        let machine = StateMachine::new(game);

        assert!(machine.run(DummyInterface::new()).is_err());
    }

    #[test]
    fn multiple_fail_with_invalid_successors() {
        let game = TestGame::new(TestCase::MultipleInvalid(3));

        let machine = StateMachine::new(game);

        assert!(machine.run(DummyInterface::new()).is_err());
    }

    #[test]
    fn multiple_fail_with_prioritized_successors() {
        let game = TestGame::new(TestCase::MultipleDescendingPriority(3, 3, false));

        let machine = StateMachine::new(game);

        assert!(machine.run(DummyInterface::new()).is_ok());
    }

    #[test]
    fn multiple_with_same_priority_asking_same_player() {
        let to_be_selected = 2;
        let count = 4;
        let game = TestGame::new(TestCase::MultipleSamePriority {
            count,
            target: to_be_selected,
            me: None,
            ask: vec![2; count],
        });

        let machine = StateMachine::new(game);

        assert!(machine
            .run(FixedSelectionInterface::new(to_be_selected).players(count))
            .is_ok());
    }
}
