use crate::components::event::Event;
use crate::components::{Card, Deck};
use crate::config::{GameConfiguration, PlayerConfiguration};
use crate::state_machine::states::{GlobalState, PlayerState};
use crate::state_machine::{PlayerMove, StateTransition};
use anyhow::Result;
use std::collections::HashMap;

/// The `StateIndex` refers to an index of the successor states.
pub type StateIndex = usize;

pub type SuccessorStateWithInputDescription = (Option<HashMap<String, InputDescription>>, String);
pub type SuccessorIndexWithInput = (Option<HashMap<String, Input>>, StateIndex);

/// This trait must be implemented by the user interface in order to allow interaction
/// between the game and the interface.
pub trait Interface<Custom> {
    /// Currently only used for deciding what state to pick next in case there are multiple options.
    /// The returned value is an index for the set of next possible states.
    fn ask_for_option(
        &mut self,
        global_state: &GlobalState<Custom>,
        player_state: &PlayerState,
        states: &[SuccessorStateWithInputDescription],
    ) -> Result<SuccessorIndexWithInput>;

    fn get_num_players(&mut self) -> usize;

    /// This function is called after each state transition with the most recent `GlobalState`.
    /// The user interface should use this possible to look into its event queue to scan for any
    /// changes.
    fn update_ui(&mut self, state: &GlobalState<Custom>, events: Vec<Event>) -> Result<()>;

    /// Initialize the user interface with the given initial state and an event source to retrieve
    /// changes.
    fn init(&mut self, _init_state: &GlobalState<Custom>) {}
}

/// This trait must be implemented for all games.
pub trait Game {
    type Custom;

    /// Return the total card set available in the game.
    fn define_card_set(&self) -> Vec<Card>;
    /// Define general properties of the game.
    fn define_configuration(&self) -> GameConfiguration;
    /// Define player-specific properties of the game. Note that this configuration applies to
    /// all players. Thus, special roles are probably not implementable.
    fn define_player_configuration(&self) -> PlayerConfiguration;
    /// Define the global set of decks. Global means these decks are e.g. put on the table and thus,
    /// not specific to a certain player.
    fn define_global_decks(&self, card_set: Vec<Card>) -> Vec<Deck>;
    /// Define the decks only available to players.
    fn define_player_decks(&self, player: usize) -> Vec<Deck>;
    /// Return the state the game starts with.
    fn init_state(&self) -> Box<dyn State<Custom = Self::Custom> + '_>;
    fn init_custom_data(&self) -> Self::Custom;
}

#[derive(Clone, Debug, PartialEq)]
/// The kinds of input requested by the game.
pub enum InputDescription {
    /// Request a card from a player's deck. Matches with `Cards` from `Input`.
    CardsFromPlayerDeck {
        /// The player id from whom it the cards were requested.
        player: usize,
        /// The number of cards which are requested.
        amount: usize,
        /// The id of the deck from which it is requested.
        deck_id: usize,
    },
    /// Request a selection of game-defined options. Matches with `CustomSelection` from `Input`.
    Custom(Vec<String>),
    /// Request a deck to be selected. The vector corresponds to the allowed decks to be selected.
    Deck(Vec<usize>),
}

#[derive(Debug, PartialEq)]
/// Contains the requested components selected by the user.
pub enum Input {
    /// Selected cards. Matches with `CardsFromPlayerDeck` from `InputDescription`.
    Cards(Vec<Card>),
    /// Selected deck. Matches with `Deck` from `InputDescription`.
    Deck(usize),
    /// Selected option. Matches with `Custom` from `InputDescription`.
    CustomSelection(usize),
}

/// Return a more 'detailed' description than what you might want to have in a
/// `Display` implementation.
pub trait UserOption {
    /// Describe the current state. This might be presented to the user in case multiple options
    /// are available.
    fn description(&self) -> String;
}

/// This is required for allowing to use a boxed `Description` whenever we want to use a function
/// that accepts a generic `T: Description`.
/// See [here](https://doc.rust-lang.org/1.38.0/src/std/io/impls.rs.html#122-143).
impl<T: UserOption + ?Sized> UserOption for Box<T> {
    fn description(&self) -> String {
        (**self).description()
    }
}

/// This trait must be implemented for all game states.
pub trait State<'a>: UserOption {
    type Custom;

    /// Return Some with the players who won. This is called after the `transition` has been called.
    fn ends_game(&self, _state: &GlobalState<Self::Custom>) -> Option<Vec<usize>> {
        None
    }
    /// Return whether this state is valid. For example, a card in UNO may only be drawn if no
    /// card can be played. This function is called before `transition` is called.
    fn valid(&self, _state: &GlobalState<Self::Custom>) -> bool {
        true
    }

    /// Define the priority of the state. If multiple successor states S are available, then only
    /// *valid* ones with the *highest* priority value are considered.
    fn priority(&self) -> isize {
        0
    }

    /// If user interaction is required because either the state
    /// requests user input or multiple successor states are available,
    /// the `StateMachine` asks which player to present the possible actions.
    /// *Important*: In the case of multiple successor states being available,
    /// ensure that all of these successor states return the same player. Otherwise,
    /// the behaviour is undefined.
    fn ask_player(&self) -> Option<usize> {
        None
    }

    /// Define whether this state requires any kind of input. Return `None` if no input is expected.
    /// The key in the HashMap is only relevant to the state itself. The key is mapped to the
    /// corresponding result in the `transition` function in order to help the state to
    /// correctly map the input in case of multiple data.
    fn input(
        &self,
        _state: &GlobalState<Self::Custom>,
    ) -> Option<HashMap<String, InputDescription>> {
        None
    }
    /// Apply this state and return a or multiple successor state(s).
    /// If input was requested, `input` contains the resulting input by the user.
    fn transition(
        self: Box<Self>,
        state: &mut GlobalState<Self::Custom>,
        input: Option<PlayerMove>,
    ) -> StateTransition<'a, Self::Custom>;
}
