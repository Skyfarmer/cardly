/// Contains interactable elements part of a card game.
pub mod components;
/// Defines configuration elements used to describe a game.
pub mod config;
/// Provides support to write games using the Lua programming language.
pub mod lua_game;
/// Contains the state machine to execute arbitrary games.
pub mod state_machine;
pub mod utilities;

#[macro_export]
macro_rules! loop_while_error {
    ($e:expr) => {{
        let result = loop {
            match $e {
                Ok(response) => break response,
                Err(err) => log::info!("{}", err),
            }
        };

        result
    }};
}

#[macro_export]
macro_rules! loop_while_error_async {
    ($e:expr) => {{
        let result = loop {
            let result = async_global_executor::block_on(async { $e });

            match result {
                Ok(response) => break response,
                Err(err) => log::info!("{}", err),
            }
        };

        result
    }};
}
