function addCardsForColor(color)
    for i=1,5 do
        engineAddCard(i, color)
    end
end

function defineCardSet ()
    addCardsForColor("Herz")
    addCardsForColor("Eichel")
    addCardsForColor("Blatt")
    addCardsForColor("Schelle")
end

function defineGlobalDecks ()
    engineAddMainDeck("drawDeck", "stacked", "hidden")
    engineAddDeck("atoutDeck", "stacked", "toponly")
    engineAddDeck("putDeck", "spreaded", "global")
    engineAddDeck("discardDeck", "stacked", "hidden")
end

function definePlayerDecks ()
    engineAddDeck("handCards", "spreaded", "player")
end

function defineInitState ()
    return StartNextRoundState:new()
end

function getPointsForCard(card)
    if card.value == "1" then
        -- Unter
        return 2
    elseif card.value == "2" then
        -- Ober
        return 3
    elseif card.value == "3" then
        -- King
        return 4
    elseif card.value == "4" then
        -- 10
        return 10
    elseif card.value == "5" then
        -- 11
        return 11
    end
end

function isCardValid(current, attempt, player)
    local cards = engineCardsOnPlayerDeck(player, "handCards")
    local eqColor = current.color == attempt.color
    local wouldStich = (current.color == attempt.color and tonumber(current.value) < tonumber(attempt.value))
        or (current.color ~= atout and attempt.color == atout)

    for i,c in ipairs(cards) do
        if c.color == attempt.color and c.value == attempt.value then
            -- Ignore played card
        elseif not eqColor and c.color == current.color then
            -- Farbzwang
            return false
        elseif not wouldStich and c.color == current.color and tonumber(c.value) > tonumber(current.value) then
            -- Stichzwang mit gleicher farbe
            return false
        elseif not eqColor and not wouldStich and c.color == atout and current.color ~= atout then
            -- Stichzwang mit Atout
            return false
        end
    end
    return true
end

function playerMaySwitchAdout(player)
    local cards = engineCardsOnPlayerDeck(player, "handCards")
    for i,c in ipairs(cards) do
        if c.color == atout and c.value == "1" then
            return true
        end
    end
    return false
end

BUMMERL = {}

StartNextRoundState = {}

function StartNextRoundState:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self

    return o
end

function StartNextRoundState:stateTransition()
    local score = "Points:\n"
    for i,p in ipairs(enginePlayers()) do
        local b = 0
        if BUMMERL[p] ~= nil then
            b = BUMMERL[p]
        end

        score = score .. "{" .. p .. "}: " .. b .. "\n"
    end
    engineScoreMessage(score)
    return { ShuffleState:new(), EndGameState:new() }
end

function StartNextRoundState:stateDescription()
    return "Start next round"
end

EndGameState = { }

function EndGameState:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self

    return o
end

function EndGameState:statePriority()
    return 2
end

function EndGameState:stateValid()
    local players = enginePlayers()
    for i,p in ipairs(players) do
        if BUMMERL[p] ~= nil and BUMMERL[p] >= 7 then
            return true
        end
    end
    return false
end

function EndGameState:stateEndsGame()
    local players = enginePlayers()
    local winner = ""
    for i,p in ipairs(players) do
        if BUMMERL[p] ~= nil and BUMMERL[p] >= 7 then
            winner = p
            break
        end
    end
    return winner
end

function EndGameState:stateDescription()
    return "End game"
end

ShuffleState = { }

-- Typical object-oriented Lua pattern
function ShuffleState:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self

    return o
end

function ShuffleState:stateTransition()
    engineShuffleDeck("drawDeck")

    return DelegateState:new()
end

function ShuffleState:stateDescription()
    return "Shuffle cards"
end

DelegateState = { }

function DelegateState:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self

    return o
end

function DelegateState:stateTransition()
    local players = enginePlayers()
    points = { }
    ansagePoints = {}
    forceColor = false
    ZUDREHEN = nil
    engineDeckSetVisibility("atoutDeck", "toponly")

    for i,p in ipairs(players) do
        points[p] = 0
        for i=1,5 do
            enginePlayerDeckDrawFrom("drawDeck", p, "handCards")
        end
    end

    engineDeckDraw("drawDeck", "atoutDeck")
    atout = enginePeekTop("atoutDeck").color

    local next = BEGINNER or players[engineRandom(2)]
    BEGINNER = engineGetPlayerAfter(next)

    return NextPlayerState:new(next)
end

function DelegateState:stateDescription()
    return "Delegate 5 cards to each player"
end

-- NextPlayerState
NextPlayerState = { }

function NextPlayerState:new(player)
    o = { }
    setmetatable(o, self)
    self.__index = self
    self.player = player

    return o
end

function NextPlayerState:stateTransition()
    return { EndRoundState:new(self.player), PlayCardState:new(self.player),
        ZudrehnState:new(self.player), SwitchAdoutState:new(self.player), AnsageState:new(self.player) }
end

function NextPlayerState:stateDescription()
    return "Deque next player"
end

PlayCardState = { }

function PlayCardState:new(player)
        o = {}
        setmetatable(o, self)
        self.__index = self
        self.player = player

        return o
end

function PlayCardState:stateAskPlayer()
    return self.player
end

function PlayCardState:stateExpectedInput()
    engineExpectDeck("put", {"putDeck"})
    engineExpectCardsFromPlayerDeck("playedCard", self.player, 1, "handCards")
end

function PlayCardState:stateTransition()
    local input = engineInputCards("playedCard")
    local card = input[1]
    local deckTop = enginePeekTop("putDeck")

    if deckTop == nil then
        engineDeckPutOnTop(self.player, "handCards", card, "putDeck")
        return NextPlayerState:new(engineGetPlayerAfter(self.player))
    else
        if forceColor and not isCardValid(deckTop, card, self.player) then
            enginePlayerErrorMessage(self.player, "Same color must be played and trick must be taken!")
            return NextPlayerState:new(self.player)
        end
        engineDeckPutOnTop(self.player, "handCards", card, "putDeck")

        local winner = engineGetPlayerAfter(self.player)
        -- Either both cards have the same color, then the one with the higher value wins
        -- or my card is atout and the other is not.
        if (deckTop.color == card.color and tonumber(deckTop.value) < tonumber(card.value))
            or (deckTop.color ~= atout and card.color == atout) then
            winner = self.player
        end

        points[winner] = points[winner] + getPointsForCard(card) + getPointsForCard(deckTop)
        if ansagePoints[winner] ~= nil then
            points[winner] = points[winner] + ansagePoints[winner]
            ansagePoints[winner] = nil
        end

        return CleanupState:new(winner)
    end
end

function PlayCardState:stateDescription()
        return "Play a card"
end

DrawCardState = { }

function DrawCardState:new(first)
        o = {}
        setmetatable(o, self)
        self.__index = self
        self.first = first

        return o
end

function DrawCardState:stateTransition()
    enginePlayerDeckDrawFromTop("drawDeck", self.first, "handCards")
    if engineAmountCardsOnDeck("drawDeck") == 0 then
        forceColor = true
        enginePlayerDeckDrawFromTop("atoutDeck", engineGetPlayerAfter(self.first), "handCards")
    else
        enginePlayerDeckDrawFromTop("drawDeck", engineGetPlayerAfter(self.first), "handCards")
    end

    return NextPlayerState:new(self.first)
end

function DrawCardState:statePriority()
    return 1
end

function DrawCardState:stateValid()
    return not forceColor
end

function DrawCardState:stateDescription()
        return "Draw a card from the main deck"
end

EndRoundState = { }

function EndRoundState:new(lastWinner)
    o = { }
    setmetatable(o, self)
    self.__index = self
    self.lastWinner = lastWinner

    return o
end

function EndRoundState:stateValid()
    local players = enginePlayers()
    local emptyCards = 0
    for i,p in ipairs(players) do
        if points[p] >= 66  then
            return true
        elseif enginePlayerAmountCardsOnDeck(p, "handCards") == 0 then
            emptyCards = emptyCards + 1
        end
    end
    return emptyCards == 2
end

function EndRoundState:statePriority()
    return 2
end

function EndRoundState:stateTransition()
    local winner = ""

    if ZUDREHEN == nil then
        local players = enginePlayers()
        for i,p in ipairs(players) do
            if points[p] >= 66 then
                winner = p
                enginePlayerInfoMessage(winner, "66 points reached")
                break
            end
        end
    else
        if points[ZUDREHEN] >= 66 then
            winner = ZUDREHEN
            enginePlayerInfoMessage(winner, "66 points reached")
        else
            winner = engineGetPlayerAfter(ZUDREHEN)
            enginePlayerInfoMessage(winner, "Opponent did not reach 66 points")
        end
    end

    if winner == "" then
        winner = self.lastWinner
    end

    local loser = engineGetPlayerAfter(winner)
    local p = 0
    if points[loser] >= 33 then
        p = 1
    elseif points[loser] > 0 then
        p = 2
    else
        p = 3
    end
    if BUMMERL[winner] == nil then
        BUMMERL[winner] = p
    else
        BUMMERL[winner] = BUMMERL[winner] + p
    end

    while engineAmountCardsOnDeck("discardDeck") ~= 0 do
        engineDeckDraw("discardDeck", "drawDeck")
    end
    while engineAmountCardsOnDeck("atoutDeck") ~= 0 do
        engineDeckDraw("atoutDeck", "drawDeck")
    end
    while engineAmountCardsOnDeck("putDeck") ~= 0 do
        engineDeckDraw("putDeck", "drawDeck")
    end

    for i,p in ipairs(enginePlayers()) do
        enginePlayerDeckTransfer(p, "handCards", "drawDeck")
    end

    return StartNextRoundState:new()
end

function EndRoundState:stateDescription()
    return "End 2er Schnapsen"
end

ZudrehnState = { }

function ZudrehnState:new(player)
        o = {}
        setmetatable(o, self)
        self.__index = self
        self.player = player

        return o
end

function ZudrehnState:stateAskPlayer()
    return self.player
end

function ZudrehnState:stateValid()
    local isBeginner = engineAmountCardsOnDeck("putDeck") == 0
    return engineAmountCardsOnDeck("drawDeck") > 1 and not forceColor and isBeginner
end

function ZudrehnState:stateTransition()
    forceColor = true
    ZUDREHEN = self.player
    engineDeckSetVisibility("atoutDeck", "hidden")

    return NextPlayerState:new(self.player)
end

function ZudrehnState:stateDescription()
        return "Zudrehen"
end

SwitchAdoutState = {}

function SwitchAdoutState:new(player)
        o = {}
        setmetatable(o, self)
        self.__index = self
        self.player = player

        return o
end

function SwitchAdoutState:stateAskPlayer()
    return self.player
end

function SwitchAdoutState:stateValid()
    return not forceColor and playerMaySwitchAdout(self.player) and engineAmountCardsOnDeck("putDeck") == 0
end

function SwitchAdoutState:stateTransition()
    enginePlayerDeckDrawFromTop("atoutDeck", self.player, "handCards")
    local card = {}
    card.color = atout
    card.value = "1"
    engineDeckPutOnTop(self.player, "handCards", card, "atoutDeck")

    return NextPlayerState:new(self.player)
end

function SwitchAdoutState:stateDescription()
        return "Switch adout"
end

AnsageState = {}

function AnsageState:new(player)
        o = {}
        setmetatable(o, self)
        self.__index = self
        self.player = player

        return o
end

function AnsageState:stateAskPlayer()
    return self.player
end

function AnsageState:getAnsageColors()
    local ansageColors = {}
    local colors = {"Herz", "Schelle", "Eichel", "Blatt"}
    for i,c in ipairs(colors) do
        local cardOber = {}
        cardOber.color = c
        cardOber.value = "2"

        local cardKing = {}
        cardKing.color = c
        cardKing.value = "3"
        if self:containsCard(cardOber) and self:containsCard(cardKing) then
            table.insert(ansageColors, c)
        end
    end
    return ansageColors
end

function AnsageState:containsCard(card)
    local cards = engineCardsOnPlayerDeck(self.player, "handCards")
    for i,c in ipairs(cards) do
        if c.color == card.color and c.value == card.value then
            return true
        end
    end
    return false
end

function AnsageState:stateValid()
    local isBeginner = engineAmountCardsOnDeck("putDeck") == 0
    self.ansageColors = self:getAnsageColors()
    return isBeginner and #self.ansageColors > 0
end

function AnsageState:stateExpectedInput()
    engineExpectCustomSelection("color", self.ansageColors)
end

function AnsageState:stateTransition()
    local input = engineInputCustom("color")
    local card = {}
    card.value = "2"
    card.color = self.ansageColors[input]

    local p = 20
    if card.color == atout then
        p = 40
    end

    if points[self.player] == 0 then
        ansagePoints[self.player] = p
    else
        points[self.player] = points[self.player] + p
    end
    engineDeckPutOnTop(self.player, "handCards", card, "putDeck")

    engineInfoMessage("{" .. self.player .. "} made an Ansage and receives " .. p .. " points")

    return NextPlayerState:new(engineGetPlayerAfter(self.player))
end

function AnsageState:stateDescription()
        return "Ansage"
end

CleanupState = {}

function CleanupState:new(winner)
        o = {}
        setmetatable(o, self)
        self.__index = self
        self.winner = winner

        return o
end

function CleanupState:stateTransition()
    --Workaround: Give frontend some time to show cards
    os.execute("sleep 2")
    -- Remove cards from table
    while engineAmountCardsOnDeck("putDeck") ~= 0 do
        engineDeckDraw("putDeck", "discardDeck")
    end

    return { DrawCardState:new(self.winner), NextPlayerState:new(self.winner) }
end

function CleanupState:stateDescription()
        return "Cleanup"
end