function addCardsForColor(color)
    for i=2,14 do
        engineAddCard(i, color)
    end
end

TRUMPS = {"Clubs", "Diamonds", "Hearts", "Spades", "No Trump"}

function defineCardSet ()
    addCardsForColor("Clubs")
    addCardsForColor("Diamonds")
    addCardsForColor("Hearts")
    addCardsForColor("Spades")
end

function defineGlobalDecks ()
    engineAddMainDeck("drawDeck", "stacked", "hidden")
    engineAddDeck("putDeck", "spreaded", "global")
    engineAddDeck("discardDeck", "stacked", "hidden")
end

function definePlayerDecks ()
    engineAddDeck("handCards", "spreaded", "player")
end

function defineInitState ()
    return StartNextRoundState:new()
end

function emojiForColor(color)
    if color == "Spades" then
        return "♠️"
    elseif color == "Hearts" then
        return "❤️"
    elseif color == "Diamonds" then
        return "♦️"
    elseif color == "Clubs" then
        return "♣️"
    elseif color == nil or color == "No Trump" then
        return "No Trump"
    else
        print("Color", color, "does not exist")
    end
end

function sendScoreMessage()
    local score = ""
    if maximum ~= nil then
        score = score .. "Current Bid: " .. maximum .. " " .. emojiForColor(TRUMP) .. " by {" .. BIDDER .. "}\n"
    end
    for i,p in ipairs(enginePlayers()) do
        if i == 3 then
            break
        end

        local b = 0
        if SCORE[p % 2] ~= nil then
            b = SCORE[p % 2]
        end
        local s = 0
        if STICHE ~= nil and STICHE[p % 2] ~= nil then
            s = STICHE[p % 2]
        end
        local teamPartner = engineGetPlayerAfter(engineGetPlayerAfter(p))
        score = score .. "Points: {" .. p .. "}/{" .. teamPartner .. "}: " .. b .. "\n"
        score = score .. "Tricks: {" .. p .. "}/{" .. teamPartner .. "}: " .. s .. "\n"
    end
    engineScoreMessage(score)
end

SCORE = {}

StartNextRoundState = {}

function StartNextRoundState:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self

    return o
end

function StartNextRoundState:stateTransition()
    STICHE = { }
    maximum = nil
    sendScoreMessage()

    return { ShuffleState:new(), EndGameState:new() }
end

function StartNextRoundState:stateDescription()
    return "Start next round"
end

EndGameState = { }

function EndGameState:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self

    return o
end

function EndGameState:statePriority()
    return 2
end

function EndGameState:stateValid()
    local players = enginePlayers()
    for i,p in ipairs(players) do
        if SCORE[p % 2] ~= nil and SCORE[p % 2] >= 1000 then
            return true
        end
    end
    return false
end

function EndGameState:stateEndsGame()
    local players = enginePlayers()
    local winner = ""
    for i,p in ipairs(players) do
        if SCORE[p % 2] ~= nil and SCORE[p % 2] >= 1000 then
            winner = p
            break
        end
    end
    local partner = engineGetPlayerAfter(engineGetPlayerAfter(winner))
    return { winner, partner }
end

function EndGameState:stateDescription()
    return "End game"
end

-- SHUFFLE STATE
ShuffleState = { }

-- Typical object-oriented Lua pattern
function ShuffleState:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self

    return o
end

function ShuffleState:stateTransition()
    engineShuffleDeck("drawDeck")

    return DelegateState:new()
end

function ShuffleState:stateDescription()
    return "Shuffle cards"
end

DelegateState = { }

function DelegateState:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self

    return o
end

function DelegateState:stateTransition()
    maximum = 0
    PASSES = 0
    DOUBLE = false
    REDOUBLE = false
    STICHE = { }

    local players = enginePlayers()
    for i,p in ipairs(players) do
        STICHE[p % 2] = 0
        for i=1,13 do
            enginePlayerDeckDrawFrom("drawDeck", p, "handCards")
        end
    end

    local next = players[engineRandom(4)]

    return { PassBidState:new(next), BidAmountAndColorState:new(next) }
end

function DelegateState:stateDescription()
    return "Delegate 13 cards to each player"
end

NextPlayerBidState = { }

function NextPlayerBidState:new(player)
    o = {}
    setmetatable(o, self)
    self.__index = self
    self.player = player

    return o
end

function NextPlayerBidState:stateTransition()
    local player = engineGetPlayerAfter(self.player)
    print("Current maximum", maximum)

    return { PassBidState:new(player), BidAmountAndColorState:new(player), DoubleBidState:new(player),
        RedoubleBidState:new(player), StartRoundState:new() }
end

function NextPlayerBidState:stateDescription()
    return "Deque next player for bidding"
end

BidAmountAndColorState = { }

function BidAmountAndColorState:new(player)
    o = { }
    setmetatable(o, self)
    self.__index = self
    self.player = player
    self.bids = {}
    local min = maximum
    if maximum == 0 then
        min = 1
    end

    for i=min,6 do
        local start = 1
        if i == maximum then
            for i,t in ipairs(TRUMPS) do
                if t == TRUMP then
                    start = i + 1
                end
            end
        end
        for j=start,5 do
           table.insert(self.bids, i .. " " .. TRUMPS[j])
        end
    end

    return o
end

function BidAmountAndColorState:stateValid()
    return #self.bids ~= 0
end

function BidAmountAndColorState:stateExpectedInput()
    engineExpectCustomSelection("bids", self.bids)
end

function BidAmountAndColorState:stateAskPlayer()
    return self.player
end

function BidAmountAndColorState:stateTransition()
    PASSES = 0
    DOUBLE = false
    REDOUBLE = false
    local input = engineInputCustom("bids")
    maximum = tonumber(string.sub(self.bids[input], 1, 1))
    TRUMP = string.sub(self.bids[input], 3, -1)
    BIDDER = self.player

    sendScoreMessage()

    return NextPlayerBidState:new(self.player)
end

function BidAmountAndColorState:stateDescription()
    return "Bid amount of Tricks"
end

PassBidState = { }

function PassBidState:new(player)
    o = { }
    setmetatable(o, self)
    self.__index = self
    self.player = player

    return o
end

function PassBidState:stateAskPlayer()
    return self.player
end

function PassBidState:stateTransition()
    PASSES = PASSES + 1

    return NextPlayerBidState:new(self.player)
end

function PassBidState:stateDescription()
    return "Pass bidding"
end

DoubleBidState = { }

function DoubleBidState:new(player)
    o = { }
    setmetatable(o, self)
    self.__index = self
    self.player = player

    return o
end

function DoubleBidState:stateValid()
    return BIDDER == engineGetPlayerBefore(self.player) and not REDOUBLE
end

function DoubleBidState:stateAskPlayer()
    return self.player
end

function DoubleBidState:stateTransition()
    PASSES = 0
    DOUBLE = true

    engineInfoMessage("{" .. self.player .. "} doubled!")

    return NextPlayerBidState:new(self.player)
end

function DoubleBidState:stateDescription()
    return "Double bidding"
end

RedoubleBidState = { }

function RedoubleBidState:new(player)
    o = { }
    setmetatable(o, self)
    self.__index = self
    self.player = player

    return o
end

function RedoubleBidState:stateValid()
    return BIDDER == engineGetPlayerBefore(engineGetPlayerBefore(self.player)) and DOUBLE
end

function RedoubleBidState:stateAskPlayer()
    return self.player
end

function RedoubleBidState:stateTransition()
    PASSES = 0
    DOUBLE = false
    REDOUBLE = true

    engineInfoMessage("{" .. self.player .. "} redoubled!")

    return NextPlayerBidState:new(self.player)
end

function RedoubleBidState:stateDescription()
    return "Redouble bidding"
end

StartRoundState = { }

function StartRoundState:new()
    o = { }
    setmetatable(o, self)
    self.__index = self

    return o
end

function StartRoundState:statePriority()
    return 1
end

function StartRoundState:stateValid()
    return PASSES == 3 and maximum ~= 0
end

function StartRoundState:stateTransition()
    local msg = "{" .. BIDDER ..  "} must achieve " .. maximum + 6 .. " tricks"
    if DOUBLE then
        msg = msg .. " (Double)"
        print("Double")
    else
        print("No double")
    end

    if REDOUBLE then
        msg = msg .. " (Redouble)"
        print("Redouble")
    else
        print("No redouble")
    end
    engineInfoMessage(msg)

    if TRUMP == "No Trump" then
        TRUMP = nil
        engineInfoMessage("There is no trump")
    else
        engineInfoMessage(TRUMP, "is trump")
    end
    local idx = 0
    for i,p in ipairs(enginePlayers()) do
        if p == BIDDER then
            if i + 2 == 4 then
                idx = 4
            else
                idx = (i + 2) % 4
            end
            print("Found partner", enginePlayers()[idx], "for", BIDDER)
            break
        end
    end

    partner = enginePlayers()[idx]

    enginePlayerDeckSetVisibility(partner, "handCards", "global")
    return PlayCardState:new(BIDDER)
end

function StartRoundState:stateDescription()
    return "Start next round"
end

PlayCardState = { }

function PlayCardState:new(player)
    o = { }
    setmetatable(o, self)
    self.__index = self
    self.player = player

    return o
end

function PlayCardState:stateExpectedInput()
    engineExpectCardsFromPlayerDeck("playedCard", self.player, 1, "handCards")
end

function playerHasNo(player, color)
    local cards = engineCardsOnPlayerDeck(player, "handCards")
    for i,c in ipairs(cards) do
        if c["color"] == color then
            return false
        end
    end
    return true
end

function PlayCardState:stateAskPlayer()
    if self.player == partner then
        return BIDDER
    else
        return self.player
    end
end

function PlayCardState:stateTransition()
    local input = engineInputCards("playedCard")
    local card = input[1]
    local deckTop = nil
    if engineAmountCardsOnDeck("putDeck")  ~= 0 then
        deckTop = enginePeekBottom("putDeck")
    end

    if deckTop == nil or
        card["color"] == deckTop["color"] or playerHasNo(self.player, deckTop["color"]) then
         engineDeckPutOnTop(self.player, "handCards", card, "putDeck")
        return NextPlayerPlayState:new(engineGetPlayerAfter(self.player))
    else
        local player = self:stateAskPlayer()
        enginePlayerErrorMessage(player, "You must play the same color", deckTop["color"], "as the card first played in this round!")

        return PlayCardState:new(self.player)
    end
end

function PlayCardState:stateDescription()
    return "Play a card from own card set"
end

NextPlayerPlayState = { }

function NextPlayerPlayState:new(player)
    o = {}
    setmetatable(o, self)
    self.__index = self
    self.player = player

    return o
end

function NextPlayerPlayState:stateTransition()
    local playCardState = PlayCardState:new(self.player)
    if self.player == partner then
        enginePlayerInfoMessage(BIDDER, "You must now play with the cards from your partner")
    end
    return { playCardState, CollectStichState:new(self.player), EndRoundState:new(STICHE, BIDDER) }
end

function NextPlayerPlayState:stateDescription()
    return "Deque next player for playing a card"
end

CollectStichState = { }

function CollectStichState:new(player)
    o = { }
    setmetatable(o, self)
    self.__index = self
    self.first = player

    return o
end

function CollectStichState:stateValid()
    return engineAmountCardsOnDeck("putDeck") == 4
end

function CollectStichState:statePriority()
    return 1
end

function CollectStichState:stateTransition()
    local cards = engineCardsOnDeck("putDeck")
    -- The bottom most was the first card so this is our reference
    local idx = 1
    local best = cards[idx]
    local winner = self.first

    local corresponding_player = self.first
    for i,c in ipairs(cards) do
        -- If a card is better than another, take it
        local isTrump = c["color"] == TRUMP
        local isColorEqual = best["color"] == c["color"]
        local isLarger = tonumber(best["value"]) < tonumber(c["value"])
        -- Either the card is a trump card and the old one is not, then the card is definitely better.
        -- Otherwise the colors must be the same and the value must be larger
        if (isTrump and not isColorEqual) or (isColorEqual and isLarger) then
            best = c
            winner = corresponding_player
        end
        corresponding_player = engineGetPlayerAfter(corresponding_player)
    end

    local next = winner
    if winner == partner then
        winner = BIDDER
    end

    print("Player", winner, "won the stich")

    STICHE[winner % 2] = STICHE[winner % 2] + 1

    -- Make sure the correct player starts the next round
    while engineAmountCardsOnDeck("putDeck") ~= 0 do
        engineDeckDraw("putDeck", "discardDeck")
    end

    sendScoreMessage()
    os.execute("sleep 2")

    return NextPlayerPlayState:new(next)
end

function CollectStichState:stateDescription()
    return "Collect all stichs"
end

EndRoundState = { }

function EndRoundState:new()
    o = { }
    setmetatable(o, self)
    self.__index = self

    return o
end

function EndRoundState:stateValid()
    return engineAmountCardsOnDeck("discardDeck") == 52
end

function EndRoundState:statePriority()
    return 2
end

function EndRoundState:stateTransition()
    local stiche = STICHE[BIDDER % 2]
    local goal = maximum + 6
    local overticks = stiche - goal
    if overticks < 0 then
        local others = engineGetPlayerAfter(BIDDER)
        local score = 0
        local underticks = -overticks

        engineInfoMessage("{" .. BIDDER .. "} and {" .. engineGetPlayerAfter(others) .. "} lost the contract")

        for p=1,underticks do
            if DOUBLE and p == 1 then
                score = score + 100
            elseif DOUBLE and (p == 2 or p == 3) then
                score = score + 200
            elseif DOUBLE and p >= 4 then
                score = score + 300
            elseif REDOUBLE and p == 2 then
                score = score + 200
            elseif REDOUBLE and (p == 2 or p == 3) then
                score = score + 400
            elseif REDOUBLE and p >= 4 then
                score = score + 600
            else
                score = score + 50
            end
        end
        SCORE[others % 2] = (SCORE[others % 2] or 0) + score
    else
        local score = 0

        engineInfoMessage("{" .. BIDDER .. "} and {" .. engineGetPlayerAfter(engineGetPlayerAfter(BIDDER)) .. "} won the contract")
        for p=1,stiche do
            local tmp = 0
            if TRUMP == nil and p == 1 then
                tmp = 40
            elseif TRUMP == nil and p >= 2 then
                tmp = 30
            elseif TRUMP == "Spades" or TRUMP == "Hearts" then
                tmp = 30
            else
                tmp = 20
            end

            if DOUBLE and p <= goal then
                tmp = tmp * 2
            elseif DOUBLE then
                tmp = tmp + 100
            end

            if REDOUBLE and p <= goal then
                tmp = tmp * 2
            elseif REDOUBLE then
                tmp = tmp + 200
            end

            score = score + tmp
        end
        SCORE[BIDDER % 2] = (SCORE[BIDDER % 2] or 0) + score
    end

    while engineAmountCardsOnDeck("discardDeck") ~= 0 do
        engineDeckDraw("discardDeck", "drawDeck")
    end

    local partner = engineGetPlayerAfter(engineGetPlayerAfter(BIDDER))
    enginePlayerDeckSetVisibility(partner, "handCards", "player")

    return StartNextRoundState:new()
end

function EndRoundState:stateDescription()
    return "Ending round"
end