function defineCardSet()
    engineAddCard(0, "Red")
    engineAddCard(1, "Red")
    engineAddCard(2, "Red")
end
function definePlayerConfiguration()
end

function defineGlobalDecks()
    engineAddMainDeck("main", "stacked", "toponly")
    engineAddDeck("side1", "stacked", "toponly")
    engineAddDeck("side2", "stacked", "toponly")
    engineAddDeck("side3", "stacked", "toponly")
    engineAddDeck("side4", "spreaded", "toponly")
    engineAddDeck("side5", "stacked", "toponly")
    engineAddDeck("side6", "stacked", "toponly")
    engineAddDeck("side7", "stacked", "toponly")
    engineAddDeck("side8", "stacked", "toponly")
end

function definePlayerDecks()
        engineAddDeck("p1", "spreaded", "player")
end

function defineInitState()
    return TestState2:new()
end

TestState2 = {}

function TestState2:new(p)
    o = {}
    setmetatable(o, self)
    self.__index = self
    self.player = p

    return o
end

function TestState2:stateDescription()
    return "Test State"
end

function TestState2:stateAskPlayer()
    return self.player
end

function TestState2:stateTransition()
    local p = enginePlayers()[1]
    local players = enginePlayers()
    for i,p in ipairs(players) do
        enginePlayerScoreMessage(p, "Player: " .. i)
        enginePlayerInfoMessage(p, "Info: " .. i)
        enginePlayerErrorMessage(p, "Error: " .. i)
    end
    return { TestState2:new(p), TestState4:new(p), TestState5:new(self.player) }
end

TestState4 = {}

function TestState4:new(p)
    o = {}
    setmetatable(o, self)
    self.__index = self
    self.player = p

    return o
end

function TestState4:stateDescription()
    return "Long Text with more text and even longer sentences"
end

function TestState4:stateAskPlayer()
    return self.player
end

function TestState4:stateTransition()
    engineScoreMessage("Global Message")
    engineInfoMessage("Global Info Message")
    engineErrorMessage("Global Error Message")
    return { TestState2:new(self.player), TestState4:new(self.player), TestState5:new(self.player) }
end

TestState5 = {}

function TestState5:new(p)
    o = {}
    setmetatable(o, self)
    self.__index = self
    self.player = p

    return o
end

function TestState5:stateDescription()
    return "Long Text with more text"
end

function TestState5:stateAskPlayer()
    return self.player
end

function TestState5:stateEndsGame()
    return self.player
end

function TestState5:stateTransition()
    return { TestState2:new(self.player), TestState4:new(self.player), TestState5:new(self.player) }
end