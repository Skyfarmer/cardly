function defineCardSet()
    engineAddCard(0, "red")
end

function definePlayerConfiguration()
end

function defineGlobalDecks()
    engineAddMainDeck("main1", "stacked", "hidden")
    engineAddDeck("deck1", "spreaded", "toponly")
    engineAddDeck("deck2")
end

function definePlayerDecks()
    engineAddDeck("deck1", "stacked", "player")
end