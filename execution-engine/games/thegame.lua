-- User defined method - not called by runtime
function addCardsForColor(color)
    for i=0,9 do
        engineAddCard(i, color)
        if i ~= 0 then
            engineAddCard(i, color)
        end
    end
end

no_color = "no_color"

function defineCardSet ()
    for i=2,99 do
        engineAddCard(i, "no_color")
    end
end

-- Called by runtime, defines global decks.
-- Global means that decks are generally available (e.g. put on the table) and not player-specific (e.g. hand cards)
-- The following runtime functions are available:
-- addMainDeck(name)
-- addDeck(name)
-- Note: addMainDeck receives the entire card set defined in defineCardSet. Thus, it may only be called once.
function defineGlobalDecks ()
    engineAddMainDeck("drawDeck", "stacked", "hidden")
    engineAddDeck("downDeck1", "stacked", "toponly")
    engineAddDeck("downDeck2", "stacked", "toponly")
    engineAddDeck("upDeck1", "stacked", "toponly")
    engineAddDeck("upDeck2", "stacked", "toponly")
end

-- Called by runtime, defines player-specific decks.
-- The following runtime functions are available:
-- addDeck(name)
-- Note: This function is called for each player once.
function definePlayerDecks ()
    engineAddDeck("handCards", "spreaded", "player")
end

-- Called by runtime, creates the initial state.
function defineInitState ()
    return ShuffleState:new()
end

-- SHUFFLE STATE
ShuffleState = { }

-- Typical object-oriented Lua pattern
function ShuffleState:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self

    return o
end

-- Called by runtime, transition into another state.
function ShuffleState:stateTransition()
    engineShuffleDeck("drawDeck")
    engineInfoMessage("The two left empty decks go down, while the two right empty decks go up")

    return DelegateState:new()
end

-- Called by runtime, describe the state which is shown in the user interface.
function ShuffleState:stateDescription()
    return "Shuffle cards"
end

DelegateState = { }

function DelegateState:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self

    return o
end

function DelegateState:stateTransition()
    local players = enginePlayers()
    for i,p in ipairs(players) do
        for i=1,7 do
            enginePlayerDeckDrawFrom("drawDeck", p, "handCards")
        end
    end

    current = players[1]

    return NextPlayerState:new()
end

function DelegateState:stateDescription()
    return "Delegate 7 cards to each player"
end

NextPlayerState = { }

function NextPlayerState:new()
    o = { }
    setmetatable(o, self)
    self.__index = self

    return o
end

function NextPlayerState:stateTransition()
    current = engineGetPlayerAfter(current)
    AMOUNT_PLAYER_CARDS = enginePlayerAmountCardsOnDeck(current, "handCards")
    SHOW_PLAYER_DRAW_INFO_MSG = false

    return { SkipPlayerState:new(current), EndGameState:new(current), PlayCardState:new(current) }
end

function NextPlayerState:stateDescription()
    return "Deque next player"
end

EndGameState = { }

function EndGameState:new(player)
    o = {}
    setmetatable(o, self)
    self.__index = self
    self.player = player

    return o
end

function EndGameState:cardsPlayable()
    local cards = engineCardsOnPlayerDeck(self.player, "handCards")
    for i,c in ipairs(cards) do
        if cardCanBePlayed("upDeck1", c) or cardCanBePlayed("upDeck2", c) or
            cardCanBePlayed("downDeck1", c) or cardCanBePlayed("downDeck2", c) then
            return true
        end
    end
    return false
end

function EndGameState:stateValid()
    local players = enginePlayers()
    local finishedPlayers = 0
    for i,p in ipairs(players) do
        if enginePlayerAmountCardsOnDeck(p, "handCards") == 0 then
            finishedPlayers = finishedPlayers + 1
        end
    end

    if finishedPlayers == #players and engineAmountCardsOnDeck("drawDeck") == 0 then
        self.players = players
        return true
    elseif enginePlayerAmountCardsOnDeck(self.player, "handCards") == 0 then
        return false
    else
        return not (canDraw(self.player) or self:cardsPlayable())
    end
end

function EndGameState:statePriority()
    return 3
end

function EndGameState:stateEndsGame()
    return self.players or {}
end

function EndGameState:stateDescription()
    return "End The Game"
end

DrawCardState = { player = { } }

function DrawCardState:new(player)
        o = {}
        setmetatable(o, self)
        self.__index = self
        self.player = player

        return o
end

function DrawCardState:stateExpectedInput()
    if engineAmountCardsOnDeck("drawDeck") ~= 0 then
        engineExpectDeck("draw", {"drawDeck"})
    end
end

function DrawCardState:stateAskPlayer()
    return self.player
end

function DrawCardState:stateValid()
    local canDraw = canDraw(self.player)
    if canDraw and not SHOW_PLAYER_DRAW_INFO_MSG then
        enginePlayerInfoMessage(self.player, "You can now end your turn")
        SHOW_PLAYER_DRAW_INFO_MSG = true
    end
    return canDraw
end

function DrawCardState:stateTransition()
    while engineAmountCardsOnDeck("drawDeck") > 0 and enginePlayerAmountCardsOnDeck(self.player, "handCards") < 7 do
        enginePlayerDeckDrawFromTop("drawDeck", self.player, "handCards")
    end

    return NextPlayerState:new()
end

function DrawCardState:stateDescription()
        return "End Turn"
end

PlayCardState = { player = { } }

function PlayCardState:new(player)
        o = {}
        setmetatable(o, self)
        self.__index = self
        self.player = player

        return o
end

function PlayCardState:stateAskPlayer()
    return self.player
end

function PlayCardState:stateValid()
        return enginePlayerAmountCardsOnDeck(self.player, "handCards") ~= 0
end

function PlayCardState:stateExpectedInput()
    engineExpectCardsFromPlayerDeck("playedCard", self.player, 1, "handCards")
    engineExpectDeck("selectedDeck", {"upDeck1", "upDeck2", "downDeck1", "downDeck2"})
end

function cardCanBePlayed(deckName, card)
    if deckName == "upDeck1" or deckName == "upDeck2" then
        local cardOnTop = enginePeekTop(deckName)
        return cardOnTop == nil
            or tonumber(card["value"]) > tonumber(cardOnTop["value"])
            or tonumber(cardOnTop["value"]) - tonumber(card["value"]) == 10
    elseif deckName == "downDeck1" or deckName == "downDeck2" then
        local cardOnTop = enginePeekTop(deckName)
        return cardOnTop == nil
            or tonumber(card["value"]) < tonumber(cardOnTop["value"])
            or tonumber(card["value"]) - tonumber(cardOnTop["value"]) == 10
    end
    return false
end

function canDraw(player)
        local cards_played = AMOUNT_PLAYER_CARDS - enginePlayerAmountCardsOnDeck(player, "handCards")
        if engineAmountCardsOnDeck("drawDeck") == 0 then
            return cards_played >= 1
        else
            return cards_played >= 2
        end
end

function PlayCardState:stateTransition()
    input = engineInputCards("playedCard")
    card = input[1]
    local player, selectedDeck = engineInputDeck("selectedDeck")

    if cardCanBePlayed(selectedDeck, card) then
            engineDeckPutOnTop(self.player, "handCards", card, selectedDeck)
    elseif selectedDeck == "upDeck1" or selectedDeck == "upDeck2" then
            enginePlayerErrorMessage(self.player, "This deck goes up but your card is too low")
    elseif selectedDeck == "downDeck1" or selectedDeck == "downDeck2" then
            enginePlayerErrorMessage(self.player, "This deck goes down but your card is too high")
    end

    return { EndGameState:new(self.player), DrawCardState:new(self.player), PlayCardState:new(self.player) }
end

function PlayCardState:stateDescription()
        return "Play a card"
end

SkipPlayerState = { }

function SkipPlayerState:new(player)
        o = {}
        setmetatable(o, self)
        self.__index = self
        self.player = player

        return o
end

function SkipPlayerState:statePriority()
    return 2
end

function SkipPlayerState:stateValid()
    return enginePlayerAmountCardsOnDeck(self.player, "handCards") == 0 and engineAmountCardsOnDeck("drawDeck") == 0
end

function SkipPlayerState:stateTransition()
    return NextPlayerState:new(next)
end

function SkipPlayerState:stateDescription()
        return "Skip player"
end