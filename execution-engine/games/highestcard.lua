function addCardsForColor(color)
    for i=2,14 do
        engineAddCard(i, color)
    end
end

TRUMPS = {"Clubs", "Diamonds", "Hearts", "Spades", "No Trump"}

function defineCardSet ()
    addCardsForColor("Clubs")
    addCardsForColor("Diamonds")
    addCardsForColor("Hearts")
    addCardsForColor("Spades")
end

-- Called by runtime, defines global decks.
-- Global means that decks are generally available (e.g. put on the table) and not player-specific (e.g. hand cards)
-- The following runtime functions are available:
-- addMainDeck(name)
-- addDeck(name)
-- Note: addMainDeck receives the entire card set defined in defineCardSet. Thus, it may only be called once.
function defineGlobalDecks ()
    engineAddMainDeck("discardDeck", "stacked", "hidden")
    engineAddDeck("putDeck", "spreaded", "global")
end

-- Called by runtime, defines player-specific decks.
-- The following runtime functions are available:
-- addDeck(name)
-- Note: This function is called for each player once.
function definePlayerDecks ()
    engineAddDeck("handCards", "stacked", "hidden")
end

-- Called by runtime, creates the initial state.
function defineInitState ()
    return ShuffleState:new()
end

-- SHUFFLE STATE
ShuffleState = { }

-- Typical object-oriented Lua pattern
function ShuffleState:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self

    return o
end

-- Called by runtime, transition into another state.
function ShuffleState:stateTransition()
    engineShuffleDeck("discardDeck")

    return DelegateState:new()
end

-- Called by runtime, describe the state which is shown in the user interface.
function ShuffleState:stateDescription()
    return "Shuffle cards"
end

DelegateState = { }

function DelegateState:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self

    return o
end

function DelegateState:stateTransition()
    local players = enginePlayers()
    local perPlayer = 52 / #players

    SCORE = {}

    updateScore()

    for i,p in ipairs(players) do
        for i=1,perPlayer do
            enginePlayerDeckDrawFrom("discardDeck", p, "handCards")
        end
    end

    current = players[1]

    return NextPlayerState:new()
end

function DelegateState:stateDescription()
    return "Delegate cards to each player"
end

NextPlayerState = { }

function NextPlayerState:new(player)
    o = { }
    setmetatable(o, self)
    self.__index = self
    self.override = player

    return o
end

function NextPlayerState:stateTransition()
    current = self.override or engineGetPlayerAfter(current)

    return { EndGameState:new(current), EndTurnState:new(current), PlayCardState:new(current) }
end

function NextPlayerState:stateDescription()
    return "Deque next player"
end

EndGameState = { }

function EndGameState:new(player)
    o = {}
    setmetatable(o, self)
    self.__index = self
    self.player = player

    return o
end

function EndGameState:stateValid()
    local players = enginePlayers()
    local empty = 0
    for i,p in ipairs(players) do
        if enginePlayerAmountCardsOnDeck(p, "handCards") == 0 then
            empty = empty + 1
        end
    end

    return empty == #players
end

function EndGameState:statePriority()
    return 2
end

function EndGameState:stateEndsGame()
    local winners = {}
    local max = 0
    for i,p in ipairs(enginePlayers()) do
       if (SCORE[p] or 0) > max then
            max = SCORE[p]
            winners = { p }
       elseif (SCORE[p] or 0) == max then
            table.insert(winners, p)
       end
    end

    return winners
end

function EndGameState:stateDescription()
    return "End game"
end

PlayCardState = { player = { } }

function PlayCardState:new(player)
        o = {}
        setmetatable(o, self)
        self.__index = self
        self.player = player

        return o
end

function PlayCardState:stateAskPlayer()
    return self.player
end

function PlayCardState:stateValid()
        return enginePlayerAmountCardsOnDeck(self.player, "handCards") ~= 0
end

function PlayCardState:stateExpectedInput()
    engineExpectCardsFromPlayerDeck("playedCard", self.player, 1, "handCards")
    engineExpectDeck("selectedDeck", {"putDeck"})
end

function PlayCardState:stateTransition()
    input = engineInputCards("playedCard")
    card = input[1]
    local player, selectedDeck = engineInputDeck("selectedDeck")

    engineDeckPutOnTop(self.player, "handCards", card, selectedDeck)

    return NextPlayerState:new()
end

function PlayCardState:stateDescription()
        return "Play a card"
end

function updateScore()
    local leader = nil
    local max = 0
    for i,p in ipairs(enginePlayers()) do
        if (SCORE[p] or 0) > max then
            max = SCORE[p]
            leader = p
        elseif SCORE[p] == max then
            leader = nil
        end
    end

    local message = "Score:\n"
    for i,p in ipairs(enginePlayers()) do
        message = message .. "{" .. p .. "}: " .. tostring(SCORE[p] or 0)
        if leader == p then
            message = message .. " " .. "👑" .. "\n"
        else
            message = message .. "\n"
        end
    end

    engineScoreMessage(message)
end

EndTurnState = { }

function EndTurnState:new(player)
        o = {}
        setmetatable(o, self)
        self.__index = self
        self.player = player

        return o
end

function EndTurnState:statePriority()
    return 1
end

function EndTurnState:stateValid()
        return engineAmountCardsOnDeck("putDeck") == #enginePlayers()
end

function EndTurnState:stateTransition()
    local cards = engineCardsOnDeck("putDeck")
    local highest = nil
    local current = self.player
    local winner = nil


    for i,c in ipairs(cards) do
        if highest == nil or tonumber(highest.value) < tonumber(c.value) then
            highest = c
            winner = current
        end
        current = engineGetPlayerAfter(current)
    end

    SCORE[winner] = (SCORE[winner] or 0) + 1

    engineInfoMessage("{" .. winner .. "} hat den Stich gewonnen!")

    updateScore()

    os.execute("sleep 2")
    while engineAmountCardsOnDeck("putDeck") ~= 0 do
        engineDeckDraw("putDeck", "discardDeck")
    end

    return NextPlayerState:new(winner)
end

function EndTurnState:stateDescription()
        return "End turn"
end