TestState = {}

function defineInitState ()
    return TestState:new()
end

function TestState:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self

    return o
end

function TestState:stateValid()
    return false
end

function TestState:stateDescription()
    return "Test State"
end

