function addCardsForColor(color)
    for i=1,5 do
        engineAddCard(i, color)
    end
end

COLORS= { "Herz", "Eichel", "Blatt", "Schelle"}
PLAYED_GAME = ""

function defineCardSet ()
    for i,c in ipairs(COLORS) do
        addCardsForColor(c)
    end
end

function defineGlobalDecks ()
    engineAddMainDeck("drawDeck", "stacked", "hidden")
    engineAddDeck("putDeck", "spreaded", "global")
    engineAddDeck("discardDeck", "stacked", "hidden")
end

function definePlayerDecks ()
    engineAddDeck("handCards", "spreaded", "player")
end

function defineInitState ()
    return StartNextRoundState:new()
end

function getPointsForCard(card)
    if card.value == "1" then
        -- Unter
        return 2
    elseif card.value == "2" then
        -- Ober
        return 3
    elseif card.value == "3" then
        -- King
        return 4
    elseif card.value == "4" then
        -- 10
        return 10
    elseif card.value == "5" then
        -- 11
        return 11
    end
end

function getPointsForGame(game)
    if game == "Bettler" then
        return 4
    elseif game == "Schnapser" then
        return 6
    elseif game == "Gang" then
        return 9
    elseif game == "Bauernschnapser" then
        return 12
    elseif game == "Kontraschnapser" then
        return 12
    elseif game == "Kontrabauernschnapser" then
        return 24
    else
        print("Invalid game", game)
    end
end

function addPointsForPlayer(player, p)
    local team = player % 2
    if ansagePoints[team] ~= nil then
        points[team] = points[team] + ansagePoints[team]
        ansagePoints[team] = nil
    end

    points[team] = points[team] + p
    print("Current points for team", team, points[team])
end

function getPointsForPlayer(player)
    local team = player % 2
    return points[team]
end

function addBummerlsForPlayer(player, p)
    local team = player % 2
    if BUMMERL[team] == nil then
        BUMMERL[team] = p
    else
        BUMMERL[team] = BUMMERL[team] + p
    end
end

function getBummerlsForPlayer(player)
    local team = player % 2
    return BUMMERL[team]
end
function addAnsagePointsForPlayer(player, p)
    local team = player % 2
    if points[team] == 0 then
        ansagePoints[team] = p
    else
        points[team] = points[team] + p
    end
end

function isCardValid(current, attempt, player)
    local cards = engineCardsOnPlayerDeck(player, "handCards")
    local eqColor = current.color == attempt.color
    local wouldStich = (current.color == attempt.color and tonumber(current.value) < tonumber(attempt.value))
        or (current.color ~= ATOUT and attempt.color == ATOUT)

    for i,c in ipairs(cards) do
        if c.color == attempt.color and c.value == attempt.value then
            -- Ignore played card
        elseif not eqColor and c.color == current.color then
            -- Farbzwang
            return false
        elseif not wouldStich and c.color == current.color and tonumber(c.value) > tonumber(current.value) then
            -- Stichzwang mit gleicher farbe
            return false
        elseif not eqColor and not wouldStich and c.color == ATOUT and current.color ~= ATOUT then
            -- Stichzwang mit Atout
            return false
        end
    end
    return true
end

BUMMERL = {}

StartNextRoundState = {}

function StartNextRoundState:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self

    return o
end

function StartNextRoundState:stateTransition()
    local score = "Points:\n"
    for i,p in ipairs(enginePlayers()) do
        if i == 3 then
            break
        end

        local b = 0
        if getBummerlsForPlayer(p) ~= nil then
            b = getBummerlsForPlayer(p)
        end
        local teamPartner = engineGetPlayerAfter(engineGetPlayerAfter(p))
        score = score .. "{" .. p .. "}/{" .. teamPartner .. "}: " .. b .. "\n"
    end
    engineScoreMessage(score)
    return { ShuffleState:new(), EndGameState:new() }
end

function StartNextRoundState:stateDescription()
    return "Start next round"
end

EndGameState = { }

function EndGameState:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self

    return o
end

function EndGameState:statePriority()
    return 2
end

function EndGameState:stateValid()
    local players = enginePlayers()
    for i,p in ipairs(players) do
        if BUMMERL[p % 2] ~= nil and BUMMERL[p % 2] >= 24 then
            return true
        end
    end
    return false
end

function EndGameState:stateEndsGame()
    local players = enginePlayers()
    local winner = ""
    for i,p in ipairs(players) do
        if BUMMERL[p % 2] ~= nil and BUMMERL[p % 2] >= 24 then
            winner = p
            break
        end
    end
    local partner = engineGetPlayerAfter(engineGetPlayerAfter(winner))
    return { winner, partner }
end

function EndGameState:stateDescription()
    return "End game"
end

ShuffleState = { }

-- Typical object-oriented Lua pattern
function ShuffleState:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self

    return o
end

function ShuffleState:stateTransition()
    engineShuffleDeck("drawDeck")
    points = { }
    ansagePoints = {}

    current = BEGINNER or enginePlayers()[engineRandom(4)]
    BEGINNER = engineGetPlayerAfter(current)

    return DelegateState:new(3, DecideAtoutState:new(current))
end

function ShuffleState:stateDescription()
    return "Shuffle cards"
end

DelegateState = { }

function DelegateState:new(amount, succ_state)
    o = {}
    setmetatable(o, self)
    self.__index = self
    self.amount = amount
    self.succ_state = succ_state

    return o
end

function DelegateState:stateTransition()
    local players = enginePlayers()

    for i,p in ipairs(players) do
        points[p % 2] = 0
        for i=1,self.amount do
            enginePlayerDeckDrawFrom("drawDeck", p, "handCards")
        end
    end

    return self.succ_state
end

function DelegateState:stateDescription()
    return "Delegate " .. self.amount .. " cards to each player"
end

DecideAtoutState = { }

function DecideAtoutState:new(player)
    o = {}
    setmetatable(o, self)
    self.__index = self
    self.player = player

    return o
end

function DecideAtoutState:stateAskPlayer()
    return self.player
end

function DecideAtoutState:stateExpectedInput()
    engineExpectCustomSelection("atout", COLORS)
end

function DecideAtoutState:stateTransition()
    local input = engineInputCustom("atout")
    ATOUT = COLORS[input]
    GAME_CALLER = current

    engineInfoMessage(ATOUT, "is Trump")

    return DelegateState:new(2, {ChooseGameState:new(self.player), PassGameState:new(self.player)} )
end

function DecideAtoutState:stateDescription()
    return "Decide atout"
end

ChooseGameState = {}

function ChooseGameState:new(player, current_game)
    o = {}
    setmetatable(o, self)
    self.__index = self
    self.player = player
    self.current_game = current_game
    if player % 2 == current % 2 then
        --Same team
        self.games = {"Bettler", "Schnapser", "Gang", "Bauernschnapser"}
    else
        self.games = {"Bettler", "Gang", "Kontraschnapser", "Kontrabauernschnapser"}
    end

    if current_game ~= nil then
        while #self.games > 0 and getPointsForGame(self.games[1]) <= getPointsForGame(current_game) do
            table.remove(self.games, 1)
        end
    end

    return o
end

function ChooseGameState:stateAskPlayer()
    return self.player
end

function ChooseGameState:stateValid()
    return #self.games ~= 0
end

function ChooseGameState:stateExpectedInput()
    engineExpectCustomSelection("game", self.games)
end

function ChooseGameState:stateTransition()
    local input = engineInputCustom("game")
    local game = self.games[input]
    GAME_CALLER = self.player
    local next = engineGetPlayerAfter(self.player)

    engineInfoMessage("{" .. GAME_CALLER .. "} called a " .. game)

    return {ChooseGameState:new(next, game), PassGameState:new(next, game), InitiateGameState:new(self.player, game) }
end

function ChooseGameState:stateDescription()
    return "Choose type of game"
end

PassGameState = {}

function PassGameState:new(player, current_game)
    o = {}
    setmetatable(o, self)
    self.__index = self
    self.player = player
    self.current_game = current_game

    return o
end

function PassGameState:stateAskPlayer()
    return self.player
end

function PassGameState:stateTransition()
    local next = engineGetPlayerAfter(self.player)

    return { ChooseGameState:new(next, self.current_game), PassGameState:new(next, self.current_game), InitiateGameState:new(self.player, self.current_game) }
end

function PassGameState:stateDescription()
    return "Don't call anything"
end

InitiateGameState = {}

function InitiateGameState:new(player, current_game)
    o = {}
    setmetatable(o, self)
    self.__index = self
    self.player = player
    self.current_game = current_game

    return o
end

function InitiateGameState:stateValid()
    return self.player == current
end

function InitiateGameState:statePriority()
    return 1
end

function InitiateGameState:stateTransition()
    GAME_POINTS = getPointsForGame(self.current_game)

    if self.current_game == "Bettler" then
        engineInfoMessage("Bettler chosen, trump is irrelevant")
        ATOUT = ""
        return NextPlayerBettlerState:new(GAME_CALLER)
    elseif self.current_game == "Schnapser" then
        TURN = 0
        engineInfoMessage("Schnapser chosen")
        return NextPlayerSchnapserState:new(GAME_CALLER)
    elseif self.current_game == "Gang" then
        engineInfoMessage("Gang chosen, trump is irrelevant")
        ATOUT = ""
        return NextPlayerGangState:new(GAME_CALLER)
    elseif self.current_game == "Kontraschnapser" then
        TURN = 0
        engineInfoMessage("Kontraschnapser chosen")
        return NextPlayerSchnapserState:new(current)
    elseif self.current_game == "Bauernschnapser" then
        engineInfoMessage("Bauernschnapser chosen")
        return NextPlayerGangState:new(GAME_CALLER)
    elseif self.current_game == "Kontrabauernschnapser" then
        engineInfoMessage("Kontrabauernschnapser chosen")
        return NextPlayerGangState:new(GAME_CALLER)
    end

    return NextPlayerState:new(self.player)
end

function InitiateGameState:stateDescription()
    return "Initiate selected game"
end

AnsageState = {}

 function AnsageState:new(player)
         o = {}
         setmetatable(o, self)
         self.__index = self
         self.player = player

         return o
 end

 function AnsageState:stateAskPlayer()
     return self.player
 end

 function AnsageState:getAnsageColors()
     local ansageColors = {}
     for i,c in ipairs(COLORS) do
         local cardOber = {}
         cardOber.color = c
         cardOber.value = "2"

         local cardKing = {}
         cardKing.color = c
         cardKing.value = "3"
         if self:containsCard(cardOber) and self:containsCard(cardKing) then
             table.insert(ansageColors, c)
         end
     end
     return ansageColors
 end

 function AnsageState:containsCard(card)
     local cards = engineCardsOnPlayerDeck(self.player, "handCards")
     for i,c in ipairs(cards) do
         if c.color == card.color and c.value == card.value then
             return true
         end
     end
     return false
 end

 function AnsageState:stateValid()
     local isBeginner = engineAmountCardsOnDeck("putDeck") == 0
     self.ansageColors = self:getAnsageColors()
     return isBeginner and #self.ansageColors > 0
 end

 function AnsageState:stateExpectedInput()
     engineExpectCustomSelection("color", self.ansageColors)
 end

 function AnsageState:stateTransition()
     local input = engineInputCustom("color")
     local card = {}
     card.value = "2"
     card.color = self.ansageColors[input]

     local p = 20
     if card.color == ATOUT then
         p = 40
     end

     engineInfoMessage("{" .. self.player .. "} has made an ansage and receives " .. p .. " points")

     addAnsagePointsForPlayer(self.player, p)
     engineDeckPutOnTop(self.player, "handCards", card, "putDeck")
     return NextPlayerState:new(engineGetPlayerAfter(self.player))
 end

 function AnsageState:stateDescription()
         return "Ansage"
 end

EndRoundState = { }

function EndRoundState:new(winner, cond, winnerCb)
    o = {}
    setmetatable(o, self)
    self.__index = self
    self.cond = cond
    self.winner = winner
    self.winnerCb = winnerCb

    return o
end

function EndRoundState:stateValid()
    return self.cond(self.winner)
end

function EndRoundState:statePriority()
    return 2
end

function EndRoundState:stateTransition()
    local winner = self.winnerCb(self.winner)
    if GAME_POINTS == nil then
        local loser = engineGetPlayerAfter(winner)
        local p = 0
        if getPointsForPlayer(loser) >= 33 then
            p = 1
        elseif getPointsForPlayer(loser) > 0 then
            p = 2
        else
            p = 3
        end
        addBummerlsForPlayer(winner, p)
    else
        addBummerlsForPlayer(winner, GAME_POINTS)
    end

    while engineAmountCardsOnDeck("discardDeck") ~= 0 do
        engineDeckDraw("discardDeck", "drawDeck")
    end
    for i,p in ipairs(enginePlayers()) do
        enginePlayerDeckTransfer(p, "handCards", "drawDeck")
    end

    return StartNextRoundState:new()
end

function EndRoundState:stateDescription()
    return "End Round"
end

PlayCardState = { }

function PlayCardState:new(player, callback, follow_up)
        o = {}
        setmetatable(o, self)
        self.__index = self
        self.player = player
        self.callback = callback
        self.follow_up = follow_up

        return o
end

function PlayCardState:stateAskPlayer()
    return self.player
end

function PlayCardState:stateExpectedInput()
    engineExpectDeck("put", {"putDeck"})
    engineExpectCardsFromPlayerDeck("playedCard", self.player, 1, "handCards")
end

function PlayCardState:stateTransition()
    local input = engineInputCards("playedCard")
    local card = input[1]
    local deckBottom = enginePeekBottom("putDeck")

    if deckBottom == nil then
        engineDeckPutOnTop(self.player, "handCards", card, "putDeck")
    else
        if not isCardValid(deckBottom, card, self.player) then
            enginePlayerErrorMessage(self.player, "Same color must be played and stich must be taken!")
            return self.follow_up(self.player)
        end
        engineDeckPutOnTop(self.player, "handCards", card, "putDeck")

        if engineAmountCardsOnDeck("putDeck") == 4 then
            local winner = engineGetPlayerAfter(self.player)
            local current = self.player
            local bestCard = deckBottom
            local addedPoints = 0
            for i,c in ipairs(engineCardsOnDeck("putDeck")) do
                addedPoints = addedPoints + getPointsForCard(c)
                current = engineGetPlayerAfter(current)
                -- Either both cards have the same color, then the one with the higher value wins
                -- or my card is atout and the other is not.
                if (bestCard.color == c.color and tonumber(bestCard.value) < tonumber(c.value))
                    or (bestCard.color ~= ATOUT and c.color == ATOUT) then
                    winner = current
                    bestCard = c
                end
            end

            return CleanupState:new(self.callback(winner, addedPoints))
        end
    end
    return self.follow_up(engineGetPlayerAfter(self.player))
end

function PlayCardState:stateDescription()
    return "Play a card"
end

NextPlayerState = { }

function NextPlayerState:new(player)
    o = { }
    setmetatable(o, self)
    self.__index = self
    self.player = player

    return o
end

function NextPlayerState:stateTransition()
    local end_game_cond = function()
        local players = enginePlayers()
        local emptyDecks = 0
        for i,p in ipairs(players) do
            if enginePlayerAmountCardsOnDeck(p, "handCards") == 0 then
                emptyDecks = emptyDecks + 1
            end
            if points[p % 2] >= 66 then
                return true
            end
        end

        return emptyDecks == 4
    end

    local winnerCb = function(winner)
            return winner
        end

    local cb = function(winner, addedPoints)
            addPointsForPlayer(winner, addedPoints)
            local partner = engineGetPlayerAfter(engineGetPlayerAfter(winner))
            engineInfoMessage("{" .. winner .. "} and {" .. partner .. "} won and receive " .. addedPoints .. " points")

            return {EndRoundState:new(winner, end_game_cond, winnerCb), NextPlayerState:new(winner) }
            end
    local f = function(p)
                return NextPlayerState:new(p)
             end

    return { PlayCardState:new(self.player, cb, f), AnsageState:new(self.player) }
end

function NextPlayerState:stateDescription()
    return "Deque next player"
end

NextPlayerBettlerState = { }

function NextPlayerBettlerState:new(player)
    o = { }
    setmetatable(o, self)
    self.__index = self
    self.player = player

    return o
end

function NextPlayerBettlerState:stateTransition()
    local end_game_cond = function(winner)
        -- Either the first player has made a stich or all cards have been played
        return winner == GAME_CALLER
            or engineAmountCardsOnDeck("discardDeck") == 20
        end

    local winnerCb = function(winner)
            if winner == GAME_CALLER then
                engineInfoMessage("{" .. winner .. "} got a trick and lost the round")
                return engineGetPlayerAfter(winner)
            else
                engineInfoMessage("{" .. winner .. "} won the Bettler!")
                return winner
            end
        end

    local cb = function(winner)
                return { EndRoundState:new(winner, end_game_cond, winnerCb), NextPlayerBettlerState:new(winner)}
                end
    local f = function(p)
                return NextPlayerBettlerState:new(p)
             end


    local player = self.player
    return PlayCardState:new(player, cb, f)
end

function NextPlayerBettlerState:stateDescription()
    return "Deque next player for Bettler"
end

NextPlayerGangState = { }

function NextPlayerGangState:new(player)
    o = { }
    setmetatable(o, self)
    self.__index = self
    self.player = player

    return o
end

function NextPlayerGangState:stateTransition()
    local endGameCond = function(winner)
        -- Either the player has not made a stich or the game is over
        return winner ~= GAME_CALLER
            or engineAmountCardsOnDeck("discardDeck") == 20
        end

    local winnerCb = function(winner)
                        if winner == GAME_CALLER then
                            engineInfoMessage("{" .. winner .. "} got all tricks!")
                            return winner
                        else
                            engineInfoMessage("{" .. winner .. "} did not get a trick and lost.")
                            return engineGetPlayerAfter(winner)
                        end
                    end


    local cb = function(winner)
                return { EndRoundState:new(winner, endGameCond, winnerCb), NextPlayerGangState:new(winner) }
                end
    local f = function(p)
                return NextPlayerGangState:new(p)
             end

    local player = self.player
    return PlayCardState:new(player, cb, f)
end

function NextPlayerGangState:stateDescription()
    return "Deque next player for Gang"
end

NextPlayerSchnapserState = { }

function NextPlayerSchnapserState:new(player)
    o = { }
    setmetatable(o, self)
    self.__index = self
    self.player = player

    return o
end

function NextPlayerSchnapserState:stateTransition()
    local end_game_cond = function(winner)
        return TURN == 3 or winner ~= GAME_CALLER
            or points[winner % 2] >= 66
        end

    local winnerCb = function(winner)
                    local partner = engineGetPlayerAfter(engineGetPlayerAfter(winner))
                    if winner == GAME_CALLER and points[winner % 2] >= 66 then
                        engineInfoMessage("{" .. winner .. "} and {" .. partner .. "} have more than 66 points")
                        return winner
                    else
                        engineInfoMessage("{" .. winner .. "} and {" .. partner .. "} did not reach more than 66 points")
                        return engineGetPlayerAfter(winner)
                    end
                end

    local cb = function(winner, addedPoints)
            TURN = TURN + 1
            addPointsForPlayer(winner, addedPoints)
            return { EndRoundState:new(winner, end_game_cond, winnerCb), NextPlayerSchnapserState:new(winner) }
            end
    local f = function(p)
                return NextPlayerSchnapserState:new(p)
             end
    local player = self.player
    return { PlayCardState:new(player, cb, f), AnsageState:new(player) }
end

function NextPlayerSchnapserState:stateDescription()
    return "Deque next player for Schnapser"
end

CleanupState = {}

function CleanupState:new(followUp)
        o = {}
        setmetatable(o, self)
        self.__index = self
        self.followUp = followUp

        return o
end

function CleanupState:stateTransition()
    --Workaround: Give frontend some time to show cards
    os.execute("sleep 2")

    -- Remove cards from table
    while engineAmountCardsOnDeck("putDeck") ~= 0 do
        engineDeckDraw("putDeck", "discardDeck")
    end

    return self.followUp
end

function CleanupState:stateDescription()
        return "Cleanup"
end
