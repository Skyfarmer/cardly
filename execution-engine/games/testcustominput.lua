function defineCardSet()
    engineAddCard(0, "Red")
    engineAddCard(1, "Red")
    engineAddCard(2, "Red")
end
function definePlayerConfiguration()
end

function defineGlobalDecks()
    engineAddMainDeck("main", "stacked", "toponly")
end

function definePlayerDecks()
end

TestState = {}

function defineInitState ()
    return TestState:new()
end

function TestState:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self
    self.amount = {"First", "Second", "Third"}
    self.trump = {"Apple", "Banana", "Cheers"}
    return o
end

function TestState:stateDescription()
    return "Test State"
end

function TestState:stateAskPlayer()
    return self.player
end

function TestState:stateValid()
    self.player = enginePlayers()[1]
    return true
end

function TestState:stateExpectedInput()

    engineExpectCustomSelection("amount", self.amount)

    engineExpectCustomSelection("trump", self.trump)
end

function TestState:stateTransition()
    local input = engineInputCustom("amount")
    local input2 = engineInputCustom("trump")
    print(self.amount[input], "and", self.trump[input2], "selected")

    return TestState:new()
end

