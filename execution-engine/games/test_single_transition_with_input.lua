TestState = {}

function defineInitState ()
    return TestState:new()
end

function TestState:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self

    return o
end

function TestState:stateDescription()
    return "Test State"
end

function TestState:stateTransition()
    input = engineInputCards("input")
    card = input[1]

    if card["color"] == "red" and card["value"] == "2" then
        return TestState:new()
    else
        return nil
    end
end
