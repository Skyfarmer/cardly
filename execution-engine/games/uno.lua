-- User defined method - not called by runtime
function addCardsForColor(color)
    for i=0,12 do
        engineAddCard(i, color)
        if i ~= 0 then
            engineAddCard(i, color)
        end
    end
end

COLORS = {"Red", "Green", "Yellow", "Blue"}

-- Called by runtime, defines card set.
-- The following runtime functions are available:
-- engineAddCard(color, value)
function defineCardSet ()
    addCardsForColor("Red")
    addCardsForColor("Green")
    addCardsForColor("Blue")
    addCardsForColor("Yellow")

    engineAddCard(20, "wild")
    engineAddCard(20, "wild")
    engineAddCard(21, "wild")
    engineAddCard(21, "wild")
end

-- Called by runtime, defines global decks.
-- Global means that decks are generally available (e.g. put on the table) and not player-specific (e.g. hand cards)
-- The following runtime functions are available:
-- addMainDeck(name)
-- addDeck(name)
-- Note: addMainDeck receives the entire card set defined in defineCardSet. Thus, it may only be called once.
function defineGlobalDecks ()
    engineAddMainDeck("drawDeck", "stacked", "hidden")
    engineAddDeck("putDeck", "stacked", "toponly")
end

-- Called by runtime, defines player-specific decks.
-- The following runtime functions are available:
-- addDeck(name)
-- Note: This function is called for each player once.
function definePlayerDecks ()
    engineAddDeck("handCards", "spreaded", "player")
end

-- Called by runtime, creates the initial state.
function defineInitState ()
    return ShuffleState:new()
end

function getNextPlayer()
    if reversed then
        return engineGetPlayerBefore(current)
    else
        return engineGetPlayerAfter(current)
    end
end

function reshuffleIfNecessary()
    if engineCardsOnDeck("drawDeck") == 0 then
        while engineCardsOnDeck("putDeck") ~= 1 do
            -- Third parameter decides how a card is drawn. Defaults to "top".
            -- Draw a card from "putDeck" from the bottom and put it on top of the deck "drawDeck"
            engineDeckDraw("putDeck", "drawDeck", "bottom")
        end

        engineShuffleDeck("drawDeck")
    end
end

-- SHUFFLE STATE
ShuffleState = { }

-- Typical object-oriented Lua pattern
function ShuffleState:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self

    return o
end

-- Called by runtime, transition into another state.
function ShuffleState:stateTransition()
    engineShuffleDeck("drawDeck")

    return DelegateState:new()
end

-- Called by runtime, describe the state which is shown in the user interface.
function ShuffleState:stateDescription()
    return "Shuffle cards"
end

DelegateState = { }

function DelegateState:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self

    return o
end

function DelegateState:stateTransition()
    local players = enginePlayers()
    for i,p in ipairs(players) do
        for i=1,7 do
            enginePlayerDeckDrawFrom("drawDeck", p, "handCards")
        end
    end

    engineDeckDraw("drawDeck", "putDeck")
    current = players[engineRandom(2)]
    reversed = false
    local deckTop = enginePeekTop("putDeck")
    if deckTop["color"] == "wild" then
        COLOR_OVERRIDE = COLORS[engineRandom(4)]
    end

    return NextPlayerState:new()
end

function DelegateState:stateDescription()
    return "Delegate 7 cards to each player"
end

NextPlayerState = { }

function NextPlayerState:new()
    o = { }
    setmetatable(o, self)
    self.__index = self

    return o
end

function NextPlayerState:stateTransition()
    current = getNextPlayer(current)

    return { EndGameState:new(), DrawCardState:new(current), PlayCardState:new(current) }
end

function NextPlayerState:stateDescription()
    return "Deque next player"
end

EndGameState = { }

function EndGameState:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self

    return o
end

function EndGameState:stateValid()
    local players = enginePlayers()
    for i,p in ipairs(players) do
        if enginePlayerAmountCardsOnDeck(p, "handCards") == 0 then
            self.winner = p
            return true
        end
    end

    return false
end

function EndGameState:statePriority()
    return 2
end

function EndGameState:stateEndsGame()
    return self.winner
end

function EndGameState:stateDescription()
    return "End Uno"
end

DrawCardState = { player = { } }

function DrawCardState:new(player)
        o = {}
        setmetatable(o, self)
        self.__index = self
        self.player = player

        return o
end

function DrawCardState:stateAskPlayer()
    return self.player
end

function DrawCardState:stateExpectedInput()
    engineExpectDeck("draw", {"drawDeck"})
end

function DrawCardState:stateValid()
    return engineAmountCardsOnDeck("drawDeck") > 0
end

function DrawCardState:stateTransition()
    local player, selectedDeck = engineInputDeck("draw")

    reshuffleIfNecessary()

    enginePlayerDeckDrawFromTop("drawDeck", self.player, "handCards")

    return {PassState:new(self.player), PlayCardState:new(self.player)}
end

function DrawCardState:stateDescription()
        return "Draw a card from the main deck"
end

PlayCardState = { player = { } }

function PlayCardState:new(player)
        o = {}
        setmetatable(o, self)
        self.__index = self
        self.player = player

        return o
end

function PlayCardState:stateAskPlayer()
    return self.player
end

function PlayCardState:stateExpectedInput()
    engineExpectCardsFromPlayerDeck("playedCard", self.player, 1, "handCards")
end

function PlayCardState:stateTransition()
    input = engineInputCards("playedCard")
    card = input[1]
    deckTop = enginePeekTop("putDeck")

    local deckTopColor = nil
    if COLOR_OVERRIDE == nil then
        deckTopColor = deckTop["color"]
    else
        deckTopColor = COLOR_OVERRIDE
    end
    print(deckTopColor)

    if card["color"] == "wild" then
        COLOR_OVERRIDE = nil
        engineDeckPutOnTop(self.player, "handCards", card, "putDeck")
        -- 20 represents choose color
        if card["value"] == "20" then
            return ChooseColorState:new(self.player, NextPlayerState:new())
        else
            -- Must be a draw 4 otherwise
            current = getNextPlayer(self.player)
            return ChooseColorState:new(self.player, Draw2State:new(current, 4))
        end
    elseif card["value"] == deckTop["value"] or card["color"] == deckTopColor then
        COLOR_OVERRIDE = nil
         engineDeckPutOnTop(self.player, "handCards", card, "putDeck")
         -- 10 equals to +2
         if card["value"] == "10" then
            current = getNextPlayer(self.player)
            return Draw2State:new(current, 2)
         -- Skip card
         elseif card["value"] == "11" then
            current = getNextPlayer(self.player)
         -- Reverse card
         elseif card["value"] == "12" then
            reversed = not reversed
         end
         return NextPlayerState:new()
    else
        enginePlayerErrorMessage(self.player, "Your card must match the color or value of the deck!")
        return { DrawCardState:new(self.player), PlayCardState:new(self.player) }
    end
end

function PlayCardState:stateDescription()
        return "Play a card"
end


Draw2State = {}

function Draw2State:new(player, amount)
        o = {}
        setmetatable(o, self)
        self.__index = self
        self.player = player
        self.amount = amount

        return o
end

function Draw2State:stateTransition()
    reshuffleIfNecessary()
    enginePlayerDeckDrawFromTop("drawDeck", self.player, "handCards")

    if self.amount == 1 then
        return NextPlayerState:new()
    else
        return Draw2State:new(current, self.amount - 1)
    end
end

function Draw2State:stateDescription()
        return "Forcefully draw a card"
end

ChooseColorState = {}

function ChooseColorState:new(player, follow_up_state)
        o = {}
        setmetatable(o, self)
        self.__index = self
        self.player = player
        self.follow_up_state = follow_up_state

        return o
end

function ChooseColorState:stateAskPlayer()
    return self.player
end

function ChooseColorState:stateExpectedInput()
    engineExpectCustomSelection("color", COLORS)
end

function ChooseColorState:stateTransition()
    local color = engineInputCustom("color")
    color = COLORS[color]
    engineInfoMessage(color, "is the new color")

    COLOR_OVERRIDE = color
    return self.follow_up_state
end

function ChooseColorState:stateDescription()
        return "Choose a color"
end

PassState = {}

function PassState:new(player, follow_up_state)
        o = {}
        setmetatable(o, self)
        self.__index = self
        self.player = player

        return o
end

function PassState:stateAskPlayer()
    return self.player
end

function PassState:stateTransition()
    return NextPlayerState:new()
end

function PassState:stateDescription()
        return "Pass to the next player"
end