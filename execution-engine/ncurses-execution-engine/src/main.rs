use anyhow::{anyhow, Context};
use crossbeam_channel::Receiver;
use cursive::align::HAlign;
use cursive::reexports::crossbeam_channel::Sender;
use cursive::reexports::log::{error, info};
use cursive::traits::{Nameable, Resizable, Scrollable};
use cursive::views::{Dialog, DummyView, LinearLayout, RadioGroup, SelectView, TextView};
use cursive_flexi_logger_view::FlexiLoggerView;
use flexi_logger::Logger;
use lib_execution_engine::components::event::Event;
use lib_execution_engine::components::{Card, Deck, DeckLayout};
use lib_execution_engine::lua_game::LuaGame;
use lib_execution_engine::state_machine::states::{GlobalState, PlayerState};

use crate::HAlign::Center;
use lib_execution_engine::state_machine::traits::{
    Input, InputDescription, Interface, StateIndex, SuccessorIndexWithInput,
    SuccessorStateWithInputDescription,
};
use lib_execution_engine::state_machine::{PlayerMove, StateMachine};
use std::borrow::Borrow;
use std::cell::RefCell;
use std::collections::HashMap;
use std::fmt::Write;
use std::sync::{Arc, Mutex};

type Model = Arc<Mutex<ModelData>>;

struct ModelData {
    cb_sink: cursive::CbSink,
    score_messages: HashMap<usize, String>,
}

impl ModelData {
    fn new(cb_sink: cursive::CbSink) -> Self {
        ModelData {
            cb_sink,
            score_messages: HashMap::new(),
        }
    }
}

// We can't run our logic inside the closures modifying the user interface
// because that would require moving the global state into the closure
// which does not work due to lifetime issues.
// Therefore, we process the expected input first and transform it into a presentable
// state.
enum InputUi {
    CardsFromPlayerDeck(String, Vec<Card>),
    Deck(String, Vec<usize>),
    Custom(String, Vec<String>),
}

struct NcursesInterface {
    model: Model,
}

impl NcursesInterface {
    /// Create new interface with the given model.
    fn new(model: Model) -> Self {
        NcursesInterface { model }
    }

    /// Write a card into a buffer, representable for our user interface.
    fn write_card(card: &Card, buffer: &mut impl Write) {
        if card.restrictions().map_or(true, |r| !r.is_empty()) {
            write!(buffer, "{} ({})", card.value(), card.color()).unwrap();
        } else {
            write!(buffer, "Hidden").unwrap();
        }
    }

    /// Turn a card into a `String`, representable for our user interface.
    fn card_to_string(card: &Card) -> String {
        let mut content = String::new();
        Self::write_card(card, &mut content);
        content
    }

    /// Turn multiple decks into a `String`, representable for our user interface.
    fn decks_to_string<T: Borrow<RefCell<Deck>>>(decks: &[T]) -> String {
        let mut content = String::new();
        for deck in decks {
            let deck = RefCell::borrow(deck.borrow());
            match deck.layout() {
                DeckLayout::Stacked => {
                    let output = deck
                        .peek_top()
                        .map_or("Empty".to_string(), Self::card_to_string);
                    writeln!(content, "{}", output).unwrap()
                }
                DeckLayout::Spreaded => {
                    for c in deck.cards_iter() {
                        Self::write_card(c, &mut content);
                        content.push(' ');
                    }
                    writeln!(content).unwrap();
                }
            }
        }
        content
    }

    fn update_global_state<Custom>(model: &ModelData, state: &GlobalState<Custom>) {
        let content = Self::decks_to_string(state.decks());

        model
            .cb_sink
            .send(Box::new(|c| {
                c.call_on_name("table", |t: &mut TextView| {
                    t.set_content(content);
                });
            }))
            .unwrap();

        for p in state.players() {
            Self::update_player_state(model, p);
        }
    }

    fn update_player_state(model: &ModelData, state: &PlayerState) {
        let mut content = Self::decks_to_string(state.decks());
        if let Some(msg) = model.score_messages.get(&state.id()) {
            content.push('\n');
            content.push_str(msg);
        }
        let player_id = state.id();

        model
            .cb_sink
            .send(Box::new(move |c| {
                c.call_on_name(&format!("{}_text", player_id), |t: &mut TextView| {
                    t.set_content(content);
                });
            }))
            .unwrap();
    }

    fn handle_input<Custom>(
        global_state: &GlobalState<Custom>,
        requested_input: HashMap<String, InputDescription>,
    ) -> anyhow::Result<Vec<InputUi>> {
        requested_input
            .into_iter()
            .map(|(name, descriptor)| match descriptor {
                InputDescription::CardsFromPlayerDeck {
                    player,
                    amount: _,
                    deck_id,
                } => {
                    let d = global_state
                        .player_by_id(player)
                        .ok_or_else(|| anyhow!("Player {} not found", player))?
                        .get_deck(deck_id)
                        .ok_or_else(|| anyhow!("Deck {} not found for player {}", deck_id, player))?
                        .borrow()
                        .cards_iter()
                        .cloned()
                        .collect();
                    Ok(InputUi::CardsFromPlayerDeck(name, d))
                }
                InputDescription::Deck(valid) => {
                    let mut decks: Vec<_> = global_state
                        .decks()
                        .iter()
                        .filter_map(|d| {
                            if valid.contains(&d.borrow().id()) {
                                Some(d.borrow().id())
                            } else {
                                None
                            }
                        })
                        .collect();
                    for player in global_state.players() {
                        decks.extend(player.decks().iter().filter_map(|d| {
                            if valid.contains(&d.borrow().id()) {
                                Some(d.borrow().id())
                            } else {
                                None
                            }
                        }));
                    }
                    Ok(InputUi::Deck(name, decks))
                }
                InputDescription::Custom(options) => Ok(InputUi::Custom(name, options)),
            })
            .collect()
    }

    fn ask_for_input<Custom>(
        model: &mut ModelData,
        requested_input: HashMap<String, InputDescription>,
        global_state: &GlobalState<Custom>,
        player_state: &PlayerState,
    ) -> anyhow::Result<PlayerMove> {
        let input_elements = Self::handle_input(global_state, requested_input)?;

        // We extract the id first because otherwise we would need to move the entire player state
        // into the closures below.
        let player = player_state.id();
        // Because our input runs on a different thread, we retrieve the selection via a channel.
        let (tx, rx): (Sender<_>, Receiver<_>) = crossbeam_channel::unbounded();

        model
            .cb_sink
            .send(Box::new(move |c| {
                let mut layout = LinearLayout::horizontal();
                let mut groups = Vec::new();
                for input in &input_elements {
                    match input {
                        InputUi::CardsFromPlayerDeck(name, cards) => {
                            let mut group = RadioGroup::new();
                            let mut group_layout = LinearLayout::vertical();
                            for (i, card) in cards.iter().enumerate() {
                                group_layout.add_child(
                                    group.button((name.clone(), i), Self::card_to_string(card)),
                                );
                            }
                            layout.add_child(group_layout);
                            groups.push(group);
                        }
                        InputUi::Custom(name, options) => {
                            let mut group = RadioGroup::new();
                            let mut group_layout = LinearLayout::vertical();
                            for (i, o) in options.iter().enumerate() {
                                group_layout.add_child(group.button((name.clone(), i), o));
                            }

                            layout.add_child(group_layout);
                            groups.push(group);
                        }
                        InputUi::Deck(name, decks) => {
                            let mut group = RadioGroup::new();
                            let mut group_layout = LinearLayout::vertical();
                            for deck in decks {
                                group_layout.add_child(
                                    group.button((name.clone(), *deck), deck.to_string()),
                                );
                            }
                            layout.add_child(group_layout);
                            groups.push(group);
                        }
                    }
                    layout.add_child(DummyView);
                }
                c.add_layer(
                    Dialog::around(layout)
                        .title(format!("Player {}: Make selection(s):", player))
                        .button("Confirm", move |c| {
                            for (group, element) in groups.iter().zip(input_elements.iter()) {
                                let selection = group.selection();
                                match element {
                                    InputUi::CardsFromPlayerDeck(_, cards) => {
                                        let card = vec![cards[selection.1].clone()];
                                        tx.send((selection.0.clone(), Input::Cards(card))).unwrap();
                                    }
                                    InputUi::Custom(_, _) => {
                                        tx.send((
                                            selection.0.clone(),
                                            Input::CustomSelection(selection.1),
                                        ))
                                        .unwrap();
                                    }
                                    InputUi::Deck(_, _) => {
                                        tx.send((selection.0.clone(), Input::Deck(selection.1)))
                                            .unwrap();
                                    }
                                }
                            }
                            c.pop_layer();
                        }),
                );
            }))
            .unwrap();

        let mut user_input = HashMap::new();
        while let Ok((name, input)) = rx.recv() {
            user_input.insert(name, input).map_or((), |_| ());
        }

        Ok(user_input)
    }

    fn handle_state_selection(
        state_names: Vec<String>,
        player_state: &PlayerState,
        model: &ModelData,
    ) -> anyhow::Result<usize> {
        // We extract the id first because otherwise we would need to move the entire player state
        // into the closures below.
        let player_id = player_state.id();

        // Because our input runs on a different thread, we retrieve the selection via a channel.
        let (tx, rx): (Sender<StateIndex>, Receiver<StateIndex>) = crossbeam_channel::unbounded();

        model
            .cb_sink
            .send(Box::new(move |c| {
                let mut select = SelectView::new().autojump();
                for (i, s) in state_names.iter().enumerate() {
                    select.add_item(s, i);
                }

                select.set_on_submit(move |s, choice| {
                    // If submitted, remove this view and send the selected option.
                    s.pop_layer();
                    tx.send(*choice).unwrap();
                });

                c.add_layer(
                    Dialog::around(select.scrollable())
                        .title(format!("Player {}: Please choose an action:", player_id)),
                );
            }))
            .unwrap();

        // Retrieve result
        rx.recv()
            .context("Could not retrieve selected successor state")
    }
}

impl<Custom> Interface<Custom> for NcursesInterface {
    fn ask_for_option(
        &mut self,
        global_state: &GlobalState<Custom>,
        player_state: &PlayerState,
        states: &[SuccessorStateWithInputDescription],
    ) -> anyhow::Result<SuccessorIndexWithInput> {
        let mut model = self.model.lock().unwrap();
        // Update the user interface with the latest states
        Self::update_global_state(&model, global_state);

        // All possible successor states
        let state_names = states.iter().map(|(_, s)| s.clone()).collect::<Vec<_>>();

        let idx = if state_names.len() == 1 {
            0
        } else {
            Self::handle_state_selection(state_names, player_state, &model)?
        };

        if let Some(input_description) = states[idx].0.as_ref() {
            let input = Self::ask_for_input(
                &mut model,
                input_description.clone(),
                global_state,
                player_state,
            )?;
            Ok((Some(input), idx))
        } else {
            Ok((None, idx))
        }
    }

    fn get_num_players(&mut self) -> usize {
        let model = self.model.lock().unwrap();

        let (tx, rx): (Sender<usize>, Receiver<usize>) = crossbeam_channel::unbounded();

        model
            .cb_sink
            .send(Box::new(|c| {
                let mut select = SelectView::new().autojump();
                for i in 1..=4 {
                    select.add_item(i.to_string(), i);
                }
                select.set_on_submit(move |s, choice| {
                    // If submitted, remove this view and send the selected option.
                    s.pop_layer();
                    tx.send(*choice).unwrap();
                });
                c.add_layer(Dialog::around(select).title("Select the number of players"))
            }))
            .unwrap();

        // Retrieve result
        rx.recv().expect("Could not retrieve amount of players")
    }

    fn update_ui(&mut self, state: &GlobalState<Custom>, events: Vec<Event>) -> anyhow::Result<()> {
        let mut model = self.model.lock().unwrap();
        for e in events {
            match e {
                Event::ScoreMessage { player_id, message } => {
                    model.score_messages.insert(player_id, message);
                }
                Event::InfoMessage { player_id, message } => info!(
                    "Game sends info message to player {}: {}",
                    player_id, message
                ),
                Event::ErrorMessage { player_id, message } => error!(
                    "Game sends error message to player {}: {}",
                    player_id, message
                ),
                Event::GameOver { winners } => {
                    let mut message = "Winners:\n".to_string();
                    for w in winners {
                        message.push_str(&w.to_string());
                        message.push('\n');
                    }
                    model
                        .cb_sink
                        .send(Box::new(|c| {
                            c.add_layer(
                                Dialog::around(TextView::new(message).h_align(Center))
                                    .title("Game Over")
                                    .button("Quit", |s| s.quit()),
                            )
                        }))
                        .unwrap();
                }
                _ => (),
            }
        }

        Self::update_global_state(&model, state);

        Ok(())
    }

    fn init(&mut self, init_state: &GlobalState<Custom>) {
        let model = self.model.lock().unwrap();
        Self::update_global_state(&model, init_state);

        for (i, p) in init_state.players().iter().enumerate() {
            let content = Self::decks_to_string(p.decks());

            model
                .cb_sink
                .send(Box::new(move |c| {
                    c.call_on_name("players", |v: &mut LinearLayout| {
                        v.add_child(
                            Dialog::around(
                                LinearLayout::vertical()
                                    .child(TextView::new(content).with_name(format!("{}_text", i)))
                                    .with_name(i.to_string()),
                            )
                            .h_align(HAlign::Center)
                            .title(format!("Player {}", i))
                            .fixed_width(50),
                        );
                    });
                }))
                .unwrap();
        }
    }
}

fn start_game(path: &str, model: Model) {
    let game = LuaGame::load(path).unwrap();

    std::thread::spawn(move || {
        let machine = StateMachine::new(game);

        match machine.run(NcursesInterface::new(Arc::clone(&model))) {
            Ok(()) => {
                info!("Exiting game")
            }
            Err(e) => {
                error!("{}", e);
                model
                    .lock()
                    .unwrap()
                    .cb_sink
                    .send(Box::new(|c| {
                        c.toggle_debug_console();
                    }))
                    .unwrap();
            }
        }
    });
}

fn main() {
    let mut siv = cursive::default();

    Logger::try_with_env_or_str("info,lib=debug")
        .expect("Could not create logger from environment")
        .log_to_writer(cursive_flexi_logger_view::cursive_flexi_logger(&siv))
        .format(flexi_logger::colored_with_thread)
        .start()
        .expect("Failed to initialize logger");

    let root = Dialog::around(
        LinearLayout::vertical()
            .child(LinearLayout::horizontal().with_name("players"))
            .child(
                LinearLayout::horizontal()
                    .child(
                        Dialog::around(TextView::empty().with_name("table"))
                            .h_align(HAlign::Center)
                            .title("Table view"),
                    )
                    .child(Dialog::around(FlexiLoggerView::scrollable()).h_align(HAlign::Center)),
            ),
    )
    .h_align(HAlign::Center);

    siv.add_fullscreen_layer(root);

    let model = Arc::new(Mutex::new(ModelData::new(siv.cb_sink().clone())));

    let args: Vec<_> = std::env::args().collect();
    start_game(&args[1], model);

    siv.run();
}
