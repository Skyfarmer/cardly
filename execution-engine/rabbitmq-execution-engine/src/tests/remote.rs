use crate::messaging::{DeliveryMessagingResponse, MockMessaging};
use crate::remote::*;
use crate::requests::MessageRequest;
use crate::responses::MessageResponse;
use crate::CloudEvent;
use lib_execution_engine::components::{Card, Deck, DeckLayout};
use lib_execution_engine::state_machine::states::{GlobalState, PlayerState};
use lib_execution_engine::state_machine::traits::{
    Input, InputDescription, Interface, State, UserOption,
};
use lib_execution_engine::state_machine::{PlayerMove, StateTransition};
use mockall::predicate;
use std::collections::{HashMap, VecDeque};

struct DummyState;

impl<'a> State<'a> for DummyState {
    type Custom = ();

    fn transition(
        self: Box<Self>,
        _state: &mut GlobalState<Self::Custom>,
        _input: Option<PlayerMove>,
    ) -> StateTransition<'a, Self::Custom> {
        StateTransition::Single(Box::new(Self))
    }
}

impl UserOption for DummyState {
    fn description(&self) -> String {
        "test".to_string()
    }
}

macro_rules! match_message_request {
    ( $x:pat_param, $y:expr ) => {{
        {
            |x: &[u8]| {
                let content = std::str::from_utf8(x).unwrap();
                let message: CloudEvent<MessageRequest> = serde_json::from_str(&content).unwrap();

                if let $x = message.value {
                    return $y;
                }

                false
            }
        }
    }};
}

fn setup(mock: MockMessaging) -> RemoteInterface<MockMessaging> {
    RemoteInterface::new("session_id".to_string(), mock, Default::default())
}

#[test]
fn should_send_messages_when_ask_for_input() {
    let exchange = "requests".to_string();
    let routing_key = "session.session_id.player.0".to_string();

    let mut mock = MockMessaging::new();
    mock.expect_publish()
        .with(
            predicate::eq(exchange),
            predicate::eq(routing_key),
            predicate::function(match_message_request!(
                MessageRequest::CurrentPlayer { .. },
                true
            )),
        )
        .times(1)
        .returning(|_, _, _| Ok(()));

    let global_state = GlobalState::with_decks_and_custom_data(Vec::new(), ());

    let deck = Deck::new(
        VecDeque::from([Card::new(1, "Red")]),
        DeckLayout::Stacked,
        None,
    );
    let decks = vec![deck.clone()];
    let global_state = global_state.add_players(vec![PlayerState::with_decks(0, decks)]);

    let json_response = serde_json::to_string(&CloudEvent::response(
        "1",
        MessageResponse::TryCardMoved {
            card: Card::new(1, "Red".to_string()),
            from: deck.id().to_string(),
            to: 2.to_string(),
        },
        0,
    ))
    .unwrap();
    mock.expect_next().with().times(1).returning(move || {
        Ok(Some(DeliveryMessagingResponse::with_static_data(
            json_response.as_bytes().to_vec(),
        )))
    });

    let mut moves = HashMap::default();
    moves.insert(
        "someKeyValue".to_string(),
        InputDescription::CardsFromPlayerDeck {
            player: 0,
            amount: 1,
            deck_id: deck.id(),
        },
    );

    let mut remote = setup(mock);
    assert_eq!(
        (
            Some(HashMap::from([(
                "someKeyValue".to_string(),
                Input::Cards(vec![Card::new(1, "Red")])
            )])),
            0
        ),
        remote
            .ask_for_option(
                &global_state,
                &PlayerState::default(),
                &[(Some(moves), "DummyState".to_string())],
            )
            .unwrap()
    )
}
