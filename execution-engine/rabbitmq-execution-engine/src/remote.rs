use crate::messaging::Messaging;
use crate::requests::MessageRequest;
use crate::responses::{MessageResponse, SessionDto};
use crate::CloudEvent;
use anyhow::{anyhow, bail, Context, Result};
use lib_execution_engine::components::event::Event;
use lib_execution_engine::components::Card;
use lib_execution_engine::state_machine::states::{GlobalState, PlayerState};
use lib_execution_engine::state_machine::traits::{
    Input, InputDescription, Interface, StateIndex, SuccessorStateWithInputDescription,
};
use log::{debug, warn};
use rand::prelude::IteratorRandom;
use std::borrow::Borrow;
use std::collections::HashMap;

pub struct RemoteInterface<T: Messaging> {
    session_id: String,
    messaging: T,
    session: SessionDto,
}

impl<X: Messaging, Custom> Interface<Custom> for RemoteInterface<X> {
    fn ask_for_option(
        &mut self,
        global_state: &GlobalState<Custom>,
        current_player: &PlayerState,
        states: &[SuccessorStateWithInputDescription],
    ) -> Result<(Option<HashMap<String, Input>>, StateIndex)> {
        let additional_options: Vec<_> = states
            .iter()
            .filter_map(|(input, o)| {
                if input
                    .as_ref()
                    .map(|input| {
                        // If all input descriptions only consist of custom selections,
                        // also provide this as additional input
                        input
                            .values()
                            .all(|i| matches!(i, InputDescription::Custom(_)))
                    })
                    .unwrap_or(true)
                {
                    Some(o.clone())
                } else {
                    None
                }
            })
            .collect();
        let other_options_exist = additional_options.len() != states.len();
        let request = MessageRequest::current_player(
            current_player.id().to_string(),
            additional_options,
            other_options_exist,
        );

        debug!(
            "Asking user to take his turn. Additional options {:#?} are available",
            request
        );

        async_global_executor::block_on(async {
            for p in global_state.players() {
                if p.id() == current_player.id() {
                    self.send_request(request.clone(), current_player.id())
                        .await?;
                } else {
                    let filtered = MessageRequest::current_player(
                        current_player.id().to_string(),
                        Vec::<String>::new(),
                        true,
                    );
                    self.send_request(filtered, p.id()).await?;
                }
            }

            debug!("Message was sent to player {}", current_player.id());

            let mut input: Option<(HashMap<String, Input>, StateIndex)> = None;
            let cond = |(input, s): &(HashMap<String, Input>, StateIndex)| {
                let (expected_input, _) = &states[*s];
                match expected_input {
                    None => false,
                    Some(expected_input) => {
                        // Check whether we have retrieved all the required input for the state.
                        !expected_input.keys().all(|k| input.contains_key(k))
                    }
                }
            };

            'outer: while input.as_ref().map_or(true, cond) {
                let response: CloudEvent<MessageResponse> = self.receive_response().await?;
                if !self.check_if_correct_recipient(current_player.id(), &response)? {
                    continue;
                }

                debug!(
                    "Received response from {}: {:?}",
                    current_player.id(),
                    response
                );
                match &response.value {
                    MessageResponse::SelectedState { option } => {
                        let index = states
                            .iter()
                            .enumerate()
                            .filter(|(_, (input, _))| {
                                input
                                    .as_ref()
                                    .map(|i| {
                                        i.values().all(|i| matches!(i, InputDescription::Custom(_)))
                                    })
                                    .unwrap_or(true)
                            })
                            .nth(*option)
                            .map(|(i, _)| i)
                            .ok_or_else(|| {
                                anyhow!("Selected option {:?} does not exist", option)
                            })?;

                        states[index]
                            .0
                            .as_ref()
                            .map(|descriptions| {
                                let options: Vec<_> = descriptions
                                    .values()
                                    .filter_map(|d| {
                                        if let InputDescription::Custom(options) = d {
                                            Some(options)
                                        } else {
                                            None
                                        }
                                    })
                                    .cloned()
                                    .collect();
                                async_global_executor::block_on(async {
                                    self.send_request(
                                        MessageRequest::user_selection(options),
                                        current_player.id(),
                                    )
                                    .await
                                })
                            })
                            .unwrap_or(Ok(()))?;
                        input = Some((HashMap::new(), index));
                    }
                    MessageResponse::SelectedOptions { options } => {
                        if let Some((ref mut input, idx)) = input.as_mut() {
                            let expected_input = states[*idx].0.as_ref().ok_or_else(|| {
                                anyhow!(
                                    "No input was expected but {:?} was received",
                                    response.value
                                )
                            })?;
                            expected_input
                                .iter()
                                .filter_map(|(key, desc)| {
                                    if let InputDescription::Custom(_) = desc {
                                        Some(key)
                                    } else {
                                        None
                                    }
                                })
                                .zip(options)
                                .for_each(|(key, idx)| {
                                    input.insert(key.clone(), Input::CustomSelection(*idx));
                                });
                        } else {
                            bail!("Not yet supported")
                        }
                    }
                    message @ MessageResponse::TryCardMoved { .. }
                    | message @ MessageResponse::TryDeckSelection { .. } => {
                        for (idx, (expected_input, _)) in states.iter().enumerate() {
                            if let Some(expected_input) = expected_input {
                                if let Ok(new_input) = Self::find_matching_input(
                                    expected_input,
                                    None,
                                    global_state,
                                    message,
                                    true,
                                ) {
                                    let mut tmp = HashMap::from([new_input]);
                                    // Consume additional events
                                    while let Ok((key, new_input)) = Self::find_matching_input(
                                        expected_input,
                                        Some(&tmp),
                                        global_state,
                                        message,
                                        false,
                                    ) {
                                        tmp.insert(key, new_input);
                                    }
                                    input = Some((tmp, idx));

                                    continue 'outer;
                                }
                            }
                        }
                        warn!(
                            "Could not find a matching InputDescription for {:?}",
                            message
                        );
                        continue;
                    }
                }
            }

            Ok(input
                .map(|(input, index)| {
                    if input.is_empty() {
                        (None, index)
                    } else {
                        (Some(input), index)
                    }
                })
                // We can safely unwrap here because otherwise the while loop would not have terminated
                .unwrap())
        })
    }

    fn get_num_players(&mut self) -> usize {
        self.session.game.min_capacity
    }

    fn update_ui(&mut self, state: &GlobalState<Custom>, events: Vec<Event>) -> Result<()> {
        for event in events {
            debug!("Received event {:?}", event);
            let message = match event {
                Event::DeckReordered { deck_id } => Some(MessageRequest::deck_reordered(
                    state
                        .find_deck_by_id(deck_id)
                        .ok_or_else(|| anyhow!("Cannot find deck with id {}", deck_id))?
                        .clone()
                        .into_inner(),
                )),
                Event::CardMoved { card, from, to } => Some(MessageRequest::card_moved(
                    card,
                    from.to_string(),
                    to.to_string(),
                )),
                Event::CardVisibilityChanged {
                    card,
                    deck_id,
                    old_visibility,
                } => {
                    for player in state.players() {
                        if let Some(msg) =
                            Self::visibility_diff(player.id(), deck_id, &card, &old_visibility)
                        {
                            async_global_executor::block_on(async {
                                self.send_request(msg, player.id()).await
                            })?;
                        }
                    }

                    None
                }
                Event::ScoreMessage { player_id, message } => {
                    async_global_executor::block_on(async {
                        self.send_request(MessageRequest::individual_text(message), player_id)
                            .await
                    })?;

                    None
                }
                Event::InfoMessage { player_id, message } => {
                    async_global_executor::block_on(async {
                        self.send_request(MessageRequest::info(message), player_id)
                            .await
                    })?;

                    None
                }
                Event::ErrorMessage { player_id, message } => {
                    async_global_executor::block_on(async {
                        self.send_request(MessageRequest::error(message), player_id)
                            .await
                    })?;

                    None
                }
                Event::GameOver { winners } => Some(MessageRequest::game_over(winners)),
            };

            if let Some(message) = message {
                for player in state.players() {
                    async_global_executor::block_on(async {
                        self.send_request(message.clone(), player.id()).await
                    })?;
                }
            }
        }
        Ok(())
    }

    fn init(&mut self, init_state: &GlobalState<Custom>) {
        let global_decks = init_state
            .decks()
            .iter()
            .map(|d| d.clone().into_inner())
            .collect();

        let player_decks = init_state
            .players()
            .iter()
            .map(|p| {
                let decks = p.decks().iter().map(|d| d.clone().into_inner()).collect();
                (p.id().to_string(), decks)
            })
            .collect();

        let message = MessageRequest::GameConfiguration {
            global_decks,
            player_decks,
        };

        for p in init_state.players() {
            if let Err(e) = async_global_executor::block_on(async {
                self.send_request(message.clone(), p.id()).await
            }) {
                warn!("Error while sending message: {}", e);
            }
        }
    }
}

impl<T: Messaging> RemoteInterface<T> {
    /// Create a new instance.
    pub fn new(session_id: String, messaging: T, session: SessionDto) -> Self {
        RemoteInterface {
            session_id,
            messaging,
            session,
        }
    }

    fn visibility_diff(
        player: usize,
        deck_id: usize,
        card: &Card,
        old_vis: &Option<Vec<usize>>,
    ) -> Option<MessageRequest> {
        let old_vis = old_vis
            .as_ref()
            .map(|r| r.contains(&player))
            .unwrap_or(true);
        let new_vis = card
            .restrictions()
            .map(|r| r.contains(&player))
            .unwrap_or(true);

        match (old_vis, new_vis) {
            (false, true) => {
                // Card turned visible
                Some(MessageRequest::card_flipped(
                    deck_id.to_string(),
                    Card::new(0, ""),
                    card.clone(),
                ))
            }
            (true, false) => {
                // Card turned invisible
                Some(MessageRequest::card_flipped(
                    deck_id.to_string(),
                    card.clone(),
                    Card::new(0, ""),
                ))
            }
            (false, false) | (true, true) => {
                // Nothing changes
                None
            }
        }
    }

    /// Serialize `message`.
    async fn send_request(
        &mut self,
        message: MessageRequest,
        destination_player_id: usize,
    ) -> Result<()> {
        debug!(
            "Sending request with key '{}' to player '{}'",
            self.session_id, destination_player_id
        );
        let cloud_event = CloudEvent::request(
            &self.session_id,
            message.apply_projection(destination_player_id),
            destination_player_id,
        );

        debug!("{:#?}", cloud_event);

        let json = serde_json::to_string(&cloud_event)?;

        debug!(
            "Sending with routing key {}",
            format!(
                "session.{}.player.{}",
                self.session_id, destination_player_id
            )
        );
        self.messaging
            .publish(
                "requests".to_string(),
                format!(
                    "session.{}.player.{}",
                    self.session_id, destination_player_id
                ),
                json.as_bytes(),
            )
            .await?;

        Ok(())
    }

    fn card_is_hidden(card: &Card) -> bool {
        card.value() == 0 && card.color() == ""
    }

    // Check whether the message can be translated into a CardsFromPlayerDeck
    fn find_cards_from_player_deck<
        Custom,
        I: Iterator<Item = (impl Borrow<String>, impl Borrow<InputDescription>)>,
        C: FnOnce() -> I,
    >(
        input: C,
        global_state: &GlobalState<Custom>,
        card: &Card,
        from: &str,
    ) -> Option<(String, Input)> {
        input()
            // Prefer CardsFromPlayerDeck over Deck
            .find_map(|(key, desc)| match desc.borrow() {
                InputDescription::CardsFromPlayerDeck {
                    player,
                    amount: _,
                    deck_id,
                } => {
                    let deck = global_state.player_by_id(*player)?.get_deck(*deck_id)?;
                    if deck.borrow().id().to_string() == *from {
                        let card = if Self::card_is_hidden(card) {
                            deck.borrow()
                                .cards_iter()
                                .choose(&mut rand::thread_rng())
                                .cloned()?
                        } else {
                            deck.borrow().cards_iter().find(|c| *c == card).cloned()?
                        };
                        Some((key.borrow().clone(), Input::Cards(vec![card])))
                    } else {
                        None
                    }
                }
                _ => None,
            })
    }

    fn find_deck<
        I: Iterator<Item = (impl Borrow<String>, impl Borrow<InputDescription>)>,
        C: FnOnce() -> I,
    >(
        input: C,
        deck: &str,
    ) -> Option<(String, Input)> {
        input().find_map(|(key, desc)| match desc.borrow() {
            InputDescription::Deck(valid) => {
                let deck = deck.parse().unwrap();
                if valid.contains(&deck) {
                    Some((key.borrow().clone(), Input::Deck(deck)))
                } else {
                    None
                }
            }
            _ => None,
        })
    }

    fn find_matching_input<Custom>(
        expected_input: &HashMap<String, InputDescription>,
        current_input: Option<&HashMap<String, Input>>,
        global_state: &GlobalState<Custom>,
        message: &MessageResponse,
        check_fulfilled_inputs: bool,
    ) -> Result<(String, Input)> {
        // This variable contains all Input Descriptions that have not yet been fulfilled
        let missing = || {
            // Prioritize inputs that have not yet been fullfilled
            expected_input
                .iter()
                .filter(|(key, _)| current_input.map(|c| !c.contains_key(*key)).unwrap_or(true))
        };
        // The user might want to override a previous decision
        let fulfilled = || {
            expected_input.iter().filter(|(key, _)| {
                check_fulfilled_inputs
                    && current_input.map(|c| c.contains_key(*key)).unwrap_or(false)
            })
        };

        match message {
            MessageResponse::TryCardMoved { card, from, to } => {
                Self::find_cards_from_player_deck(missing, global_state, card, from)
                    .or_else(|| Self::find_deck(missing, from))
                    .or_else(|| Self::find_deck(missing, to))
                    // Try already fulfilled states
                    .or_else(|| {
                        Self::find_cards_from_player_deck(fulfilled, global_state, card, from)
                    })
                    .or_else(|| Self::find_deck(fulfilled, from))
                    .or_else(|| Self::find_deck(fulfilled, to))
                    .ok_or_else(|| anyhow!("No matching input description found for {:?}", message))
            }
            MessageResponse::TryDeckSelection { deck } => Self::find_deck(missing, deck)
                .or_else(|| Self::find_deck(fulfilled, deck))
                .ok_or_else(|| anyhow!("No matching input description found for {:?}", message)),
            _ => bail!("Cannot handle message {:?}", message),
        }
    }

    /// Receives a message and deserializes it.
    async fn receive_response(&mut self) -> Result<CloudEvent<MessageResponse>> {
        let queue_name = crate::get_queue_name_responses(&self.session_id);

        debug!("Waiting for message... (queue {})", queue_name);

        let message = loop {
            match self.messaging.next().await? {
                Some(delivery) => {
                    let message = crate::deserialize_message(delivery.get_data())?;

                    break Ok(message);
                }
                _ => continue,
            }
        };

        message
    }

    /// Checks the header of `response` and the `current_player_id` if they match.
    /// If not then send a message to the UI that the move was invalid and then
    /// return an error. If was ok, then do nothing.
    fn check_if_correct_recipient<X>(
        &mut self,
        current_player_id: usize,
        response: &CloudEvent<X>,
    ) -> Result<bool> {
        if !response.check_if_correct_recipient_when_from_client(current_player_id) {
            debug!(
                "Message was not from this player '{}' but from player {:?}",
                current_player_id,
                response.get_source_player_id()
            );

            if let Some(player_id) = response.get_source_player_id() {
                self.send_invalid_move_message(
                    player_id.parse().context("Cannot parse player_id")?,
                )?;
            } else {
                warn!("Cannot invalid turn message because it has no destination player");
            }

            Ok(false)
        } else {
            debug!("Recipient ok");

            Ok(true)
        }
    }

    /// Send a message to the UI can display that an invalid move has happened.
    fn send_invalid_move_message(&mut self, player_id: usize) -> Result<()> {
        let request = MessageRequest::error("ERROR");
        async_global_executor::block_on(async { self.send_request(request, player_id).await })
    }
}
