use lib_execution_engine::components::{Card, Deck};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(tag = "type")]
/// Main wrapper struct for client messages
pub enum MessageRequest {
    GameConfiguration {
        global_decks: Vec<Deck>,
        player_decks: HashMap<String, Vec<Deck>>,
    },
    DeckReordered {
        deck: Deck,
    },
    CardFlipped {
        deck_id: String,
        old_card: Card,
        new_card: Card,
    },
    CardMoved {
        card: Card,
        from: String,
        to: String,
    },
    CurrentPlayer {
        player_id: String,
        additional_options: Vec<String>,
        /// Tells the frontend to force a certain selection from the ```additional_options```
        other_options_exist: bool,
    },
    UserSelection {
        options: Vec<Vec<String>>,
    },
    GameOver {
        winners: Vec<usize>,
    },
    Info {
        message: String,
    },
    Error {
        message: String,
    },
    IndividualText {
        text: String,
    },
}

impl MessageRequest {
    pub fn current_player(
        player_id: String,
        options: Vec<impl Into<String>>,
        other_options_exist: bool,
    ) -> Self {
        Self::CurrentPlayer {
            player_id,
            additional_options: options.into_iter().map(|x| x.into()).collect(),
            other_options_exist,
        }
    }

    pub fn deck_reordered(deck: Deck) -> Self {
        Self::DeckReordered { deck }
    }

    pub fn card_flipped(deck_id: impl Into<String>, old_card: Card, new_card: Card) -> Self {
        Self::CardFlipped {
            deck_id: deck_id.into(),
            old_card,
            new_card,
        }
    }

    pub fn card_moved(card: Card, from: String, to: String) -> Self {
        Self::CardMoved { card, from, to }
    }

    pub fn user_selection(options: Vec<Vec<impl Into<String>>>) -> Self {
        Self::UserSelection {
            options: options
                .into_iter()
                .map(|x| x.into_iter().map(|x| x.into()).collect())
                .collect(),
        }
    }

    pub fn individual_text(text: impl Into<String>) -> Self {
        Self::IndividualText { text: text.into() }
    }

    pub fn game_over(winners: Vec<usize>) -> Self {
        Self::GameOver { winners }
    }

    pub fn info(message: impl Into<String>) -> Self {
        Self::Info {
            message: message.into(),
        }
    }

    pub fn error(message: impl Into<String>) -> Self {
        Self::Error {
            message: message.into(),
        }
    }

    fn apply_projection_on_deck(deck: &Deck, player: usize) -> Deck {
        let mut cloned = deck.clone();
        cloned.cards_iter_mut().for_each(|c| {
            if let Some(r) = c.restrictions() {
                if !r.contains(&player) {
                    *c = Card::new(0, "");
                }
            }
        });
        cloned
    }

    pub fn apply_projection(self, player: usize) -> Self {
        match self {
            MessageRequest::GameConfiguration {
                mut global_decks,
                player_decks,
            } => {
                global_decks.iter_mut().for_each(|d| {
                    *d = Self::apply_projection_on_deck(d, player);
                });

                MessageRequest::GameConfiguration {
                    global_decks,
                    player_decks,
                }
            }
            MessageRequest::DeckReordered { deck } => {
                let deck = Self::apply_projection_on_deck(&deck, player);

                MessageRequest::DeckReordered { deck }
            }
            MessageRequest::CardMoved { card, from, to } => {
                if let Some(v) = card.restrictions() {
                    if !v.contains(&player) {
                        return MessageRequest::CardMoved {
                            card: Card::new(0, ""),
                            from,
                            to,
                        };
                    }
                }
                MessageRequest::CardMoved { card, from, to }
            }
            m => m,
        }
    }
}
