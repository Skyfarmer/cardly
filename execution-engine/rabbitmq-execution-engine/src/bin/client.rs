use anyhow::{anyhow, bail, Context, Result};

use dotenv::dotenv;
use futures_lite::StreamExt;
use lapin::options::{
    BasicAckOptions, BasicConsumeOptions, BasicPublishOptions, ExchangeDeclareOptions,
    QueueBindOptions, QueueDeclareOptions,
};
use lapin::types::FieldTable;
use lapin::{BasicProperties, Channel, Connection, ConnectionProperties, ExchangeKind};
use lib_execution_engine::components::{Card, Deck, DeckLayout};
use log::{debug, error, info, warn};
use rabbitmq_execution_engine::requests::MessageRequest;
use rabbitmq_execution_engine::responses::MessageResponse::{SelectedState, TryCardMoved};
use rabbitmq_execution_engine::responses::SessionDto;
use rabbitmq_execution_engine::{
    deserialize_message, get_queue_name_request_for_local_dev, get_queue_name_responses, CloudEvent,
};
use std::thread;

const EXCHANGE_NAME_RESPONSES: &str = "responses";
const EXCHANGE_NAME_REQUESTS: &str = "requests";

#[cfg(not(tarpaulin_include))]
fn main() -> Result<()> {
    dotenv().ok();
    env_logger::init_from_env(
        env_logger::Env::default().filter_or(env_logger::DEFAULT_FILTER_ENV, "debug"),
    );

    let addr = std::env::var("AMQP_ADDR")
        .unwrap_or_else(|_| "amqp://changeme:changeme@127.0.0.1:5672/%2f".into());
    let session_id = std::env::var("SESSION_ID").context("SESSION_ID was not provided")?;
    let rabbitmq_setup = std::env::var("RABBITMQ_SETUP")
        .unwrap_or_else(|_| "true".to_string())
        .parse::<bool>()
        .context("Cannot parse boolean for RABBITMQ_SETUP")?;
    let initial_message = std::env::var("INITIAL_MESSAGE")
        .unwrap_or_else(|_| "false".to_string())
        .parse::<bool>()
        .context("Cannot parse boolean for RABBITMQ_SETUP")?;
    let player_id = std::env::var("PLAYER_ID")
        .context("PLAYER_ID was not provided")?
        .parse::<usize>()
        .context("PLAYER_ID is not a valid number")?;

    debug!("Connecting to {}", addr);
    info!("This is the player with id '{}'", player_id);

    let mut channel = async_global_executor::block_on(setup_rabbitmq(
        &addr,
        session_id.clone(),
        rabbitmq_setup,
        player_id,
    ))
    .context("Cannot setup rabbitmq")?;

    debug!("Channel is setup");

    if initial_message {
        async_global_executor::block_on(send_initial_message(
            &mut channel,
            session_id.clone(),
            read_session(session_id.clone(), player_id)?,
        ))
        .context("Cannot send initial message")?;
    } else {
        warn!("Not sending initial message");
    }

    loop {
        async_global_executor::block_on(async {
            if let Err(err) = run(&mut channel, session_id.clone(), player_id).await {
                error!("ERROR: {}", err);
                err.chain()
                    .skip(1)
                    .for_each(|cause| error!("because: {}", cause));
                std::process::exit(-1);
            }
        });
    }
}

fn read_session(session_id: impl Into<String>, player_id: usize) -> Result<CloudEvent<SessionDto>> {
    let contents = std::fs::read_to_string("../backend/input.json")
        .context("Something went wrong reading ../backend/input.json")?;

    Ok(CloudEvent::response(
        session_id,
        serde_json::from_str(&contents).context("Cannot deserialize json")?,
        player_id,
    ))
}

/// Setups the channel
async fn setup_rabbitmq(
    addr: impl AsRef<str>,
    session_id: impl Into<String> + Clone,
    rabbitmq_setup: bool,
    player_id: usize,
) -> Result<Channel> {
    let conn = Connection::connect(addr.as_ref(), ConnectionProperties::default())
        .await
        .context("Cannot create connection")?;

    info!("CONNECTED");

    let channel = conn
        .create_channel()
        .await
        .context("Cannot create channel")?;

    let queue_name_requests =
        &get_queue_name_request_for_local_dev(session_id.clone(), format!("{}", player_id.clone()));
    let queue_name_responses = &get_queue_name_responses(session_id.clone());

    let options = ExchangeDeclareOptions {
        durable: true,
        ..Default::default()
    };

    if rabbitmq_setup {
        // Create a `requests` exchange
        let _ = channel
            .exchange_declare(
                EXCHANGE_NAME_REQUESTS,
                ExchangeKind::Topic,
                options,
                FieldTable::default(),
            )
            .await?;

        // Create a `responses` exchange
        let _ = channel
            .exchange_declare(
                EXCHANGE_NAME_RESPONSES,
                ExchangeKind::Topic,
                options,
                FieldTable::default(),
            )
            .await?;
    }

    let queue_options = QueueDeclareOptions {
        auto_delete: true,
        ..Default::default()
    };

    let _ = channel
        .queue_declare(queue_name_requests, queue_options, FieldTable::default())
        .await?;

    let _ = channel
        .queue_declare(queue_name_responses, queue_options, FieldTable::default())
        .await?;

    // Messages get publishes wit the `routing_key`.
    // Based on the key, rabbitmq decided what queue to choose.
    let session_id = session_id.clone().into();
    channel
        .queue_bind(
            queue_name_requests,
            EXCHANGE_NAME_REQUESTS,
            &format!("session.{}.player.{}", session_id.clone(), player_id),
            QueueBindOptions::default(),
            FieldTable::default(),
        )
        .await?;

    channel
        .queue_bind(
            queue_name_responses,
            EXCHANGE_NAME_RESPONSES,
            &format!("session.{}.#", session_id),
            QueueBindOptions::default(),
            FieldTable::default(),
        )
        .await?;

    Ok(channel)
}

/// Sending a initial message to the exchange.
/// This not normally done by the client, but instead the backend.
/// Because the execution-engine doesn't know what game to load at the beginning.
async fn send_initial_message(
    channel: &mut Channel,
    session_id: impl Into<String>,
    session: CloudEvent<SessionDto>,
) -> Result<()> {
    let json = serde_json::to_string(&session)?;

    debug!("Sending initial message {:#?}", json);

    channel
        .basic_publish(
            "responses",
            &format!("session.{}.#", session_id.into()),
            BasicPublishOptions::default(),
            json.as_bytes(),
            BasicProperties::default(),
        )
        .await?
        .await?;

    info!("Initial game was sent!");

    Ok(())
}

async fn handle_user_input(
    input: &str,
    session_id: String,
    player_id: usize,
    channel: &mut Channel,
) -> Result<()> {
    let mut splitted = input.trim().split(' ');
    let missing = || anyhow!("Missing option");
    let message = match splitted.next().ok_or_else(missing)?.parse()? {
        1 => SelectedState {
            option: splitted.next().ok_or_else(missing)?.parse()?,
        },
        2 => TryCardMoved {
            card: Card::new(
                splitted.next().ok_or_else(missing)?.parse()?,
                splitted.next().ok_or_else(missing)?,
            ),
            from: splitted.next().ok_or_else(missing)?.parse()?,
            to: splitted.next().ok_or_else(missing)?.parse()?,
        },
        _ => bail!("Unknown message selected"),
    };

    let cloud_event = CloudEvent::response(session_id.clone(), message, player_id);
    debug!(
        "Sending response {:#?} to (exchange {} and routing key {})",
        cloud_event,
        EXCHANGE_NAME_RESPONSES,
        session_id.clone()
    );
    let json = serde_json::to_string(&cloud_event)?;

    channel
        .basic_publish(
            EXCHANGE_NAME_RESPONSES,
            &format!("session.{}.#", &session_id),
            BasicPublishOptions::default(),
            json.as_bytes(),
            BasicProperties::default(),
        )
        .await?
        .await?;
    Ok(())
}

async fn run(
    channel: &mut Channel,
    session_id: impl Into<String> + Clone,
    player_id: usize,
) -> Result<()> {
    let queue_name =
        &get_queue_name_request_for_local_dev(session_id.clone(), player_id.to_string());

    // This is the consumer for the all interactions
    // the execution-engine requires input.
    let mut consumer = channel
        .basic_consume(
            queue_name,
            &format!("execution-engine-client_player_id_{}", player_id),
            BasicConsumeOptions::default(),
            FieldTable::default(),
        )
        .await
        .context("Cannot consume message")?;

    let session_id = session_id.into();
    let mut channel2 = channel.clone();
    thread::spawn(move || {
        let mut message = String::new();
        message.push_str("You may send the following messages:\n");
        message.push_str("SelectedOption [1] Option\n");
        message.push_str("TryCardMoved [2] Card-Value Card-Color From To\n");
        print!("{}", message);

        loop {
            let mut buffer = String::new();
            std::io::stdin().read_line(&mut buffer).unwrap();

            if let Err(e) = async_global_executor::block_on(async {
                handle_user_input(&buffer, session_id.clone(), player_id, &mut channel2).await
            }) {
                error!("User error: {}", e);
                println!("{}", message);
            }
        }
    });

    loop {
        let message = loop {
            debug!("Waiting for message... (queue {})", queue_name);
            match consumer.next().await {
                Some(Ok(delivery)) => {
                    info!("Message received");

                    let message =
                        deserialize_message::<CloudEvent<MessageRequest>>(&delivery.data)?;

                    // Check if the message is for this client.
                    if !message.check_if_correct_recipient_when_from_server(player_id) {
                        debug!("Message is ignored because it was meant for a different client");
                        continue;
                    } else {
                        debug!("Message accepted for player with id '{}'", player_id);
                    }

                    debug!("{:#?}", message);

                    delivery
                        .ack(BasicAckOptions::default())
                        .await
                        .context("Cannot ack")?;

                    break message;
                }
                Some(Err(error)) => {
                    error!("{}", error);
                    continue;
                }
                _ => continue,
            }
        };

        info!("Received message : {:#?}", &message.value);

        // Main Client Logic
        match message.get_inner() {
            MessageRequest::UserSelection { options } => {
                info!("Choose...");

                options.iter().for_each(|option| {
                    let message = option.iter().fold(String::new(), |mut acc, o| {
                        acc.push_str(o);
                        acc
                    });
                    info!("{}", message);
                });
            }
            MessageRequest::Error { message } => {
                error!("Error: {}", message);
            }
            MessageRequest::GameConfiguration {
                global_decks,
                player_decks: _,
            } => {
                info!("Received initial game configuration");

                print_decks(&global_decks);
            }
            _ => (),
        };
    }
}

/// Convert `Card` to a string.
fn card_to_string(card: &Card) -> String {
    if card.restrictions().map_or(true, |r| !r.is_empty()) {
        format!("{}-{}", card.color(), card.value())
    } else {
        String::from("Hidden")
    }
}

/// Convert `Deck` to a string.
fn deck_to_string(deck: &Deck) -> String {
    match deck.layout() {
        DeckLayout::Stacked => deck
            .peek_top()
            .map(card_to_string)
            .unwrap_or_else(|| "empty".to_string()),
        DeckLayout::Spreaded => deck.cards_iter().fold(String::new(), |output, card| {
            output + &card_to_string(card) + " "
        }),
    }
}

/// Print the 'table' of the game on stdout.
fn print_decks(decks: &[Deck]) {
    info!("Table:");
    for d in decks {
        info!("{}: {}", d.id(), deck_to_string(d));
    }
}
