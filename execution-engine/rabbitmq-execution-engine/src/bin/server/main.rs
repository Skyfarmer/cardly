use anyhow::{bail, Context, Result};
use log::{error, info};
use std::time::Duration;

use lib_execution_engine::lua_game::LuaGame;
use lib_execution_engine::state_machine::StateMachine;
use rabbitmq_execution_engine::messaging::{Messaging, MessagingRabbitmqConfiguration};
use rabbitmq_execution_engine::remote::RemoteInterface;

#[cfg(not(tarpaulin_include))]
fn main() -> Result<()> {
    env_logger::init_from_env(
        env_logger::Env::default().filter_or(env_logger::DEFAULT_FILTER_ENV, "debug"),
    );

    if let Err(err) = run() {
        error!("ERROR: {}", err);
        err.chain()
            .skip(1)
            .for_each(|cause| error!("because: {}", cause));
        std::process::exit(-1);
    }

    // Do not terminate immediately
    std::thread::sleep(Duration::new(1000, 0));

    Ok(())
}

fn run() -> Result<()> {
    let addr =
        std::env::var("AMQP_ADDR").unwrap_or_else(|_| "amqp://rabbitmq-service:5672/%2f".into());
    let session_id = std::env::var("SESSION_ID").context("SESSION_ID was not provided")?;

    info!("Connection to {}", addr);
    info!("Session id is {}", session_id);

    let mut messaging_configuration = async_global_executor::block_on(
        MessagingRabbitmqConfiguration::setup(addr, session_id.clone()),
    )
    .context("Cannot setup rabbitmq")?;

    let session = async_global_executor::block_on(messaging_configuration.get_session())?;
    let base64_decoded =
        base64::decode(&session.game.source_code).context("Converting base64 code failed")?;
    let source_code = std::str::from_utf8(&base64_decoded).context("Code is not valid base64")?;
    let game = match LuaGame::try_from(source_code.to_string()) {
        Ok(g) => g,
        Err(e) => {
            bail!("Could not load game: {}", e);
        }
    };

    let interface = RemoteInterface::new(session_id, messaging_configuration, session);
    let machine = StateMachine::new(game);

    machine.run(interface)?;

    Ok(())
}
