use anyhow::Context;
use log::{debug, trace};
use serde::de::DeserializeOwned;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use unescape::unescape;

/// Contains the structs to ask the client to provide some input.
pub mod requests;

pub mod messaging;
pub mod remote;
/// Contains the structs for the responses which the client gives.
pub mod responses;

#[cfg(test)]
mod tests;

pub const HEADER_DESTINATION_PLAYER_ID: &str = "destination_player_id";
const HEADER_SOURCE_PLAYER_ID: &str = "source_player_id";
const SESSION: &str = "session_id";

#[derive(Debug, Serialize, PartialEq, Deserialize)]
pub struct CloudEvent<T> {
    pub key: String,
    pub headers: HashMap<String, String>,
    pub value: T,
}

impl<T> CloudEvent<T> {
    pub fn get_inner(self) -> T {
        self.value
    }

    pub fn request(key: impl Into<String>, value: T, destination_player_id: usize) -> Self {
        let mut headers = HashMap::new();
        let key = key.into();
        headers.insert(
            HEADER_DESTINATION_PLAYER_ID.to_string(),
            destination_player_id.to_string(),
        );
        headers.insert(SESSION.to_string(), key.clone());

        Self {
            key,
            headers,
            value,
        }
    }

    pub fn response(key: impl Into<String>, value: T, source_player_id: usize) -> Self {
        let mut headers = HashMap::new();
        let key = key.into();
        headers.insert(
            HEADER_SOURCE_PLAYER_ID.to_string(),
            source_player_id.to_string(),
        );
        headers.insert(SESSION.to_string(), key.clone());

        Self {
            key,
            headers,
            value,
        }
    }

    /// Compares the header `destination_player_id` with given `player_id`.
    /// Returns `true` if it matches.
    pub fn check_if_correct_recipient_when_from_server(&self, player_id: usize) -> bool {
        let player_id = &format!("{}", player_id);
        self.headers
            .get(HEADER_DESTINATION_PLAYER_ID)
            .map(|x| x == player_id)
            .unwrap_or(false)
    }

    /// Compares the header `source_player_id` with given `player_id`.
    /// Returns `true` if it matches.
    pub fn check_if_correct_recipient_when_from_client(&self, player_id: usize) -> bool {
        let player_id = &format!("{}", player_id);
        self.headers
            .get(HEADER_SOURCE_PLAYER_ID)
            .map(|x| x == player_id)
            .unwrap_or(false)
    }

    pub fn get_session_id(&self) -> Option<&String> {
        self.headers.get(SESSION)
    }

    pub fn get_dest_player_id(&self) -> Option<&String> {
        self.headers.get(HEADER_SOURCE_PLAYER_ID)
    }

    pub fn get_source_player_id(&self) -> Option<&String> {
        self.headers.get(HEADER_SOURCE_PLAYER_ID)
    }
}

/// Deserialize the message.
pub fn deserialize_message<T: DeserializeOwned + Serialize>(data: &[u8]) -> anyhow::Result<T> {
    let content = std::str::from_utf8(data).context("Data is not utf8")?;
    trace!("Message raw content {}", content);
    let new_content = unescape(content).context("Cannot unescape")?;
    debug!("Message content {}", new_content);

    if new_content.starts_with('\"') && new_content.ends_with('\"') {
        let updated = &new_content[1..new_content.len() - 1];
        debug!("Updated {}", updated);
        serde_json::from_str(updated).context("Cannot deserialize when cutting quotes")
    } else {
        serde_json::from_str(&new_content).context("Cannot deserialize")
    }
}

/// Get the queue name for the requests.
/// The requests are sent from the execution-engine to the client.
pub fn get_queue_name_requests(_session_id: impl Into<String>) -> String {
    "requests".to_string()
}

pub fn get_queue_name_request_for_local_dev(
    session_id: impl Into<String>,
    player_id: impl Into<String>,
) -> String {
    format!(
        "session_with_id_{}_player_id_{}_requests",
        session_id.into(),
        player_id.into()
    )
}

/// Get the queue name for the responses.
/// The responses are sent from the client to the execution engine.
pub fn get_queue_name_responses(session_id: impl Into<String>) -> String {
    format!("session_with_id_{}_responses", session_id.into())
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::responses::SessionDto;

    #[test]
    fn testing_deserde_session() {
        let x = r#"{"headers":{"session_id":"41"},"key":"41","value":{"id":41,"status":"STARTED","slots":[{"id":27,"slotNumber":0,"player":{"id":1}},{"id":28,"slotNumber":1,"player":{"id":0}}],"game":{"id":1,"name":"my_game","source_code":"test", "min_capacity": 2}}}"#;
        let _session = deserialize_message::<CloudEvent<SessionDto>>(x.as_bytes()).unwrap();
    }

    #[test]
    fn testing_deserde_session_when_quotes() {
        let x = r#"\"{"headers":{"session_id":"41"},"key":"41","value":{"id":41,"status":"STARTED","slots":[{"id":27,"slotNumber":0,"player":{"id":1}},{"id":28,"slotNumber":1,"player":{"id":0}}],"game":{"id":1,"name":"my_game","source_code":"test", "min_capacity": 2}}}\""#;
        let _session = deserialize_message::<CloudEvent<SessionDto>>(x.as_bytes()).unwrap();
    }

    #[test]
    fn cloud_event_should_create_header_with_destination_player_id() {
        let test_value = 10;
        let event = CloudEvent::request("keyValue", test_value, 0);
        assert_eq!(&event.key, "keyValue");
        assert_eq!(
            event.headers.get(HEADER_DESTINATION_PLAYER_ID),
            Some(&"0".to_string())
        );
        assert_eq!(event.get_inner(), test_value);
    }

    #[test]
    fn cloud_event_should_create_header_with_source_player_id() {
        let test_value = 10;
        let event = CloudEvent::response("keyValue", test_value, 0);
        assert_eq!(&event.key, "keyValue");
        assert_eq!(
            event.headers.get(HEADER_SOURCE_PLAYER_ID),
            Some(&"0".to_string())
        );
        assert_eq!(event.get_inner(), test_value);
    }

    #[test]
    fn cloud_event_should_check_if_correct_recipient() {
        let event = CloudEvent::request("keyValue", (), 0);
        assert!(event.check_if_correct_recipient_when_from_server(0));
        assert!(!event.check_if_correct_recipient_when_from_server(1));
    }
}
