use crate::responses::SessionDto;
use crate::CloudEvent;
use anyhow::{Context, Result};
use async_trait::async_trait;
use futures_lite::StreamExt;
use lapin::message::Delivery;
use lapin::options::{BasicAckOptions, BasicConsumeOptions, BasicPublishOptions};
use lapin::types::FieldTable;
use lapin::{BasicProperties, Channel, Connection, ConnectionProperties, Consumer};
use log::{debug, error, info};
use mockall::automock;

pub struct MessagingRabbitmqConfiguration {
    channel: Channel,
    consumer: Consumer,
}

pub enum DeliveryMessagingResponse {
    RabbitMq { delivery: Box<Delivery> },
    Static { data: Vec<u8> },
}

impl MessagingRabbitmqConfiguration {
    pub fn new(channel: Channel, consumer: Consumer) -> Self {
        Self { channel, consumer }
    }

    /// Waits for the initial message from the client. This message contains
    /// the source code of the game. This method will also loop until
    /// it has got a correct message.
    pub async fn consume_initial_message(&mut self) -> Result<SessionDto> {
        debug!("Starting to consume initial message");
        let message = loop {
            match self.next().await {
                Ok(Some(delivery)) => {
                    info!("Message received");
                    let message =
                        crate::deserialize_message::<CloudEvent<SessionDto>>(delivery.get_data())?;
                    debug!("{:#?}", message);

                    break message.get_inner();
                }
                Err(error) => {
                    error!("{}", error);
                    continue;
                }
                _ => continue,
            }
        };

        Ok(message)
    }
}

#[automock]
#[async_trait]
pub trait Messaging: Sized {
    async fn setup(addr: String, session_id: String) -> Result<Self>;
    async fn get_session(&mut self) -> Result<SessionDto>;
    async fn publish(
        &mut self,
        exchange: String,
        routing_key: String,
        payload: &[u8],
    ) -> Result<()>;
    async fn next(&mut self) -> Result<Option<DeliveryMessagingResponse>>;
}

#[async_trait]
impl Messaging for MessagingRabbitmqConfiguration {
    async fn setup(addr: String, session_id: String) -> Result<Self> {
        let conn = Connection::connect(addr.as_ref(), ConnectionProperties::default())
            .await
            .context("Cannot create connection")?;
        let channel = conn
            .create_channel()
            .await
            .context("Cannot create channel")?;

        info!("Connected.");

        let consumer = channel
            .basic_consume(
                &crate::get_queue_name_responses(session_id),
                "execution-engine",
                BasicConsumeOptions::default(),
                FieldTable::default(),
            )
            .await
            .context("Cannot consume message")?;

        let messaging = MessagingRabbitmqConfiguration::new(channel, consumer);

        Ok(messaging)
    }

    async fn get_session(&mut self) -> Result<SessionDto> {
        let session = self
            .consume_initial_message()
            .await
            .context("Cannot consume initial message")?;

        Ok(session)
    }

    async fn publish(
        &mut self,
        exchange: String,
        routing_key: String,
        payload: &[u8],
    ) -> Result<()> {
        self.channel
            .basic_publish(
                &exchange,
                &routing_key,
                BasicPublishOptions::default(),
                payload,
                BasicProperties::default(),
            )
            .await?
            .await?;

        Ok(())
    }

    /// Consumes the next from the consumer.
    async fn next(&mut self) -> Result<Option<DeliveryMessagingResponse>> {
        match self.consumer.next().await {
            Some(delivery) => {
                let delivery = delivery.context("Error during delivery")?;

                // We are acknowledging before parsing the message.
                // This means that we are losing the message when the parsing fails.
                // However, this is developer friendly.
                delivery.ack(BasicAckOptions::default()).await?;

                return Ok(Some(DeliveryMessagingResponse::RabbitMq {
                    delivery: Box::new(delivery),
                }));
            }
            None => Ok(None),
        }
    }
}

impl DeliveryMessagingResponse {
    pub fn with_static_data(data: Vec<u8>) -> Self {
        DeliveryMessagingResponse::Static { data }
    }

    pub async fn ack(&mut self) -> Result<()> {
        match self {
            DeliveryMessagingResponse::RabbitMq { delivery } => {
                delivery.ack(BasicAckOptions::default()).await?;
            }
            _ => {
                debug!("Not acknowledging because static data")
            }
        }

        Ok(())
    }

    pub fn get_data(&self) -> &[u8] {
        match self {
            DeliveryMessagingResponse::Static { data } => data,
            DeliveryMessagingResponse::RabbitMq { delivery } => &delivery.data,
        }
    }
}
