use lib_execution_engine::components::Card;
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize, Default)]
pub struct SessionDto {
    pub id: usize,
    pub game: GameDto,
}

#[derive(Debug, Serialize, Deserialize, Default)]
pub struct GameDto {
    pub id: usize,
    pub source_code: String,
    pub min_capacity: usize,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(tag = "type")]
/// Main wrapper struct for client messages
pub enum MessageResponse {
    SelectedState {
        /// Tracks which option the user has chosen.
        option: usize,
    },
    SelectedOptions {
        /// Tracks which option the user has chosen.
        options: Vec<usize>,
    },
    TryDeckSelection {
        /// A deck has been selected
        deck: String,
    },
    TryCardMoved {
        card: Card,
        from: String,
        to: String,
    },
}

impl MessageResponse {
    pub fn selected_options(options: Vec<usize>) -> Self {
        Self::SelectedOptions { options }
    }
}
